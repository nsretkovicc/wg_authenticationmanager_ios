﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericVirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct GenericVirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct GenericVirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericInterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct GenericInterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct GenericInterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper
struct SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97;
// Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate
struct ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4;
// Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate
struct ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0;
// Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper
struct SWIGStringHelper_t0D99651DC671811558BDB3D299B82F879F5E731B;
// Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate
struct SWIGStringDelegate_tF943E218400FF77BFF8F047D209AA7CA94915E00;
// Firebase.Auth.Credential
struct Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630;
// Firebase.Auth.FirebaseAuth
struct FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D;
// Firebase.Auth.FirebaseAuth/<>c
struct U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF;
// Firebase.Auth.FirebaseAuth/<>c__DisplayClass19_0
struct U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8;
// Firebase.Auth.FirebaseAuth/<>c__DisplayClass19_1
struct U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683;
// Firebase.Auth.FirebaseAuth/<>c__DisplayClass22_0
struct U3CU3Ec__DisplayClass22_0_t0B9E0A71713E1E0BCBE5C32D0A17C7786CF35818;
// Firebase.Auth.FirebaseAuth/<>c__DisplayClass22_1
struct U3CU3Ec__DisplayClass22_1_tE096CD1670F7DFF2DC0A9E52B629F80ED0B6ED1B;
// Firebase.Auth.FirebaseAuth/<>c__DisplayClass48_0
struct U3CU3Ec__DisplayClass48_0_t0332496AFB260910C4258BB3F40976B2A20A0312;
// Firebase.Auth.FirebaseAuth/<>c__DisplayClass50_0
struct U3CU3Ec__DisplayClass50_0_t3165B97C90B070A7EAE8BAE6F1E43B1CFD2F46C2;
// Firebase.Auth.FirebaseAuth/StateChangedDelegate
struct StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294;
// Firebase.Auth.FirebaseUser
struct FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD;
// Firebase.Auth.Future_User
struct Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01;
// Firebase.Auth.Future_User/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_t89802218D77EADC8D4FDB0DD3CF8FC5447E97B8D;
// Firebase.Auth.Future_User/Action
struct Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327;
// Firebase.Auth.Future_User/SWIG_CompletionDelegate
struct SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2;
// Firebase.Auth.IUserInfo
struct IUserInfo_t7265E03A4665719EDE78D7F016A2B46DD9C8FE45;
// Firebase.Auth.IUserInfo[]
struct IUserInfoU5BU5D_tE61C9224BA7F49769BA6D01A174F32C4955EE4D2;
// Firebase.Auth.UserInfoInterface
struct UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C;
// Firebase.Auth.UserInfoInterfaceList
struct UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422;
// Firebase.Auth.UserInfoInterfaceList/UserInfoInterfaceListEnumerator
struct UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A;
// Firebase.Auth.UserInfoInterface[]
struct UserInfoInterfaceU5BU5D_tD810A76761E9D37FB729A09C273728E21098926B;
// Firebase.Auth.UserProfile
struct UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31;
// Firebase.FirebaseApp
struct FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986;
// Firebase.FirebaseException
struct FirebaseException_t947F51F8FDF9CB2D145DC24E92160DC67BF12AEF;
// Firebase.FutureBase
struct FutureBase_t75BACF0EF2911C731F998E85086202A1A2F55397;
// Firebase.FutureVoid
struct FutureVoid_tF512775335C40422BAC529CBF374A246FD8ED51D;
// Firebase.FutureVoid/SWIG_CompletionDelegate
struct SWIG_CompletionDelegate_t84CBBD1E196A142480C0E94C912F5761358B4988;
// Firebase.InitializationException
struct InitializationException_t4950F6485639018AF98C04DD016B37F1745F8098;
// Firebase.Platform.FirebaseAppPlatform
struct FirebaseAppPlatform_tDB2126CA2A03AFD8EA2B368EAB733DE52BFF6D75;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<Firebase.Auth.FirebaseAuth>
struct Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E;
// System.Action`1<System.Object>
struct Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0;
// System.Action`1<System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>>
struct Action_1_t5D2D131226D740682F598DD09436558292112781;
// System.Action`1<System.Threading.Tasks.Task`1<System.Object>>
struct Action_1_tF41DEFE08D91E5A3638655E075175E27AA82D1DC;
// System.AggregateException
struct AggregateException_t9217B9E89DF820D5632411F2BD92F444B17BD60E;
// System.ApplicationException
struct ApplicationException_t664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74;
// System.ArgumentException
struct ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1;
// System.ArgumentNullException
struct ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA;
// System.ArithmeticException
struct ArithmeticException_tF9EF5FE94597830EF315C5934258F994B8648269;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2/Entry<System.Int32,Firebase.Auth.Future_User/Action>[]
struct EntryU5BU5D_t534F4E28AFD1A9874924380C0D4660E6A0F59204;
// System.Collections.Generic.Dictionary`2/Entry<System.IntPtr,Firebase.Auth.FirebaseAuth>[]
struct EntryU5BU5D_t6CC77CA77E8558E20CB600BF84F17A706A0D4E36;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Firebase.Auth.Future_User/Action>
struct KeyCollection_t44B348B2BBD9CCAA412495660D16B78BE78E94D8;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>
struct KeyCollection_tA950924F9FED14F66234F7CA5BDF129C6A1B0815;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Firebase.Auth.Future_User/Action>
struct ValueCollection_t87FC45B0A7C2D45AB676565AB2B9ED18CC61BB75;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.IntPtr,Firebase.Auth.FirebaseAuth>
struct ValueCollection_tFF4B293126CA06BB0F3B773C375AFCDB57CFFC25;
// System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>
struct Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A;
// System.Collections.Generic.Dictionary`2<System.Int32,Firebase.FutureVoid/Action>
struct Dictionary_2_t087E572E4674F35454E806EDE6D0179962696A2B;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t03608389BB57475AA3F4B2B79D176A27807BC884;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.Tasks.Task>
struct Dictionary_2_t70161CFEB8DA3C79E19E31D0ED948D3C2925095F;
// System.Collections.Generic.Dictionary`2<System.IntPtr,Firebase.Auth.FirebaseAuth>
struct Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D;
// System.Collections.Generic.Dictionary`2<System.IntPtr,Firebase.FirebaseApp>
struct Dictionary_2_t16FA637EF27DE97C11197B9DE231BEA419C59A62;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>
struct Dictionary_2_tE930FC0A0DF3FBFFE9DC13EED210134C371C368E;
// System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp>
struct Dictionary_2_t2E1F4545167E342F82AFA012A31DD7DD8752B6FE;
// System.Collections.Generic.IEnumerable`1<Firebase.Auth.IUserInfo>
struct IEnumerable_1_t62E378E9873F9801909C3B38FB5B032D7A9929EA;
// System.Collections.Generic.IEnumerator`1<Firebase.Auth.UserInfoInterface>
struct IEnumerator_1_tA813365869032766BDFF63949F181A0B8D63D61A;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t7B82AA0F8B96BAAA21E36DDF7A1FE4348BDDBE95;
// System.Collections.Generic.IEqualityComparer`1<System.IntPtr>
struct IEqualityComparer_1_tB3E7C5F256B78F0F0AC4D0D75860F996DF2C125E;
// System.Collections.Generic.List`1<Firebase.Auth.IUserInfo>
struct List_1_tDAC656203301889991AA49AC3FC48456D0214689;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Exception>
struct ReadOnlyCollection_1_t6D5AC6FC0BF91A16C9E9159F577DEDA4DD3414C8;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.DivideByZeroException
struct DivideByZeroException_tD233835DD9A31EE4E64DD93F2D6143646CFD3FBC;
// System.EventArgs
struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E;
// System.EventHandler
struct EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C;
// System.Exception
struct Exception_t;
// System.Func`1<System.Threading.Tasks.Task/ContingentProperties>
struct Func_1_t48C2978A48CE3F2F6EB5B6DE269D00746483BB1F;
// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>>
struct Func_2_t385E06614170490B1EFD01D106616F0668133752;
// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<System.Object>>
struct Func_2_t8A17E987C73781D20C93E1140DAE0B3595A19A4B;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.IOException
struct IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA;
// System.IndexOutOfRangeException
struct IndexOutOfRangeException_tEC7665FC66525AB6A6916A7EB505E5591683F0CF;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.InvalidCastException
struct InvalidCastException_t91DF9E7D7FCCDA6C562CB4A9A18903E016680FDA;
// System.InvalidOperationException
struct InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1;
// System.NullReferenceException
struct NullReferenceException_t204B194BC4DDA3259AF5A8633EA248AE5977ABDC;
// System.OutOfMemoryException
struct OutOfMemoryException_t2DF3EAC178583BD1DEFAAECBEDB2AF1EA86FBFC7;
// System.OverflowException
struct OverflowException_tD89571E2350DE06D9DE4AB65ADCA77D607B5693D;
// System.Predicate`1<System.Object>
struct Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979;
// System.Predicate`1<System.Threading.Tasks.Task>
struct Predicate_1_tF4286C34BB184CE5690FDCEBA7F09FC68D229335;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.SystemException
struct SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782;
// System.Threading.ContextCallback
struct ContextCallback_t8AE8A965AC6C7ECD396F527F15CDC8E683BE1676;
// System.Threading.Tasks.StackGuard
struct StackGuard_tE431ED3BBD1A18705FEE6F948EBF7FA2E99D64A9;
// System.Threading.Tasks.Task
struct Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2;
// System.Threading.Tasks.Task/ContingentProperties
struct ContingentProperties_t7149A27D01507C74E8BDAAA3848B45D2644FDF08;
// System.Threading.Tasks.TaskCompletionSource`1<Firebase.Auth.FirebaseUser>
struct TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED;
// System.Threading.Tasks.TaskCompletionSource`1<System.Object>
struct TaskCompletionSource_1_t3ECF7B56E418D2A3FFC4E551B9DA6A7E38C7D304;
// System.Threading.Tasks.TaskFactory
struct TaskFactory_tF3C6D983390ACFB40B4979E225368F78006D6155;
// System.Threading.Tasks.TaskFactory`1<Firebase.Auth.FirebaseUser>
struct TaskFactory_1_t405C1C82B1AA4CD904A0712FBAA47E60A993E2E7;
// System.Threading.Tasks.TaskFactory`1<System.Object>
struct TaskFactory_1_t1EC236EC6D34F0BBC2E87658D2A11017407240D4;
// System.Threading.Tasks.TaskScheduler
struct TaskScheduler_t966F2798F198FA90A0DA8EFC92BAC08297793114;
// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>
struct Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7;
// System.Threading.Tasks.Task`1<System.Object>
struct Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Uri/UriInfo
struct UriInfo_t9FCC6BD4EC1EA14D75209E6A35417057BF6EDC5E;
// System.UriParser
struct UriParser_t07C77D673CCE8D2DA253B8A7ACCB010147F1A4AC;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;

IL2CPP_EXTERN_C RuntimeClass* Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_1_t5D2D131226D740682F598DD09436558292112781_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_t591D2A86165F896B4B800BB5C25CE18672A55579_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ApplicationException_t664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArithmeticException_tF9EF5FE94597830EF315C5934258F994B8648269_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DivideByZeroException_tD233835DD9A31EE4E64DD93F2D6143646CFD3FBC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FirebaseException_t947F51F8FDF9CB2D145DC24E92160DC67BF12AEF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FutureVoid_tF512775335C40422BAC529CBF374A246FD8ED51D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GC_tC1D7BD74E8F44ECCEF5CD2B5D84BFF9AAE02D01D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IndexOutOfRangeException_tEC7665FC66525AB6A6916A7EB505E5591683F0CF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InitializationException_t4950F6485639018AF98C04DD016B37F1745F8098_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InvalidCastException_t91DF9E7D7FCCDA6C562CB4A9A18903E016680FDA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tDAC656203301889991AA49AC3FC48456D0214689_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LogUtil_t09D9F776A2032DC33A29AA0E2D4E4E508B8508C4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NullReferenceException_t204B194BC4DDA3259AF5A8633EA248AE5977ABDC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OutOfMemoryException_t2DF3EAC178583BD1DEFAAECBEDB2AF1EA86FBFC7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OverflowException_tD89571E2350DE06D9DE4AB65ADCA77D607B5693D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SWIGStringDelegate_tF943E218400FF77BFF8F047D209AA7CA94915E00_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SWIGStringHelper_t0D99651DC671811558BDB3D299B82F879F5E731B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass22_0_t0B9E0A71713E1E0BCBE5C32D0A17C7786CF35818_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass22_1_tE096CD1670F7DFF2DC0A9E52B629F80ED0B6ED1B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass48_0_t0332496AFB260910C4258BB3F40976B2A20A0312_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass4_0_t89802218D77EADC8D4FDB0DD3CF8FC5447E97B8D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass50_0_t3165B97C90B070A7EAE8BAE6F1E43B1CFD2F46C2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral19EDC1210777BA4D45049C29280D9CC5E1064C25;
IL2CPP_EXTERN_C String_t* _stringLiteral4BD075D15756473773D5A51697A6E781E4129F82;
IL2CPP_EXTERN_C String_t* _stringLiteral519E5A114736C3C9A5709C059C9D5F69D5683AB2;
IL2CPP_EXTERN_C String_t* _stringLiteral75214116684BF6C2BF7B4CD15219AD3BD7A9A2A5;
IL2CPP_EXTERN_C String_t* _stringLiteral8DE36E4005BD5F6F460D1074B3B6D78F7EB671CD;
IL2CPP_EXTERN_C String_t* _stringLiteral8DF36EA2B52717B5109B7D5B88A7A8F713B3640C;
IL2CPP_EXTERN_C String_t* _stringLiteralA804D931F27BE6EF35191274DAF148C0B4FCEEF6;
IL2CPP_EXTERN_C String_t* _stringLiteralABB6C9CBB2EDF2CD0153266C346AC751C2957845;
IL2CPP_EXTERN_C String_t* _stringLiteralB738C54097BC7E491A6F86FB293C8585DDE5AF46;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralE53B9A10A9CA094221A6C1DB46D0C244AB081420;
IL2CPP_EXTERN_C String_t* _stringLiteralE540CDD1328B2B21E29A95405C301B9313B7C346;
IL2CPP_EXTERN_C String_t* _stringLiteralE7064F0B80F61DBC65915311032D27BAA569AE2A;
IL2CPP_EXTERN_C String_t* _stringLiteralEE9F38E186BA06F57B7B74D7E626B94E13CE2556;
IL2CPP_EXTERN_C String_t* _stringLiteralF3368570EE5DB7FA93890C049EB03DD764108E6C;
IL2CPP_EXTERN_C String_t* _stringLiteralFA5342C4F12AD1A860B71DA5AD002761768999C3;
IL2CPP_EXTERN_C String_t* _stringLiteralFB1AE274028B6974E9FDF9D77F5D12C65C8CD4D4;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1_Invoke_mF026A39A2533644C539653019481EDEAE3F2BB04_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_m4089BBB88624AEE2FB2C8D495020C7FB36BDB2D3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_mE56C97A498D5E1CFF95301DEEF78027CD25448E1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AuthUtil_CreateAuthStateListener_m5B798BC3C73CF1288B517705947C8F968F49C828_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AuthUtil_CreateIdTokenListener_m48BB536BB8AFB937461FEB96BB910727FAD27187_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AuthUtil_DestroyAuthStateListener_m37102E0B2C69B71C1517E7771899467C04916BC7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* AuthUtil_DestroyIdTokenListener_m57CA413B20CBFF946AA457063051AF94A03ECF22_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Credential_IsValid_m301CF4DEDE9449F85400AECDABF195D322DEA039_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Remove_m11C98448049706AEDFE36C6B229DC8C1DA5AD74F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Remove_m8D084F34E8CA42993A2B124C27B12C0520B916BD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_TryGetValue_m2C05B94C421C439F201A3C24298D040A66655D23_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_TryGetValue_mBF4E69D81E01649E50C6DFD9BFE384903C7B81C8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m0CBFF732991FC69E50314435AC3E598F357091AE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mD0C4E66AB396EE592D08876CA7D94D68B67B90A1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_m076684AAD9154CBFD0ACA5E778B83C7D2443BA8F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_mABDE8D3E6C60B55410DDB2AA6FE39F9CD5D31BC9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FacebookAuthProvider_GetCredential_m29842AFC31DCAFC30F2E1D492DD575A83512C3F5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseAuth_GetAuthInternal_mC4406E480123387B5EBE17F44BBB61A469EE059D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseAuth_IdTokenChangedFunction_m4867AD7FBED6DB5C1529D485866EB591AFC67BBF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseAuth_LogHeartbeatInternal_m39BE7DCAB1FCAC3F93F7366A80B69B5ADE4CC145_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseAuth_OnAppDisposed_m432A4ECCD62471F891EC24B73F0BE81794DCA4EB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseAuth_ReleaseReferenceInternal_mDCB0650BAC51CBB644F738FF72AF2B18D2F3862B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseAuth_SignInAnonymouslyInternalAsync_m03786609BE942FA6FEE38405709D18F9F501291C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseAuth_SignInWithCredentialInternalAsync_mE39D174D69D03D600340190C92DCBE0390FEF28B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseAuth_SignOut_m6849FBD2DC9A11FD9BE593450D195BDA0F8B4C0D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseAuth_StateChangedFunction_m3EB4E147700973B35AAB4C934E401A47B0098CB7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseAuth_ThrowIfNull_m927509F7D041820C618B0061E723697AEDEFC3EC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseAuth_get_CurrentUserInternal_mBA35503BDA1894FCF3E6A586BB0B68E34F148C74_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseUser_LinkWithCredentialAsync_mC6D4BAE39FF8EE275EB3878D63690AE08F129627_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseUser_get_DisplayName_mC9A77732AD5665647AB92F193A19C10E67EE34AB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseUser_get_Email_m824D4D6FADD78BE6C4C6E1FF3FC2645D5F4ADF06_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseUser_get_IsAnonymous_m66E9F5CD028313FE66A48CD0A2C334AC35908902_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseUser_get_PhotoUrlInternal_mAAE831A4F08E1CD94A486970B5F029F7D1D1216F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* FirebaseUser_get_UserId_m1A1850E055379A8AED15B80FE8DDDA60EE4F42DD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Future_User_GetResult_mE1A953E51D10BE4DD15137A78F45FFAA9D566902_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Future_User_SWIG_CompletionDispatcher_m9608732E8FCF7D93C2A70B63BA750D3231DCAD47_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Future_User_SWIG_FreeCompletionData_mE5A33CAC1849B25DD160DC042A980BFDB6F7D45A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Future_User_SWIG_OnCompletion_mD2853E95978B586313F3D904FEDA65F0DC04B44A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Future_User_ThrowIfDisposed_m963BF98F552F3F37DFEC0AEB1DF07207809835DE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m52A7A5A55C7C1AB245A219997212D466AFA9176D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m93A21423354207CBD75E21125B75C91D9C17FE60_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PlayGamesAuthProvider_GetCredential_mA75F27769EA5F8148F14D040FB1C9783DF4ABC33_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingApplicationException_m1EB96681AA0CAEE7C648B0488735AC4B34644625_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentException_mCC2449972A52DACB566318E5D82883A1AE0FCC77_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentNullException_mDDF072DC234116293DF4DD4CAE04931816995032_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m64A3753D6C93082BF5431CA3B095125235597BA7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingArithmeticException_m6FD83BB7EC4D0C5A758FC33434AFF95000BB2145_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingDivideByZeroException_mC428FC6EAD280918627E7AD8285B5EC490842944_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingIOException_mE61B638B09F96D19FA24D9B2BD85230650FC59B2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m8AC3EB8E22CAB0B37997524B3D8EB61E7C4B9888_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidCastException_m3B7ACF97DC9FB2B9F197C4E3168B9C4F8A4D0719_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingInvalidOperationException_mECD029C557A14DBA585161CF1E3EDD1C3201813D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingNullReferenceException_mB61F8433F7B8D82845D1635AC67E6132F1F3A7B9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingOutOfMemoryException_mD766FD3AB3B0EF9E768367C0199FC00A9362119E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingOverflowException_m4F7B5FA76EE51D9CB219856AB845929CD819FDF2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGExceptionHelper_SetPendingSystemException_mB83BE9B10C217AA022F9C626585DCE16A1E3BF43_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGPendingException_Set_m2A770FCDB68242E723C1E3A5CA7B4B86B969155C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SWIGStringHelper_CreateString_m8CFC15A5CFF34D0E23227F3DB4BD483E6B495134_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TaskCompletionSourceCompat_1_SetException_m60E20FA51C992BE0F3CD0F9BA9C20887A1204F1D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TaskCompletionSource_1_SetCanceled_mE99DCDD6E5327656A075BA8D5EC8EAEF2F99AAB6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TaskCompletionSource_1_SetException_m0F8FFC0B85AFA0B942C0966332B7E3275CF42A1B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TaskCompletionSource_1_SetResult_mE9B14A44C05A19AB24B379A0CB6C4EC6F21BF410_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TaskCompletionSource_1__ctor_m886A4697ACB84BF43814F0D64469B76A08FDFAC5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TaskCompletionSource_1_get_Task_m003148330C7CDC741C0872FC50DA059683141623_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Task_1_ContinueWith_mF2A8F0D71FCEA9B5D7B2A79A5DCF193DA524B01E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Task_1_get_Result_m547A389B49652DF34BED42F12292E2483FC2059D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CIdTokenChangedFunctionU3Eb__24_0_mBDB096D23092A8E0948E7EEABA9ECE55DD9C92FB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CStateChangedFunctionU3Eb__23_0_m4DF5A2B9C4EF839583FBDD03461848F5EF489F4E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass19_1_U3CGetAuthU3Eb__0_m50656295764711C10015E4E049FAD60671EC162A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass22_1_U3CForwardStateChangeU3Eb__0_m4958CF79DC4F966EB6B44A3100894727D07F5D49_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass48_0_U3CSignInWithCredentialAsyncU3Eb__0_m732B3D2DD97881C4CEEF6D96D047F28D1DE67CAE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass4_0_U3CGetTaskU3Eb__0_mC165A76D4A3E3CA94F84D8F613ECE4E75D2EF7DF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass50_0_U3CSignInAnonymouslyAsyncU3Eb__0_m1D34F38B75501F86230E71CD29B059F63C572E4A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UserInfoInterfaceListEnumerator_Reset_m89E71EC357585556E719F9C537D6A4954D0804E4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UserInfoInterfaceListEnumerator_get_Current_mE95C0E6329FA074B3BB193C170A0B46DD8A6A720_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UserInfoInterfaceList_Add_m272A420019BB6431562C0EE4CF84E86280F2BBD9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UserInfoInterfaceList_Clear_mF43F449B72C454169BE9001F0BDB87A43B9BC711_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UserInfoInterfaceList_Contains_m3D0CA1C4D5D72A7D59BE69C5067AAE50CF4063F8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UserInfoInterfaceList_CopyTo_mA6612EBF0827029C8CBF0CEE383D0FFB11D32B9E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UserInfoInterfaceList_IndexOf_m814BFA29319E2C0C125EE6E87B0FD742C890CB86_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UserInfoInterfaceList_Insert_mEF354596EFD206D50AE0F69B9A76E1A6B4DC1878_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UserInfoInterfaceList_RemoveAt_mBE2A2F80ACBA832F24F7B066A861019BFC1F4E0B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UserInfoInterfaceList_Remove_m6AE184FAF9D7EC2568042FF0304127885EC5F422_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UserInfoInterfaceList_getitem_mFD9B0EB9CA5CA925534712BDF225DEE3B2439CEA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UserInfoInterfaceList_getitemcopy_m886AA8C76520C49BFC25FEE8AFC1C2767BBA2A00_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UserInfoInterfaceList_setitem_m87AC7CBE94A90F753BCF80E9658D7FD0C9A8F80F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UserInfoInterfaceList_size_m370AE0FA3028E3D76252D636A6274D1FBD12BC6B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UserInfoInterface_get_DisplayName_m3DCD08821470A2241296F8AE5E40E28935E61832_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UserInfoInterface_get_Email_m02D9D14CC54BD35802499A248FD7382F9E95C6AC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UserInfoInterface_get_PhotoUrlInternal_m9F70731EE318CA6D5F39817B161185E198BAC024_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UserInfoInterface_get_ProviderId_mA71AA9527D83D2FBACFBCB38ACACA341F197660C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UserInfoInterface_get_UserId_m3A02719809CE964AFB9ABECF26FFCF81D2415C9D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UserProfile__ctor_m4747BEB722B1243AD8AAA126F205FD8A08D8C809_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UserProfile_set_DisplayName_m0D8F5FA21420F08A93200DDA53AF9452882A6557_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UserProfile_set_PhotoUrlInternal_mED2EA1583FC42D01FE71BA105537EDF6E2019363_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t AuthUtilPINVOKE__cctor_mB959A2D2962E2F91E1F004B060423C21556019E0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AuthUtil_CreateAuthStateListener_m5B798BC3C73CF1288B517705947C8F968F49C828_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AuthUtil_CreateIdTokenListener_m48BB536BB8AFB937461FEB96BB910727FAD27187_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AuthUtil_DestroyAuthStateListener_m37102E0B2C69B71C1517E7771899467C04916BC7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AuthUtil_DestroyIdTokenListener_m57CA413B20CBFF946AA457063051AF94A03ECF22_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Credential_Dispose_m76866FF791102DEEC0E64363C978654CA2E0DD87_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Credential_Dispose_m78B018A97AF219BCDE1DCA819A2E94EA8BF85FC5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Credential_IsValid_m301CF4DEDE9449F85400AECDABF195D322DEA039_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Credential_getCPtr_m892678A92D70544624773536D6E26DDEDFE63E42_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookAuthProvider_GetCredential_m29842AFC31DCAFC30F2E1D492DD575A83512C3F5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FacebookAuthProvider__cctor_mF5D3E8AA0DA69CEDDE05F5547497DC789089ECF6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_CompleteFirebaseUserTask_mD79D870B498E279B1654740125368DD6A6FB8BFF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_DisposeInternal_m5DD53E6F76144CC4B417157E6C3475F7704117DC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_Dispose_mDAE8E88D96D92B5D08A8E3165DFA4F3F4DCF30DA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_ForwardStateChange_m493936DA530F8A8DE041E7F2C8E1409B0924BE5C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_GetAuthInternal_mC4406E480123387B5EBE17F44BBB61A469EE059D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_GetAuth_mEFC135E9B1D37E4739D3E2A8D30A1C80DDE3F157_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_IdTokenChangedFunction_m4867AD7FBED6DB5C1529D485866EB591AFC67BBF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_LogHeartbeatInternal_m39BE7DCAB1FCAC3F93F7366A80B69B5ADE4CC145_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_ProxyFromAppCPtr_mB07E3A48DDE2BFB10C206669CC172BAC2FE69A15_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_ReleaseReferenceInternal_mDCB0650BAC51CBB644F738FF72AF2B18D2F3862B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_SignInAnonymouslyAsync_m6963AC820328CD24A672D8800D1B51BD98FF0995_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_SignInAnonymouslyInternalAsync_m03786609BE942FA6FEE38405709D18F9F501291C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_SignInWithCredentialAsync_m0C8097559DE43C62ED1AAC3D6105B155B02C948F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_SignInWithCredentialInternalAsync_mE39D174D69D03D600340190C92DCBE0390FEF28B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_SignOut_m6849FBD2DC9A11FD9BE593450D195BDA0F8B4C0D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_StateChangedFunction_m3EB4E147700973B35AAB4C934E401A47B0098CB7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_ThrowIfNull_m927509F7D041820C618B0061E723697AEDEFC3EC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_UpdateCurrentUser_mAA765D9A52031913DB1D3BBFD50DF4A4F5C4D4CF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth__cctor_m9714CC491B97AB4DE8C0D5763ACB8BAEB9E80936_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_add_StateChanged_mFA3F7A1FE096865FF349DBD64644371C0C8CCB34_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_add_stateChangedImpl_m1387CE8F8F0F4796FFDF97ADD352101FEE55335C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_getCPtr_m43139A9D3674A9DFF8AED10B43D345217CE378C9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_get_CurrentUserInternal_mBA35503BDA1894FCF3E6A586BB0B68E34F148C74_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_get_CurrentUser_m7BE23C546ECA1E95C9E383EE434BFCF22A92CFFF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_get_DefaultInstance_mA307D08C178B8C5A8EA7BE0F3222668A544FA228_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseAuth_remove_stateChangedImpl_m6BC5FB994A66D606B5783D721FE3D17FD3D5B4D2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseUser_Dispose_m347979011C289888965AA04311F9CD818CB5994A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseUser_LinkWithCredentialAsync_mC6D4BAE39FF8EE275EB3878D63690AE08F129627_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseUser_UpdateEmailAsync_m8D0B4CA7BE4605CF684B74CEE6C5A2ADA2E1F5A7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseUser_UpdateUserProfileAsync_mACFB318A8FA0A47B70F317197B0C5884D5772D9D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseUser__ctor_m19BF183B130EA0D6A400A715A62A07877B7F15D5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseUser_getCPtr_mE5B6DFE6A8DE44DC9F744D57A7863DBC40F3284D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseUser_get_DisplayName_mC9A77732AD5665647AB92F193A19C10E67EE34AB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseUser_get_Email_m824D4D6FADD78BE6C4C6E1FF3FC2645D5F4ADF06_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseUser_get_IsAnonymous_m66E9F5CD028313FE66A48CD0A2C334AC35908902_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseUser_get_PhotoUrlInternal_mAAE831A4F08E1CD94A486970B5F029F7D1D1216F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseUser_get_PhotoUrl_mAC0F8F1390483E1FD30D932527542C89E9C389C6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseUser_get_ProviderData_mE5D70ADC73F94F71D686939C53A8984CCEC682D0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FirebaseUser_get_UserId_m1A1850E055379A8AED15B80FE8DDDA60EE4F42DD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Future_User_Dispose_m82E8E0011FC005AB27FDF878D851C18D22FB19E1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Future_User_GetResult_mE1A953E51D10BE4DD15137A78F45FFAA9D566902_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Future_User_GetTask_mF54DC9C5FC9DD22F21D53B3E36FC8BE82023A506_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Future_User_SWIG_CompletionDispatcher_m9608732E8FCF7D93C2A70B63BA750D3231DCAD47_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Future_User_SWIG_FreeCompletionData_mE5A33CAC1849B25DD160DC042A980BFDB6F7D45A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Future_User_SWIG_OnCompletion_mD2853E95978B586313F3D904FEDA65F0DC04B44A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Future_User_SetOnCompletionCallback_mA4F7DBBBBBA68E9A54C2231034CCE80415DD80A6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Future_User_ThrowIfDisposed_m963BF98F552F3F37DFEC0AEB1DF07207809835DE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Future_User__cctor_m4576D93855EFC88AA0CD018753370EBD1F2BE0EA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Future_User__ctor_mF7A226B0665A25144444D5F951A9C29D7F7D755A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayGamesAuthProvider_GetCredential_mA75F27769EA5F8148F14D040FB1C9783DF4ABC33_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PlayGamesAuthProvider__cctor_m8EE6103C650CD01A16F0ECFB31FD4E50E6E17CBA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SWIGExceptionHelper_SetPendingApplicationException_m1EB96681AA0CAEE7C648B0488735AC4B34644625_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SWIGExceptionHelper_SetPendingArgumentException_mCC2449972A52DACB566318E5D82883A1AE0FCC77_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SWIGExceptionHelper_SetPendingArgumentNullException_mDDF072DC234116293DF4DD4CAE04931816995032_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m64A3753D6C93082BF5431CA3B095125235597BA7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SWIGExceptionHelper_SetPendingArithmeticException_m6FD83BB7EC4D0C5A758FC33434AFF95000BB2145_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SWIGExceptionHelper_SetPendingDivideByZeroException_mC428FC6EAD280918627E7AD8285B5EC490842944_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SWIGExceptionHelper_SetPendingIOException_mE61B638B09F96D19FA24D9B2BD85230650FC59B2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m8AC3EB8E22CAB0B37997524B3D8EB61E7C4B9888_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SWIGExceptionHelper_SetPendingInvalidCastException_m3B7ACF97DC9FB2B9F197C4E3168B9C4F8A4D0719_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SWIGExceptionHelper_SetPendingInvalidOperationException_mECD029C557A14DBA585161CF1E3EDD1C3201813D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SWIGExceptionHelper_SetPendingNullReferenceException_mB61F8433F7B8D82845D1635AC67E6132F1F3A7B9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SWIGExceptionHelper_SetPendingOutOfMemoryException_mD766FD3AB3B0EF9E768367C0199FC00A9362119E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SWIGExceptionHelper_SetPendingOverflowException_m4F7B5FA76EE51D9CB219856AB845929CD819FDF2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SWIGExceptionHelper_SetPendingSystemException_mB83BE9B10C217AA022F9C626585DCE16A1E3BF43_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SWIGExceptionHelper__cctor_mC03977EF4F677D23618BA72DEE999DAD6233D0CE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SWIGPendingException_Retrieve_m47C7FCF8ED6E8C49646820EE0691C3EDE0CFFDCD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SWIGPendingException_Set_m2A770FCDB68242E723C1E3A5CA7B4B86B969155C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SWIGPendingException__cctor_mC42B091F09F3F6365C73742A66AD460617B4B7EB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SWIGPendingException_get_Pending_m746EAD61F79AF2A98C4350069D4507EAC1D251DE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SWIGStringHelper__cctor_mE675A6A15898F4EF6BD38CADB60C065692FF609E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SWIG_CompletionDelegate_BeginInvoke_m1F1D032BA7DC7797D7B332090F05CF043978EA1F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t StateChangedDelegate_BeginInvoke_m57C156934DDB33999507D45527F6E84DF9F6BA35_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec_U3CIdTokenChangedFunctionU3Eb__24_0_mBDB096D23092A8E0948E7EEABA9ECE55DD9C92FB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec_U3CStateChangedFunctionU3Eb__23_0_m4DF5A2B9C4EF839583FBDD03461848F5EF489F4E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass19_1_U3CGetAuthU3Eb__0_m50656295764711C10015E4E049FAD60671EC162A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass22_1_U3CForwardStateChangeU3Eb__0_m4958CF79DC4F966EB6B44A3100894727D07F5D49_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass4_0_U3CGetTaskU3Eb__0_mC165A76D4A3E3CA94F84D8F613ECE4E75D2EF7DF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__cctor_m07998BEFD2CCB1494ECB6037ACA9B180B78E0086_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterfaceListEnumerator_Reset_m89E71EC357585556E719F9C537D6A4954D0804E4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterfaceListEnumerator_get_Current_mE95C0E6329FA074B3BB193C170A0B46DD8A6A720_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterfaceList_Add_m272A420019BB6431562C0EE4CF84E86280F2BBD9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterfaceList_Clear_mF43F449B72C454169BE9001F0BDB87A43B9BC711_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterfaceList_Contains_m3D0CA1C4D5D72A7D59BE69C5067AAE50CF4063F8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterfaceList_CopyTo_mA6612EBF0827029C8CBF0CEE383D0FFB11D32B9E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterfaceList_Dispose_m6903C9BF5E1050559288FD30150C273778B4DC05_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterfaceList_Dispose_mC25B3B40D335F63D189E23B81DEEA426C0E76350_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterfaceList_GetEnumerator_m34C89390756E8DCA0D4FDE9EC4020AF7AFBB4021_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterfaceList_IndexOf_m814BFA29319E2C0C125EE6E87B0FD742C890CB86_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterfaceList_Insert_mEF354596EFD206D50AE0F69B9A76E1A6B4DC1878_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterfaceList_RemoveAt_mBE2A2F80ACBA832F24F7B066A861019BFC1F4E0B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterfaceList_Remove_m6AE184FAF9D7EC2568042FF0304127885EC5F422_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterfaceList_getitem_mFD9B0EB9CA5CA925534712BDF225DEE3B2439CEA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterfaceList_getitemcopy_m886AA8C76520C49BFC25FEE8AFC1C2767BBA2A00_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterfaceList_globalU3AU3ASystem_Collections_Generic_IEnumerableU3CFirebase_Auth_UserInfoInterfaceU3E_GetEnumerator_mCDDED8A7BA953D28567C3DE977F584DC759DE4C3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterfaceList_globalU3AU3ASystem_Collections_IEnumerable_GetEnumerator_m0D58B068226E88BDF4B38BF06648A191AC0E75F9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterfaceList_setitem_m87AC7CBE94A90F753BCF80E9658D7FD0C9A8F80F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterfaceList_size_m370AE0FA3028E3D76252D636A6274D1FBD12BC6B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterface_Dispose_m8532EF4241914613A205B8E2E1720C39FD85C1AA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterface_Dispose_mE1B360122F9E6726A8FE40382AE6BD8683EAD61B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterface_getCPtr_m15659A46894D5F64CC2EE953C4F5D97DCB243680_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterface_get_DisplayName_m3DCD08821470A2241296F8AE5E40E28935E61832_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterface_get_Email_m02D9D14CC54BD35802499A248FD7382F9E95C6AC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterface_get_PhotoUrlInternal_m9F70731EE318CA6D5F39817B161185E198BAC024_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterface_get_PhotoUrl_m459FA1AD2C0AB62EFEF0B264F4ED9532CC15484C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterface_get_ProviderId_mA71AA9527D83D2FBACFBCB38ACACA341F197660C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserInfoInterface_get_UserId_m3A02719809CE964AFB9ABECF26FFCF81D2415C9D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserProfile_Dispose_m5BC283516965B4FC98A8AF63957F03D035DD94C3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserProfile_Dispose_m8B47F62EA95EDA7AF6819F157ED0C3043944380C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserProfile__ctor_m4747BEB722B1243AD8AAA126F205FD8A08D8C809_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserProfile_getCPtr_m43D66A8F09ED56F3026B25C5DFEEEA8A3BEB7CEC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserProfile_set_DisplayName_m0D8F5FA21420F08A93200DDA53AF9452882A6557_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserProfile_set_PhotoUrlInternal_mED2EA1583FC42D01FE71BA105537EDF6E2019363_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UserProfile_set_PhotoUrl_mCDA8A14D63367925AD4D59808B472B88086D977A_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct UserInfoInterfaceU5BU5D_tD810A76761E9D37FB729A09C273728E21098926B;
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3C8CB9279550DE19F9BF93E079B88CBD9F456CF4 
{
public:

public:
};


// System.Object


// Firebase.Auth.AuthUtil
struct  AuthUtil_t72619D090D6BE5653BD03CB7D39D6DBDB93CB24F  : public RuntimeObject
{
public:

public:
};


// Firebase.Auth.AuthUtilPINVOKE
struct  AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B  : public RuntimeObject
{
public:

public:
};

struct AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_StaticFields
{
public:
	// Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper Firebase.Auth.AuthUtilPINVOKE::swigExceptionHelper
	SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97 * ___swigExceptionHelper_0;
	// Firebase.Auth.AuthUtilPINVOKE_SWIGStringHelper Firebase.Auth.AuthUtilPINVOKE::swigStringHelper
	SWIGStringHelper_t0D99651DC671811558BDB3D299B82F879F5E731B * ___swigStringHelper_1;

public:
	inline static int32_t get_offset_of_swigExceptionHelper_0() { return static_cast<int32_t>(offsetof(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_StaticFields, ___swigExceptionHelper_0)); }
	inline SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97 * get_swigExceptionHelper_0() const { return ___swigExceptionHelper_0; }
	inline SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97 ** get_address_of_swigExceptionHelper_0() { return &___swigExceptionHelper_0; }
	inline void set_swigExceptionHelper_0(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97 * value)
	{
		___swigExceptionHelper_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___swigExceptionHelper_0), (void*)value);
	}

	inline static int32_t get_offset_of_swigStringHelper_1() { return static_cast<int32_t>(offsetof(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_StaticFields, ___swigStringHelper_1)); }
	inline SWIGStringHelper_t0D99651DC671811558BDB3D299B82F879F5E731B * get_swigStringHelper_1() const { return ___swigStringHelper_1; }
	inline SWIGStringHelper_t0D99651DC671811558BDB3D299B82F879F5E731B ** get_address_of_swigStringHelper_1() { return &___swigStringHelper_1; }
	inline void set_swigStringHelper_1(SWIGStringHelper_t0D99651DC671811558BDB3D299B82F879F5E731B * value)
	{
		___swigStringHelper_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___swigStringHelper_1), (void*)value);
	}
};


// Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper
struct  SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97  : public RuntimeObject
{
public:

public:
};

struct SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields
{
public:
	// Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::applicationDelegate
	ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___applicationDelegate_0;
	// Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::arithmeticDelegate
	ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___arithmeticDelegate_1;
	// Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::divideByZeroDelegate
	ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___divideByZeroDelegate_2;
	// Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::indexOutOfRangeDelegate
	ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___indexOutOfRangeDelegate_3;
	// Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::invalidCastDelegate
	ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___invalidCastDelegate_4;
	// Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::invalidOperationDelegate
	ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___invalidOperationDelegate_5;
	// Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::ioDelegate
	ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___ioDelegate_6;
	// Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::nullReferenceDelegate
	ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___nullReferenceDelegate_7;
	// Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::outOfMemoryDelegate
	ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___outOfMemoryDelegate_8;
	// Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::overflowDelegate
	ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___overflowDelegate_9;
	// Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::systemDelegate
	ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___systemDelegate_10;
	// Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::argumentDelegate
	ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * ___argumentDelegate_11;
	// Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::argumentNullDelegate
	ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * ___argumentNullDelegate_12;
	// Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::argumentOutOfRangeDelegate
	ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * ___argumentOutOfRangeDelegate_13;

public:
	inline static int32_t get_offset_of_applicationDelegate_0() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields, ___applicationDelegate_0)); }
	inline ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * get_applicationDelegate_0() const { return ___applicationDelegate_0; }
	inline ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 ** get_address_of_applicationDelegate_0() { return &___applicationDelegate_0; }
	inline void set_applicationDelegate_0(ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * value)
	{
		___applicationDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___applicationDelegate_0), (void*)value);
	}

	inline static int32_t get_offset_of_arithmeticDelegate_1() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields, ___arithmeticDelegate_1)); }
	inline ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * get_arithmeticDelegate_1() const { return ___arithmeticDelegate_1; }
	inline ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 ** get_address_of_arithmeticDelegate_1() { return &___arithmeticDelegate_1; }
	inline void set_arithmeticDelegate_1(ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * value)
	{
		___arithmeticDelegate_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arithmeticDelegate_1), (void*)value);
	}

	inline static int32_t get_offset_of_divideByZeroDelegate_2() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields, ___divideByZeroDelegate_2)); }
	inline ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * get_divideByZeroDelegate_2() const { return ___divideByZeroDelegate_2; }
	inline ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 ** get_address_of_divideByZeroDelegate_2() { return &___divideByZeroDelegate_2; }
	inline void set_divideByZeroDelegate_2(ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * value)
	{
		___divideByZeroDelegate_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___divideByZeroDelegate_2), (void*)value);
	}

	inline static int32_t get_offset_of_indexOutOfRangeDelegate_3() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields, ___indexOutOfRangeDelegate_3)); }
	inline ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * get_indexOutOfRangeDelegate_3() const { return ___indexOutOfRangeDelegate_3; }
	inline ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 ** get_address_of_indexOutOfRangeDelegate_3() { return &___indexOutOfRangeDelegate_3; }
	inline void set_indexOutOfRangeDelegate_3(ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * value)
	{
		___indexOutOfRangeDelegate_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___indexOutOfRangeDelegate_3), (void*)value);
	}

	inline static int32_t get_offset_of_invalidCastDelegate_4() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields, ___invalidCastDelegate_4)); }
	inline ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * get_invalidCastDelegate_4() const { return ___invalidCastDelegate_4; }
	inline ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 ** get_address_of_invalidCastDelegate_4() { return &___invalidCastDelegate_4; }
	inline void set_invalidCastDelegate_4(ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * value)
	{
		___invalidCastDelegate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___invalidCastDelegate_4), (void*)value);
	}

	inline static int32_t get_offset_of_invalidOperationDelegate_5() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields, ___invalidOperationDelegate_5)); }
	inline ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * get_invalidOperationDelegate_5() const { return ___invalidOperationDelegate_5; }
	inline ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 ** get_address_of_invalidOperationDelegate_5() { return &___invalidOperationDelegate_5; }
	inline void set_invalidOperationDelegate_5(ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * value)
	{
		___invalidOperationDelegate_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___invalidOperationDelegate_5), (void*)value);
	}

	inline static int32_t get_offset_of_ioDelegate_6() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields, ___ioDelegate_6)); }
	inline ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * get_ioDelegate_6() const { return ___ioDelegate_6; }
	inline ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 ** get_address_of_ioDelegate_6() { return &___ioDelegate_6; }
	inline void set_ioDelegate_6(ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * value)
	{
		___ioDelegate_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ioDelegate_6), (void*)value);
	}

	inline static int32_t get_offset_of_nullReferenceDelegate_7() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields, ___nullReferenceDelegate_7)); }
	inline ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * get_nullReferenceDelegate_7() const { return ___nullReferenceDelegate_7; }
	inline ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 ** get_address_of_nullReferenceDelegate_7() { return &___nullReferenceDelegate_7; }
	inline void set_nullReferenceDelegate_7(ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * value)
	{
		___nullReferenceDelegate_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nullReferenceDelegate_7), (void*)value);
	}

	inline static int32_t get_offset_of_outOfMemoryDelegate_8() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields, ___outOfMemoryDelegate_8)); }
	inline ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * get_outOfMemoryDelegate_8() const { return ___outOfMemoryDelegate_8; }
	inline ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 ** get_address_of_outOfMemoryDelegate_8() { return &___outOfMemoryDelegate_8; }
	inline void set_outOfMemoryDelegate_8(ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * value)
	{
		___outOfMemoryDelegate_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___outOfMemoryDelegate_8), (void*)value);
	}

	inline static int32_t get_offset_of_overflowDelegate_9() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields, ___overflowDelegate_9)); }
	inline ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * get_overflowDelegate_9() const { return ___overflowDelegate_9; }
	inline ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 ** get_address_of_overflowDelegate_9() { return &___overflowDelegate_9; }
	inline void set_overflowDelegate_9(ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * value)
	{
		___overflowDelegate_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___overflowDelegate_9), (void*)value);
	}

	inline static int32_t get_offset_of_systemDelegate_10() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields, ___systemDelegate_10)); }
	inline ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * get_systemDelegate_10() const { return ___systemDelegate_10; }
	inline ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 ** get_address_of_systemDelegate_10() { return &___systemDelegate_10; }
	inline void set_systemDelegate_10(ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * value)
	{
		___systemDelegate_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___systemDelegate_10), (void*)value);
	}

	inline static int32_t get_offset_of_argumentDelegate_11() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields, ___argumentDelegate_11)); }
	inline ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * get_argumentDelegate_11() const { return ___argumentDelegate_11; }
	inline ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 ** get_address_of_argumentDelegate_11() { return &___argumentDelegate_11; }
	inline void set_argumentDelegate_11(ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * value)
	{
		___argumentDelegate_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___argumentDelegate_11), (void*)value);
	}

	inline static int32_t get_offset_of_argumentNullDelegate_12() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields, ___argumentNullDelegate_12)); }
	inline ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * get_argumentNullDelegate_12() const { return ___argumentNullDelegate_12; }
	inline ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 ** get_address_of_argumentNullDelegate_12() { return &___argumentNullDelegate_12; }
	inline void set_argumentNullDelegate_12(ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * value)
	{
		___argumentNullDelegate_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___argumentNullDelegate_12), (void*)value);
	}

	inline static int32_t get_offset_of_argumentOutOfRangeDelegate_13() { return static_cast<int32_t>(offsetof(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields, ___argumentOutOfRangeDelegate_13)); }
	inline ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * get_argumentOutOfRangeDelegate_13() const { return ___argumentOutOfRangeDelegate_13; }
	inline ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 ** get_address_of_argumentOutOfRangeDelegate_13() { return &___argumentOutOfRangeDelegate_13; }
	inline void set_argumentOutOfRangeDelegate_13(ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * value)
	{
		___argumentOutOfRangeDelegate_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___argumentOutOfRangeDelegate_13), (void*)value);
	}
};


// Firebase.Auth.AuthUtilPINVOKE_SWIGPendingException
struct  SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48  : public RuntimeObject
{
public:

public:
};

struct SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_StaticFields
{
public:
	// System.Int32 Firebase.Auth.AuthUtilPINVOKE_SWIGPendingException::numExceptionsPending
	int32_t ___numExceptionsPending_1;
	// System.Object Firebase.Auth.AuthUtilPINVOKE_SWIGPendingException::exceptionsLock
	RuntimeObject * ___exceptionsLock_2;

public:
	inline static int32_t get_offset_of_numExceptionsPending_1() { return static_cast<int32_t>(offsetof(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_StaticFields, ___numExceptionsPending_1)); }
	inline int32_t get_numExceptionsPending_1() const { return ___numExceptionsPending_1; }
	inline int32_t* get_address_of_numExceptionsPending_1() { return &___numExceptionsPending_1; }
	inline void set_numExceptionsPending_1(int32_t value)
	{
		___numExceptionsPending_1 = value;
	}

	inline static int32_t get_offset_of_exceptionsLock_2() { return static_cast<int32_t>(offsetof(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_StaticFields, ___exceptionsLock_2)); }
	inline RuntimeObject * get_exceptionsLock_2() const { return ___exceptionsLock_2; }
	inline RuntimeObject ** get_address_of_exceptionsLock_2() { return &___exceptionsLock_2; }
	inline void set_exceptionsLock_2(RuntimeObject * value)
	{
		___exceptionsLock_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___exceptionsLock_2), (void*)value);
	}
};

struct SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_ThreadStaticFields
{
public:
	// System.Exception Firebase.Auth.AuthUtilPINVOKE_SWIGPendingException::pendingException
	Exception_t * ___pendingException_0;

public:
	inline static int32_t get_offset_of_pendingException_0() { return static_cast<int32_t>(offsetof(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_ThreadStaticFields, ___pendingException_0)); }
	inline Exception_t * get_pendingException_0() const { return ___pendingException_0; }
	inline Exception_t ** get_address_of_pendingException_0() { return &___pendingException_0; }
	inline void set_pendingException_0(Exception_t * value)
	{
		___pendingException_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pendingException_0), (void*)value);
	}
};


// Firebase.Auth.AuthUtilPINVOKE_SWIGStringHelper
struct  SWIGStringHelper_t0D99651DC671811558BDB3D299B82F879F5E731B  : public RuntimeObject
{
public:

public:
};

struct SWIGStringHelper_t0D99651DC671811558BDB3D299B82F879F5E731B_StaticFields
{
public:
	// Firebase.Auth.AuthUtilPINVOKE_SWIGStringHelper_SWIGStringDelegate Firebase.Auth.AuthUtilPINVOKE_SWIGStringHelper::stringDelegate
	SWIGStringDelegate_tF943E218400FF77BFF8F047D209AA7CA94915E00 * ___stringDelegate_0;

public:
	inline static int32_t get_offset_of_stringDelegate_0() { return static_cast<int32_t>(offsetof(SWIGStringHelper_t0D99651DC671811558BDB3D299B82F879F5E731B_StaticFields, ___stringDelegate_0)); }
	inline SWIGStringDelegate_tF943E218400FF77BFF8F047D209AA7CA94915E00 * get_stringDelegate_0() const { return ___stringDelegate_0; }
	inline SWIGStringDelegate_tF943E218400FF77BFF8F047D209AA7CA94915E00 ** get_address_of_stringDelegate_0() { return &___stringDelegate_0; }
	inline void set_stringDelegate_0(SWIGStringDelegate_tF943E218400FF77BFF8F047D209AA7CA94915E00 * value)
	{
		___stringDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stringDelegate_0), (void*)value);
	}
};


// Firebase.Auth.FacebookAuthProvider
struct  FacebookAuthProvider_t05151B6B548EB3C91D029FABAB200D7EF11500B2  : public RuntimeObject
{
public:

public:
};


// Firebase.Auth.FirebaseAuth_<>c
struct  U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_StaticFields
{
public:
	// Firebase.Auth.FirebaseAuth_<>c Firebase.Auth.FirebaseAuth_<>c::<>9
	U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF * ___U3CU3E9_0;
	// System.Action`1<Firebase.Auth.FirebaseAuth> Firebase.Auth.FirebaseAuth_<>c::<>9__23_0
	Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * ___U3CU3E9__23_0_1;
	// System.Action`1<Firebase.Auth.FirebaseAuth> Firebase.Auth.FirebaseAuth_<>c::<>9__24_0
	Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * ___U3CU3E9__24_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__23_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_StaticFields, ___U3CU3E9__23_0_1)); }
	inline Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * get_U3CU3E9__23_0_1() const { return ___U3CU3E9__23_0_1; }
	inline Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E ** get_address_of_U3CU3E9__23_0_1() { return &___U3CU3E9__23_0_1; }
	inline void set_U3CU3E9__23_0_1(Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * value)
	{
		___U3CU3E9__23_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__23_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_StaticFields, ___U3CU3E9__24_0_2)); }
	inline Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * get_U3CU3E9__24_0_2() const { return ___U3CU3E9__24_0_2; }
	inline Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E ** get_address_of_U3CU3E9__24_0_2() { return &___U3CU3E9__24_0_2; }
	inline void set_U3CU3E9__24_0_2(Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * value)
	{
		___U3CU3E9__24_0_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__24_0_2), (void*)value);
	}
};


// Firebase.Auth.FirebaseAuth_<>c__DisplayClass19_0
struct  U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8  : public RuntimeObject
{
public:
	// Firebase.Auth.FirebaseAuth Firebase.Auth.FirebaseAuth_<>c__DisplayClass19_0::auth
	FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * ___auth_0;
	// Firebase.FirebaseApp Firebase.Auth.FirebaseAuth_<>c__DisplayClass19_0::app
	FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * ___app_1;

public:
	inline static int32_t get_offset_of_auth_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8, ___auth_0)); }
	inline FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * get_auth_0() const { return ___auth_0; }
	inline FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D ** get_address_of_auth_0() { return &___auth_0; }
	inline void set_auth_0(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * value)
	{
		___auth_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___auth_0), (void*)value);
	}

	inline static int32_t get_offset_of_app_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8, ___app_1)); }
	inline FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * get_app_1() const { return ___app_1; }
	inline FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 ** get_address_of_app_1() { return &___app_1; }
	inline void set_app_1(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * value)
	{
		___app_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___app_1), (void*)value);
	}
};


// Firebase.Auth.FirebaseAuth_<>c__DisplayClass22_0
struct  U3CU3Ec__DisplayClass22_0_t0B9E0A71713E1E0BCBE5C32D0A17C7786CF35818  : public RuntimeObject
{
public:
	// System.Action`1<Firebase.Auth.FirebaseAuth> Firebase.Auth.FirebaseAuth_<>c__DisplayClass22_0::stateChangeClosure
	Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * ___stateChangeClosure_0;

public:
	inline static int32_t get_offset_of_stateChangeClosure_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t0B9E0A71713E1E0BCBE5C32D0A17C7786CF35818, ___stateChangeClosure_0)); }
	inline Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * get_stateChangeClosure_0() const { return ___stateChangeClosure_0; }
	inline Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E ** get_address_of_stateChangeClosure_0() { return &___stateChangeClosure_0; }
	inline void set_stateChangeClosure_0(Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * value)
	{
		___stateChangeClosure_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stateChangeClosure_0), (void*)value);
	}
};


// Firebase.Auth.FirebaseAuth_<>c__DisplayClass22_1
struct  U3CU3Ec__DisplayClass22_1_tE096CD1670F7DFF2DC0A9E52B629F80ED0B6ED1B  : public RuntimeObject
{
public:
	// Firebase.Auth.FirebaseAuth Firebase.Auth.FirebaseAuth_<>c__DisplayClass22_1::auth
	FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * ___auth_0;
	// Firebase.Auth.FirebaseAuth_<>c__DisplayClass22_0 Firebase.Auth.FirebaseAuth_<>c__DisplayClass22_1::CSU24<>8__locals1
	U3CU3Ec__DisplayClass22_0_t0B9E0A71713E1E0BCBE5C32D0A17C7786CF35818 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_auth_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_1_tE096CD1670F7DFF2DC0A9E52B629F80ED0B6ED1B, ___auth_0)); }
	inline FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * get_auth_0() const { return ___auth_0; }
	inline FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D ** get_address_of_auth_0() { return &___auth_0; }
	inline void set_auth_0(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * value)
	{
		___auth_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___auth_0), (void*)value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_1_tE096CD1670F7DFF2DC0A9E52B629F80ED0B6ED1B, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass22_0_t0B9E0A71713E1E0BCBE5C32D0A17C7786CF35818 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass22_0_t0B9E0A71713E1E0BCBE5C32D0A17C7786CF35818 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass22_0_t0B9E0A71713E1E0BCBE5C32D0A17C7786CF35818 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CSU24U3CU3E8__locals1_1), (void*)value);
	}
};


// Firebase.Auth.FirebaseAuth_<>c__DisplayClass48_0
struct  U3CU3Ec__DisplayClass48_0_t0332496AFB260910C4258BB3F40976B2A20A0312  : public RuntimeObject
{
public:
	// Firebase.Auth.FirebaseAuth Firebase.Auth.FirebaseAuth_<>c__DisplayClass48_0::<>4__this
	FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * ___U3CU3E4__this_0;
	// System.Threading.Tasks.TaskCompletionSource`1<Firebase.Auth.FirebaseUser> Firebase.Auth.FirebaseAuth_<>c__DisplayClass48_0::taskCompletionSource
	TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * ___taskCompletionSource_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass48_0_t0332496AFB260910C4258BB3F40976B2A20A0312, ___U3CU3E4__this_0)); }
	inline FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_taskCompletionSource_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass48_0_t0332496AFB260910C4258BB3F40976B2A20A0312, ___taskCompletionSource_1)); }
	inline TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * get_taskCompletionSource_1() const { return ___taskCompletionSource_1; }
	inline TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED ** get_address_of_taskCompletionSource_1() { return &___taskCompletionSource_1; }
	inline void set_taskCompletionSource_1(TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * value)
	{
		___taskCompletionSource_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___taskCompletionSource_1), (void*)value);
	}
};


// Firebase.Auth.FirebaseAuth_<>c__DisplayClass50_0
struct  U3CU3Ec__DisplayClass50_0_t3165B97C90B070A7EAE8BAE6F1E43B1CFD2F46C2  : public RuntimeObject
{
public:
	// Firebase.Auth.FirebaseAuth Firebase.Auth.FirebaseAuth_<>c__DisplayClass50_0::<>4__this
	FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * ___U3CU3E4__this_0;
	// System.Threading.Tasks.TaskCompletionSource`1<Firebase.Auth.FirebaseUser> Firebase.Auth.FirebaseAuth_<>c__DisplayClass50_0::taskCompletionSource
	TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * ___taskCompletionSource_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass50_0_t3165B97C90B070A7EAE8BAE6F1E43B1CFD2F46C2, ___U3CU3E4__this_0)); }
	inline FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_taskCompletionSource_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass50_0_t3165B97C90B070A7EAE8BAE6F1E43B1CFD2F46C2, ___taskCompletionSource_1)); }
	inline TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * get_taskCompletionSource_1() const { return ___taskCompletionSource_1; }
	inline TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED ** get_address_of_taskCompletionSource_1() { return &___taskCompletionSource_1; }
	inline void set_taskCompletionSource_1(TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * value)
	{
		___taskCompletionSource_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___taskCompletionSource_1), (void*)value);
	}
};


// Firebase.Auth.Future_User_<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t89802218D77EADC8D4FDB0DD3CF8FC5447E97B8D  : public RuntimeObject
{
public:
	// Firebase.Auth.Future_User Firebase.Auth.Future_User_<>c__DisplayClass4_0::fu
	Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * ___fu_0;
	// System.Threading.Tasks.TaskCompletionSource`1<Firebase.Auth.FirebaseUser> Firebase.Auth.Future_User_<>c__DisplayClass4_0::tcs
	TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * ___tcs_1;

public:
	inline static int32_t get_offset_of_fu_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t89802218D77EADC8D4FDB0DD3CF8FC5447E97B8D, ___fu_0)); }
	inline Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * get_fu_0() const { return ___fu_0; }
	inline Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 ** get_address_of_fu_0() { return &___fu_0; }
	inline void set_fu_0(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * value)
	{
		___fu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fu_0), (void*)value);
	}

	inline static int32_t get_offset_of_tcs_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t89802218D77EADC8D4FDB0DD3CF8FC5447E97B8D, ___tcs_1)); }
	inline TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * get_tcs_1() const { return ___tcs_1; }
	inline TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED ** get_address_of_tcs_1() { return &___tcs_1; }
	inline void set_tcs_1(TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * value)
	{
		___tcs_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tcs_1), (void*)value);
	}
};


// Firebase.Auth.PlayGamesAuthProvider
struct  PlayGamesAuthProvider_t68590058994858EAA3037ECD3E0CB2264C369324  : public RuntimeObject
{
public:

public:
};


// Firebase.Auth.UserInfoInterfaceList_UserInfoInterfaceListEnumerator
struct  UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A  : public RuntimeObject
{
public:
	// Firebase.Auth.UserInfoInterfaceList Firebase.Auth.UserInfoInterfaceList_UserInfoInterfaceListEnumerator::collectionRef
	UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * ___collectionRef_0;
	// System.Int32 Firebase.Auth.UserInfoInterfaceList_UserInfoInterfaceListEnumerator::currentIndex
	int32_t ___currentIndex_1;
	// System.Object Firebase.Auth.UserInfoInterfaceList_UserInfoInterfaceListEnumerator::currentObject
	RuntimeObject * ___currentObject_2;
	// System.Int32 Firebase.Auth.UserInfoInterfaceList_UserInfoInterfaceListEnumerator::currentSize
	int32_t ___currentSize_3;

public:
	inline static int32_t get_offset_of_collectionRef_0() { return static_cast<int32_t>(offsetof(UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A, ___collectionRef_0)); }
	inline UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * get_collectionRef_0() const { return ___collectionRef_0; }
	inline UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 ** get_address_of_collectionRef_0() { return &___collectionRef_0; }
	inline void set_collectionRef_0(UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * value)
	{
		___collectionRef_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___collectionRef_0), (void*)value);
	}

	inline static int32_t get_offset_of_currentIndex_1() { return static_cast<int32_t>(offsetof(UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A, ___currentIndex_1)); }
	inline int32_t get_currentIndex_1() const { return ___currentIndex_1; }
	inline int32_t* get_address_of_currentIndex_1() { return &___currentIndex_1; }
	inline void set_currentIndex_1(int32_t value)
	{
		___currentIndex_1 = value;
	}

	inline static int32_t get_offset_of_currentObject_2() { return static_cast<int32_t>(offsetof(UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A, ___currentObject_2)); }
	inline RuntimeObject * get_currentObject_2() const { return ___currentObject_2; }
	inline RuntimeObject ** get_address_of_currentObject_2() { return &___currentObject_2; }
	inline void set_currentObject_2(RuntimeObject * value)
	{
		___currentObject_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentObject_2), (void*)value);
	}

	inline static int32_t get_offset_of_currentSize_3() { return static_cast<int32_t>(offsetof(UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A, ___currentSize_3)); }
	inline int32_t get_currentSize_3() const { return ___currentSize_3; }
	inline int32_t* get_address_of_currentSize_3() { return &___currentSize_3; }
	inline void set_currentSize_3(int32_t value)
	{
		___currentSize_3 = value;
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User_Action>
struct  Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___buckets_0;
	// System.Collections.Generic.Dictionary`2_Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t534F4E28AFD1A9874924380C0D4660E6A0F59204* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2_KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t44B348B2BBD9CCAA412495660D16B78BE78E94D8 * ___keys_7;
	// System.Collections.Generic.Dictionary`2_ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t87FC45B0A7C2D45AB676565AB2B9ED18CC61BB75 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A, ___buckets_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A, ___entries_1)); }
	inline EntryU5BU5D_t534F4E28AFD1A9874924380C0D4660E6A0F59204* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t534F4E28AFD1A9874924380C0D4660E6A0F59204** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t534F4E28AFD1A9874924380C0D4660E6A0F59204* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A, ___keys_7)); }
	inline KeyCollection_t44B348B2BBD9CCAA412495660D16B78BE78E94D8 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t44B348B2BBD9CCAA412495660D16B78BE78E94D8 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t44B348B2BBD9CCAA412495660D16B78BE78E94D8 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A, ___values_8)); }
	inline ValueCollection_t87FC45B0A7C2D45AB676565AB2B9ED18CC61BB75 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t87FC45B0A7C2D45AB676565AB2B9ED18CC61BB75 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t87FC45B0A7C2D45AB676565AB2B9ED18CC61BB75 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.Dictionary`2<System.IntPtr,Firebase.Auth.FirebaseAuth>
struct  Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___buckets_0;
	// System.Collections.Generic.Dictionary`2_Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t6CC77CA77E8558E20CB600BF84F17A706A0D4E36* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2_KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_tA950924F9FED14F66234F7CA5BDF129C6A1B0815 * ___keys_7;
	// System.Collections.Generic.Dictionary`2_ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_tFF4B293126CA06BB0F3B773C375AFCDB57CFFC25 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D, ___buckets_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D, ___entries_1)); }
	inline EntryU5BU5D_t6CC77CA77E8558E20CB600BF84F17A706A0D4E36* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t6CC77CA77E8558E20CB600BF84F17A706A0D4E36** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t6CC77CA77E8558E20CB600BF84F17A706A0D4E36* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D, ___keys_7)); }
	inline KeyCollection_tA950924F9FED14F66234F7CA5BDF129C6A1B0815 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_tA950924F9FED14F66234F7CA5BDF129C6A1B0815 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_tA950924F9FED14F66234F7CA5BDF129C6A1B0815 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D, ___values_8)); }
	inline ValueCollection_tFF4B293126CA06BB0F3B773C375AFCDB57CFFC25 * get_values_8() const { return ___values_8; }
	inline ValueCollection_tFF4B293126CA06BB0F3B773C375AFCDB57CFFC25 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_tFF4B293126CA06BB0F3B773C375AFCDB57CFFC25 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.List`1<Firebase.Auth.IUserInfo>
struct  List_1_tDAC656203301889991AA49AC3FC48456D0214689  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	IUserInfoU5BU5D_tE61C9224BA7F49769BA6D01A174F32C4955EE4D2* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tDAC656203301889991AA49AC3FC48456D0214689, ____items_1)); }
	inline IUserInfoU5BU5D_tE61C9224BA7F49769BA6D01A174F32C4955EE4D2* get__items_1() const { return ____items_1; }
	inline IUserInfoU5BU5D_tE61C9224BA7F49769BA6D01A174F32C4955EE4D2** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(IUserInfoU5BU5D_tE61C9224BA7F49769BA6D01A174F32C4955EE4D2* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tDAC656203301889991AA49AC3FC48456D0214689, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tDAC656203301889991AA49AC3FC48456D0214689, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tDAC656203301889991AA49AC3FC48456D0214689, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tDAC656203301889991AA49AC3FC48456D0214689_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	IUserInfoU5BU5D_tE61C9224BA7F49769BA6D01A174F32C4955EE4D2* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tDAC656203301889991AA49AC3FC48456D0214689_StaticFields, ____emptyArray_5)); }
	inline IUserInfoU5BU5D_tE61C9224BA7F49769BA6D01A174F32C4955EE4D2* get__emptyArray_5() const { return ____emptyArray_5; }
	inline IUserInfoU5BU5D_tE61C9224BA7F49769BA6D01A174F32C4955EE4D2** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(IUserInfoU5BU5D_tE61C9224BA7F49769BA6D01A174F32C4955EE4D2* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_0), (void*)value);
	}
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.Threading.Tasks.TaskCompletionSource`1<Firebase.Auth.FirebaseUser>
struct  TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED  : public RuntimeObject
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Threading.Tasks.TaskCompletionSource`1::m_task
	Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED, ___m_task_0)); }
	inline Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_task_0), (void*)value);
	}
};


// System.Threading.Tasks.TaskCompletionSource`1<System.Object>
struct  TaskCompletionSource_1_t3ECF7B56E418D2A3FFC4E551B9DA6A7E38C7D304  : public RuntimeObject
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Threading.Tasks.TaskCompletionSource`1::m_task
	Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskCompletionSource_1_t3ECF7B56E418D2A3FFC4E551B9DA6A7E38C7D304, ___m_task_0)); }
	inline Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_task_0), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.UInt32
struct  UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// Firebase.FutureStatus
struct  FutureStatus_t5123D2217054CACFA9365BD542C5A3300449255C 
{
public:
	// System.Int32 Firebase.FutureStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FutureStatus_t5123D2217054CACFA9365BD542C5A3300449255C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Firebase.InitResult
struct  InitResult_t0159C076DA44626ECA5EA139DB147F9BFE9AB31F 
{
public:
	// System.Int32 Firebase.InitResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InitResult_t0159C076DA44626ECA5EA139DB147F9BFE9AB31F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Firebase.LogLevel
struct  LogLevel_tC69575EF4D1F372E563D26D15A24B00EA684B4F8 
{
public:
	// System.Int32 Firebase.LogLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogLevel_tC69575EF4D1F372E563D26D15A24B00EA684B4F8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.Runtime.InteropServices.HandleRef
struct  HandleRef_t876E76124F400D12395BF61D562162AB6822204A 
{
public:
	// System.Object System.Runtime.InteropServices.HandleRef::m_wrapper
	RuntimeObject * ___m_wrapper_0;
	// System.IntPtr System.Runtime.InteropServices.HandleRef::m_handle
	intptr_t ___m_handle_1;

public:
	inline static int32_t get_offset_of_m_wrapper_0() { return static_cast<int32_t>(offsetof(HandleRef_t876E76124F400D12395BF61D562162AB6822204A, ___m_wrapper_0)); }
	inline RuntimeObject * get_m_wrapper_0() const { return ___m_wrapper_0; }
	inline RuntimeObject ** get_address_of_m_wrapper_0() { return &___m_wrapper_0; }
	inline void set_m_wrapper_0(RuntimeObject * value)
	{
		___m_wrapper_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_wrapper_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_handle_1() { return static_cast<int32_t>(offsetof(HandleRef_t876E76124F400D12395BF61D562162AB6822204A, ___m_handle_1)); }
	inline intptr_t get_m_handle_1() const { return ___m_handle_1; }
	inline intptr_t* get_address_of_m_handle_1() { return &___m_handle_1; }
	inline void set_m_handle_1(intptr_t value)
	{
		___m_handle_1 = value;
	}
};


// System.Threading.Tasks.Task
struct  Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2  : public RuntimeObject
{
public:
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_taskId
	int32_t ___m_taskId_4;
	// System.Object System.Threading.Tasks.Task::m_action
	RuntimeObject * ___m_action_5;
	// System.Object System.Threading.Tasks.Task::m_stateObject
	RuntimeObject * ___m_stateObject_6;
	// System.Threading.Tasks.TaskScheduler System.Threading.Tasks.Task::m_taskScheduler
	TaskScheduler_t966F2798F198FA90A0DA8EFC92BAC08297793114 * ___m_taskScheduler_7;
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::m_parent
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_parent_8;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_stateFlags
	int32_t ___m_stateFlags_9;
	// System.Object modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_continuationObject
	RuntimeObject * ___m_continuationObject_10;
	// System.Threading.Tasks.Task_ContingentProperties modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Tasks.Task::m_contingentProperties
	ContingentProperties_t7149A27D01507C74E8BDAAA3848B45D2644FDF08 * ___m_contingentProperties_15;

public:
	inline static int32_t get_offset_of_m_taskId_4() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2, ___m_taskId_4)); }
	inline int32_t get_m_taskId_4() const { return ___m_taskId_4; }
	inline int32_t* get_address_of_m_taskId_4() { return &___m_taskId_4; }
	inline void set_m_taskId_4(int32_t value)
	{
		___m_taskId_4 = value;
	}

	inline static int32_t get_offset_of_m_action_5() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2, ___m_action_5)); }
	inline RuntimeObject * get_m_action_5() const { return ___m_action_5; }
	inline RuntimeObject ** get_address_of_m_action_5() { return &___m_action_5; }
	inline void set_m_action_5(RuntimeObject * value)
	{
		___m_action_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_action_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_stateObject_6() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2, ___m_stateObject_6)); }
	inline RuntimeObject * get_m_stateObject_6() const { return ___m_stateObject_6; }
	inline RuntimeObject ** get_address_of_m_stateObject_6() { return &___m_stateObject_6; }
	inline void set_m_stateObject_6(RuntimeObject * value)
	{
		___m_stateObject_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_stateObject_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_taskScheduler_7() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2, ___m_taskScheduler_7)); }
	inline TaskScheduler_t966F2798F198FA90A0DA8EFC92BAC08297793114 * get_m_taskScheduler_7() const { return ___m_taskScheduler_7; }
	inline TaskScheduler_t966F2798F198FA90A0DA8EFC92BAC08297793114 ** get_address_of_m_taskScheduler_7() { return &___m_taskScheduler_7; }
	inline void set_m_taskScheduler_7(TaskScheduler_t966F2798F198FA90A0DA8EFC92BAC08297793114 * value)
	{
		___m_taskScheduler_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_taskScheduler_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_parent_8() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2, ___m_parent_8)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_m_parent_8() const { return ___m_parent_8; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_m_parent_8() { return &___m_parent_8; }
	inline void set_m_parent_8(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___m_parent_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_parent_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_stateFlags_9() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2, ___m_stateFlags_9)); }
	inline int32_t get_m_stateFlags_9() const { return ___m_stateFlags_9; }
	inline int32_t* get_address_of_m_stateFlags_9() { return &___m_stateFlags_9; }
	inline void set_m_stateFlags_9(int32_t value)
	{
		___m_stateFlags_9 = value;
	}

	inline static int32_t get_offset_of_m_continuationObject_10() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2, ___m_continuationObject_10)); }
	inline RuntimeObject * get_m_continuationObject_10() const { return ___m_continuationObject_10; }
	inline RuntimeObject ** get_address_of_m_continuationObject_10() { return &___m_continuationObject_10; }
	inline void set_m_continuationObject_10(RuntimeObject * value)
	{
		___m_continuationObject_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_continuationObject_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_contingentProperties_15() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2, ___m_contingentProperties_15)); }
	inline ContingentProperties_t7149A27D01507C74E8BDAAA3848B45D2644FDF08 * get_m_contingentProperties_15() const { return ___m_contingentProperties_15; }
	inline ContingentProperties_t7149A27D01507C74E8BDAAA3848B45D2644FDF08 ** get_address_of_m_contingentProperties_15() { return &___m_contingentProperties_15; }
	inline void set_m_contingentProperties_15(ContingentProperties_t7149A27D01507C74E8BDAAA3848B45D2644FDF08 * value)
	{
		___m_contingentProperties_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_contingentProperties_15), (void*)value);
	}
};

struct Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields
{
public:
	// System.Int32 System.Threading.Tasks.Task::s_taskIdCounter
	int32_t ___s_taskIdCounter_2;
	// System.Threading.Tasks.TaskFactory System.Threading.Tasks.Task::s_factory
	TaskFactory_tF3C6D983390ACFB40B4979E225368F78006D6155 * ___s_factory_3;
	// System.Object System.Threading.Tasks.Task::s_taskCompletionSentinel
	RuntimeObject * ___s_taskCompletionSentinel_11;
	// System.Boolean System.Threading.Tasks.Task::s_asyncDebuggingEnabled
	bool ___s_asyncDebuggingEnabled_12;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Threading.Tasks.Task> System.Threading.Tasks.Task::s_currentActiveTasks
	Dictionary_2_t70161CFEB8DA3C79E19E31D0ED948D3C2925095F * ___s_currentActiveTasks_13;
	// System.Object System.Threading.Tasks.Task::s_activeTasksLock
	RuntimeObject * ___s_activeTasksLock_14;
	// System.Action`1<System.Object> System.Threading.Tasks.Task::s_taskCancelCallback
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___s_taskCancelCallback_16;
	// System.Func`1<System.Threading.Tasks.Task_ContingentProperties> System.Threading.Tasks.Task::s_createContingentProperties
	Func_1_t48C2978A48CE3F2F6EB5B6DE269D00746483BB1F * ___s_createContingentProperties_17;
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::s_completedTask
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___s_completedTask_18;
	// System.Predicate`1<System.Threading.Tasks.Task> System.Threading.Tasks.Task::s_IsExceptionObservedByParentPredicate
	Predicate_1_tF4286C34BB184CE5690FDCEBA7F09FC68D229335 * ___s_IsExceptionObservedByParentPredicate_19;
	// System.Threading.ContextCallback System.Threading.Tasks.Task::s_ecCallback
	ContextCallback_t8AE8A965AC6C7ECD396F527F15CDC8E683BE1676 * ___s_ecCallback_20;
	// System.Predicate`1<System.Object> System.Threading.Tasks.Task::s_IsTaskContinuationNullPredicate
	Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * ___s_IsTaskContinuationNullPredicate_21;

public:
	inline static int32_t get_offset_of_s_taskIdCounter_2() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_taskIdCounter_2)); }
	inline int32_t get_s_taskIdCounter_2() const { return ___s_taskIdCounter_2; }
	inline int32_t* get_address_of_s_taskIdCounter_2() { return &___s_taskIdCounter_2; }
	inline void set_s_taskIdCounter_2(int32_t value)
	{
		___s_taskIdCounter_2 = value;
	}

	inline static int32_t get_offset_of_s_factory_3() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_factory_3)); }
	inline TaskFactory_tF3C6D983390ACFB40B4979E225368F78006D6155 * get_s_factory_3() const { return ___s_factory_3; }
	inline TaskFactory_tF3C6D983390ACFB40B4979E225368F78006D6155 ** get_address_of_s_factory_3() { return &___s_factory_3; }
	inline void set_s_factory_3(TaskFactory_tF3C6D983390ACFB40B4979E225368F78006D6155 * value)
	{
		___s_factory_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_factory_3), (void*)value);
	}

	inline static int32_t get_offset_of_s_taskCompletionSentinel_11() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_taskCompletionSentinel_11)); }
	inline RuntimeObject * get_s_taskCompletionSentinel_11() const { return ___s_taskCompletionSentinel_11; }
	inline RuntimeObject ** get_address_of_s_taskCompletionSentinel_11() { return &___s_taskCompletionSentinel_11; }
	inline void set_s_taskCompletionSentinel_11(RuntimeObject * value)
	{
		___s_taskCompletionSentinel_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_taskCompletionSentinel_11), (void*)value);
	}

	inline static int32_t get_offset_of_s_asyncDebuggingEnabled_12() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_asyncDebuggingEnabled_12)); }
	inline bool get_s_asyncDebuggingEnabled_12() const { return ___s_asyncDebuggingEnabled_12; }
	inline bool* get_address_of_s_asyncDebuggingEnabled_12() { return &___s_asyncDebuggingEnabled_12; }
	inline void set_s_asyncDebuggingEnabled_12(bool value)
	{
		___s_asyncDebuggingEnabled_12 = value;
	}

	inline static int32_t get_offset_of_s_currentActiveTasks_13() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_currentActiveTasks_13)); }
	inline Dictionary_2_t70161CFEB8DA3C79E19E31D0ED948D3C2925095F * get_s_currentActiveTasks_13() const { return ___s_currentActiveTasks_13; }
	inline Dictionary_2_t70161CFEB8DA3C79E19E31D0ED948D3C2925095F ** get_address_of_s_currentActiveTasks_13() { return &___s_currentActiveTasks_13; }
	inline void set_s_currentActiveTasks_13(Dictionary_2_t70161CFEB8DA3C79E19E31D0ED948D3C2925095F * value)
	{
		___s_currentActiveTasks_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_currentActiveTasks_13), (void*)value);
	}

	inline static int32_t get_offset_of_s_activeTasksLock_14() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_activeTasksLock_14)); }
	inline RuntimeObject * get_s_activeTasksLock_14() const { return ___s_activeTasksLock_14; }
	inline RuntimeObject ** get_address_of_s_activeTasksLock_14() { return &___s_activeTasksLock_14; }
	inline void set_s_activeTasksLock_14(RuntimeObject * value)
	{
		___s_activeTasksLock_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_activeTasksLock_14), (void*)value);
	}

	inline static int32_t get_offset_of_s_taskCancelCallback_16() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_taskCancelCallback_16)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get_s_taskCancelCallback_16() const { return ___s_taskCancelCallback_16; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of_s_taskCancelCallback_16() { return &___s_taskCancelCallback_16; }
	inline void set_s_taskCancelCallback_16(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		___s_taskCancelCallback_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_taskCancelCallback_16), (void*)value);
	}

	inline static int32_t get_offset_of_s_createContingentProperties_17() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_createContingentProperties_17)); }
	inline Func_1_t48C2978A48CE3F2F6EB5B6DE269D00746483BB1F * get_s_createContingentProperties_17() const { return ___s_createContingentProperties_17; }
	inline Func_1_t48C2978A48CE3F2F6EB5B6DE269D00746483BB1F ** get_address_of_s_createContingentProperties_17() { return &___s_createContingentProperties_17; }
	inline void set_s_createContingentProperties_17(Func_1_t48C2978A48CE3F2F6EB5B6DE269D00746483BB1F * value)
	{
		___s_createContingentProperties_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_createContingentProperties_17), (void*)value);
	}

	inline static int32_t get_offset_of_s_completedTask_18() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_completedTask_18)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_s_completedTask_18() const { return ___s_completedTask_18; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_s_completedTask_18() { return &___s_completedTask_18; }
	inline void set_s_completedTask_18(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___s_completedTask_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_completedTask_18), (void*)value);
	}

	inline static int32_t get_offset_of_s_IsExceptionObservedByParentPredicate_19() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_IsExceptionObservedByParentPredicate_19)); }
	inline Predicate_1_tF4286C34BB184CE5690FDCEBA7F09FC68D229335 * get_s_IsExceptionObservedByParentPredicate_19() const { return ___s_IsExceptionObservedByParentPredicate_19; }
	inline Predicate_1_tF4286C34BB184CE5690FDCEBA7F09FC68D229335 ** get_address_of_s_IsExceptionObservedByParentPredicate_19() { return &___s_IsExceptionObservedByParentPredicate_19; }
	inline void set_s_IsExceptionObservedByParentPredicate_19(Predicate_1_tF4286C34BB184CE5690FDCEBA7F09FC68D229335 * value)
	{
		___s_IsExceptionObservedByParentPredicate_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_IsExceptionObservedByParentPredicate_19), (void*)value);
	}

	inline static int32_t get_offset_of_s_ecCallback_20() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_ecCallback_20)); }
	inline ContextCallback_t8AE8A965AC6C7ECD396F527F15CDC8E683BE1676 * get_s_ecCallback_20() const { return ___s_ecCallback_20; }
	inline ContextCallback_t8AE8A965AC6C7ECD396F527F15CDC8E683BE1676 ** get_address_of_s_ecCallback_20() { return &___s_ecCallback_20; }
	inline void set_s_ecCallback_20(ContextCallback_t8AE8A965AC6C7ECD396F527F15CDC8E683BE1676 * value)
	{
		___s_ecCallback_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ecCallback_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_IsTaskContinuationNullPredicate_21() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_StaticFields, ___s_IsTaskContinuationNullPredicate_21)); }
	inline Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * get_s_IsTaskContinuationNullPredicate_21() const { return ___s_IsTaskContinuationNullPredicate_21; }
	inline Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 ** get_address_of_s_IsTaskContinuationNullPredicate_21() { return &___s_IsTaskContinuationNullPredicate_21; }
	inline void set_s_IsTaskContinuationNullPredicate_21(Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * value)
	{
		___s_IsTaskContinuationNullPredicate_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_IsTaskContinuationNullPredicate_21), (void*)value);
	}
};

struct Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_ThreadStaticFields
{
public:
	// System.Threading.Tasks.Task System.Threading.Tasks.Task::t_currentTask
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___t_currentTask_0;
	// System.Threading.Tasks.StackGuard System.Threading.Tasks.Task::t_stackGuard
	StackGuard_tE431ED3BBD1A18705FEE6F948EBF7FA2E99D64A9 * ___t_stackGuard_1;

public:
	inline static int32_t get_offset_of_t_currentTask_0() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_ThreadStaticFields, ___t_currentTask_0)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_t_currentTask_0() const { return ___t_currentTask_0; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_t_currentTask_0() { return &___t_currentTask_0; }
	inline void set_t_currentTask_0(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___t_currentTask_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_currentTask_0), (void*)value);
	}

	inline static int32_t get_offset_of_t_stackGuard_1() { return static_cast<int32_t>(offsetof(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2_ThreadStaticFields, ___t_stackGuard_1)); }
	inline StackGuard_tE431ED3BBD1A18705FEE6F948EBF7FA2E99D64A9 * get_t_stackGuard_1() const { return ___t_stackGuard_1; }
	inline StackGuard_tE431ED3BBD1A18705FEE6F948EBF7FA2E99D64A9 ** get_address_of_t_stackGuard_1() { return &___t_stackGuard_1; }
	inline void set_t_stackGuard_1(StackGuard_tE431ED3BBD1A18705FEE6F948EBF7FA2E99D64A9 * value)
	{
		___t_stackGuard_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_stackGuard_1), (void*)value);
	}
};


// System.Uri_Flags
struct  Flags_tEBE7CABEBD13F16920D6950B384EB8F988250A2A 
{
public:
	// System.UInt64 System.Uri_Flags::value__
	uint64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Flags_tEBE7CABEBD13F16920D6950B384EB8F988250A2A, ___value___2)); }
	inline uint64_t get_value___2() const { return ___value___2; }
	inline uint64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint64_t value)
	{
		___value___2 = value;
	}
};


// System.UriFormat
struct  UriFormat_t4355763D39FF6F0FAA2B43E3A209BA8500730992 
{
public:
	// System.Int32 System.UriFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriFormat_t4355763D39FF6F0FAA2B43E3A209BA8500730992, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.UriIdnScope
struct  UriIdnScope_tE1574B39C7492C761EFE2FC12DDE82DE013AC9D1 
{
public:
	// System.Int32 System.UriIdnScope::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriIdnScope_tE1574B39C7492C761EFE2FC12DDE82DE013AC9D1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.UriKind
struct  UriKind_t26D0760DDF148ADC939FECD934C0B9FF5C71EA08 
{
public:
	// System.Int32 System.UriKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriKind_t26D0760DDF148ADC939FECD934C0B9FF5C71EA08, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Firebase.Auth.Credential
struct  Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.Auth.Credential::swigCPtr
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___swigCPtr_0;
	// System.Boolean Firebase.Auth.Credential::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630, ___swigCPtr_0)); }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t876E76124F400D12395BF61D562162AB6822204A  value)
	{
		___swigCPtr_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___swigCPtr_0))->___m_wrapper_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};


// Firebase.Auth.FirebaseAuth
struct  FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.Auth.FirebaseAuth::swigCPtr
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___swigCPtr_0;
	// System.Boolean Firebase.Auth.FirebaseAuth::swigCMemOwn
	bool ___swigCMemOwn_1;
	// Firebase.FirebaseApp Firebase.Auth.FirebaseAuth::appProxy
	FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * ___appProxy_2;
	// System.IntPtr Firebase.Auth.FirebaseAuth::appCPtr
	intptr_t ___appCPtr_3;
	// System.IntPtr Firebase.Auth.FirebaseAuth::authStateListener
	intptr_t ___authStateListener_4;
	// System.IntPtr Firebase.Auth.FirebaseAuth::idTokenListener
	intptr_t ___idTokenListener_5;
	// Firebase.Auth.FirebaseUser Firebase.Auth.FirebaseAuth::currentUser
	FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * ___currentUser_6;
	// System.EventHandler Firebase.Auth.FirebaseAuth::stateChangedImpl
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___stateChangedImpl_7;
	// System.EventHandler Firebase.Auth.FirebaseAuth::idTokenChangedImpl
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___idTokenChangedImpl_8;
	// System.Boolean Firebase.Auth.FirebaseAuth::persistentLoaded
	bool ___persistentLoaded_9;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D, ___swigCPtr_0)); }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t876E76124F400D12395BF61D562162AB6822204A  value)
	{
		___swigCPtr_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___swigCPtr_0))->___m_wrapper_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}

	inline static int32_t get_offset_of_appProxy_2() { return static_cast<int32_t>(offsetof(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D, ___appProxy_2)); }
	inline FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * get_appProxy_2() const { return ___appProxy_2; }
	inline FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 ** get_address_of_appProxy_2() { return &___appProxy_2; }
	inline void set_appProxy_2(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * value)
	{
		___appProxy_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___appProxy_2), (void*)value);
	}

	inline static int32_t get_offset_of_appCPtr_3() { return static_cast<int32_t>(offsetof(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D, ___appCPtr_3)); }
	inline intptr_t get_appCPtr_3() const { return ___appCPtr_3; }
	inline intptr_t* get_address_of_appCPtr_3() { return &___appCPtr_3; }
	inline void set_appCPtr_3(intptr_t value)
	{
		___appCPtr_3 = value;
	}

	inline static int32_t get_offset_of_authStateListener_4() { return static_cast<int32_t>(offsetof(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D, ___authStateListener_4)); }
	inline intptr_t get_authStateListener_4() const { return ___authStateListener_4; }
	inline intptr_t* get_address_of_authStateListener_4() { return &___authStateListener_4; }
	inline void set_authStateListener_4(intptr_t value)
	{
		___authStateListener_4 = value;
	}

	inline static int32_t get_offset_of_idTokenListener_5() { return static_cast<int32_t>(offsetof(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D, ___idTokenListener_5)); }
	inline intptr_t get_idTokenListener_5() const { return ___idTokenListener_5; }
	inline intptr_t* get_address_of_idTokenListener_5() { return &___idTokenListener_5; }
	inline void set_idTokenListener_5(intptr_t value)
	{
		___idTokenListener_5 = value;
	}

	inline static int32_t get_offset_of_currentUser_6() { return static_cast<int32_t>(offsetof(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D, ___currentUser_6)); }
	inline FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * get_currentUser_6() const { return ___currentUser_6; }
	inline FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD ** get_address_of_currentUser_6() { return &___currentUser_6; }
	inline void set_currentUser_6(FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * value)
	{
		___currentUser_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentUser_6), (void*)value);
	}

	inline static int32_t get_offset_of_stateChangedImpl_7() { return static_cast<int32_t>(offsetof(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D, ___stateChangedImpl_7)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_stateChangedImpl_7() const { return ___stateChangedImpl_7; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_stateChangedImpl_7() { return &___stateChangedImpl_7; }
	inline void set_stateChangedImpl_7(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___stateChangedImpl_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stateChangedImpl_7), (void*)value);
	}

	inline static int32_t get_offset_of_idTokenChangedImpl_8() { return static_cast<int32_t>(offsetof(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D, ___idTokenChangedImpl_8)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_idTokenChangedImpl_8() const { return ___idTokenChangedImpl_8; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_idTokenChangedImpl_8() { return &___idTokenChangedImpl_8; }
	inline void set_idTokenChangedImpl_8(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___idTokenChangedImpl_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___idTokenChangedImpl_8), (void*)value);
	}

	inline static int32_t get_offset_of_persistentLoaded_9() { return static_cast<int32_t>(offsetof(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D, ___persistentLoaded_9)); }
	inline bool get_persistentLoaded_9() const { return ___persistentLoaded_9; }
	inline bool* get_address_of_persistentLoaded_9() { return &___persistentLoaded_9; }
	inline void set_persistentLoaded_9(bool value)
	{
		___persistentLoaded_9 = value;
	}
};

struct FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.IntPtr,Firebase.Auth.FirebaseAuth> Firebase.Auth.FirebaseAuth::appCPtrToAuth
	Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * ___appCPtrToAuth_10;

public:
	inline static int32_t get_offset_of_appCPtrToAuth_10() { return static_cast<int32_t>(offsetof(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_StaticFields, ___appCPtrToAuth_10)); }
	inline Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * get_appCPtrToAuth_10() const { return ___appCPtrToAuth_10; }
	inline Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D ** get_address_of_appCPtrToAuth_10() { return &___appCPtrToAuth_10; }
	inline void set_appCPtrToAuth_10(Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * value)
	{
		___appCPtrToAuth_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___appCPtrToAuth_10), (void*)value);
	}
};


// Firebase.Auth.FirebaseAuth_<>c__DisplayClass19_1
struct  U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683  : public RuntimeObject
{
public:
	// Firebase.InitResult Firebase.Auth.FirebaseAuth_<>c__DisplayClass19_1::init_result
	int32_t ___init_result_0;
	// Firebase.Auth.FirebaseAuth_<>c__DisplayClass19_0 Firebase.Auth.FirebaseAuth_<>c__DisplayClass19_1::CSU24<>8__locals1
	U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_init_result_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683, ___init_result_0)); }
	inline int32_t get_init_result_0() const { return ___init_result_0; }
	inline int32_t* get_address_of_init_result_0() { return &___init_result_0; }
	inline void set_init_result_0(int32_t value)
	{
		___init_result_0 = value;
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CSU24U3CU3E8__locals1_1), (void*)value);
	}
};


// Firebase.Auth.UserInfoInterface
struct  UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.Auth.UserInfoInterface::swigCPtr
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___swigCPtr_0;
	// System.Boolean Firebase.Auth.UserInfoInterface::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C, ___swigCPtr_0)); }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t876E76124F400D12395BF61D562162AB6822204A  value)
	{
		___swigCPtr_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___swigCPtr_0))->___m_wrapper_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};


// Firebase.Auth.UserInfoInterfaceList
struct  UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.Auth.UserInfoInterfaceList::swigCPtr
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___swigCPtr_0;
	// System.Boolean Firebase.Auth.UserInfoInterfaceList::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422, ___swigCPtr_0)); }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t876E76124F400D12395BF61D562162AB6822204A  value)
	{
		___swigCPtr_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___swigCPtr_0))->___m_wrapper_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};


// Firebase.Auth.UserProfile
struct  UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.Auth.UserProfile::swigCPtr
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___swigCPtr_0;
	// System.Boolean Firebase.Auth.UserProfile::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31, ___swigCPtr_0)); }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t876E76124F400D12395BF61D562162AB6822204A  value)
	{
		___swigCPtr_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___swigCPtr_0))->___m_wrapper_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};


// Firebase.FirebaseApp
struct  FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.FirebaseApp::swigCPtr
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___swigCPtr_0;
	// System.Boolean Firebase.FirebaseApp::swigCMemOwn
	bool ___swigCMemOwn_1;
	// System.String Firebase.FirebaseApp::name
	String_t* ___name_3;
	// System.EventHandler Firebase.FirebaseApp::AppDisposed
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___AppDisposed_4;
	// Firebase.Platform.FirebaseAppPlatform Firebase.FirebaseApp::appPlatform
	FirebaseAppPlatform_tDB2126CA2A03AFD8EA2B368EAB733DE52BFF6D75 * ___appPlatform_15;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986, ___swigCPtr_0)); }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t876E76124F400D12395BF61D562162AB6822204A  value)
	{
		___swigCPtr_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___swigCPtr_0))->___m_wrapper_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_3), (void*)value);
	}

	inline static int32_t get_offset_of_AppDisposed_4() { return static_cast<int32_t>(offsetof(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986, ___AppDisposed_4)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_AppDisposed_4() const { return ___AppDisposed_4; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_AppDisposed_4() { return &___AppDisposed_4; }
	inline void set_AppDisposed_4(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___AppDisposed_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AppDisposed_4), (void*)value);
	}

	inline static int32_t get_offset_of_appPlatform_15() { return static_cast<int32_t>(offsetof(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986, ___appPlatform_15)); }
	inline FirebaseAppPlatform_tDB2126CA2A03AFD8EA2B368EAB733DE52BFF6D75 * get_appPlatform_15() const { return ___appPlatform_15; }
	inline FirebaseAppPlatform_tDB2126CA2A03AFD8EA2B368EAB733DE52BFF6D75 ** get_address_of_appPlatform_15() { return &___appPlatform_15; }
	inline void set_appPlatform_15(FirebaseAppPlatform_tDB2126CA2A03AFD8EA2B368EAB733DE52BFF6D75 * value)
	{
		___appPlatform_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___appPlatform_15), (void*)value);
	}
};

struct FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_StaticFields
{
public:
	// System.Object Firebase.FirebaseApp::disposeLock
	RuntimeObject * ___disposeLock_2;
	// System.Collections.Generic.Dictionary`2<System.String,Firebase.FirebaseApp> Firebase.FirebaseApp::nameToProxy
	Dictionary_2_t2E1F4545167E342F82AFA012A31DD7DD8752B6FE * ___nameToProxy_5;
	// System.Collections.Generic.Dictionary`2<System.IntPtr,Firebase.FirebaseApp> Firebase.FirebaseApp::cPtrToProxy
	Dictionary_2_t16FA637EF27DE97C11197B9DE231BEA419C59A62 * ___cPtrToProxy_6;
	// System.Boolean Firebase.FirebaseApp::AppUtilCallbacksInitialized
	bool ___AppUtilCallbacksInitialized_7;
	// System.Object Firebase.FirebaseApp::AppUtilCallbacksLock
	RuntimeObject * ___AppUtilCallbacksLock_8;
	// System.Boolean Firebase.FirebaseApp::PreventOnAllAppsDestroyed
	bool ___PreventOnAllAppsDestroyed_9;
	// System.Boolean Firebase.FirebaseApp::crashlyticsInitializationAttempted
	bool ___crashlyticsInitializationAttempted_10;
	// System.Int32 Firebase.FirebaseApp::CheckDependenciesThread
	int32_t ___CheckDependenciesThread_13;
	// System.Object Firebase.FirebaseApp::CheckDependenciesThreadLock
	RuntimeObject * ___CheckDependenciesThreadLock_14;

public:
	inline static int32_t get_offset_of_disposeLock_2() { return static_cast<int32_t>(offsetof(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_StaticFields, ___disposeLock_2)); }
	inline RuntimeObject * get_disposeLock_2() const { return ___disposeLock_2; }
	inline RuntimeObject ** get_address_of_disposeLock_2() { return &___disposeLock_2; }
	inline void set_disposeLock_2(RuntimeObject * value)
	{
		___disposeLock_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___disposeLock_2), (void*)value);
	}

	inline static int32_t get_offset_of_nameToProxy_5() { return static_cast<int32_t>(offsetof(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_StaticFields, ___nameToProxy_5)); }
	inline Dictionary_2_t2E1F4545167E342F82AFA012A31DD7DD8752B6FE * get_nameToProxy_5() const { return ___nameToProxy_5; }
	inline Dictionary_2_t2E1F4545167E342F82AFA012A31DD7DD8752B6FE ** get_address_of_nameToProxy_5() { return &___nameToProxy_5; }
	inline void set_nameToProxy_5(Dictionary_2_t2E1F4545167E342F82AFA012A31DD7DD8752B6FE * value)
	{
		___nameToProxy_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nameToProxy_5), (void*)value);
	}

	inline static int32_t get_offset_of_cPtrToProxy_6() { return static_cast<int32_t>(offsetof(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_StaticFields, ___cPtrToProxy_6)); }
	inline Dictionary_2_t16FA637EF27DE97C11197B9DE231BEA419C59A62 * get_cPtrToProxy_6() const { return ___cPtrToProxy_6; }
	inline Dictionary_2_t16FA637EF27DE97C11197B9DE231BEA419C59A62 ** get_address_of_cPtrToProxy_6() { return &___cPtrToProxy_6; }
	inline void set_cPtrToProxy_6(Dictionary_2_t16FA637EF27DE97C11197B9DE231BEA419C59A62 * value)
	{
		___cPtrToProxy_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cPtrToProxy_6), (void*)value);
	}

	inline static int32_t get_offset_of_AppUtilCallbacksInitialized_7() { return static_cast<int32_t>(offsetof(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_StaticFields, ___AppUtilCallbacksInitialized_7)); }
	inline bool get_AppUtilCallbacksInitialized_7() const { return ___AppUtilCallbacksInitialized_7; }
	inline bool* get_address_of_AppUtilCallbacksInitialized_7() { return &___AppUtilCallbacksInitialized_7; }
	inline void set_AppUtilCallbacksInitialized_7(bool value)
	{
		___AppUtilCallbacksInitialized_7 = value;
	}

	inline static int32_t get_offset_of_AppUtilCallbacksLock_8() { return static_cast<int32_t>(offsetof(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_StaticFields, ___AppUtilCallbacksLock_8)); }
	inline RuntimeObject * get_AppUtilCallbacksLock_8() const { return ___AppUtilCallbacksLock_8; }
	inline RuntimeObject ** get_address_of_AppUtilCallbacksLock_8() { return &___AppUtilCallbacksLock_8; }
	inline void set_AppUtilCallbacksLock_8(RuntimeObject * value)
	{
		___AppUtilCallbacksLock_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AppUtilCallbacksLock_8), (void*)value);
	}

	inline static int32_t get_offset_of_PreventOnAllAppsDestroyed_9() { return static_cast<int32_t>(offsetof(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_StaticFields, ___PreventOnAllAppsDestroyed_9)); }
	inline bool get_PreventOnAllAppsDestroyed_9() const { return ___PreventOnAllAppsDestroyed_9; }
	inline bool* get_address_of_PreventOnAllAppsDestroyed_9() { return &___PreventOnAllAppsDestroyed_9; }
	inline void set_PreventOnAllAppsDestroyed_9(bool value)
	{
		___PreventOnAllAppsDestroyed_9 = value;
	}

	inline static int32_t get_offset_of_crashlyticsInitializationAttempted_10() { return static_cast<int32_t>(offsetof(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_StaticFields, ___crashlyticsInitializationAttempted_10)); }
	inline bool get_crashlyticsInitializationAttempted_10() const { return ___crashlyticsInitializationAttempted_10; }
	inline bool* get_address_of_crashlyticsInitializationAttempted_10() { return &___crashlyticsInitializationAttempted_10; }
	inline void set_crashlyticsInitializationAttempted_10(bool value)
	{
		___crashlyticsInitializationAttempted_10 = value;
	}

	inline static int32_t get_offset_of_CheckDependenciesThread_13() { return static_cast<int32_t>(offsetof(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_StaticFields, ___CheckDependenciesThread_13)); }
	inline int32_t get_CheckDependenciesThread_13() const { return ___CheckDependenciesThread_13; }
	inline int32_t* get_address_of_CheckDependenciesThread_13() { return &___CheckDependenciesThread_13; }
	inline void set_CheckDependenciesThread_13(int32_t value)
	{
		___CheckDependenciesThread_13 = value;
	}

	inline static int32_t get_offset_of_CheckDependenciesThreadLock_14() { return static_cast<int32_t>(offsetof(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_StaticFields, ___CheckDependenciesThreadLock_14)); }
	inline RuntimeObject * get_CheckDependenciesThreadLock_14() const { return ___CheckDependenciesThreadLock_14; }
	inline RuntimeObject ** get_address_of_CheckDependenciesThreadLock_14() { return &___CheckDependenciesThreadLock_14; }
	inline void set_CheckDependenciesThreadLock_14(RuntimeObject * value)
	{
		___CheckDependenciesThreadLock_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CheckDependenciesThreadLock_14), (void*)value);
	}
};


// Firebase.FirebaseException
struct  FirebaseException_t947F51F8FDF9CB2D145DC24E92160DC67BF12AEF  : public Exception_t
{
public:
	// System.Int32 Firebase.FirebaseException::<ErrorCode>k__BackingField
	int32_t ___U3CErrorCodeU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_U3CErrorCodeU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(FirebaseException_t947F51F8FDF9CB2D145DC24E92160DC67BF12AEF, ___U3CErrorCodeU3Ek__BackingField_17)); }
	inline int32_t get_U3CErrorCodeU3Ek__BackingField_17() const { return ___U3CErrorCodeU3Ek__BackingField_17; }
	inline int32_t* get_address_of_U3CErrorCodeU3Ek__BackingField_17() { return &___U3CErrorCodeU3Ek__BackingField_17; }
	inline void set_U3CErrorCodeU3Ek__BackingField_17(int32_t value)
	{
		___U3CErrorCodeU3Ek__BackingField_17 = value;
	}
};


// Firebase.FutureBase
struct  FutureBase_t75BACF0EF2911C731F998E85086202A1A2F55397  : public RuntimeObject
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.FutureBase::swigCPtr
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___swigCPtr_0;
	// System.Boolean Firebase.FutureBase::swigCMemOwn
	bool ___swigCMemOwn_1;

public:
	inline static int32_t get_offset_of_swigCPtr_0() { return static_cast<int32_t>(offsetof(FutureBase_t75BACF0EF2911C731F998E85086202A1A2F55397, ___swigCPtr_0)); }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A  get_swigCPtr_0() const { return ___swigCPtr_0; }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A * get_address_of_swigCPtr_0() { return &___swigCPtr_0; }
	inline void set_swigCPtr_0(HandleRef_t876E76124F400D12395BF61D562162AB6822204A  value)
	{
		___swigCPtr_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___swigCPtr_0))->___m_wrapper_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_swigCMemOwn_1() { return static_cast<int32_t>(offsetof(FutureBase_t75BACF0EF2911C731F998E85086202A1A2F55397, ___swigCMemOwn_1)); }
	inline bool get_swigCMemOwn_1() const { return ___swigCMemOwn_1; }
	inline bool* get_address_of_swigCMemOwn_1() { return &___swigCMemOwn_1; }
	inline void set_swigCMemOwn_1(bool value)
	{
		___swigCMemOwn_1 = value;
	}
};


// Firebase.InitializationException
struct  InitializationException_t4950F6485639018AF98C04DD016B37F1745F8098  : public Exception_t
{
public:
	// Firebase.InitResult Firebase.InitializationException::<InitResult>k__BackingField
	int32_t ___U3CInitResultU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_U3CInitResultU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(InitializationException_t4950F6485639018AF98C04DD016B37F1745F8098, ___U3CInitResultU3Ek__BackingField_17)); }
	inline int32_t get_U3CInitResultU3Ek__BackingField_17() const { return ___U3CInitResultU3Ek__BackingField_17; }
	inline int32_t* get_address_of_U3CInitResultU3Ek__BackingField_17() { return &___U3CInitResultU3Ek__BackingField_17; }
	inline void set_U3CInitResultU3Ek__BackingField_17(int32_t value)
	{
		___U3CInitResultU3Ek__BackingField_17 = value;
	}
};


// System.AggregateException
struct  AggregateException_t9217B9E89DF820D5632411F2BD92F444B17BD60E  : public Exception_t
{
public:
	// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Exception> System.AggregateException::m_innerExceptions
	ReadOnlyCollection_1_t6D5AC6FC0BF91A16C9E9159F577DEDA4DD3414C8 * ___m_innerExceptions_17;

public:
	inline static int32_t get_offset_of_m_innerExceptions_17() { return static_cast<int32_t>(offsetof(AggregateException_t9217B9E89DF820D5632411F2BD92F444B17BD60E, ___m_innerExceptions_17)); }
	inline ReadOnlyCollection_1_t6D5AC6FC0BF91A16C9E9159F577DEDA4DD3414C8 * get_m_innerExceptions_17() const { return ___m_innerExceptions_17; }
	inline ReadOnlyCollection_1_t6D5AC6FC0BF91A16C9E9159F577DEDA4DD3414C8 ** get_address_of_m_innerExceptions_17() { return &___m_innerExceptions_17; }
	inline void set_m_innerExceptions_17(ReadOnlyCollection_1_t6D5AC6FC0BF91A16C9E9159F577DEDA4DD3414C8 * value)
	{
		___m_innerExceptions_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_innerExceptions_17), (void*)value);
	}
};


// System.ApplicationException
struct  ApplicationException_t664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74  : public Exception_t
{
public:

public:
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};


// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>
struct  Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7  : public Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2
{
public:
	// TResult System.Threading.Tasks.Task`1::m_result
	FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * ___m_result_22;

public:
	inline static int32_t get_offset_of_m_result_22() { return static_cast<int32_t>(offsetof(Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7, ___m_result_22)); }
	inline FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * get_m_result_22() const { return ___m_result_22; }
	inline FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD ** get_address_of_m_result_22() { return &___m_result_22; }
	inline void set_m_result_22(FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * value)
	{
		___m_result_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_result_22), (void*)value);
	}
};

struct Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7_StaticFields
{
public:
	// System.Threading.Tasks.TaskFactory`1<TResult> System.Threading.Tasks.Task`1::s_Factory
	TaskFactory_1_t405C1C82B1AA4CD904A0712FBAA47E60A993E2E7 * ___s_Factory_23;
	// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<TResult>> System.Threading.Tasks.Task`1::TaskWhenAnyCast
	Func_2_t385E06614170490B1EFD01D106616F0668133752 * ___TaskWhenAnyCast_24;

public:
	inline static int32_t get_offset_of_s_Factory_23() { return static_cast<int32_t>(offsetof(Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7_StaticFields, ___s_Factory_23)); }
	inline TaskFactory_1_t405C1C82B1AA4CD904A0712FBAA47E60A993E2E7 * get_s_Factory_23() const { return ___s_Factory_23; }
	inline TaskFactory_1_t405C1C82B1AA4CD904A0712FBAA47E60A993E2E7 ** get_address_of_s_Factory_23() { return &___s_Factory_23; }
	inline void set_s_Factory_23(TaskFactory_1_t405C1C82B1AA4CD904A0712FBAA47E60A993E2E7 * value)
	{
		___s_Factory_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Factory_23), (void*)value);
	}

	inline static int32_t get_offset_of_TaskWhenAnyCast_24() { return static_cast<int32_t>(offsetof(Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7_StaticFields, ___TaskWhenAnyCast_24)); }
	inline Func_2_t385E06614170490B1EFD01D106616F0668133752 * get_TaskWhenAnyCast_24() const { return ___TaskWhenAnyCast_24; }
	inline Func_2_t385E06614170490B1EFD01D106616F0668133752 ** get_address_of_TaskWhenAnyCast_24() { return &___TaskWhenAnyCast_24; }
	inline void set_TaskWhenAnyCast_24(Func_2_t385E06614170490B1EFD01D106616F0668133752 * value)
	{
		___TaskWhenAnyCast_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TaskWhenAnyCast_24), (void*)value);
	}
};


// System.Threading.Tasks.Task`1<System.Object>
struct  Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09  : public Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2
{
public:
	// TResult System.Threading.Tasks.Task`1::m_result
	RuntimeObject * ___m_result_22;

public:
	inline static int32_t get_offset_of_m_result_22() { return static_cast<int32_t>(offsetof(Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09, ___m_result_22)); }
	inline RuntimeObject * get_m_result_22() const { return ___m_result_22; }
	inline RuntimeObject ** get_address_of_m_result_22() { return &___m_result_22; }
	inline void set_m_result_22(RuntimeObject * value)
	{
		___m_result_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_result_22), (void*)value);
	}
};

struct Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09_StaticFields
{
public:
	// System.Threading.Tasks.TaskFactory`1<TResult> System.Threading.Tasks.Task`1::s_Factory
	TaskFactory_1_t1EC236EC6D34F0BBC2E87658D2A11017407240D4 * ___s_Factory_23;
	// System.Func`2<System.Threading.Tasks.Task`1<System.Threading.Tasks.Task>,System.Threading.Tasks.Task`1<TResult>> System.Threading.Tasks.Task`1::TaskWhenAnyCast
	Func_2_t8A17E987C73781D20C93E1140DAE0B3595A19A4B * ___TaskWhenAnyCast_24;

public:
	inline static int32_t get_offset_of_s_Factory_23() { return static_cast<int32_t>(offsetof(Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09_StaticFields, ___s_Factory_23)); }
	inline TaskFactory_1_t1EC236EC6D34F0BBC2E87658D2A11017407240D4 * get_s_Factory_23() const { return ___s_Factory_23; }
	inline TaskFactory_1_t1EC236EC6D34F0BBC2E87658D2A11017407240D4 ** get_address_of_s_Factory_23() { return &___s_Factory_23; }
	inline void set_s_Factory_23(TaskFactory_1_t1EC236EC6D34F0BBC2E87658D2A11017407240D4 * value)
	{
		___s_Factory_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Factory_23), (void*)value);
	}

	inline static int32_t get_offset_of_TaskWhenAnyCast_24() { return static_cast<int32_t>(offsetof(Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09_StaticFields, ___TaskWhenAnyCast_24)); }
	inline Func_2_t8A17E987C73781D20C93E1140DAE0B3595A19A4B * get_TaskWhenAnyCast_24() const { return ___TaskWhenAnyCast_24; }
	inline Func_2_t8A17E987C73781D20C93E1140DAE0B3595A19A4B ** get_address_of_TaskWhenAnyCast_24() { return &___TaskWhenAnyCast_24; }
	inline void set_TaskWhenAnyCast_24(Func_2_t8A17E987C73781D20C93E1140DAE0B3595A19A4B * value)
	{
		___TaskWhenAnyCast_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TaskWhenAnyCast_24), (void*)value);
	}
};


// System.Uri
struct  Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E  : public RuntimeObject
{
public:
	// System.String System.Uri::m_String
	String_t* ___m_String_16;
	// System.String System.Uri::m_originalUnicodeString
	String_t* ___m_originalUnicodeString_17;
	// System.UriParser System.Uri::m_Syntax
	UriParser_t07C77D673CCE8D2DA253B8A7ACCB010147F1A4AC * ___m_Syntax_18;
	// System.String System.Uri::m_DnsSafeHost
	String_t* ___m_DnsSafeHost_19;
	// System.Uri_Flags System.Uri::m_Flags
	uint64_t ___m_Flags_20;
	// System.Uri_UriInfo System.Uri::m_Info
	UriInfo_t9FCC6BD4EC1EA14D75209E6A35417057BF6EDC5E * ___m_Info_21;
	// System.Boolean System.Uri::m_iriParsing
	bool ___m_iriParsing_22;

public:
	inline static int32_t get_offset_of_m_String_16() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E, ___m_String_16)); }
	inline String_t* get_m_String_16() const { return ___m_String_16; }
	inline String_t** get_address_of_m_String_16() { return &___m_String_16; }
	inline void set_m_String_16(String_t* value)
	{
		___m_String_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_String_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_originalUnicodeString_17() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E, ___m_originalUnicodeString_17)); }
	inline String_t* get_m_originalUnicodeString_17() const { return ___m_originalUnicodeString_17; }
	inline String_t** get_address_of_m_originalUnicodeString_17() { return &___m_originalUnicodeString_17; }
	inline void set_m_originalUnicodeString_17(String_t* value)
	{
		___m_originalUnicodeString_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_originalUnicodeString_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_Syntax_18() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E, ___m_Syntax_18)); }
	inline UriParser_t07C77D673CCE8D2DA253B8A7ACCB010147F1A4AC * get_m_Syntax_18() const { return ___m_Syntax_18; }
	inline UriParser_t07C77D673CCE8D2DA253B8A7ACCB010147F1A4AC ** get_address_of_m_Syntax_18() { return &___m_Syntax_18; }
	inline void set_m_Syntax_18(UriParser_t07C77D673CCE8D2DA253B8A7ACCB010147F1A4AC * value)
	{
		___m_Syntax_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Syntax_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_DnsSafeHost_19() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E, ___m_DnsSafeHost_19)); }
	inline String_t* get_m_DnsSafeHost_19() const { return ___m_DnsSafeHost_19; }
	inline String_t** get_address_of_m_DnsSafeHost_19() { return &___m_DnsSafeHost_19; }
	inline void set_m_DnsSafeHost_19(String_t* value)
	{
		___m_DnsSafeHost_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DnsSafeHost_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_Flags_20() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E, ___m_Flags_20)); }
	inline uint64_t get_m_Flags_20() const { return ___m_Flags_20; }
	inline uint64_t* get_address_of_m_Flags_20() { return &___m_Flags_20; }
	inline void set_m_Flags_20(uint64_t value)
	{
		___m_Flags_20 = value;
	}

	inline static int32_t get_offset_of_m_Info_21() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E, ___m_Info_21)); }
	inline UriInfo_t9FCC6BD4EC1EA14D75209E6A35417057BF6EDC5E * get_m_Info_21() const { return ___m_Info_21; }
	inline UriInfo_t9FCC6BD4EC1EA14D75209E6A35417057BF6EDC5E ** get_address_of_m_Info_21() { return &___m_Info_21; }
	inline void set_m_Info_21(UriInfo_t9FCC6BD4EC1EA14D75209E6A35417057BF6EDC5E * value)
	{
		___m_Info_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Info_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_iriParsing_22() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E, ___m_iriParsing_22)); }
	inline bool get_m_iriParsing_22() const { return ___m_iriParsing_22; }
	inline bool* get_address_of_m_iriParsing_22() { return &___m_iriParsing_22; }
	inline void set_m_iriParsing_22(bool value)
	{
		___m_iriParsing_22 = value;
	}
};

struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields
{
public:
	// System.String System.Uri::UriSchemeFile
	String_t* ___UriSchemeFile_0;
	// System.String System.Uri::UriSchemeFtp
	String_t* ___UriSchemeFtp_1;
	// System.String System.Uri::UriSchemeGopher
	String_t* ___UriSchemeGopher_2;
	// System.String System.Uri::UriSchemeHttp
	String_t* ___UriSchemeHttp_3;
	// System.String System.Uri::UriSchemeHttps
	String_t* ___UriSchemeHttps_4;
	// System.String System.Uri::UriSchemeWs
	String_t* ___UriSchemeWs_5;
	// System.String System.Uri::UriSchemeWss
	String_t* ___UriSchemeWss_6;
	// System.String System.Uri::UriSchemeMailto
	String_t* ___UriSchemeMailto_7;
	// System.String System.Uri::UriSchemeNews
	String_t* ___UriSchemeNews_8;
	// System.String System.Uri::UriSchemeNntp
	String_t* ___UriSchemeNntp_9;
	// System.String System.Uri::UriSchemeNetTcp
	String_t* ___UriSchemeNetTcp_10;
	// System.String System.Uri::UriSchemeNetPipe
	String_t* ___UriSchemeNetPipe_11;
	// System.String System.Uri::SchemeDelimiter
	String_t* ___SchemeDelimiter_12;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_ConfigInitialized
	bool ___s_ConfigInitialized_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_ConfigInitializing
	bool ___s_ConfigInitializing_24;
	// System.UriIdnScope modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_IdnScope
	int32_t ___s_IdnScope_25;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_IriParsing
	bool ___s_IriParsing_26;
	// System.Boolean System.Uri::useDotNetRelativeOrAbsolute
	bool ___useDotNetRelativeOrAbsolute_27;
	// System.Boolean System.Uri::IsWindowsFileSystem
	bool ___IsWindowsFileSystem_29;
	// System.Object System.Uri::s_initLock
	RuntimeObject * ___s_initLock_30;
	// System.Char[] System.Uri::HexLowerChars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___HexLowerChars_34;
	// System.Char[] System.Uri::_WSchars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ____WSchars_35;

public:
	inline static int32_t get_offset_of_UriSchemeFile_0() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeFile_0)); }
	inline String_t* get_UriSchemeFile_0() const { return ___UriSchemeFile_0; }
	inline String_t** get_address_of_UriSchemeFile_0() { return &___UriSchemeFile_0; }
	inline void set_UriSchemeFile_0(String_t* value)
	{
		___UriSchemeFile_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeFile_0), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeFtp_1() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeFtp_1)); }
	inline String_t* get_UriSchemeFtp_1() const { return ___UriSchemeFtp_1; }
	inline String_t** get_address_of_UriSchemeFtp_1() { return &___UriSchemeFtp_1; }
	inline void set_UriSchemeFtp_1(String_t* value)
	{
		___UriSchemeFtp_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeFtp_1), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeGopher_2() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeGopher_2)); }
	inline String_t* get_UriSchemeGopher_2() const { return ___UriSchemeGopher_2; }
	inline String_t** get_address_of_UriSchemeGopher_2() { return &___UriSchemeGopher_2; }
	inline void set_UriSchemeGopher_2(String_t* value)
	{
		___UriSchemeGopher_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeGopher_2), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeHttp_3() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeHttp_3)); }
	inline String_t* get_UriSchemeHttp_3() const { return ___UriSchemeHttp_3; }
	inline String_t** get_address_of_UriSchemeHttp_3() { return &___UriSchemeHttp_3; }
	inline void set_UriSchemeHttp_3(String_t* value)
	{
		___UriSchemeHttp_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeHttp_3), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeHttps_4() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeHttps_4)); }
	inline String_t* get_UriSchemeHttps_4() const { return ___UriSchemeHttps_4; }
	inline String_t** get_address_of_UriSchemeHttps_4() { return &___UriSchemeHttps_4; }
	inline void set_UriSchemeHttps_4(String_t* value)
	{
		___UriSchemeHttps_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeHttps_4), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeWs_5() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeWs_5)); }
	inline String_t* get_UriSchemeWs_5() const { return ___UriSchemeWs_5; }
	inline String_t** get_address_of_UriSchemeWs_5() { return &___UriSchemeWs_5; }
	inline void set_UriSchemeWs_5(String_t* value)
	{
		___UriSchemeWs_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeWs_5), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeWss_6() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeWss_6)); }
	inline String_t* get_UriSchemeWss_6() const { return ___UriSchemeWss_6; }
	inline String_t** get_address_of_UriSchemeWss_6() { return &___UriSchemeWss_6; }
	inline void set_UriSchemeWss_6(String_t* value)
	{
		___UriSchemeWss_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeWss_6), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeMailto_7() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeMailto_7)); }
	inline String_t* get_UriSchemeMailto_7() const { return ___UriSchemeMailto_7; }
	inline String_t** get_address_of_UriSchemeMailto_7() { return &___UriSchemeMailto_7; }
	inline void set_UriSchemeMailto_7(String_t* value)
	{
		___UriSchemeMailto_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeMailto_7), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeNews_8() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeNews_8)); }
	inline String_t* get_UriSchemeNews_8() const { return ___UriSchemeNews_8; }
	inline String_t** get_address_of_UriSchemeNews_8() { return &___UriSchemeNews_8; }
	inline void set_UriSchemeNews_8(String_t* value)
	{
		___UriSchemeNews_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeNews_8), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeNntp_9() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeNntp_9)); }
	inline String_t* get_UriSchemeNntp_9() const { return ___UriSchemeNntp_9; }
	inline String_t** get_address_of_UriSchemeNntp_9() { return &___UriSchemeNntp_9; }
	inline void set_UriSchemeNntp_9(String_t* value)
	{
		___UriSchemeNntp_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeNntp_9), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeNetTcp_10() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeNetTcp_10)); }
	inline String_t* get_UriSchemeNetTcp_10() const { return ___UriSchemeNetTcp_10; }
	inline String_t** get_address_of_UriSchemeNetTcp_10() { return &___UriSchemeNetTcp_10; }
	inline void set_UriSchemeNetTcp_10(String_t* value)
	{
		___UriSchemeNetTcp_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeNetTcp_10), (void*)value);
	}

	inline static int32_t get_offset_of_UriSchemeNetPipe_11() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___UriSchemeNetPipe_11)); }
	inline String_t* get_UriSchemeNetPipe_11() const { return ___UriSchemeNetPipe_11; }
	inline String_t** get_address_of_UriSchemeNetPipe_11() { return &___UriSchemeNetPipe_11; }
	inline void set_UriSchemeNetPipe_11(String_t* value)
	{
		___UriSchemeNetPipe_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UriSchemeNetPipe_11), (void*)value);
	}

	inline static int32_t get_offset_of_SchemeDelimiter_12() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___SchemeDelimiter_12)); }
	inline String_t* get_SchemeDelimiter_12() const { return ___SchemeDelimiter_12; }
	inline String_t** get_address_of_SchemeDelimiter_12() { return &___SchemeDelimiter_12; }
	inline void set_SchemeDelimiter_12(String_t* value)
	{
		___SchemeDelimiter_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SchemeDelimiter_12), (void*)value);
	}

	inline static int32_t get_offset_of_s_ConfigInitialized_23() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___s_ConfigInitialized_23)); }
	inline bool get_s_ConfigInitialized_23() const { return ___s_ConfigInitialized_23; }
	inline bool* get_address_of_s_ConfigInitialized_23() { return &___s_ConfigInitialized_23; }
	inline void set_s_ConfigInitialized_23(bool value)
	{
		___s_ConfigInitialized_23 = value;
	}

	inline static int32_t get_offset_of_s_ConfigInitializing_24() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___s_ConfigInitializing_24)); }
	inline bool get_s_ConfigInitializing_24() const { return ___s_ConfigInitializing_24; }
	inline bool* get_address_of_s_ConfigInitializing_24() { return &___s_ConfigInitializing_24; }
	inline void set_s_ConfigInitializing_24(bool value)
	{
		___s_ConfigInitializing_24 = value;
	}

	inline static int32_t get_offset_of_s_IdnScope_25() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___s_IdnScope_25)); }
	inline int32_t get_s_IdnScope_25() const { return ___s_IdnScope_25; }
	inline int32_t* get_address_of_s_IdnScope_25() { return &___s_IdnScope_25; }
	inline void set_s_IdnScope_25(int32_t value)
	{
		___s_IdnScope_25 = value;
	}

	inline static int32_t get_offset_of_s_IriParsing_26() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___s_IriParsing_26)); }
	inline bool get_s_IriParsing_26() const { return ___s_IriParsing_26; }
	inline bool* get_address_of_s_IriParsing_26() { return &___s_IriParsing_26; }
	inline void set_s_IriParsing_26(bool value)
	{
		___s_IriParsing_26 = value;
	}

	inline static int32_t get_offset_of_useDotNetRelativeOrAbsolute_27() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___useDotNetRelativeOrAbsolute_27)); }
	inline bool get_useDotNetRelativeOrAbsolute_27() const { return ___useDotNetRelativeOrAbsolute_27; }
	inline bool* get_address_of_useDotNetRelativeOrAbsolute_27() { return &___useDotNetRelativeOrAbsolute_27; }
	inline void set_useDotNetRelativeOrAbsolute_27(bool value)
	{
		___useDotNetRelativeOrAbsolute_27 = value;
	}

	inline static int32_t get_offset_of_IsWindowsFileSystem_29() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___IsWindowsFileSystem_29)); }
	inline bool get_IsWindowsFileSystem_29() const { return ___IsWindowsFileSystem_29; }
	inline bool* get_address_of_IsWindowsFileSystem_29() { return &___IsWindowsFileSystem_29; }
	inline void set_IsWindowsFileSystem_29(bool value)
	{
		___IsWindowsFileSystem_29 = value;
	}

	inline static int32_t get_offset_of_s_initLock_30() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___s_initLock_30)); }
	inline RuntimeObject * get_s_initLock_30() const { return ___s_initLock_30; }
	inline RuntimeObject ** get_address_of_s_initLock_30() { return &___s_initLock_30; }
	inline void set_s_initLock_30(RuntimeObject * value)
	{
		___s_initLock_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_initLock_30), (void*)value);
	}

	inline static int32_t get_offset_of_HexLowerChars_34() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ___HexLowerChars_34)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_HexLowerChars_34() const { return ___HexLowerChars_34; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_HexLowerChars_34() { return &___HexLowerChars_34; }
	inline void set_HexLowerChars_34(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___HexLowerChars_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___HexLowerChars_34), (void*)value);
	}

	inline static int32_t get_offset_of__WSchars_35() { return static_cast<int32_t>(offsetof(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E_StaticFields, ____WSchars_35)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get__WSchars_35() const { return ____WSchars_35; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of__WSchars_35() { return &____WSchars_35; }
	inline void set__WSchars_35(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		____WSchars_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____WSchars_35), (void*)value);
	}
};


// Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate
struct  ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4  : public MulticastDelegate_t
{
public:

public:
};


// Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate
struct  ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0  : public MulticastDelegate_t
{
public:

public:
};


// Firebase.Auth.AuthUtilPINVOKE_SWIGStringHelper_SWIGStringDelegate
struct  SWIGStringDelegate_tF943E218400FF77BFF8F047D209AA7CA94915E00  : public MulticastDelegate_t
{
public:

public:
};


// Firebase.Auth.FirebaseAuth_StateChangedDelegate
struct  StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294  : public MulticastDelegate_t
{
public:

public:
};


// Firebase.Auth.FirebaseUser
struct  FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD  : public UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.Auth.FirebaseUser::swigCPtr
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___swigCPtr_2;
	// Firebase.Auth.FirebaseAuth Firebase.Auth.FirebaseUser::authProxy
	FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * ___authProxy_3;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD, ___swigCPtr_2)); }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t876E76124F400D12395BF61D562162AB6822204A  value)
	{
		___swigCPtr_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___swigCPtr_2))->___m_wrapper_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_authProxy_3() { return static_cast<int32_t>(offsetof(FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD, ___authProxy_3)); }
	inline FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * get_authProxy_3() const { return ___authProxy_3; }
	inline FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D ** get_address_of_authProxy_3() { return &___authProxy_3; }
	inline void set_authProxy_3(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * value)
	{
		___authProxy_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___authProxy_3), (void*)value);
	}
};


// Firebase.Auth.Future_User
struct  Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01  : public FutureBase_t75BACF0EF2911C731F998E85086202A1A2F55397
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.Auth.Future_User::swigCPtr
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___swigCPtr_2;
	// System.IntPtr Firebase.Auth.Future_User::callbackData
	intptr_t ___callbackData_6;
	// Firebase.Auth.Future_User_SWIG_CompletionDelegate Firebase.Auth.Future_User::SWIG_CompletionCB
	SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2 * ___SWIG_CompletionCB_7;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01, ___swigCPtr_2)); }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t876E76124F400D12395BF61D562162AB6822204A  value)
	{
		___swigCPtr_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___swigCPtr_2))->___m_wrapper_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_callbackData_6() { return static_cast<int32_t>(offsetof(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01, ___callbackData_6)); }
	inline intptr_t get_callbackData_6() const { return ___callbackData_6; }
	inline intptr_t* get_address_of_callbackData_6() { return &___callbackData_6; }
	inline void set_callbackData_6(intptr_t value)
	{
		___callbackData_6 = value;
	}

	inline static int32_t get_offset_of_SWIG_CompletionCB_7() { return static_cast<int32_t>(offsetof(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01, ___SWIG_CompletionCB_7)); }
	inline SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2 * get_SWIG_CompletionCB_7() const { return ___SWIG_CompletionCB_7; }
	inline SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2 ** get_address_of_SWIG_CompletionCB_7() { return &___SWIG_CompletionCB_7; }
	inline void set_SWIG_CompletionCB_7(SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2 * value)
	{
		___SWIG_CompletionCB_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SWIG_CompletionCB_7), (void*)value);
	}
};

struct Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User_Action> Firebase.Auth.Future_User::Callbacks
	Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A * ___Callbacks_3;
	// System.Int32 Firebase.Auth.Future_User::CallbackIndex
	int32_t ___CallbackIndex_4;
	// System.Object Firebase.Auth.Future_User::CallbackLock
	RuntimeObject * ___CallbackLock_5;

public:
	inline static int32_t get_offset_of_Callbacks_3() { return static_cast<int32_t>(offsetof(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_StaticFields, ___Callbacks_3)); }
	inline Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A * get_Callbacks_3() const { return ___Callbacks_3; }
	inline Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A ** get_address_of_Callbacks_3() { return &___Callbacks_3; }
	inline void set_Callbacks_3(Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A * value)
	{
		___Callbacks_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Callbacks_3), (void*)value);
	}

	inline static int32_t get_offset_of_CallbackIndex_4() { return static_cast<int32_t>(offsetof(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_StaticFields, ___CallbackIndex_4)); }
	inline int32_t get_CallbackIndex_4() const { return ___CallbackIndex_4; }
	inline int32_t* get_address_of_CallbackIndex_4() { return &___CallbackIndex_4; }
	inline void set_CallbackIndex_4(int32_t value)
	{
		___CallbackIndex_4 = value;
	}

	inline static int32_t get_offset_of_CallbackLock_5() { return static_cast<int32_t>(offsetof(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_StaticFields, ___CallbackLock_5)); }
	inline RuntimeObject * get_CallbackLock_5() const { return ___CallbackLock_5; }
	inline RuntimeObject ** get_address_of_CallbackLock_5() { return &___CallbackLock_5; }
	inline void set_CallbackLock_5(RuntimeObject * value)
	{
		___CallbackLock_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CallbackLock_5), (void*)value);
	}
};


// Firebase.Auth.Future_User_Action
struct  Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327  : public MulticastDelegate_t
{
public:

public:
};


// Firebase.Auth.Future_User_SWIG_CompletionDelegate
struct  SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2  : public MulticastDelegate_t
{
public:

public:
};


// Firebase.FutureVoid
struct  FutureVoid_tF512775335C40422BAC529CBF374A246FD8ED51D  : public FutureBase_t75BACF0EF2911C731F998E85086202A1A2F55397
{
public:
	// System.Runtime.InteropServices.HandleRef Firebase.FutureVoid::swigCPtr
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___swigCPtr_2;
	// System.IntPtr Firebase.FutureVoid::callbackData
	intptr_t ___callbackData_6;
	// Firebase.FutureVoid_SWIG_CompletionDelegate Firebase.FutureVoid::SWIG_CompletionCB
	SWIG_CompletionDelegate_t84CBBD1E196A142480C0E94C912F5761358B4988 * ___SWIG_CompletionCB_7;

public:
	inline static int32_t get_offset_of_swigCPtr_2() { return static_cast<int32_t>(offsetof(FutureVoid_tF512775335C40422BAC529CBF374A246FD8ED51D, ___swigCPtr_2)); }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A  get_swigCPtr_2() const { return ___swigCPtr_2; }
	inline HandleRef_t876E76124F400D12395BF61D562162AB6822204A * get_address_of_swigCPtr_2() { return &___swigCPtr_2; }
	inline void set_swigCPtr_2(HandleRef_t876E76124F400D12395BF61D562162AB6822204A  value)
	{
		___swigCPtr_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___swigCPtr_2))->___m_wrapper_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_callbackData_6() { return static_cast<int32_t>(offsetof(FutureVoid_tF512775335C40422BAC529CBF374A246FD8ED51D, ___callbackData_6)); }
	inline intptr_t get_callbackData_6() const { return ___callbackData_6; }
	inline intptr_t* get_address_of_callbackData_6() { return &___callbackData_6; }
	inline void set_callbackData_6(intptr_t value)
	{
		___callbackData_6 = value;
	}

	inline static int32_t get_offset_of_SWIG_CompletionCB_7() { return static_cast<int32_t>(offsetof(FutureVoid_tF512775335C40422BAC529CBF374A246FD8ED51D, ___SWIG_CompletionCB_7)); }
	inline SWIG_CompletionDelegate_t84CBBD1E196A142480C0E94C912F5761358B4988 * get_SWIG_CompletionCB_7() const { return ___SWIG_CompletionCB_7; }
	inline SWIG_CompletionDelegate_t84CBBD1E196A142480C0E94C912F5761358B4988 ** get_address_of_SWIG_CompletionCB_7() { return &___SWIG_CompletionCB_7; }
	inline void set_SWIG_CompletionCB_7(SWIG_CompletionDelegate_t84CBBD1E196A142480C0E94C912F5761358B4988 * value)
	{
		___SWIG_CompletionCB_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SWIG_CompletionCB_7), (void*)value);
	}
};

struct FutureVoid_tF512775335C40422BAC529CBF374A246FD8ED51D_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,Firebase.FutureVoid_Action> Firebase.FutureVoid::Callbacks
	Dictionary_2_t087E572E4674F35454E806EDE6D0179962696A2B * ___Callbacks_3;
	// System.Int32 Firebase.FutureVoid::CallbackIndex
	int32_t ___CallbackIndex_4;
	// System.Object Firebase.FutureVoid::CallbackLock
	RuntimeObject * ___CallbackLock_5;

public:
	inline static int32_t get_offset_of_Callbacks_3() { return static_cast<int32_t>(offsetof(FutureVoid_tF512775335C40422BAC529CBF374A246FD8ED51D_StaticFields, ___Callbacks_3)); }
	inline Dictionary_2_t087E572E4674F35454E806EDE6D0179962696A2B * get_Callbacks_3() const { return ___Callbacks_3; }
	inline Dictionary_2_t087E572E4674F35454E806EDE6D0179962696A2B ** get_address_of_Callbacks_3() { return &___Callbacks_3; }
	inline void set_Callbacks_3(Dictionary_2_t087E572E4674F35454E806EDE6D0179962696A2B * value)
	{
		___Callbacks_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Callbacks_3), (void*)value);
	}

	inline static int32_t get_offset_of_CallbackIndex_4() { return static_cast<int32_t>(offsetof(FutureVoid_tF512775335C40422BAC529CBF374A246FD8ED51D_StaticFields, ___CallbackIndex_4)); }
	inline int32_t get_CallbackIndex_4() const { return ___CallbackIndex_4; }
	inline int32_t* get_address_of_CallbackIndex_4() { return &___CallbackIndex_4; }
	inline void set_CallbackIndex_4(int32_t value)
	{
		___CallbackIndex_4 = value;
	}

	inline static int32_t get_offset_of_CallbackLock_5() { return static_cast<int32_t>(offsetof(FutureVoid_tF512775335C40422BAC529CBF374A246FD8ED51D_StaticFields, ___CallbackLock_5)); }
	inline RuntimeObject * get_CallbackLock_5() const { return ___CallbackLock_5; }
	inline RuntimeObject ** get_address_of_CallbackLock_5() { return &___CallbackLock_5; }
	inline void set_CallbackLock_5(RuntimeObject * value)
	{
		___CallbackLock_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CallbackLock_5), (void*)value);
	}
};


// System.Action
struct  Action_t591D2A86165F896B4B800BB5C25CE18672A55579  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`1<Firebase.Auth.FirebaseAuth>
struct  Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`1<System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>>
struct  Action_1_t5D2D131226D740682F598DD09436558292112781  : public MulticastDelegate_t
{
public:

public:
};


// System.ArgumentException
struct  ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_paramName_17), (void*)value);
	}
};


// System.ArithmeticException
struct  ArithmeticException_tF9EF5FE94597830EF315C5934258F994B8648269  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};


// System.AsyncCallback
struct  AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4  : public MulticastDelegate_t
{
public:

public:
};


// System.EventHandler
struct  EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C  : public MulticastDelegate_t
{
public:

public:
};


// System.IO.IOException
struct  IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.IO.IOException::_maybeFullPath
	String_t* ____maybeFullPath_17;

public:
	inline static int32_t get_offset_of__maybeFullPath_17() { return static_cast<int32_t>(offsetof(IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA, ____maybeFullPath_17)); }
	inline String_t* get__maybeFullPath_17() const { return ____maybeFullPath_17; }
	inline String_t** get_address_of__maybeFullPath_17() { return &____maybeFullPath_17; }
	inline void set__maybeFullPath_17(String_t* value)
	{
		____maybeFullPath_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____maybeFullPath_17), (void*)value);
	}
};


// System.IndexOutOfRangeException
struct  IndexOutOfRangeException_tEC7665FC66525AB6A6916A7EB505E5591683F0CF  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};


// System.InvalidCastException
struct  InvalidCastException_t91DF9E7D7FCCDA6C562CB4A9A18903E016680FDA  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};


// System.InvalidOperationException
struct  InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};


// System.NullReferenceException
struct  NullReferenceException_t204B194BC4DDA3259AF5A8633EA248AE5977ABDC  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};


// System.OutOfMemoryException
struct  OutOfMemoryException_t2DF3EAC178583BD1DEFAAECBEDB2AF1EA86FBFC7  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};


// System.ArgumentNullException
struct  ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD  : public ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1
{
public:

public:
};


// System.ArgumentOutOfRangeException
struct  ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA  : public ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1
{
public:
	// System.Object System.ArgumentOutOfRangeException::m_actualValue
	RuntimeObject * ___m_actualValue_19;

public:
	inline static int32_t get_offset_of_m_actualValue_19() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA, ___m_actualValue_19)); }
	inline RuntimeObject * get_m_actualValue_19() const { return ___m_actualValue_19; }
	inline RuntimeObject ** get_address_of_m_actualValue_19() { return &___m_actualValue_19; }
	inline void set_m_actualValue_19(RuntimeObject * value)
	{
		___m_actualValue_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_actualValue_19), (void*)value);
	}
};

struct ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_StaticFields
{
public:
	// System.String modreq(System.Runtime.CompilerServices.IsVolatile) System.ArgumentOutOfRangeException::_rangeMessage
	String_t* ____rangeMessage_18;

public:
	inline static int32_t get_offset_of__rangeMessage_18() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_StaticFields, ____rangeMessage_18)); }
	inline String_t* get__rangeMessage_18() const { return ____rangeMessage_18; }
	inline String_t** get_address_of__rangeMessage_18() { return &____rangeMessage_18; }
	inline void set__rangeMessage_18(String_t* value)
	{
		____rangeMessage_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rangeMessage_18), (void*)value);
	}
};


// System.DivideByZeroException
struct  DivideByZeroException_tD233835DD9A31EE4E64DD93F2D6143646CFD3FBC  : public ArithmeticException_tF9EF5FE94597830EF315C5934258F994B8648269
{
public:

public:
};


// System.OverflowException
struct  OverflowException_tD89571E2350DE06D9DE4AB65ADCA77D607B5693D  : public ArithmeticException_tF9EF5FE94597830EF315C5934258F994B8648269
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// Firebase.Auth.UserInfoInterface[]
struct UserInfoInterfaceU5BU5D_tD810A76761E9D37FB729A09C273728E21098926B  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * m_Items[1];

public:
	inline UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::TryGetValue(!0,!1&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_mB972A6602BDD10CBB612188FDED9917370EC3F17_gshared (Dictionary_2_tE930FC0A0DF3FBFFE9DC13EED210134C371C368E * __this, intptr_t ___key0, RuntimeObject ** ___value1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::set_Item(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_set_Item_m26B2DBC3BE8767E18A97FFCF3FB2EFC766886BD4_gshared (Dictionary_2_tE930FC0A0DF3FBFFE9DC13EED210134C371C368E * __this, intptr_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::Remove(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_Remove_m8F5E306A3F995390237D97553B63674C8D1D725F_gshared (Dictionary_2_tE930FC0A0DF3FBFFE9DC13EED210134C371C368E * __this, intptr_t ___key0, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.Threading.Tasks.TaskCompletionSource`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TaskCompletionSource_1__ctor_m894F4B43C47FD23EB3FD138E3CBBD71203DC5FA7_gshared (TaskCompletionSource_1_t3ECF7B56E418D2A3FFC4E551B9DA6A7E38C7D304 * __this, const RuntimeMethod* method);
// System.Threading.Tasks.Task System.Threading.Tasks.Task`1<System.Object>::ContinueWith(System.Action`1<System.Threading.Tasks.Task`1<!0>>)
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * Task_1_ContinueWith_mCFC2582CE8330039C62FC88152095FF923BAECE8_gshared (Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09 * __this, Action_1_tF41DEFE08D91E5A3638655E075175E27AA82D1DC * ___continuationAction0, const RuntimeMethod* method);
// System.Threading.Tasks.Task`1<!0> System.Threading.Tasks.TaskCompletionSource`1<System.Object>::get_Task()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09 * TaskCompletionSource_1_get_Task_mF1CB937DAB68DC43DA33893E95F48EFB07F385A0_gshared_inline (TaskCompletionSource_1_t3ECF7B56E418D2A3FFC4E551B9DA6A7E38C7D304 * __this, const RuntimeMethod* method);
// System.Void System.Threading.Tasks.TaskCompletionSource`1<System.Object>::SetCanceled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TaskCompletionSource_1_SetCanceled_m9BD3702F5D2F7D1C0D9C7E4598B758F46B734B65_gshared (TaskCompletionSource_1_t3ECF7B56E418D2A3FFC4E551B9DA6A7E38C7D304 * __this, const RuntimeMethod* method);
// System.Void Firebase.Internal.TaskCompletionSourceCompat`1<System.Object>::SetException(System.Threading.Tasks.TaskCompletionSource`1<!0>,System.AggregateException)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TaskCompletionSourceCompat_1_SetException_m534DA13EA8E0D72BDF334398BCE2448B4C25E233_gshared (TaskCompletionSource_1_t3ECF7B56E418D2A3FFC4E551B9DA6A7E38C7D304 * ___tcs0, AggregateException_t9217B9E89DF820D5632411F2BD92F444B17BD60E * ___exception1, const RuntimeMethod* method);
// !0 System.Threading.Tasks.Task`1<System.Object>::get_Result()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Task_1_get_Result_m828E969718CE9AD38335D3F48B6F9C84AB98DEE9_gshared (Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09 * __this, const RuntimeMethod* method);
// System.Void System.Threading.Tasks.TaskCompletionSource`1<System.Object>::SetResult(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TaskCompletionSource_1_SetResult_m0EBFE5138C570B9515B706987C2983D4DFBE5337_gshared (TaskCompletionSource_1_t3ECF7B56E418D2A3FFC4E551B9DA6A7E38C7D304 * __this, RuntimeObject * ___result0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m901D4901A751B7C8F3CCD2608AA792ADBF53B75A_gshared (Dictionary_2_tE930FC0A0DF3FBFFE9DC13EED210134C371C368E * __this, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::Invoke(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1_Invoke_mB86FC1B303E77C41ED0E94FC3592A9CF8DA571D5_gshared (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Void System.Threading.Tasks.TaskCompletionSource`1<System.Object>::SetException(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TaskCompletionSource_1_SetException_mF32CD1B09A692369399D38F7B54F9FD2EC5394CE_gshared (TaskCompletionSource_1_t3ECF7B56E418D2A3FFC4E551B9DA6A7E38C7D304 * __this, Exception_t * ___exception0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m7D745ADE56151C2895459668F4A4242985E526D8_gshared (Dictionary_2_t03608389BB57475AA3F4B2B79D176A27807BC884 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::set_Item(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_set_Item_mF9A6FBE4006C89D15B8C88B2CB46E9B24D18B7FC_gshared (Dictionary_2_t03608389BB57475AA3F4B2B79D176A27807BC884 * __this, int32_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::TryGetValue(!0,!1&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_m867F6DA953678D0333A55270B7C6EF38EFC293FF_gshared (Dictionary_2_t03608389BB57475AA3F4B2B79D176A27807BC884 * __this, int32_t ___key0, RuntimeObject ** ___value1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Remove(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_Remove_m2204D6D532702FD13AB2A9AD8DB538E4E8FB1913_gshared (Dictionary_2_t03608389BB57475AA3F4B2B79D176A27807BC884 * __this, int32_t ___key0, const RuntimeMethod* method);

// System.Runtime.InteropServices.HandleRef Firebase.Auth.FirebaseAuth::getCPtr(Firebase.Auth.FirebaseAuth)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HandleRef_t876E76124F400D12395BF61D562162AB6822204A  FirebaseAuth_getCPtr_m43139A9D3674A9DFF8AED10B43D345217CE378C9 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * ___obj0, const RuntimeMethod* method);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::CreateAuthStateListener(System.Runtime.InteropServices.HandleRef,Firebase.Auth.FirebaseAuth/StateChangedDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_CreateAuthStateListener_m093D034502CF32CE850E6A9E286C15EAE6785C2B (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294 * ___jarg21, const RuntimeMethod* method);
// System.Boolean Firebase.AppUtilPINVOKE/SWIGPendingException::get_Pending()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC (const RuntimeMethod* method);
// System.Exception Firebase.AppUtilPINVOKE/SWIGPendingException::Retrieve()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1 (const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE::DestroyAuthStateListener(System.Runtime.InteropServices.HandleRef,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_DestroyAuthStateListener_m632301CCA03C97183D31D2E819196742BBB8BB1B (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, intptr_t ___jarg21, const RuntimeMethod* method);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::CreateIdTokenListener(System.Runtime.InteropServices.HandleRef,Firebase.Auth.FirebaseAuth/StateChangedDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_CreateIdTokenListener_mA59101F16559583539311A7139734B88B26CF10B (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294 * ___jarg21, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE::DestroyIdTokenListener(System.Runtime.InteropServices.HandleRef,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_DestroyIdTokenListener_m7CACC17CE6A8EB523C7FC9DC96051AB52A6571F1 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, intptr_t ___jarg21, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper__ctor_mFF2CE315200639030B26C32F018A7E7354B81663 (SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97 * __this, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGStringHelper__ctor_m22B2245AE72469FC2016E3B70329E4117E38AF96 (SWIGStringHelper_t0D99651DC671811558BDB3D299B82F879F5E731B * __this, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingApplicationException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingApplicationException_m1EB96681AA0CAEE7C648B0488735AC4B34644625 (String_t* ___message0, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingArithmeticException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingArithmeticException_m6FD83BB7EC4D0C5A758FC33434AFF95000BB2145 (String_t* ___message0, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingDivideByZeroException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingDivideByZeroException_mC428FC6EAD280918627E7AD8285B5EC490842944 (String_t* ___message0, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingIndexOutOfRangeException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m8AC3EB8E22CAB0B37997524B3D8EB61E7C4B9888 (String_t* ___message0, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingInvalidCastException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingInvalidCastException_m3B7ACF97DC9FB2B9F197C4E3168B9C4F8A4D0719 (String_t* ___message0, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingInvalidOperationException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingInvalidOperationException_mECD029C557A14DBA585161CF1E3EDD1C3201813D (String_t* ___message0, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingIOException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingIOException_mE61B638B09F96D19FA24D9B2BD85230650FC59B2 (String_t* ___message0, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingNullReferenceException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingNullReferenceException_mB61F8433F7B8D82845D1635AC67E6132F1F3A7B9 (String_t* ___message0, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingOutOfMemoryException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingOutOfMemoryException_mD766FD3AB3B0EF9E768367C0199FC00A9362119E (String_t* ___message0, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingOverflowException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingOverflowException_m4F7B5FA76EE51D9CB219856AB845929CD819FDF2 (String_t* ___message0, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingSystemException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingSystemException_mB83BE9B10C217AA022F9C626585DCE16A1E3BF43 (String_t* ___message0, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentException(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingArgumentException_mCC2449972A52DACB566318E5D82883A1AE0FCC77 (String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentNullException(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingArgumentNullException_mDDF072DC234116293DF4DD4CAE04931816995032 (String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SetPendingArgumentOutOfRangeException(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m64A3753D6C93082BF5431CA3B095125235597BA7 (String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method);
// System.Exception Firebase.Auth.AuthUtilPINVOKE/SWIGPendingException::Retrieve()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * SWIGPendingException_Retrieve_m47C7FCF8ED6E8C49646820EE0691C3EDE0CFFDCD (const RuntimeMethod* method);
// System.Void System.ApplicationException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ApplicationException__ctor_m0AF8988654AD1CA2DCA8EC12E6D46563E4EE703F (ApplicationException_t664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74 * __this, String_t* ___message0, Exception_t * ___innerException1, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGPendingException::Set(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGPendingException_Set_m2A770FCDB68242E723C1E3A5CA7B4B86B969155C (Exception_t * ___e0, const RuntimeMethod* method);
// System.Void System.ArithmeticException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArithmeticException__ctor_m1412E36F7AF7D25CF6A00670AE2296E88DA85F5F (ArithmeticException_tF9EF5FE94597830EF315C5934258F994B8648269 * __this, String_t* ___message0, Exception_t * ___innerException1, const RuntimeMethod* method);
// System.Void System.DivideByZeroException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DivideByZeroException__ctor_m12EDCCC81402AA666F9C3CF1CB8A8A7AA8D61169 (DivideByZeroException_tD233835DD9A31EE4E64DD93F2D6143646CFD3FBC * __this, String_t* ___message0, Exception_t * ___innerException1, const RuntimeMethod* method);
// System.Void System.IndexOutOfRangeException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IndexOutOfRangeException__ctor_m6CE231E888365F20BD05664560F7AF3830E9D21A (IndexOutOfRangeException_tEC7665FC66525AB6A6916A7EB505E5591683F0CF * __this, String_t* ___message0, Exception_t * ___innerException1, const RuntimeMethod* method);
// System.Void System.InvalidCastException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvalidCastException__ctor_m54AEC55A8A1F1EAD6485B691BB597C7EB284B2A5 (InvalidCastException_t91DF9E7D7FCCDA6C562CB4A9A18903E016680FDA * __this, String_t* ___message0, Exception_t * ___innerException1, const RuntimeMethod* method);
// System.Void System.InvalidOperationException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvalidOperationException__ctor_mC40AA9579B996C6FBAE023590139C16304D81DC6 (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * __this, String_t* ___message0, Exception_t * ___innerException1, const RuntimeMethod* method);
// System.Void System.IO.IOException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IOException__ctor_m37262C706BEB979358ABEFEA9F9F253E8773D2B7 (IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA * __this, String_t* ___message0, Exception_t * ___innerException1, const RuntimeMethod* method);
// System.Void System.NullReferenceException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NullReferenceException__ctor_mCD292E9CAC2B24447B04F5404C318B1B2AF4023D (NullReferenceException_t204B194BC4DDA3259AF5A8633EA248AE5977ABDC * __this, String_t* ___message0, Exception_t * ___innerException1, const RuntimeMethod* method);
// System.Void System.OutOfMemoryException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OutOfMemoryException__ctor_m155C798916559886F6DD9C93B414E5764C55FD52 (OutOfMemoryException_t2DF3EAC178583BD1DEFAAECBEDB2AF1EA86FBFC7 * __this, String_t* ___message0, Exception_t * ___innerException1, const RuntimeMethod* method);
// System.Void System.OverflowException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OverflowException__ctor_m15CD001EEB2E79D7497E101546B04D9A5520357E (OverflowException_tD89571E2350DE06D9DE4AB65ADCA77D607B5693D * __this, String_t* ___message0, Exception_t * ___innerException1, const RuntimeMethod* method);
// System.Void System.SystemException::.ctor(System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SystemException__ctor_mA18D2EA5642C066F35CB8C965398F9A542C33B0A (SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782 * __this, String_t* ___message0, Exception_t * ___innerException1, const RuntimeMethod* method);
// System.Void System.ArgumentException::.ctor(System.String,System.String,System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m8E5D9390593273B9774DBD2E967805E8EDE668E3 (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * __this, String_t* ___message0, String_t* ___paramName1, Exception_t * ___innerException2, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mF4626905368D6558695A823466A1AF65EADB9923 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
// System.Void System.ArgumentNullException::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_m9EA692D49986AEBAC433CE3381331146109AE20F (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * __this, String_t* ___paramName0, String_t* ___message1, const RuntimeMethod* method);
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentOutOfRangeException__ctor_m300CE4D04A068C209FD858101AC361C1B600B5AE (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * __this, String_t* ___paramName0, String_t* ___message1, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExceptionDelegate__ctor_m512E847FCB55576B6340A2669679D2D5179DDE8D (ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExceptionArgumentDelegate__ctor_mC3898DBFD267903C5001A38AD119C7DA5FE561F2 (ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacks_AuthUtil(Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_AuthUtil_mD13FDDDDC03DD49540B6AFFC935A58DE0B534762 (ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___applicationDelegate0, ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___arithmeticDelegate1, ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___divideByZeroDelegate2, ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___indexOutOfRangeDelegate3, ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___invalidCastDelegate4, ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___invalidOperationDelegate5, ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___ioDelegate6, ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___nullReferenceDelegate7, ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___outOfMemoryDelegate8, ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___overflowDelegate9, ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___systemExceptionDelegate10, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper::SWIGRegisterExceptionCallbacksArgument_AuthUtil(Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate,Firebase.Auth.AuthUtilPINVOKE/SWIGExceptionHelper/ExceptionArgumentDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_AuthUtil_m85D92F4B1140B7B2228AB92E057C747B9A07FCA7 (ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * ___argumentDelegate0, ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * ___argumentNullDelegate1, ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * ___argumentOutOfRangeDelegate2, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Enter(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Enter_m903755FCC479745619842CCDBF5E6355319FA102 (RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Exit(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2 (RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.String Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper::CreateString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SWIGStringHelper_CreateString_m8CFC15A5CFF34D0E23227F3DB4BD483E6B495134 (String_t* ___cString0, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGStringDelegate__ctor_m34FFAA789252BBCC866BE5712BD2072DEC39AA75 (SWIGStringDelegate_tF943E218400FF77BFF8F047D209AA7CA94915E00 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper::SWIGRegisterStringCallback_AuthUtil(Firebase.Auth.AuthUtilPINVOKE/SWIGStringHelper/SWIGStringDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGStringHelper_SWIGRegisterStringCallback_AuthUtil_mCF54A64D405E0EE541CB782629AE4D3EFFF01E69 (SWIGStringDelegate_tF943E218400FF77BFF8F047D209AA7CA94915E00 * ___stringDelegate0, const RuntimeMethod* method);
// System.Void System.Runtime.InteropServices.HandleRef::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HandleRef__ctor_mCD64E7F91766FE7602EE34A8CEEF6C1EF4C943BB (HandleRef_t876E76124F400D12395BF61D562162AB6822204A * __this, RuntimeObject * ___wrapper0, intptr_t ___handle1, const RuntimeMethod* method);
// System.Void Firebase.Auth.Credential::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Credential_Dispose_m78B018A97AF219BCDE1DCA819A2E94EA8BF85FC5 (Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * __this, bool ___disposing0, const RuntimeMethod* method);
// System.Void System.Object::Finalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Finalize_m4015B7D3A44DE125C5FE34D7276CD4697C06F380 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void System.GC::SuppressFinalize(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GC_SuppressFinalize_m037319A9B95A5BA437E806DE592802225EE5B425 (RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.IntPtr System.Runtime.InteropServices.HandleRef::get_Handle()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR intptr_t HandleRef_get_Handle_m095712F299EA36BF75975343C726FA160EE38277_inline (HandleRef_t876E76124F400D12395BF61D562162AB6822204A * __this, const RuntimeMethod* method);
// System.Boolean System.IntPtr::op_Inequality(System.IntPtr,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61 (intptr_t ___value10, intptr_t ___value21, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE::delete_Credential(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_delete_Credential_m02588ECD12EF7FBADE68921F2AC6F2DBA0E37C88 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.Boolean Firebase.Auth.AuthUtilPINVOKE::Credential_IsValid(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AuthUtilPINVOKE_Credential_IsValid_m590E8B8C5882B73A5D6C253E2E6591D789A61081 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.Void Firebase.LogUtil::InitializeLogging()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LogUtil_InitializeLogging_m3DC0D9A5186F8E0E1972761477DB5461FFCEC354 (const RuntimeMethod* method);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FacebookAuthProvider_GetCredential(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_FacebookAuthProvider_GetCredential_m6C17A5533AB95072B52FEFFEF1BC520C8B3B89BF (String_t* ___jarg10, const RuntimeMethod* method);
// System.Void Firebase.Auth.Credential::.ctor(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Credential__ctor_mBC968251CEBE306DF7CD89C85E74D1B1D451CFC7 (Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method);
// System.Void Firebase.Auth.FirebaseAuth::StateChangedFunction(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_StateChangedFunction_m3EB4E147700973B35AAB4C934E401A47B0098CB7 (intptr_t ___appCPtr0, const RuntimeMethod* method);
// System.Void Firebase.Auth.FirebaseAuth::IdTokenChangedFunction(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_IdTokenChangedFunction_m4867AD7FBED6DB5C1529D485866EB591AFC67BBF (intptr_t ___appCPtr0, const RuntimeMethod* method);
// System.Void Firebase.Auth.FirebaseAuth::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_Dispose_m31BB65B92A235E510C6FBC6B250BFE0D5D4D4A18 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, bool ___disposing0, const RuntimeMethod* method);
// System.Void Firebase.Auth.FirebaseAuth::DisposeInternal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_DisposeInternal_m5DD53E6F76144CC4B417157E6C3475F7704117DC (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,Firebase.Auth.FirebaseAuth>::TryGetValue(!0,!1&)
inline bool Dictionary_2_TryGetValue_mBF4E69D81E01649E50C6DFD9BFE384903C7B81C8 (Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * __this, intptr_t ___key0, FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D ** ___value1, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D *, intptr_t, FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D **, const RuntimeMethod*))Dictionary_2_TryGetValue_mB972A6602BDD10CBB612188FDED9917370EC3F17_gshared)(__this, ___key0, ___value1, method);
}
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IntPtr_op_Equality_mEE8D9FD2DFE312BBAA8B4ED3BF7976B3142A5934 (intptr_t ___value10, intptr_t ___value21, const RuntimeMethod* method);
// System.Void System.NullReferenceException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NullReferenceException__ctor_m7D46E331C349DD29CBA488C9B6A950A3E7DD5CAE (NullReferenceException_t204B194BC4DDA3259AF5A8633EA248AE5977ABDC * __this, const RuntimeMethod* method);
// System.Void Firebase.Auth.FirebaseAuth/<>c__DisplayClass19_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass19_0__ctor_mACC4F54E5C54C1F4E1983D751939FEE59B3EAE4D (U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * __this, const RuntimeMethod* method);
// System.Void Firebase.Auth.FirebaseAuth/<>c__DisplayClass19_1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass19_1__ctor_m1C29623F09C98E18908C68A9AAEA1F77D1CC820F (U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683 * __this, const RuntimeMethod* method);
// System.Runtime.InteropServices.HandleRef Firebase.FirebaseApp::getCPtr(Firebase.FirebaseApp)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HandleRef_t876E76124F400D12395BF61D562162AB6822204A  FirebaseApp_getCPtr_mF922A07C69469D0EB5A268A9278C495805BDCA4F (FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * ___obj0, const RuntimeMethod* method);
// Firebase.Auth.FirebaseAuth Firebase.Auth.FirebaseAuth::ProxyFromAppCPtr(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * FirebaseAuth_ProxyFromAppCPtr_mB07E3A48DDE2BFB10C206669CC172BAC2FE69A15 (intptr_t ___appCPtr0, const RuntimeMethod* method);
// System.Void Firebase.Auth.FirebaseAuth::LogHeartbeatInternal(Firebase.FirebaseApp)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_LogHeartbeatInternal_m39BE7DCAB1FCAC3F93F7366A80B69B5ADE4CC145 (FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * ___app0, const RuntimeMethod* method);
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action__ctor_m570E96B2A0C48BC1DC6788460316191F24572760 (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void Firebase.FirebaseApp::TranslateDllNotFoundException(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseApp_TranslateDllNotFoundException_mC6F04F02034B21A8A258012156CA7F263FB594F6 (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___closureToExecute0, const RuntimeMethod* method);
// System.Void Firebase.Auth.FirebaseAuth/StateChangedDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StateChangedDelegate__ctor_m5A465ECFE2F3056DE914C988625D85656F431F1C (StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.IntPtr Firebase.Auth.AuthUtil::CreateAuthStateListener(Firebase.Auth.FirebaseAuth,Firebase.Auth.FirebaseAuth/StateChangedDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtil_CreateAuthStateListener_m5B798BC3C73CF1288B517705947C8F968F49C828 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * ___auth0, StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294 * ___state_changed_delegate1, const RuntimeMethod* method);
// System.IntPtr Firebase.Auth.AuthUtil::CreateIdTokenListener(Firebase.Auth.FirebaseAuth,Firebase.Auth.FirebaseAuth/StateChangedDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtil_CreateIdTokenListener_m48BB536BB8AFB937461FEB96BB910727FAD27187 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * ___auth0, StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294 * ___state_changed_delegate1, const RuntimeMethod* method);
// System.Void System.EventHandler::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void Firebase.FirebaseApp::add_AppDisposed(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseApp_add_AppDisposed_m1CA5C0D303E4A5536DFDBE3356A900B3B966D4B6 (FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,Firebase.Auth.FirebaseAuth>::set_Item(!0,!1)
inline void Dictionary_2_set_Item_mABDE8D3E6C60B55410DDB2AA6FE39F9CD5D31BC9 (Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * __this, intptr_t ___key0, FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D *, intptr_t, FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D *, const RuntimeMethod*))Dictionary_2_set_Item_m26B2DBC3BE8767E18A97FFCF3FB2EFC766886BD4_gshared)(__this, ___key0, ___value1, method);
}
// System.Void Firebase.Auth.FirebaseAuth::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_Dispose_mDAE8E88D96D92B5D08A8E3165DFA4F3F4DCF30DA (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.IntPtr,Firebase.Auth.FirebaseAuth>::Remove(!0)
inline bool Dictionary_2_Remove_m11C98448049706AEDFE36C6B229DC8C1DA5AD74F (Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * __this, intptr_t ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D *, intptr_t, const RuntimeMethod*))Dictionary_2_Remove_m8F5E306A3F995390237D97553B63674C8D1D725F_gshared)(__this, ___key0, method);
}
// System.Void Firebase.FirebaseApp::remove_AppDisposed(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseApp_remove_AppDisposed_mE9EBC4903F93C997C038F8B12940A3D658624EA9 (FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method);
// System.Void Firebase.Auth.UserInfoInterface::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterface_Dispose_mE1B360122F9E6726A8FE40382AE6BD8683EAD61B (UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * __this, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtil::DestroyAuthStateListener(Firebase.Auth.FirebaseAuth,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtil_DestroyAuthStateListener_m37102E0B2C69B71C1517E7771899467C04916BC7 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * ___auth0, intptr_t ___listener1, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtil::DestroyIdTokenListener(Firebase.Auth.FirebaseAuth,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtil_DestroyIdTokenListener_m57CA413B20CBFF946AA457063051AF94A03ECF22 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * ___auth0, intptr_t ___listener1, const RuntimeMethod* method);
// System.Void Firebase.Auth.FirebaseAuth::ReleaseReferenceInternal(Firebase.Auth.FirebaseAuth)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_ReleaseReferenceInternal_mDCB0650BAC51CBB644F738FF72AF2B18D2F3862B (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * ___instance0, const RuntimeMethod* method);
// System.Void Firebase.Auth.FirebaseAuth/<>c__DisplayClass22_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass22_0__ctor_m3362924530F3A7340346D7C34BC4B0E66A70A061 (U3CU3Ec__DisplayClass22_0_t0B9E0A71713E1E0BCBE5C32D0A17C7786CF35818 * __this, const RuntimeMethod* method);
// System.Void Firebase.Auth.FirebaseAuth/<>c__DisplayClass22_1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass22_1__ctor_mE1858D9A2B9F52467C9D68DF38C2EFDCC99DE22D (U3CU3Ec__DisplayClass22_1_tE096CD1670F7DFF2DC0A9E52B629F80ED0B6ED1B * __this, const RuntimeMethod* method);
// System.Void Firebase.ExceptionAggregator::Wrap(System.Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExceptionAggregator_Wrap_m93A9BD0DC888F1D02E77860288095CB49D4CFFC2 (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___action0, const RuntimeMethod* method);
// System.Void System.Action`1<Firebase.Auth.FirebaseAuth>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m4089BBB88624AEE2FB2C8D495020C7FB36BDB2D3 (Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared)(__this, ___object0, ___method1, method);
}
// System.Void Firebase.Auth.FirebaseAuth::ForwardStateChange(System.IntPtr,System.Action`1<Firebase.Auth.FirebaseAuth>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_ForwardStateChange_m493936DA530F8A8DE041E7F2C8E1409B0924BE5C (intptr_t ___appCPtr0, Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * ___stateChangeClosure1, const RuntimeMethod* method);
// Firebase.FirebaseApp Firebase.FirebaseApp::get_DefaultInstance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * FirebaseApp_get_DefaultInstance_m46ACEAEC916D0AC3014302498F51607462EE5C63 (const RuntimeMethod* method);
// Firebase.Auth.FirebaseAuth Firebase.Auth.FirebaseAuth::GetAuth(Firebase.FirebaseApp)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * FirebaseAuth_GetAuth_mEFC135E9B1D37E4739D3E2A8D30A1C80DDE3F157 (FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * ___app0, const RuntimeMethod* method);
// System.Void Firebase.Auth.FirebaseAuth::add_stateChangedImpl(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_add_stateChangedImpl_m1387CE8F8F0F4796FFDF97ADD352101FEE55335C (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method);
// System.Void System.EventHandler::Invoke(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventHandler_Invoke_mD23D5EFEA562A05C5EACDD3E91EEDD2BF6C22800 (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * __this, RuntimeObject * ___sender0, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___e1, const RuntimeMethod* method);
// System.Void Firebase.Auth.FirebaseAuth::remove_stateChangedImpl(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_remove_stateChangedImpl_m6BC5FB994A66D606B5783D721FE3D17FD3D5B4D2 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method);
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1 (Delegate_t * ___a0, Delegate_t * ___b1, const RuntimeMethod* method);
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D (Delegate_t * ___source0, Delegate_t * ___value1, const RuntimeMethod* method);
// System.Runtime.InteropServices.HandleRef Firebase.Auth.FirebaseUser::getCPtr(Firebase.Auth.FirebaseUser)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HandleRef_t876E76124F400D12395BF61D562162AB6822204A  FirebaseUser_getCPtr_mE5B6DFE6A8DE44DC9F744D57A7863DBC40F3284D (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * ___obj0, const RuntimeMethod* method);
// Firebase.Auth.FirebaseUser Firebase.Auth.FirebaseAuth::get_CurrentUserInternal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * FirebaseAuth_get_CurrentUserInternal_mBA35503BDA1894FCF3E6A586BB0B68E34F148C74 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, const RuntimeMethod* method);
// Firebase.Auth.FirebaseUser Firebase.Auth.FirebaseAuth::UpdateCurrentUser(Firebase.Auth.FirebaseUser)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * FirebaseAuth_UpdateCurrentUser_mAA765D9A52031913DB1D3BBFD50DF4A4F5C4D4CF (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * ___proxy0, const RuntimeMethod* method);
// System.Void Firebase.Auth.FirebaseAuth/<>c__DisplayClass48_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass48_0__ctor_m2347AE3FAE11B4DFFD2348CD75D799A145AFEDCC (U3CU3Ec__DisplayClass48_0_t0332496AFB260910C4258BB3F40976B2A20A0312 * __this, const RuntimeMethod* method);
// System.Void Firebase.Auth.FirebaseAuth::ThrowIfNull()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_ThrowIfNull_m927509F7D041820C618B0061E723697AEDEFC3EC (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, const RuntimeMethod* method);
// System.Void System.Threading.Tasks.TaskCompletionSource`1<Firebase.Auth.FirebaseUser>::.ctor()
inline void TaskCompletionSource_1__ctor_m886A4697ACB84BF43814F0D64469B76A08FDFAC5 (TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * __this, const RuntimeMethod* method)
{
	((  void (*) (TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED *, const RuntimeMethod*))TaskCompletionSource_1__ctor_m894F4B43C47FD23EB3FD138E3CBBD71203DC5FA7_gshared)(__this, method);
}
// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Firebase.Auth.FirebaseAuth::SignInWithCredentialInternalAsync(Firebase.Auth.Credential)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * FirebaseAuth_SignInWithCredentialInternalAsync_mE39D174D69D03D600340190C92DCBE0390FEF28B (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * ___credential0, const RuntimeMethod* method);
// System.Void System.Action`1<System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_mE56C97A498D5E1CFF95301DEEF78027CD25448E1 (Action_1_t5D2D131226D740682F598DD09436558292112781 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t5D2D131226D740682F598DD09436558292112781 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared)(__this, ___object0, ___method1, method);
}
// System.Threading.Tasks.Task System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>::ContinueWith(System.Action`1<System.Threading.Tasks.Task`1<!0>>)
inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * Task_1_ContinueWith_mF2A8F0D71FCEA9B5D7B2A79A5DCF193DA524B01E (Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * __this, Action_1_t5D2D131226D740682F598DD09436558292112781 * ___continuationAction0, const RuntimeMethod* method)
{
	return ((  Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * (*) (Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 *, Action_1_t5D2D131226D740682F598DD09436558292112781 *, const RuntimeMethod*))Task_1_ContinueWith_mCFC2582CE8330039C62FC88152095FF923BAECE8_gshared)(__this, ___continuationAction0, method);
}
// System.Threading.Tasks.Task`1<!0> System.Threading.Tasks.TaskCompletionSource`1<Firebase.Auth.FirebaseUser>::get_Task()
inline Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * TaskCompletionSource_1_get_Task_m003148330C7CDC741C0872FC50DA059683141623_inline (TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * __this, const RuntimeMethod* method)
{
	return ((  Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * (*) (TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED *, const RuntimeMethod*))TaskCompletionSource_1_get_Task_mF1CB937DAB68DC43DA33893E95F48EFB07F385A0_gshared_inline)(__this, method);
}
// System.Void Firebase.Auth.FirebaseAuth/<>c__DisplayClass50_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass50_0__ctor_m91E489361C82BCFD50E78CF12F492893906F7B5A (U3CU3Ec__DisplayClass50_0_t3165B97C90B070A7EAE8BAE6F1E43B1CFD2F46C2 * __this, const RuntimeMethod* method);
// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Firebase.Auth.FirebaseAuth::SignInAnonymouslyInternalAsync()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * FirebaseAuth_SignInAnonymouslyInternalAsync_m03786609BE942FA6FEE38405709D18F9F501291C (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, const RuntimeMethod* method);
// System.Boolean System.Threading.Tasks.Task::get_IsCanceled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Task_get_IsCanceled_m42A47FCA2C6F33308A08C92C8489E802448F6F42 (Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * __this, const RuntimeMethod* method);
// System.Void System.Threading.Tasks.TaskCompletionSource`1<Firebase.Auth.FirebaseUser>::SetCanceled()
inline void TaskCompletionSource_1_SetCanceled_mE99DCDD6E5327656A075BA8D5EC8EAEF2F99AAB6 (TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * __this, const RuntimeMethod* method)
{
	((  void (*) (TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED *, const RuntimeMethod*))TaskCompletionSource_1_SetCanceled_m9BD3702F5D2F7D1C0D9C7E4598B758F46B734B65_gshared)(__this, method);
}
// System.Boolean System.Threading.Tasks.Task::get_IsFaulted()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Task_get_IsFaulted_m7337D2694F4BF380C5B8893B4A924D7F0E762A48 (Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * __this, const RuntimeMethod* method);
// System.AggregateException System.Threading.Tasks.Task::get_Exception()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AggregateException_t9217B9E89DF820D5632411F2BD92F444B17BD60E * Task_get_Exception_mA61AAD3E52CBEB631D1956217B521456E7960B95 (Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * __this, const RuntimeMethod* method);
// System.Void Firebase.Internal.TaskCompletionSourceCompat`1<Firebase.Auth.FirebaseUser>::SetException(System.Threading.Tasks.TaskCompletionSource`1<!0>,System.AggregateException)
inline void TaskCompletionSourceCompat_1_SetException_m60E20FA51C992BE0F3CD0F9BA9C20887A1204F1D (TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * ___tcs0, AggregateException_t9217B9E89DF820D5632411F2BD92F444B17BD60E * ___exception1, const RuntimeMethod* method)
{
	((  void (*) (TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED *, AggregateException_t9217B9E89DF820D5632411F2BD92F444B17BD60E *, const RuntimeMethod*))TaskCompletionSourceCompat_1_SetException_m534DA13EA8E0D72BDF334398BCE2448B4C25E233_gshared)(___tcs0, ___exception1, method);
}
// !0 System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>::get_Result()
inline FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * Task_1_get_Result_m547A389B49652DF34BED42F12292E2483FC2059D (Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * __this, const RuntimeMethod* method)
{
	return ((  FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * (*) (Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 *, const RuntimeMethod*))Task_1_get_Result_m828E969718CE9AD38335D3F48B6F9C84AB98DEE9_gshared)(__this, method);
}
// System.Void System.Threading.Tasks.TaskCompletionSource`1<Firebase.Auth.FirebaseUser>::SetResult(!0)
inline void TaskCompletionSource_1_SetResult_mE9B14A44C05A19AB24B379A0CB6C4EC6F21BF410 (TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * __this, FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * ___result0, const RuntimeMethod* method)
{
	((  void (*) (TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED *, FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *, const RuntimeMethod*))TaskCompletionSource_1_SetResult_m0EBFE5138C570B9515B706987C2983D4DFBE5337_gshared)(__this, ___result0, method);
}
// System.Runtime.InteropServices.HandleRef Firebase.Auth.Credential::getCPtr(Firebase.Auth.Credential)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HandleRef_t876E76124F400D12395BF61D562162AB6822204A  Credential_getCPtr_m892678A92D70544624773536D6E26DDEDFE63E42 (Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * ___obj0, const RuntimeMethod* method);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseAuth_SignInWithCredentialInternal(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_FirebaseAuth_SignInWithCredentialInternal_m286B534BC062DE3FD14436764BCE057923965B67 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg21, const RuntimeMethod* method);
// System.Void Firebase.Auth.Future_User::.ctor(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Future_User__ctor_mF7A226B0665A25144444D5F951A9C29D7F7D755A (Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method);
// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Firebase.Auth.Future_User::GetTask(Firebase.Auth.Future_User)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * Future_User_GetTask_mF54DC9C5FC9DD22F21D53B3E36FC8BE82023A506 (Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * ___fu0, const RuntimeMethod* method);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseAuth_SignInAnonymouslyInternal(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_FirebaseAuth_SignInAnonymouslyInternal_m769C6AFE4B52A39BFFB28D8298F6664514DB39C6 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE::FirebaseAuth_SignOut(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_FirebaseAuth_SignOut_m03EDFAB11CF0A459DD7CA6A3E097790BA3D91458 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseAuth_GetAuthInternal(System.Runtime.InteropServices.HandleRef,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_FirebaseAuth_GetAuthInternal_m9CA73CC68A18B4B86AFFB925E37E8F6A0DAE72AA (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, int32_t* ___jarg21, const RuntimeMethod* method);
// System.Void Firebase.Auth.FirebaseAuth::.ctor(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth__ctor_m5A11B7E7ACB1FF9296510387F4A203804EC9B5D0 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE::FirebaseAuth_LogHeartbeatInternal(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_FirebaseAuth_LogHeartbeatInternal_m40FB5B32F8D0549946D5FA26631C4D88338C6D5C (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE::FirebaseAuth_ReleaseReferenceInternal(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_FirebaseAuth_ReleaseReferenceInternal_m77FAC25E87B2644CA0EF2B9BFF0201C2F38349D9 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseAuth_CurrentUserInternal_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_FirebaseAuth_CurrentUserInternal_get_m1646DA22D3C6243710AFD36C183194346B494495 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.Void Firebase.Auth.FirebaseUser::.ctor(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseUser__ctor_m19BF183B130EA0D6A400A715A62A07877B7F15D5 (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.IntPtr,Firebase.Auth.FirebaseAuth>::.ctor()
inline void Dictionary_2__ctor_mD0C4E66AB396EE592D08876CA7D94D68B67B90A1 (Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D *, const RuntimeMethod*))Dictionary_2__ctor_m901D4901A751B7C8F3CCD2608AA792ADBF53B75A_gshared)(__this, method);
}
// System.Void Firebase.Auth.FirebaseAuth/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m17B1FF4992C7A229A6F9003C935994D8A8687BCB (U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF * __this, const RuntimeMethod* method);
// Firebase.Auth.FirebaseAuth Firebase.Auth.FirebaseAuth::GetAuthInternal(Firebase.FirebaseApp,Firebase.InitResult&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * FirebaseAuth_GetAuthInternal_mC4406E480123387B5EBE17F44BBB61A469EE059D (FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * ___app0, int32_t* ___init_result_out1, const RuntimeMethod* method);
// System.Void Firebase.InitializationException::.ctor(Firebase.InitResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InitializationException__ctor_m3BBD8A30C4B1757E8686AB762DBA7C7B6CB81591 (InitializationException_t4950F6485639018AF98C04DD016B37F1745F8098 * __this, int32_t ___result0, const RuntimeMethod* method);
// System.Void System.Action`1<Firebase.Auth.FirebaseAuth>::Invoke(!0)
inline void Action_1_Invoke_mF026A39A2533644C539653019481EDEAE3F2BB04 (Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * __this, FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * ___obj0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E *, FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D *, const RuntimeMethod*))Action_1_Invoke_mB86FC1B303E77C41ED0E94FC3592A9CF8DA571D5_gshared)(__this, ___obj0, method);
}
// System.Void Firebase.Auth.FirebaseAuth::CompleteFirebaseUserTask(System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>,System.Threading.Tasks.TaskCompletionSource`1<Firebase.Auth.FirebaseUser>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_CompleteFirebaseUserTask_mD79D870B498E279B1654740125368DD6A6FB8BFF (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * ___task0, TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * ___taskCompletionSource1, const RuntimeMethod* method);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_SWIGUpcast(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_FirebaseUser_SWIGUpcast_m2E75C4601FD7E308456F2F0BF75ECB73F92D0E44 (intptr_t ___jarg10, const RuntimeMethod* method);
// System.Void Firebase.Auth.UserInfoInterface::.ctor(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterface__ctor_m7E75B0EBDFD46089D31A77871A8B74520EEBC04C (UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE::delete_FirebaseUser(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_delete_FirebaseUser_m21C9A5C887AD120B4228AC2E15E7EA499F745A9F (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.Void Firebase.Auth.UserInfoInterface::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterface_Dispose_m8532EF4241914613A205B8E2E1720C39FD85C1AA (UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * __this, bool ___disposing0, const RuntimeMethod* method);
// System.String Firebase.Auth.FirebaseUser::get_PhotoUrlInternal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FirebaseUser_get_PhotoUrlInternal_mAAE831A4F08E1CD94A486970B5F029F7D1D1216F (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * __this, const RuntimeMethod* method);
// System.Uri Firebase.FirebaseApp::UrlStringToUri(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * FirebaseApp_UrlStringToUri_m09F6D262D86EE9898214D35510E1E8CEBF53D0DA (String_t* ___urlString0, const RuntimeMethod* method);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_UpdateEmail(System.Runtime.InteropServices.HandleRef,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_FirebaseUser_UpdateEmail_mE4BC3966DE87BDA371C7CE7A0F23D91BEC313CCB (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, String_t* ___jarg21, const RuntimeMethod* method);
// System.Void Firebase.FutureVoid::.ctor(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FutureVoid__ctor_mE27D03F52C5F3CE33751637C530DB89045EE97DA (FutureVoid_tF512775335C40422BAC529CBF374A246FD8ED51D * __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method);
// System.Threading.Tasks.Task Firebase.FutureVoid::GetTask(Firebase.FutureVoid)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * FutureVoid_GetTask_m81734F11961D1BC3DD5D3E313B006EA275C06B83 (FutureVoid_tF512775335C40422BAC529CBF374A246FD8ED51D * ___fu0, const RuntimeMethod* method);
// System.Runtime.InteropServices.HandleRef Firebase.Auth.UserProfile::getCPtr(Firebase.Auth.UserProfile)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HandleRef_t876E76124F400D12395BF61D562162AB6822204A  UserProfile_getCPtr_m43D66A8F09ED56F3026B25C5DFEEEA8A3BEB7CEC (UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * ___obj0, const RuntimeMethod* method);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_UpdateUserProfile(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_FirebaseUser_UpdateUserProfile_m828FCDD053CA923BA34246E6679C39F6BCBF076C (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg21, const RuntimeMethod* method);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_LinkWithCredential(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_FirebaseUser_LinkWithCredential_mBC153FDCFAC9320B9628591B105955EFCCA5E03F (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg21, const RuntimeMethod* method);
// System.String Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_DisplayName_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AuthUtilPINVOKE_FirebaseUser_DisplayName_get_mDD546C27F066D7A7C9F9612FB9CC18C6E8D9E2BF (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.String Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_Email_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AuthUtilPINVOKE_FirebaseUser_Email_get_m1C74E1D469EF27F32E2810738DBF9CA1BA23582D (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.Boolean Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_IsAnonymous_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AuthUtilPINVOKE_FirebaseUser_IsAnonymous_get_m62129D05B6F8B37AF44E526484F3F44094D907D8 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.String Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_PhotoUrlInternal_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AuthUtilPINVOKE_FirebaseUser_PhotoUrlInternal_get_m12B7DDC9E668B2B40694C4DEB10A2C4EAC81FD00 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_ProviderData_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_FirebaseUser_ProviderData_get_mD388B7BE41C3645C7464C3C0DAA81290CF2CED47 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.Void Firebase.Auth.UserInfoInterfaceList::.ctor(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterfaceList__ctor_m969BCAE5B0171B9F24C435611C2D57CEF93C4F54 (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Firebase.Auth.IUserInfo>::.ctor()
inline void List_1__ctor_m93A21423354207CBD75E21125B75C91D9C17FE60 (List_1_tDAC656203301889991AA49AC3FC48456D0214689 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tDAC656203301889991AA49AC3FC48456D0214689 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// Firebase.Auth.UserInfoInterfaceList/UserInfoInterfaceListEnumerator Firebase.Auth.UserInfoInterfaceList::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A * UserInfoInterfaceList_GetEnumerator_m34C89390756E8DCA0D4FDE9EC4020AF7AFBB4021 (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, const RuntimeMethod* method);
// Firebase.Auth.UserInfoInterface Firebase.Auth.UserInfoInterfaceList/UserInfoInterfaceListEnumerator::get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * UserInfoInterfaceListEnumerator_get_Current_mE95C0E6329FA074B3BB193C170A0B46DD8A6A720 (UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Firebase.Auth.IUserInfo>::Add(!0)
inline void List_1_Add_m52A7A5A55C7C1AB245A219997212D466AFA9176D (List_1_tDAC656203301889991AA49AC3FC48456D0214689 * __this, RuntimeObject* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tDAC656203301889991AA49AC3FC48456D0214689 *, RuntimeObject*, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, ___item0, method);
}
// System.Boolean Firebase.Auth.UserInfoInterfaceList/UserInfoInterfaceListEnumerator::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UserInfoInterfaceListEnumerator_MoveNext_mB0E76F84C535182420B86E45C821E100E902F0A9 (UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A * __this, const RuntimeMethod* method);
// System.String Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_UserId_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AuthUtilPINVOKE_FirebaseUser_UserId_get_mA5AC5F6494A1F4A6A3F0DBC90E0D4D1006F5CF5A (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.Void Firebase.Auth.Future_User::SWIG_CompletionDispatcher(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Future_User_SWIG_CompletionDispatcher_m9608732E8FCF7D93C2A70B63BA750D3231DCAD47 (int32_t ___key0, const RuntimeMethod* method);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::Future_User_SWIGUpcast(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_Future_User_SWIGUpcast_m9F5C5F23C6908B207633FFC69E6B33329CCDD5FC (intptr_t ___jarg10, const RuntimeMethod* method);
// System.Void Firebase.FutureBase::.ctor(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FutureBase__ctor_mC05DD84980454E9B8210087E01926E5D56FC017F (FutureBase_t75BACF0EF2911C731F998E85086202A1A2F55397 * __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method);
// System.Void Firebase.Auth.Future_User::SetCompletionData(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Future_User_SetCompletionData_m7B693E6CE4A587D18797FE4A21D00A4C823DB7BC (Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * __this, intptr_t ___data0, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE::delete_Future_User(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_delete_Future_User_m09EF8AC9392014A450B9A39F9237AB703460AE42 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.Void Firebase.FutureBase::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FutureBase_Dispose_mD60CE0048F80E6E0FF167F3D088F949F10217BBF (FutureBase_t75BACF0EF2911C731F998E85086202A1A2F55397 * __this, bool ___disposing0, const RuntimeMethod* method);
// System.Void Firebase.Auth.Future_User/<>c__DisplayClass4_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass4_0__ctor_m69741D9DAF70E435DFBFF0CFFA06B08796349300 (U3CU3Ec__DisplayClass4_0_t89802218D77EADC8D4FDB0DD3CF8FC5447E97B8D * __this, const RuntimeMethod* method);
// System.Boolean Firebase.Auth.AuthUtilPINVOKE/SWIGPendingException::get_Pending()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SWIGPendingException_get_Pending_m746EAD61F79AF2A98C4350069D4507EAC1D251DE (const RuntimeMethod* method);
// System.Void System.Threading.Tasks.TaskCompletionSource`1<Firebase.Auth.FirebaseUser>::SetException(System.Exception)
inline void TaskCompletionSource_1_SetException_m0F8FFC0B85AFA0B942C0966332B7E3275CF42A1B (TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * __this, Exception_t * ___exception0, const RuntimeMethod* method)
{
	((  void (*) (TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED *, Exception_t *, const RuntimeMethod*))TaskCompletionSource_1_SetException_mF32CD1B09A692369399D38F7B54F9FD2EC5394CE_gshared)(__this, ___exception0, method);
}
// Firebase.FutureStatus Firebase.FutureBase::status()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FutureBase_status_mD675FFD3FAE10D36169C16590EE3C6F1861DD136 (FutureBase_t75BACF0EF2911C731F998E85086202A1A2F55397 * __this, const RuntimeMethod* method);
// System.Void Firebase.FirebaseException::.ctor(System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseException__ctor_mB31E783657F428BA72B055DF8A4CE6EA6996996C (FirebaseException_t947F51F8FDF9CB2D145DC24E92160DC67BF12AEF * __this, int32_t ___errorCode0, String_t* ___message1, const RuntimeMethod* method);
// System.Void Firebase.Auth.Future_User/Action::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action__ctor_m9AE0C8C97132B6FFDB6D84FDC9A8802717222F39 (Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void Firebase.Auth.Future_User::SetOnCompletionCallback(Firebase.Auth.Future_User/Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Future_User_SetOnCompletionCallback_mA4F7DBBBBBA68E9A54C2231034CCE80415DD80A6 (Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * __this, Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327 * ___userCompletionCallback0, const RuntimeMethod* method);
// System.Void System.ArgumentNullException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * __this, String_t* ___paramName0, const RuntimeMethod* method);
// System.Void Firebase.Auth.Future_User::ThrowIfDisposed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Future_User_ThrowIfDisposed_m963BF98F552F3F37DFEC0AEB1DF07207809835DE (Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * __this, const RuntimeMethod* method);
// System.Void Firebase.Auth.Future_User/SWIG_CompletionDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIG_CompletionDelegate__ctor_m9FCE4E269635690B019F5FF509AFF343FA13DBDC (SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::.ctor()
inline void Dictionary_2__ctor_m0CBFF732991FC69E50314435AC3E598F357091AE (Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A *, const RuntimeMethod*))Dictionary_2__ctor_m7D745ADE56151C2895459668F4A4242985E526D8_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::set_Item(!0,!1)
inline void Dictionary_2_set_Item_m076684AAD9154CBFD0ACA5E778B83C7D2443BA8F (Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A * __this, int32_t ___key0, Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327 * ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A *, int32_t, Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327 *, const RuntimeMethod*))Dictionary_2_set_Item_mF9A6FBE4006C89D15B8C88B2CB46E9B24D18B7FC_gshared)(__this, ___key0, ___value1, method);
}
// System.IntPtr Firebase.Auth.Future_User::SWIG_OnCompletion(Firebase.Auth.Future_User/SWIG_CompletionDelegate,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t Future_User_SWIG_OnCompletion_mD2853E95978B586313F3D904FEDA65F0DC04B44A (Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * __this, SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2 * ___cs_callback0, int32_t ___cs_key1, const RuntimeMethod* method);
// System.Void Firebase.Auth.Future_User::SWIG_FreeCompletionData(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Future_User_SWIG_FreeCompletionData_mE5A33CAC1849B25DD160DC042A980BFDB6F7D45A (Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * __this, intptr_t ___data0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::TryGetValue(!0,!1&)
inline bool Dictionary_2_TryGetValue_m2C05B94C421C439F201A3C24298D040A66655D23 (Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A * __this, int32_t ___key0, Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327 ** ___value1, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A *, int32_t, Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327 **, const RuntimeMethod*))Dictionary_2_TryGetValue_m867F6DA953678D0333A55270B7C6EF38EFC293FF_gshared)(__this, ___key0, ___value1, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Firebase.Auth.Future_User/Action>::Remove(!0)
inline bool Dictionary_2_Remove_m8D084F34E8CA42993A2B124C27B12C0520B916BD (Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A * __this, int32_t ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A *, int32_t, const RuntimeMethod*))Dictionary_2_Remove_m2204D6D532702FD13AB2A9AD8DB538E4E8FB1913_gshared)(__this, ___key0, method);
}
// System.Void Firebase.Auth.Future_User/Action::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_Invoke_m8B7DBC95C4F8A5255426959F447C3FDDC7DFB2B5 (Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327 * __this, const RuntimeMethod* method);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::Future_User_SWIG_OnCompletion(System.Runtime.InteropServices.HandleRef,Firebase.Auth.Future_User/SWIG_CompletionDelegate,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_Future_User_SWIG_OnCompletion_mA4865E7D9C1E147E26B7FD5F81078FA99F7ADF32 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2 * ___jarg21, int32_t ___jarg32, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE::Future_User_SWIG_FreeCompletionData(System.Runtime.InteropServices.HandleRef,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_Future_User_SWIG_FreeCompletionData_m4E8D0B09D212ED4E404E86B97439D0AF51879DB1 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, intptr_t ___jarg21, const RuntimeMethod* method);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::Future_User_GetResult(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_Future_User_GetResult_m36CFE956376E3A4DC187BCC9D6F2732417615FE9 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.Int32 Firebase.FutureBase::error()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FutureBase_error_m70CFE004FF942C34ED11D221D0B6033AD40C1825 (FutureBase_t75BACF0EF2911C731F998E85086202A1A2F55397 * __this, const RuntimeMethod* method);
// System.String Firebase.FutureBase::error_message()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FutureBase_error_message_m12EFFA84132AB25450D3D3F349A414EF597CCCE3 (FutureBase_t75BACF0EF2911C731F998E85086202A1A2F55397 * __this, const RuntimeMethod* method);
// Firebase.Auth.FirebaseUser Firebase.Auth.Future_User::GetResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * Future_User_GetResult_mE1A953E51D10BE4DD15137A78F45FFAA9D566902 (Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m0ACDD8B34764E4040AED0B3EEB753567E4576BFA (String_t* ___format0, RuntimeObject * ___arg01, const RuntimeMethod* method);
// System.Void Firebase.LogUtil::LogMessage(Firebase.LogLevel,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LogUtil_LogMessage_mE0D4736B7C636462B2AD246F65EFAF5F1785822F (int32_t ___logLevel0, String_t* ___message1, const RuntimeMethod* method);
// System.Void Firebase.FutureBase::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FutureBase_Dispose_mE480130D78254414AA6A59E2FA63B9AFC4AC01BC (FutureBase_t75BACF0EF2911C731F998E85086202A1A2F55397 * __this, const RuntimeMethod* method);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::PlayGamesAuthProvider_GetCredential(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_PlayGamesAuthProvider_GetCredential_mF15F71E21D192B4940648E872A8F7F8E8713A0CA (String_t* ___jarg10, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE::delete_UserInfoInterface(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_delete_UserInfoInterface_m6EECB261E56A9D7F8BB61C69988423AD41B16747 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.String Firebase.Auth.UserInfoInterface::get_PhotoUrlInternal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UserInfoInterface_get_PhotoUrlInternal_m9F70731EE318CA6D5F39817B161185E198BAC024 (UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * __this, const RuntimeMethod* method);
// System.String Firebase.Auth.AuthUtilPINVOKE::UserInfoInterface_UserId_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AuthUtilPINVOKE_UserInfoInterface_UserId_get_m3B7A2BC624181BC02ACF3481BF437903B1EFC954 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.String Firebase.Auth.AuthUtilPINVOKE::UserInfoInterface_Email_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AuthUtilPINVOKE_UserInfoInterface_Email_get_m49D0AB8130C3E528C1D44B66CC7D681B2F2E804B (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.String Firebase.Auth.AuthUtilPINVOKE::UserInfoInterface_DisplayName_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AuthUtilPINVOKE_UserInfoInterface_DisplayName_get_m40F5D1957524C394F0A8022F47B399931BE4DFF7 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.String Firebase.Auth.AuthUtilPINVOKE::UserInfoInterface_PhotoUrlInternal_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AuthUtilPINVOKE_UserInfoInterface_PhotoUrlInternal_get_mA1C69290B7452547D58BA4675DAE501090CCA5B7 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.String Firebase.Auth.AuthUtilPINVOKE::UserInfoInterface_ProviderId_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AuthUtilPINVOKE_UserInfoInterface_ProviderId_get_mB0AC6D00DCC186EB3D315809B0727D666F24C9B0 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE::delete_UserInfoInterfaceList(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_delete_UserInfoInterfaceList_mF4C1DD6531A015AC1E86A763A37E9E2EDC5B1A51 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// Firebase.Auth.UserInfoInterface Firebase.Auth.UserInfoInterfaceList::getitem(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * UserInfoInterfaceList_getitem_mFD9B0EB9CA5CA925534712BDF225DEE3B2439CEA (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void Firebase.Auth.UserInfoInterfaceList::setitem(System.Int32,Firebase.Auth.UserInfoInterface)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterfaceList_setitem_m87AC7CBE94A90F753BCF80E9658D7FD0C9A8F80F (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, int32_t ___index0, UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * ___val1, const RuntimeMethod* method);
// System.UInt32 Firebase.Auth.UserInfoInterfaceList::size()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t UserInfoInterfaceList_size_m370AE0FA3028E3D76252D636A6274D1FBD12BC6B (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, const RuntimeMethod* method);
// System.Int32 Firebase.Auth.UserInfoInterfaceList::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UserInfoInterfaceList_get_Count_mE191B3C8A1BDBE827045B98CAC48447631613074 (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, const RuntimeMethod* method);
// System.Void Firebase.Auth.UserInfoInterfaceList::CopyTo(System.Int32,Firebase.Auth.UserInfoInterface[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterfaceList_CopyTo_mA6612EBF0827029C8CBF0CEE383D0FFB11D32B9E (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, int32_t ___index0, UserInfoInterfaceU5BU5D_tD810A76761E9D37FB729A09C273728E21098926B* ___array1, int32_t ___arrayIndex2, int32_t ___count3, const RuntimeMethod* method);
// System.Int32 System.Array::get_Rank()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Array_get_Rank_m38145B59D67D75F9896A3F8CDA9B966641AE99E1 (RuntimeArray * __this, const RuntimeMethod* method);
// System.Void System.ArgumentException::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m26DC3463C6F3C98BF33EA39598DD2B32F0249CA8 (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * __this, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method);
// System.Void System.ArgumentException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m9A85EF7FEFEC21DDD525A67E831D77278E5165B7 (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * __this, String_t* ___message0, const RuntimeMethod* method);
// Firebase.Auth.UserInfoInterface Firebase.Auth.UserInfoInterfaceList::getitemcopy(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * UserInfoInterfaceList_getitemcopy_m886AA8C76520C49BFC25FEE8AFC1C2767BBA2A00 (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Array::SetValue(System.Object,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_SetValue_m3C6811CE9C45D1E461404B5D2FBD4EC1A054FDCA (RuntimeArray * __this, RuntimeObject * ___value0, int32_t ___index1, const RuntimeMethod* method);
// System.Void Firebase.Auth.UserInfoInterfaceList/UserInfoInterfaceListEnumerator::.ctor(Firebase.Auth.UserInfoInterfaceList)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterfaceListEnumerator__ctor_mB52655D936C618ADDC4D506EF165F8ADAFAF350E (UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A * __this, UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * ___collection0, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_Clear(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_UserInfoInterfaceList_Clear_m0477B48FDCFEEC92FC007E3F43EC65647F9E240A (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.Runtime.InteropServices.HandleRef Firebase.Auth.UserInfoInterface::getCPtr(Firebase.Auth.UserInfoInterface)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HandleRef_t876E76124F400D12395BF61D562162AB6822204A  UserInfoInterface_getCPtr_m15659A46894D5F64CC2EE953C4F5D97DCB243680 (UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * ___obj0, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_Add(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_UserInfoInterfaceList_Add_m235699A22C7864EC52A0A694E8549F1C4C1AE089 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg21, const RuntimeMethod* method);
// System.UInt32 Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_size(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t AuthUtilPINVOKE_UserInfoInterfaceList_size_m4022FD61AA1F01B2682E30DD2660D39144EE3875 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_getitemcopy(System.Runtime.InteropServices.HandleRef,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_UserInfoInterfaceList_getitemcopy_mB07EF1F7557FF0370E3ACE819AEE2612D4CB524D (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, int32_t ___jarg21, const RuntimeMethod* method);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_getitem(System.Runtime.InteropServices.HandleRef,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_UserInfoInterfaceList_getitem_m57D02F5BE29828EB28F2B8B3DC5591207B34BEE9 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, int32_t ___jarg21, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_setitem(System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_UserInfoInterfaceList_setitem_mCB2A5B2BF72BD7E40187C4B07E9CC8FA9344A441 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, int32_t ___jarg21, HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg32, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_Insert(System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_UserInfoInterfaceList_Insert_mF8231269D3DB49CE00D815C59748E6D77C5E8A6D (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, int32_t ___jarg21, HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg32, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_RemoveAt(System.Runtime.InteropServices.HandleRef,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_UserInfoInterfaceList_RemoveAt_m3E7CC088E5D8F62308D0A7193D34DE0DA9215179 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, int32_t ___jarg21, const RuntimeMethod* method);
// System.Boolean Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_Contains(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AuthUtilPINVOKE_UserInfoInterfaceList_Contains_mACD9BBB51A6B1007C9107D484C7963ACC01C1803 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg21, const RuntimeMethod* method);
// System.Int32 Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_IndexOf(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AuthUtilPINVOKE_UserInfoInterfaceList_IndexOf_mC0DFA98501EF7C4F2D513ACF7B513877B71E69D3 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg21, const RuntimeMethod* method);
// System.Boolean Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_Remove(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AuthUtilPINVOKE_UserInfoInterfaceList_Remove_m58DD7F22CAFF88AABC98EBB2B4003720BCDEE7BC (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg21, const RuntimeMethod* method);
// System.Void System.InvalidOperationException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvalidOperationException__ctor_m72027D5F1D513C25C05137E203EEED8FD8297706 (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * __this, String_t* ___message0, const RuntimeMethod* method);
// Firebase.Auth.UserInfoInterface Firebase.Auth.UserInfoInterfaceList::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * UserInfoInterfaceList_get_Item_m2CF8D552EFD6768C39D2579B76356659F2C570DD (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void Firebase.Auth.UserProfile::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserProfile_Dispose_m5BC283516965B4FC98A8AF63957F03D035DD94C3 (UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * __this, bool ___disposing0, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE::delete_UserProfile(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_delete_UserProfile_mEBF4B6D168A27F787149E7D71C5F6DA18AA6104E (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method);
// System.String Firebase.FirebaseApp::UriToUrlString(System.Uri)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FirebaseApp_UriToUrlString_m2BB38FE415141D88AE4884175162ADA40561AE26 (Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___uri0, const RuntimeMethod* method);
// System.Void Firebase.Auth.UserProfile::set_PhotoUrlInternal(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserProfile_set_PhotoUrlInternal_mED2EA1583FC42D01FE71BA105537EDF6E2019363 (UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::new_UserProfile()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_new_UserProfile_mF862E0AB1127CCB19BEA8E520C8EC4E4AD85FF1C (const RuntimeMethod* method);
// System.Void Firebase.Auth.UserProfile::.ctor(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserProfile__ctor_m3F41707B2949CA717EB71681388E002A3106801A (UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE::UserProfile_DisplayName_set(System.Runtime.InteropServices.HandleRef,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_UserProfile_DisplayName_set_m2E3AA6A6AFEED65380B898B3BFD7FDD033CB5B59 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, String_t* ___jarg21, const RuntimeMethod* method);
// System.Void Firebase.Auth.AuthUtilPINVOKE::UserProfile_PhotoUrlInternal_set(System.Runtime.InteropServices.HandleRef,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_UserProfile_PhotoUrlInternal_set_mA5391563F2C002A4D053CF4BC28C2B7548E699B0 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, String_t* ___jarg21, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.IntPtr Firebase.Auth.AuthUtil::CreateAuthStateListener(Firebase.Auth.FirebaseAuth,Firebase.Auth.FirebaseAuth_StateChangedDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtil_CreateAuthStateListener_m5B798BC3C73CF1288B517705947C8F968F49C828 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * ___auth0, StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294 * ___state_changed_delegate1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthUtil_CreateAuthStateListener_m5B798BC3C73CF1288B517705947C8F968F49C828_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	intptr_t V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_0 = ___auth0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_1 = FirebaseAuth_getCPtr_m43139A9D3674A9DFF8AED10B43D345217CE378C9(L_0, /*hidden argument*/NULL);
		StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294 * L_2 = ___state_changed_delegate1;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		intptr_t L_3 = AuthUtilPINVOKE_CreateAuthStateListener_m093D034502CF32CE850E6A9E286C15EAE6785C2B(L_1, L_2, /*hidden argument*/NULL);
		V_0 = (intptr_t)L_3;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_4 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_6 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, AuthUtil_CreateAuthStateListener_m5B798BC3C73CF1288B517705947C8F968F49C828_RuntimeMethod_var);
	}

IL_001d:
	{
		intptr_t L_7 = V_0;
		V_2 = (intptr_t)L_7;
		goto IL_0021;
	}

IL_0021:
	{
		intptr_t L_8 = V_2;
		return (intptr_t)L_8;
	}
}
// System.Void Firebase.Auth.AuthUtil::DestroyAuthStateListener(Firebase.Auth.FirebaseAuth,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtil_DestroyAuthStateListener_m37102E0B2C69B71C1517E7771899467C04916BC7 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * ___auth0, intptr_t ___listener1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthUtil_DestroyAuthStateListener_m37102E0B2C69B71C1517E7771899467C04916BC7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_0 = ___auth0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_1 = FirebaseAuth_getCPtr_m43139A9D3674A9DFF8AED10B43D345217CE378C9(L_0, /*hidden argument*/NULL);
		intptr_t L_2 = ___listener1;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		AuthUtilPINVOKE_DestroyAuthStateListener_m632301CCA03C97183D31D2E819196742BBB8BB1B(L_1, (intptr_t)L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_3 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_0 = L_3;
		bool L_4 = V_0;
		if (!L_4)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_5 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, AuthUtil_DestroyAuthStateListener_m37102E0B2C69B71C1517E7771899467C04916BC7_RuntimeMethod_var);
	}

IL_001d:
	{
		return;
	}
}
// System.IntPtr Firebase.Auth.AuthUtil::CreateIdTokenListener(Firebase.Auth.FirebaseAuth,Firebase.Auth.FirebaseAuth_StateChangedDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtil_CreateIdTokenListener_m48BB536BB8AFB937461FEB96BB910727FAD27187 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * ___auth0, StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294 * ___state_changed_delegate1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthUtil_CreateIdTokenListener_m48BB536BB8AFB937461FEB96BB910727FAD27187_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	intptr_t V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_0 = ___auth0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_1 = FirebaseAuth_getCPtr_m43139A9D3674A9DFF8AED10B43D345217CE378C9(L_0, /*hidden argument*/NULL);
		StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294 * L_2 = ___state_changed_delegate1;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		intptr_t L_3 = AuthUtilPINVOKE_CreateIdTokenListener_mA59101F16559583539311A7139734B88B26CF10B(L_1, L_2, /*hidden argument*/NULL);
		V_0 = (intptr_t)L_3;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_4 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_6 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, AuthUtil_CreateIdTokenListener_m48BB536BB8AFB937461FEB96BB910727FAD27187_RuntimeMethod_var);
	}

IL_001d:
	{
		intptr_t L_7 = V_0;
		V_2 = (intptr_t)L_7;
		goto IL_0021;
	}

IL_0021:
	{
		intptr_t L_8 = V_2;
		return (intptr_t)L_8;
	}
}
// System.Void Firebase.Auth.AuthUtil::DestroyIdTokenListener(Firebase.Auth.FirebaseAuth,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtil_DestroyIdTokenListener_m57CA413B20CBFF946AA457063051AF94A03ECF22 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * ___auth0, intptr_t ___listener1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthUtil_DestroyIdTokenListener_m57CA413B20CBFF946AA457063051AF94A03ECF22_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_0 = ___auth0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_1 = FirebaseAuth_getCPtr_m43139A9D3674A9DFF8AED10B43D345217CE378C9(L_0, /*hidden argument*/NULL);
		intptr_t L_2 = ___listener1;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		AuthUtilPINVOKE_DestroyIdTokenListener_m7CACC17CE6A8EB523C7FC9DC96051AB52A6571F1(L_1, (intptr_t)L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_3 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_0 = L_3;
		bool L_4 = V_0;
		if (!L_4)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_5 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, AuthUtil_DestroyIdTokenListener_m57CA413B20CBFF946AA457063051AF94A03ECF22_RuntimeMethod_var);
	}

IL_001d:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Auth.AuthUtilPINVOKE::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE__cctor_mB959A2D2962E2F91E1F004B060423C21556019E0 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AuthUtilPINVOKE__cctor_mB959A2D2962E2F91E1F004B060423C21556019E0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97 * L_0 = (SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97 *)il2cpp_codegen_object_new(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var);
		SWIGExceptionHelper__ctor_mFF2CE315200639030B26C32F018A7E7354B81663(L_0, /*hidden argument*/NULL);
		((AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_StaticFields*)il2cpp_codegen_static_fields_for(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var))->set_swigExceptionHelper_0(L_0);
		SWIGStringHelper_t0D99651DC671811558BDB3D299B82F879F5E731B * L_1 = (SWIGStringHelper_t0D99651DC671811558BDB3D299B82F879F5E731B *)il2cpp_codegen_object_new(SWIGStringHelper_t0D99651DC671811558BDB3D299B82F879F5E731B_il2cpp_TypeInfo_var);
		SWIGStringHelper__ctor_m22B2245AE72469FC2016E3B70329E4117E38AF96(L_1, /*hidden argument*/NULL);
		((AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_StaticFields*)il2cpp_codegen_static_fields_for(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var))->set_swigStringHelper_1(L_1);
		return;
	}
}
IL2CPP_EXTERN_C void DEFAULT_CALL Firebase_Auth_CSharp_UserInfoInterfaceList_Clear(void*);
// System.Void Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_Clear(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_UserInfoInterfaceList_Clear_m0477B48FDCFEEC92FC007E3F43EC65647F9E240A (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_UserInfoInterfaceList_Clear)(____jarg10_marshaled);

}
IL2CPP_EXTERN_C void DEFAULT_CALL Firebase_Auth_CSharp_UserInfoInterfaceList_Add(void*, void*);
// System.Void Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_Add(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_UserInfoInterfaceList_Add_m235699A22C7864EC52A0A694E8549F1C4C1AE089 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg21, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Marshaling of parameter '___jarg21' to native representation
	void* ____jarg21_marshaled = NULL;
	____jarg21_marshaled = (void*)___jarg21.get_m_handle_1();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_UserInfoInterfaceList_Add)(____jarg10_marshaled, ____jarg21_marshaled);

}
IL2CPP_EXTERN_C uint32_t DEFAULT_CALL Firebase_Auth_CSharp_UserInfoInterfaceList_size(void*);
// System.UInt32 Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_size(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t AuthUtilPINVOKE_UserInfoInterfaceList_size_m4022FD61AA1F01B2682E30DD2660D39144EE3875 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef uint32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	uint32_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_UserInfoInterfaceList_size)(____jarg10_marshaled);

	return returnValue;
}
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL Firebase_Auth_CSharp_UserInfoInterfaceList_getitemcopy(void*, int32_t);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_getitemcopy(System.Runtime.InteropServices.HandleRef,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_UserInfoInterfaceList_getitemcopy_mB07EF1F7557FF0370E3ACE819AEE2612D4CB524D (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, int32_t ___jarg21, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, int32_t);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_UserInfoInterfaceList_getitemcopy)(____jarg10_marshaled, ___jarg21);

	return returnValue;
}
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL Firebase_Auth_CSharp_UserInfoInterfaceList_getitem(void*, int32_t);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_getitem(System.Runtime.InteropServices.HandleRef,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_UserInfoInterfaceList_getitem_m57D02F5BE29828EB28F2B8B3DC5591207B34BEE9 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, int32_t ___jarg21, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, int32_t);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_UserInfoInterfaceList_getitem)(____jarg10_marshaled, ___jarg21);

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL Firebase_Auth_CSharp_UserInfoInterfaceList_setitem(void*, int32_t, void*);
// System.Void Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_setitem(System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_UserInfoInterfaceList_setitem_mCB2A5B2BF72BD7E40187C4B07E9CC8FA9344A441 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, int32_t ___jarg21, HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg32, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Marshaling of parameter '___jarg32' to native representation
	void* ____jarg32_marshaled = NULL;
	____jarg32_marshaled = (void*)___jarg32.get_m_handle_1();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_UserInfoInterfaceList_setitem)(____jarg10_marshaled, ___jarg21, ____jarg32_marshaled);

}
IL2CPP_EXTERN_C void DEFAULT_CALL Firebase_Auth_CSharp_UserInfoInterfaceList_Insert(void*, int32_t, void*);
// System.Void Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_Insert(System.Runtime.InteropServices.HandleRef,System.Int32,System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_UserInfoInterfaceList_Insert_mF8231269D3DB49CE00D815C59748E6D77C5E8A6D (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, int32_t ___jarg21, HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg32, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t, void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Marshaling of parameter '___jarg32' to native representation
	void* ____jarg32_marshaled = NULL;
	____jarg32_marshaled = (void*)___jarg32.get_m_handle_1();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_UserInfoInterfaceList_Insert)(____jarg10_marshaled, ___jarg21, ____jarg32_marshaled);

}
IL2CPP_EXTERN_C void DEFAULT_CALL Firebase_Auth_CSharp_UserInfoInterfaceList_RemoveAt(void*, int32_t);
// System.Void Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_RemoveAt(System.Runtime.InteropServices.HandleRef,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_UserInfoInterfaceList_RemoveAt_m3E7CC088E5D8F62308D0A7193D34DE0DA9215179 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, int32_t ___jarg21, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, int32_t);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_UserInfoInterfaceList_RemoveAt)(____jarg10_marshaled, ___jarg21);

}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL Firebase_Auth_CSharp_UserInfoInterfaceList_Contains(void*, void*);
// System.Boolean Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_Contains(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AuthUtilPINVOKE_UserInfoInterfaceList_Contains_mACD9BBB51A6B1007C9107D484C7963ACC01C1803 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg21, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*, void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Marshaling of parameter '___jarg21' to native representation
	void* ____jarg21_marshaled = NULL;
	____jarg21_marshaled = (void*)___jarg21.get_m_handle_1();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_UserInfoInterfaceList_Contains)(____jarg10_marshaled, ____jarg21_marshaled);

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL Firebase_Auth_CSharp_UserInfoInterfaceList_IndexOf(void*, void*);
// System.Int32 Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_IndexOf(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AuthUtilPINVOKE_UserInfoInterfaceList_IndexOf_mC0DFA98501EF7C4F2D513ACF7B513877B71E69D3 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg21, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*, void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Marshaling of parameter '___jarg21' to native representation
	void* ____jarg21_marshaled = NULL;
	____jarg21_marshaled = (void*)___jarg21.get_m_handle_1();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_UserInfoInterfaceList_IndexOf)(____jarg10_marshaled, ____jarg21_marshaled);

	return returnValue;
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL Firebase_Auth_CSharp_UserInfoInterfaceList_Remove(void*, void*);
// System.Boolean Firebase.Auth.AuthUtilPINVOKE::UserInfoInterfaceList_Remove(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AuthUtilPINVOKE_UserInfoInterfaceList_Remove_m58DD7F22CAFF88AABC98EBB2B4003720BCDEE7BC (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg21, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*, void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Marshaling of parameter '___jarg21' to native representation
	void* ____jarg21_marshaled = NULL;
	____jarg21_marshaled = (void*)___jarg21.get_m_handle_1();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_UserInfoInterfaceList_Remove)(____jarg10_marshaled, ____jarg21_marshaled);

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C void DEFAULT_CALL Firebase_Auth_CSharp_delete_UserInfoInterfaceList(void*);
// System.Void Firebase.Auth.AuthUtilPINVOKE::delete_UserInfoInterfaceList(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_delete_UserInfoInterfaceList_mF4C1DD6531A015AC1E86A763A37E9E2EDC5B1A51 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_delete_UserInfoInterfaceList)(____jarg10_marshaled);

}
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL Firebase_Auth_CSharp_Future_User_SWIG_OnCompletion(void*, Il2CppMethodPointer, int32_t);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::Future_User_SWIG_OnCompletion(System.Runtime.InteropServices.HandleRef,Firebase.Auth.Future_User_SWIG_CompletionDelegate,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_Future_User_SWIG_OnCompletion_mA4865E7D9C1E147E26B7FD5F81078FA99F7ADF32 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2 * ___jarg21, int32_t ___jarg32, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer, int32_t);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Marshaling of parameter '___jarg21' to native representation
	Il2CppMethodPointer ____jarg21_marshaled = NULL;
	____jarg21_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___jarg21));

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_Future_User_SWIG_OnCompletion)(____jarg10_marshaled, ____jarg21_marshaled, ___jarg32);

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL Firebase_Auth_CSharp_Future_User_SWIG_FreeCompletionData(void*, intptr_t);
// System.Void Firebase.Auth.AuthUtilPINVOKE::Future_User_SWIG_FreeCompletionData(System.Runtime.InteropServices.HandleRef,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_Future_User_SWIG_FreeCompletionData_m4E8D0B09D212ED4E404E86B97439D0AF51879DB1 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, intptr_t ___jarg21, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_Future_User_SWIG_FreeCompletionData)(____jarg10_marshaled, ___jarg21);

}
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL Firebase_Auth_CSharp_Future_User_GetResult(void*);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::Future_User_GetResult(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_Future_User_GetResult_m36CFE956376E3A4DC187BCC9D6F2732417615FE9 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_Future_User_GetResult)(____jarg10_marshaled);

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL Firebase_Auth_CSharp_delete_Future_User(void*);
// System.Void Firebase.Auth.AuthUtilPINVOKE::delete_Future_User(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_delete_Future_User_m09EF8AC9392014A450B9A39F9237AB703460AE42 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_delete_Future_User)(____jarg10_marshaled);

}
IL2CPP_EXTERN_C void DEFAULT_CALL Firebase_Auth_CSharp_delete_Credential(void*);
// System.Void Firebase.Auth.AuthUtilPINVOKE::delete_Credential(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_delete_Credential_m02588ECD12EF7FBADE68921F2AC6F2DBA0E37C88 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_delete_Credential)(____jarg10_marshaled);

}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL Firebase_Auth_CSharp_Credential_IsValid(void*);
// System.Boolean Firebase.Auth.AuthUtilPINVOKE::Credential_IsValid(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AuthUtilPINVOKE_Credential_IsValid_m590E8B8C5882B73A5D6C253E2E6591D789A61081 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_Credential_IsValid)(____jarg10_marshaled);

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL Firebase_Auth_CSharp_FacebookAuthProvider_GetCredential(char*);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FacebookAuthProvider_GetCredential(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_FacebookAuthProvider_GetCredential_m6C17A5533AB95072B52FEFFEF1BC520C8B3B89BF (String_t* ___jarg10, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___jarg10' to native representation
	char* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = il2cpp_codegen_marshal_string(___jarg10);

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_FacebookAuthProvider_GetCredential)(____jarg10_marshaled);

	// Marshaling cleanup of parameter '___jarg10' native representation
	il2cpp_codegen_marshal_free(____jarg10_marshaled);
	____jarg10_marshaled = NULL;

	return returnValue;
}
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL Firebase_Auth_CSharp_PlayGamesAuthProvider_GetCredential(char*);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::PlayGamesAuthProvider_GetCredential(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_PlayGamesAuthProvider_GetCredential_mF15F71E21D192B4940648E872A8F7F8E8713A0CA (String_t* ___jarg10, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___jarg10' to native representation
	char* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = il2cpp_codegen_marshal_string(___jarg10);

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_PlayGamesAuthProvider_GetCredential)(____jarg10_marshaled);

	// Marshaling cleanup of parameter '___jarg10' native representation
	il2cpp_codegen_marshal_free(____jarg10_marshaled);
	____jarg10_marshaled = NULL;

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL Firebase_Auth_CSharp_delete_UserInfoInterface(void*);
// System.Void Firebase.Auth.AuthUtilPINVOKE::delete_UserInfoInterface(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_delete_UserInfoInterface_m6EECB261E56A9D7F8BB61C69988423AD41B16747 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_delete_UserInfoInterface)(____jarg10_marshaled);

}
IL2CPP_EXTERN_C char* DEFAULT_CALL Firebase_Auth_CSharp_UserInfoInterface_UserId_get(void*);
// System.String Firebase.Auth.AuthUtilPINVOKE::UserInfoInterface_UserId_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AuthUtilPINVOKE_UserInfoInterface_UserId_get_m3B7A2BC624181BC02ACF3481BF437903B1EFC954 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_UserInfoInterface_UserId_get)(____jarg10_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
IL2CPP_EXTERN_C char* DEFAULT_CALL Firebase_Auth_CSharp_UserInfoInterface_Email_get(void*);
// System.String Firebase.Auth.AuthUtilPINVOKE::UserInfoInterface_Email_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AuthUtilPINVOKE_UserInfoInterface_Email_get_m49D0AB8130C3E528C1D44B66CC7D681B2F2E804B (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_UserInfoInterface_Email_get)(____jarg10_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
IL2CPP_EXTERN_C char* DEFAULT_CALL Firebase_Auth_CSharp_UserInfoInterface_DisplayName_get(void*);
// System.String Firebase.Auth.AuthUtilPINVOKE::UserInfoInterface_DisplayName_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AuthUtilPINVOKE_UserInfoInterface_DisplayName_get_m40F5D1957524C394F0A8022F47B399931BE4DFF7 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_UserInfoInterface_DisplayName_get)(____jarg10_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
IL2CPP_EXTERN_C char* DEFAULT_CALL Firebase_Auth_CSharp_UserInfoInterface_PhotoUrlInternal_get(void*);
// System.String Firebase.Auth.AuthUtilPINVOKE::UserInfoInterface_PhotoUrlInternal_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AuthUtilPINVOKE_UserInfoInterface_PhotoUrlInternal_get_mA1C69290B7452547D58BA4675DAE501090CCA5B7 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_UserInfoInterface_PhotoUrlInternal_get)(____jarg10_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
IL2CPP_EXTERN_C char* DEFAULT_CALL Firebase_Auth_CSharp_UserInfoInterface_ProviderId_get(void*);
// System.String Firebase.Auth.AuthUtilPINVOKE::UserInfoInterface_ProviderId_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AuthUtilPINVOKE_UserInfoInterface_ProviderId_get_mB0AC6D00DCC186EB3D315809B0727D666F24C9B0 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_UserInfoInterface_ProviderId_get)(____jarg10_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
IL2CPP_EXTERN_C void DEFAULT_CALL Firebase_Auth_CSharp_delete_FirebaseUser(void*);
// System.Void Firebase.Auth.AuthUtilPINVOKE::delete_FirebaseUser(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_delete_FirebaseUser_m21C9A5C887AD120B4228AC2E15E7EA499F745A9F (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_delete_FirebaseUser)(____jarg10_marshaled);

}
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL Firebase_Auth_CSharp_FirebaseUser_UpdateEmail(void*, char*);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_UpdateEmail(System.Runtime.InteropServices.HandleRef,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_FirebaseUser_UpdateEmail_mE4BC3966DE87BDA371C7CE7A0F23D91BEC313CCB (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, String_t* ___jarg21, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, char*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Marshaling of parameter '___jarg21' to native representation
	char* ____jarg21_marshaled = NULL;
	____jarg21_marshaled = il2cpp_codegen_marshal_string(___jarg21);

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_FirebaseUser_UpdateEmail)(____jarg10_marshaled, ____jarg21_marshaled);

	// Marshaling cleanup of parameter '___jarg21' native representation
	il2cpp_codegen_marshal_free(____jarg21_marshaled);
	____jarg21_marshaled = NULL;

	return returnValue;
}
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL Firebase_Auth_CSharp_FirebaseUser_UpdateUserProfile(void*, void*);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_UpdateUserProfile(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_FirebaseUser_UpdateUserProfile_m828FCDD053CA923BA34246E6679C39F6BCBF076C (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg21, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Marshaling of parameter '___jarg21' to native representation
	void* ____jarg21_marshaled = NULL;
	____jarg21_marshaled = (void*)___jarg21.get_m_handle_1();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_FirebaseUser_UpdateUserProfile)(____jarg10_marshaled, ____jarg21_marshaled);

	return returnValue;
}
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL Firebase_Auth_CSharp_FirebaseUser_LinkWithCredential(void*, void*);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_LinkWithCredential(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_FirebaseUser_LinkWithCredential_mBC153FDCFAC9320B9628591B105955EFCCA5E03F (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg21, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Marshaling of parameter '___jarg21' to native representation
	void* ____jarg21_marshaled = NULL;
	____jarg21_marshaled = (void*)___jarg21.get_m_handle_1();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_FirebaseUser_LinkWithCredential)(____jarg10_marshaled, ____jarg21_marshaled);

	return returnValue;
}
IL2CPP_EXTERN_C char* DEFAULT_CALL Firebase_Auth_CSharp_FirebaseUser_DisplayName_get(void*);
// System.String Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_DisplayName_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AuthUtilPINVOKE_FirebaseUser_DisplayName_get_mDD546C27F066D7A7C9F9612FB9CC18C6E8D9E2BF (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_FirebaseUser_DisplayName_get)(____jarg10_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
IL2CPP_EXTERN_C char* DEFAULT_CALL Firebase_Auth_CSharp_FirebaseUser_Email_get(void*);
// System.String Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_Email_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AuthUtilPINVOKE_FirebaseUser_Email_get_m1C74E1D469EF27F32E2810738DBF9CA1BA23582D (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_FirebaseUser_Email_get)(____jarg10_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL Firebase_Auth_CSharp_FirebaseUser_IsAnonymous_get(void*);
// System.Boolean Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_IsAnonymous_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AuthUtilPINVOKE_FirebaseUser_IsAnonymous_get_m62129D05B6F8B37AF44E526484F3F44094D907D8 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_FirebaseUser_IsAnonymous_get)(____jarg10_marshaled);

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C char* DEFAULT_CALL Firebase_Auth_CSharp_FirebaseUser_PhotoUrlInternal_get(void*);
// System.String Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_PhotoUrlInternal_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AuthUtilPINVOKE_FirebaseUser_PhotoUrlInternal_get_m12B7DDC9E668B2B40694C4DEB10A2C4EAC81FD00 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_FirebaseUser_PhotoUrlInternal_get)(____jarg10_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL Firebase_Auth_CSharp_FirebaseUser_ProviderData_get(void*);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_ProviderData_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_FirebaseUser_ProviderData_get_mD388B7BE41C3645C7464C3C0DAA81290CF2CED47 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_FirebaseUser_ProviderData_get)(____jarg10_marshaled);

	return returnValue;
}
IL2CPP_EXTERN_C char* DEFAULT_CALL Firebase_Auth_CSharp_FirebaseUser_UserId_get(void*);
// System.String Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_UserId_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AuthUtilPINVOKE_FirebaseUser_UserId_get_mA5AC5F6494A1F4A6A3F0DBC90E0D4D1006F5CF5A (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_FirebaseUser_UserId_get)(____jarg10_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL Firebase_Auth_CSharp_new_UserProfile();
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::new_UserProfile()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_new_UserProfile_mF862E0AB1127CCB19BEA8E520C8EC4E4AD85FF1C (const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_new_UserProfile)();

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL Firebase_Auth_CSharp_UserProfile_DisplayName_set(void*, char*);
// System.Void Firebase.Auth.AuthUtilPINVOKE::UserProfile_DisplayName_set(System.Runtime.InteropServices.HandleRef,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_UserProfile_DisplayName_set_m2E3AA6A6AFEED65380B898B3BFD7FDD033CB5B59 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, String_t* ___jarg21, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Marshaling of parameter '___jarg21' to native representation
	char* ____jarg21_marshaled = NULL;
	____jarg21_marshaled = il2cpp_codegen_marshal_string(___jarg21);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_UserProfile_DisplayName_set)(____jarg10_marshaled, ____jarg21_marshaled);

	// Marshaling cleanup of parameter '___jarg21' native representation
	il2cpp_codegen_marshal_free(____jarg21_marshaled);
	____jarg21_marshaled = NULL;

}
IL2CPP_EXTERN_C void DEFAULT_CALL Firebase_Auth_CSharp_UserProfile_PhotoUrlInternal_set(void*, char*);
// System.Void Firebase.Auth.AuthUtilPINVOKE::UserProfile_PhotoUrlInternal_set(System.Runtime.InteropServices.HandleRef,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_UserProfile_PhotoUrlInternal_set_mA5391563F2C002A4D053CF4BC28C2B7548E699B0 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, String_t* ___jarg21, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, char*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Marshaling of parameter '___jarg21' to native representation
	char* ____jarg21_marshaled = NULL;
	____jarg21_marshaled = il2cpp_codegen_marshal_string(___jarg21);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_UserProfile_PhotoUrlInternal_set)(____jarg10_marshaled, ____jarg21_marshaled);

	// Marshaling cleanup of parameter '___jarg21' native representation
	il2cpp_codegen_marshal_free(____jarg21_marshaled);
	____jarg21_marshaled = NULL;

}
IL2CPP_EXTERN_C void DEFAULT_CALL Firebase_Auth_CSharp_delete_UserProfile(void*);
// System.Void Firebase.Auth.AuthUtilPINVOKE::delete_UserProfile(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_delete_UserProfile_mEBF4B6D168A27F787149E7D71C5F6DA18AA6104E (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_delete_UserProfile)(____jarg10_marshaled);

}
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL Firebase_Auth_CSharp_FirebaseAuth_SignInWithCredentialInternal(void*, void*);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseAuth_SignInWithCredentialInternal(System.Runtime.InteropServices.HandleRef,System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_FirebaseAuth_SignInWithCredentialInternal_m286B534BC062DE3FD14436764BCE057923965B67 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg21, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Marshaling of parameter '___jarg21' to native representation
	void* ____jarg21_marshaled = NULL;
	____jarg21_marshaled = (void*)___jarg21.get_m_handle_1();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_FirebaseAuth_SignInWithCredentialInternal)(____jarg10_marshaled, ____jarg21_marshaled);

	return returnValue;
}
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL Firebase_Auth_CSharp_FirebaseAuth_SignInAnonymouslyInternal(void*);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseAuth_SignInAnonymouslyInternal(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_FirebaseAuth_SignInAnonymouslyInternal_m769C6AFE4B52A39BFFB28D8298F6664514DB39C6 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_FirebaseAuth_SignInAnonymouslyInternal)(____jarg10_marshaled);

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL Firebase_Auth_CSharp_FirebaseAuth_SignOut(void*);
// System.Void Firebase.Auth.AuthUtilPINVOKE::FirebaseAuth_SignOut(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_FirebaseAuth_SignOut_m03EDFAB11CF0A459DD7CA6A3E097790BA3D91458 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_FirebaseAuth_SignOut)(____jarg10_marshaled);

}
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL Firebase_Auth_CSharp_FirebaseAuth_GetAuthInternal(void*, int32_t*);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseAuth_GetAuthInternal(System.Runtime.InteropServices.HandleRef,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_FirebaseAuth_GetAuthInternal_m9CA73CC68A18B4B86AFFB925E37E8F6A0DAE72AA (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, int32_t* ___jarg21, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, int32_t*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_FirebaseAuth_GetAuthInternal)(____jarg10_marshaled, ___jarg21);

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL Firebase_Auth_CSharp_FirebaseAuth_LogHeartbeatInternal(void*);
// System.Void Firebase.Auth.AuthUtilPINVOKE::FirebaseAuth_LogHeartbeatInternal(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_FirebaseAuth_LogHeartbeatInternal_m40FB5B32F8D0549946D5FA26631C4D88338C6D5C (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_FirebaseAuth_LogHeartbeatInternal)(____jarg10_marshaled);

}
IL2CPP_EXTERN_C void DEFAULT_CALL Firebase_Auth_CSharp_FirebaseAuth_ReleaseReferenceInternal(void*);
// System.Void Firebase.Auth.AuthUtilPINVOKE::FirebaseAuth_ReleaseReferenceInternal(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_FirebaseAuth_ReleaseReferenceInternal_m77FAC25E87B2644CA0EF2B9BFF0201C2F38349D9 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_FirebaseAuth_ReleaseReferenceInternal)(____jarg10_marshaled);

}
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL Firebase_Auth_CSharp_FirebaseAuth_CurrentUserInternal_get(void*);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseAuth_CurrentUserInternal_get(System.Runtime.InteropServices.HandleRef)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_FirebaseAuth_CurrentUserInternal_get_m1646DA22D3C6243710AFD36C183194346B494495 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_FirebaseAuth_CurrentUserInternal_get)(____jarg10_marshaled);

	return returnValue;
}
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL Firebase_Auth_CSharp_CreateAuthStateListener(void*, Il2CppMethodPointer);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::CreateAuthStateListener(System.Runtime.InteropServices.HandleRef,Firebase.Auth.FirebaseAuth_StateChangedDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_CreateAuthStateListener_m093D034502CF32CE850E6A9E286C15EAE6785C2B (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294 * ___jarg21, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Marshaling of parameter '___jarg21' to native representation
	Il2CppMethodPointer ____jarg21_marshaled = NULL;
	____jarg21_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___jarg21));

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_CreateAuthStateListener)(____jarg10_marshaled, ____jarg21_marshaled);

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL Firebase_Auth_CSharp_DestroyAuthStateListener(void*, intptr_t);
// System.Void Firebase.Auth.AuthUtilPINVOKE::DestroyAuthStateListener(System.Runtime.InteropServices.HandleRef,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_DestroyAuthStateListener_m632301CCA03C97183D31D2E819196742BBB8BB1B (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, intptr_t ___jarg21, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_DestroyAuthStateListener)(____jarg10_marshaled, ___jarg21);

}
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL Firebase_Auth_CSharp_CreateIdTokenListener(void*, Il2CppMethodPointer);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::CreateIdTokenListener(System.Runtime.InteropServices.HandleRef,Firebase.Auth.FirebaseAuth_StateChangedDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_CreateIdTokenListener_mA59101F16559583539311A7139734B88B26CF10B (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294 * ___jarg21, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (void*, Il2CppMethodPointer);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Marshaling of parameter '___jarg21' to native representation
	Il2CppMethodPointer ____jarg21_marshaled = NULL;
	____jarg21_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___jarg21));

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_CreateIdTokenListener)(____jarg10_marshaled, ____jarg21_marshaled);

	return returnValue;
}
IL2CPP_EXTERN_C void DEFAULT_CALL Firebase_Auth_CSharp_DestroyIdTokenListener(void*, intptr_t);
// System.Void Firebase.Auth.AuthUtilPINVOKE::DestroyIdTokenListener(System.Runtime.InteropServices.HandleRef,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AuthUtilPINVOKE_DestroyIdTokenListener_m7CACC17CE6A8EB523C7FC9DC96051AB52A6571F1 (HandleRef_t876E76124F400D12395BF61D562162AB6822204A  ___jarg10, intptr_t ___jarg21, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (void*, intptr_t);

	// Marshaling of parameter '___jarg10' to native representation
	void* ____jarg10_marshaled = NULL;
	____jarg10_marshaled = (void*)___jarg10.get_m_handle_1();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_DestroyIdTokenListener)(____jarg10_marshaled, ___jarg21);

}
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL Firebase_Auth_CSharp_Future_User_SWIGUpcast(intptr_t);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::Future_User_SWIGUpcast(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_Future_User_SWIGUpcast_m9F5C5F23C6908B207633FFC69E6B33329CCDD5FC (intptr_t ___jarg10, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_Future_User_SWIGUpcast)(___jarg10);

	return returnValue;
}
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL Firebase_Auth_CSharp_FirebaseUser_SWIGUpcast(intptr_t);
// System.IntPtr Firebase.Auth.AuthUtilPINVOKE::FirebaseUser_SWIGUpcast(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t AuthUtilPINVOKE_FirebaseUser_SWIGUpcast_m2E75C4601FD7E308456F2F0BF75ECB73F92D0E44 (intptr_t ___jarg10, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(Firebase_Auth_CSharp_FirebaseUser_SWIGUpcast)(___jarg10);

	return returnValue;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingApplicationException_m1EB96681AA0CAEE7C648B0488735AC4B34644625(char* ___message0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingApplicationException_m1EB96681AA0CAEE7C648B0488735AC4B34644625(____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArithmeticException_m6FD83BB7EC4D0C5A758FC33434AFF95000BB2145(char* ___message0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingArithmeticException_m6FD83BB7EC4D0C5A758FC33434AFF95000BB2145(____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingDivideByZeroException_mC428FC6EAD280918627E7AD8285B5EC490842944(char* ___message0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingDivideByZeroException_mC428FC6EAD280918627E7AD8285B5EC490842944(____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m8AC3EB8E22CAB0B37997524B3D8EB61E7C4B9888(char* ___message0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m8AC3EB8E22CAB0B37997524B3D8EB61E7C4B9888(____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingInvalidCastException_m3B7ACF97DC9FB2B9F197C4E3168B9C4F8A4D0719(char* ___message0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingInvalidCastException_m3B7ACF97DC9FB2B9F197C4E3168B9C4F8A4D0719(____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingInvalidOperationException_mECD029C557A14DBA585161CF1E3EDD1C3201813D(char* ___message0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingInvalidOperationException_mECD029C557A14DBA585161CF1E3EDD1C3201813D(____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingIOException_mE61B638B09F96D19FA24D9B2BD85230650FC59B2(char* ___message0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingIOException_mE61B638B09F96D19FA24D9B2BD85230650FC59B2(____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingNullReferenceException_mB61F8433F7B8D82845D1635AC67E6132F1F3A7B9(char* ___message0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingNullReferenceException_mB61F8433F7B8D82845D1635AC67E6132F1F3A7B9(____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingOutOfMemoryException_mD766FD3AB3B0EF9E768367C0199FC00A9362119E(char* ___message0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingOutOfMemoryException_mD766FD3AB3B0EF9E768367C0199FC00A9362119E(____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingOverflowException_m4F7B5FA76EE51D9CB219856AB845929CD819FDF2(char* ___message0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingOverflowException_m4F7B5FA76EE51D9CB219856AB845929CD819FDF2(____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingSystemException_mB83BE9B10C217AA022F9C626585DCE16A1E3BF43(char* ___message0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingSystemException_mB83BE9B10C217AA022F9C626585DCE16A1E3BF43(____message0_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentException_mCC2449972A52DACB566318E5D82883A1AE0FCC77(char* ___message0, char* ___paramName1)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Marshaling of parameter '___paramName1' to managed representation
	String_t* ____paramName1_unmarshaled = NULL;
	____paramName1_unmarshaled = il2cpp_codegen_marshal_string_result(___paramName1);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingArgumentException_mCC2449972A52DACB566318E5D82883A1AE0FCC77(____message0_unmarshaled, ____paramName1_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentNullException_mDDF072DC234116293DF4DD4CAE04931816995032(char* ___message0, char* ___paramName1)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Marshaling of parameter '___paramName1' to managed representation
	String_t* ____paramName1_unmarshaled = NULL;
	____paramName1_unmarshaled = il2cpp_codegen_marshal_string_result(___paramName1);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingArgumentNullException_mDDF072DC234116293DF4DD4CAE04931816995032(____message0_unmarshaled, ____paramName1_unmarshaled, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m64A3753D6C93082BF5431CA3B095125235597BA7(char* ___message0, char* ___paramName1)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___message0' to managed representation
	String_t* ____message0_unmarshaled = NULL;
	____message0_unmarshaled = il2cpp_codegen_marshal_string_result(___message0);

	// Marshaling of parameter '___paramName1' to managed representation
	String_t* ____paramName1_unmarshaled = NULL;
	____paramName1_unmarshaled = il2cpp_codegen_marshal_string_result(___paramName1);

	// Managed method invocation
	SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m64A3753D6C93082BF5431CA3B095125235597BA7(____message0_unmarshaled, ____paramName1_unmarshaled, NULL);

}
IL2CPP_EXTERN_C void DEFAULT_CALL SWIGRegisterExceptionCallbacks_AuthUtil(Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SWIGRegisterExceptionCallbacks_AuthUtil(Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_AuthUtil_mD13FDDDDC03DD49540B6AFFC935A58DE0B534762 (ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___applicationDelegate0, ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___arithmeticDelegate1, ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___divideByZeroDelegate2, ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___indexOutOfRangeDelegate3, ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___invalidCastDelegate4, ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___invalidOperationDelegate5, ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___ioDelegate6, ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___nullReferenceDelegate7, ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___outOfMemoryDelegate8, ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___overflowDelegate9, ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * ___systemExceptionDelegate10, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);

	// Marshaling of parameter '___applicationDelegate0' to native representation
	Il2CppMethodPointer ____applicationDelegate0_marshaled = NULL;
	____applicationDelegate0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___applicationDelegate0));

	// Marshaling of parameter '___arithmeticDelegate1' to native representation
	Il2CppMethodPointer ____arithmeticDelegate1_marshaled = NULL;
	____arithmeticDelegate1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___arithmeticDelegate1));

	// Marshaling of parameter '___divideByZeroDelegate2' to native representation
	Il2CppMethodPointer ____divideByZeroDelegate2_marshaled = NULL;
	____divideByZeroDelegate2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___divideByZeroDelegate2));

	// Marshaling of parameter '___indexOutOfRangeDelegate3' to native representation
	Il2CppMethodPointer ____indexOutOfRangeDelegate3_marshaled = NULL;
	____indexOutOfRangeDelegate3_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___indexOutOfRangeDelegate3));

	// Marshaling of parameter '___invalidCastDelegate4' to native representation
	Il2CppMethodPointer ____invalidCastDelegate4_marshaled = NULL;
	____invalidCastDelegate4_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___invalidCastDelegate4));

	// Marshaling of parameter '___invalidOperationDelegate5' to native representation
	Il2CppMethodPointer ____invalidOperationDelegate5_marshaled = NULL;
	____invalidOperationDelegate5_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___invalidOperationDelegate5));

	// Marshaling of parameter '___ioDelegate6' to native representation
	Il2CppMethodPointer ____ioDelegate6_marshaled = NULL;
	____ioDelegate6_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___ioDelegate6));

	// Marshaling of parameter '___nullReferenceDelegate7' to native representation
	Il2CppMethodPointer ____nullReferenceDelegate7_marshaled = NULL;
	____nullReferenceDelegate7_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___nullReferenceDelegate7));

	// Marshaling of parameter '___outOfMemoryDelegate8' to native representation
	Il2CppMethodPointer ____outOfMemoryDelegate8_marshaled = NULL;
	____outOfMemoryDelegate8_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___outOfMemoryDelegate8));

	// Marshaling of parameter '___overflowDelegate9' to native representation
	Il2CppMethodPointer ____overflowDelegate9_marshaled = NULL;
	____overflowDelegate9_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___overflowDelegate9));

	// Marshaling of parameter '___systemExceptionDelegate10' to native representation
	Il2CppMethodPointer ____systemExceptionDelegate10_marshaled = NULL;
	____systemExceptionDelegate10_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___systemExceptionDelegate10));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SWIGRegisterExceptionCallbacks_AuthUtil)(____applicationDelegate0_marshaled, ____arithmeticDelegate1_marshaled, ____divideByZeroDelegate2_marshaled, ____indexOutOfRangeDelegate3_marshaled, ____invalidCastDelegate4_marshaled, ____invalidOperationDelegate5_marshaled, ____ioDelegate6_marshaled, ____nullReferenceDelegate7_marshaled, ____outOfMemoryDelegate8_marshaled, ____overflowDelegate9_marshaled, ____systemExceptionDelegate10_marshaled);

}
IL2CPP_EXTERN_C void DEFAULT_CALL SWIGRegisterExceptionArgumentCallbacks_AuthUtil(Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SWIGRegisterExceptionCallbacksArgument_AuthUtil(Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate,Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_AuthUtil_m85D92F4B1140B7B2228AB92E057C747B9A07FCA7 (ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * ___argumentDelegate0, ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * ___argumentNullDelegate1, ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * ___argumentOutOfRangeDelegate2, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer, Il2CppMethodPointer, Il2CppMethodPointer);

	// Marshaling of parameter '___argumentDelegate0' to native representation
	Il2CppMethodPointer ____argumentDelegate0_marshaled = NULL;
	____argumentDelegate0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___argumentDelegate0));

	// Marshaling of parameter '___argumentNullDelegate1' to native representation
	Il2CppMethodPointer ____argumentNullDelegate1_marshaled = NULL;
	____argumentNullDelegate1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___argumentNullDelegate1));

	// Marshaling of parameter '___argumentOutOfRangeDelegate2' to native representation
	Il2CppMethodPointer ____argumentOutOfRangeDelegate2_marshaled = NULL;
	____argumentOutOfRangeDelegate2_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___argumentOutOfRangeDelegate2));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SWIGRegisterExceptionArgumentCallbacks_AuthUtil)(____argumentDelegate0_marshaled, ____argumentNullDelegate1_marshaled, ____argumentOutOfRangeDelegate2_marshaled);

}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingApplicationException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingApplicationException_m1EB96681AA0CAEE7C648B0488735AC4B34644625 (String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingApplicationException_m1EB96681AA0CAEE7C648B0488735AC4B34644625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		Exception_t * L_1 = SWIGPendingException_Retrieve_m47C7FCF8ED6E8C49646820EE0691C3EDE0CFFDCD(/*hidden argument*/NULL);
		ApplicationException_t664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74 * L_2 = (ApplicationException_t664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74 *)il2cpp_codegen_object_new(ApplicationException_t664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74_il2cpp_TypeInfo_var);
		ApplicationException__ctor_m0AF8988654AD1CA2DCA8EC12E6D46563E4EE703F(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2A770FCDB68242E723C1E3A5CA7B4B86B969155C(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingArithmeticException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingArithmeticException_m6FD83BB7EC4D0C5A758FC33434AFF95000BB2145 (String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingArithmeticException_m6FD83BB7EC4D0C5A758FC33434AFF95000BB2145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		Exception_t * L_1 = SWIGPendingException_Retrieve_m47C7FCF8ED6E8C49646820EE0691C3EDE0CFFDCD(/*hidden argument*/NULL);
		ArithmeticException_tF9EF5FE94597830EF315C5934258F994B8648269 * L_2 = (ArithmeticException_tF9EF5FE94597830EF315C5934258F994B8648269 *)il2cpp_codegen_object_new(ArithmeticException_tF9EF5FE94597830EF315C5934258F994B8648269_il2cpp_TypeInfo_var);
		ArithmeticException__ctor_m1412E36F7AF7D25CF6A00670AE2296E88DA85F5F(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2A770FCDB68242E723C1E3A5CA7B4B86B969155C(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingDivideByZeroException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingDivideByZeroException_mC428FC6EAD280918627E7AD8285B5EC490842944 (String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingDivideByZeroException_mC428FC6EAD280918627E7AD8285B5EC490842944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		Exception_t * L_1 = SWIGPendingException_Retrieve_m47C7FCF8ED6E8C49646820EE0691C3EDE0CFFDCD(/*hidden argument*/NULL);
		DivideByZeroException_tD233835DD9A31EE4E64DD93F2D6143646CFD3FBC * L_2 = (DivideByZeroException_tD233835DD9A31EE4E64DD93F2D6143646CFD3FBC *)il2cpp_codegen_object_new(DivideByZeroException_tD233835DD9A31EE4E64DD93F2D6143646CFD3FBC_il2cpp_TypeInfo_var);
		DivideByZeroException__ctor_m12EDCCC81402AA666F9C3CF1CB8A8A7AA8D61169(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2A770FCDB68242E723C1E3A5CA7B4B86B969155C(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingIndexOutOfRangeException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m8AC3EB8E22CAB0B37997524B3D8EB61E7C4B9888 (String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m8AC3EB8E22CAB0B37997524B3D8EB61E7C4B9888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		Exception_t * L_1 = SWIGPendingException_Retrieve_m47C7FCF8ED6E8C49646820EE0691C3EDE0CFFDCD(/*hidden argument*/NULL);
		IndexOutOfRangeException_tEC7665FC66525AB6A6916A7EB505E5591683F0CF * L_2 = (IndexOutOfRangeException_tEC7665FC66525AB6A6916A7EB505E5591683F0CF *)il2cpp_codegen_object_new(IndexOutOfRangeException_tEC7665FC66525AB6A6916A7EB505E5591683F0CF_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m6CE231E888365F20BD05664560F7AF3830E9D21A(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2A770FCDB68242E723C1E3A5CA7B4B86B969155C(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingInvalidCastException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingInvalidCastException_m3B7ACF97DC9FB2B9F197C4E3168B9C4F8A4D0719 (String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingInvalidCastException_m3B7ACF97DC9FB2B9F197C4E3168B9C4F8A4D0719_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		Exception_t * L_1 = SWIGPendingException_Retrieve_m47C7FCF8ED6E8C49646820EE0691C3EDE0CFFDCD(/*hidden argument*/NULL);
		InvalidCastException_t91DF9E7D7FCCDA6C562CB4A9A18903E016680FDA * L_2 = (InvalidCastException_t91DF9E7D7FCCDA6C562CB4A9A18903E016680FDA *)il2cpp_codegen_object_new(InvalidCastException_t91DF9E7D7FCCDA6C562CB4A9A18903E016680FDA_il2cpp_TypeInfo_var);
		InvalidCastException__ctor_m54AEC55A8A1F1EAD6485B691BB597C7EB284B2A5(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2A770FCDB68242E723C1E3A5CA7B4B86B969155C(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingInvalidOperationException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingInvalidOperationException_mECD029C557A14DBA585161CF1E3EDD1C3201813D (String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingInvalidOperationException_mECD029C557A14DBA585161CF1E3EDD1C3201813D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		Exception_t * L_1 = SWIGPendingException_Retrieve_m47C7FCF8ED6E8C49646820EE0691C3EDE0CFFDCD(/*hidden argument*/NULL);
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_2 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_mC40AA9579B996C6FBAE023590139C16304D81DC6(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2A770FCDB68242E723C1E3A5CA7B4B86B969155C(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingIOException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingIOException_mE61B638B09F96D19FA24D9B2BD85230650FC59B2 (String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingIOException_mE61B638B09F96D19FA24D9B2BD85230650FC59B2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		Exception_t * L_1 = SWIGPendingException_Retrieve_m47C7FCF8ED6E8C49646820EE0691C3EDE0CFFDCD(/*hidden argument*/NULL);
		IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA * L_2 = (IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA *)il2cpp_codegen_object_new(IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA_il2cpp_TypeInfo_var);
		IOException__ctor_m37262C706BEB979358ABEFEA9F9F253E8773D2B7(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2A770FCDB68242E723C1E3A5CA7B4B86B969155C(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingNullReferenceException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingNullReferenceException_mB61F8433F7B8D82845D1635AC67E6132F1F3A7B9 (String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingNullReferenceException_mB61F8433F7B8D82845D1635AC67E6132F1F3A7B9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		Exception_t * L_1 = SWIGPendingException_Retrieve_m47C7FCF8ED6E8C49646820EE0691C3EDE0CFFDCD(/*hidden argument*/NULL);
		NullReferenceException_t204B194BC4DDA3259AF5A8633EA248AE5977ABDC * L_2 = (NullReferenceException_t204B194BC4DDA3259AF5A8633EA248AE5977ABDC *)il2cpp_codegen_object_new(NullReferenceException_t204B194BC4DDA3259AF5A8633EA248AE5977ABDC_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_mCD292E9CAC2B24447B04F5404C318B1B2AF4023D(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2A770FCDB68242E723C1E3A5CA7B4B86B969155C(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingOutOfMemoryException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingOutOfMemoryException_mD766FD3AB3B0EF9E768367C0199FC00A9362119E (String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingOutOfMemoryException_mD766FD3AB3B0EF9E768367C0199FC00A9362119E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		Exception_t * L_1 = SWIGPendingException_Retrieve_m47C7FCF8ED6E8C49646820EE0691C3EDE0CFFDCD(/*hidden argument*/NULL);
		OutOfMemoryException_t2DF3EAC178583BD1DEFAAECBEDB2AF1EA86FBFC7 * L_2 = (OutOfMemoryException_t2DF3EAC178583BD1DEFAAECBEDB2AF1EA86FBFC7 *)il2cpp_codegen_object_new(OutOfMemoryException_t2DF3EAC178583BD1DEFAAECBEDB2AF1EA86FBFC7_il2cpp_TypeInfo_var);
		OutOfMemoryException__ctor_m155C798916559886F6DD9C93B414E5764C55FD52(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2A770FCDB68242E723C1E3A5CA7B4B86B969155C(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingOverflowException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingOverflowException_m4F7B5FA76EE51D9CB219856AB845929CD819FDF2 (String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingOverflowException_m4F7B5FA76EE51D9CB219856AB845929CD819FDF2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		Exception_t * L_1 = SWIGPendingException_Retrieve_m47C7FCF8ED6E8C49646820EE0691C3EDE0CFFDCD(/*hidden argument*/NULL);
		OverflowException_tD89571E2350DE06D9DE4AB65ADCA77D607B5693D * L_2 = (OverflowException_tD89571E2350DE06D9DE4AB65ADCA77D607B5693D *)il2cpp_codegen_object_new(OverflowException_tD89571E2350DE06D9DE4AB65ADCA77D607B5693D_il2cpp_TypeInfo_var);
		OverflowException__ctor_m15CD001EEB2E79D7497E101546B04D9A5520357E(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2A770FCDB68242E723C1E3A5CA7B4B86B969155C(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingSystemException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingSystemException_mB83BE9B10C217AA022F9C626585DCE16A1E3BF43 (String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingSystemException_mB83BE9B10C217AA022F9C626585DCE16A1E3BF43_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		Exception_t * L_1 = SWIGPendingException_Retrieve_m47C7FCF8ED6E8C49646820EE0691C3EDE0CFFDCD(/*hidden argument*/NULL);
		SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782 * L_2 = (SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782 *)il2cpp_codegen_object_new(SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782_il2cpp_TypeInfo_var);
		SystemException__ctor_mA18D2EA5642C066F35CB8C965398F9A542C33B0A(L_2, L_0, L_1, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2A770FCDB68242E723C1E3A5CA7B4B86B969155C(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingArgumentException(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingArgumentException_mCC2449972A52DACB566318E5D82883A1AE0FCC77 (String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingArgumentException_mCC2449972A52DACB566318E5D82883A1AE0FCC77_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___message0;
		String_t* L_1 = ___paramName1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		Exception_t * L_2 = SWIGPendingException_Retrieve_m47C7FCF8ED6E8C49646820EE0691C3EDE0CFFDCD(/*hidden argument*/NULL);
		ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * L_3 = (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 *)il2cpp_codegen_object_new(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m8E5D9390593273B9774DBD2E967805E8EDE668E3(L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		SWIGPendingException_Set_m2A770FCDB68242E723C1E3A5CA7B4B86B969155C(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingArgumentNullException(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingArgumentNullException_mDDF072DC234116293DF4DD4CAE04931816995032 (String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingArgumentNullException_mDDF072DC234116293DF4DD4CAE04931816995032_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t * V_0 = NULL;
	bool V_1 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		Exception_t * L_0 = SWIGPendingException_Retrieve_m47C7FCF8ED6E8C49646820EE0691C3EDE0CFFDCD(/*hidden argument*/NULL);
		V_0 = L_0;
		Exception_t * L_1 = V_0;
		V_1 = (bool)((!(((RuntimeObject*)(Exception_t *)L_1) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_3 = ___message0;
		Exception_t * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String System.Exception::get_Message() */, L_4);
		String_t* L_6 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(L_3, _stringLiteralA804D931F27BE6EF35191274DAF148C0B4FCEEF6, L_5, /*hidden argument*/NULL);
		___message0 = L_6;
	}

IL_0022:
	{
		String_t* L_7 = ___paramName1;
		String_t* L_8 = ___message0;
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_9 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m9EA692D49986AEBAC433CE3381331146109AE20F(L_9, L_7, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		SWIGPendingException_Set_m2A770FCDB68242E723C1E3A5CA7B4B86B969155C(L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::SetPendingArgumentOutOfRangeException(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m64A3753D6C93082BF5431CA3B095125235597BA7 (String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m64A3753D6C93082BF5431CA3B095125235597BA7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t * V_0 = NULL;
	bool V_1 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		Exception_t * L_0 = SWIGPendingException_Retrieve_m47C7FCF8ED6E8C49646820EE0691C3EDE0CFFDCD(/*hidden argument*/NULL);
		V_0 = L_0;
		Exception_t * L_1 = V_0;
		V_1 = (bool)((!(((RuntimeObject*)(Exception_t *)L_1) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_3 = ___message0;
		Exception_t * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(5 /* System.String System.Exception::get_Message() */, L_4);
		String_t* L_6 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(L_3, _stringLiteralA804D931F27BE6EF35191274DAF148C0B4FCEEF6, L_5, /*hidden argument*/NULL);
		___message0 = L_6;
	}

IL_0022:
	{
		String_t* L_7 = ___paramName1;
		String_t* L_8 = ___message0;
		ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * L_9 = (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m300CE4D04A068C209FD858101AC361C1B600B5AE(L_9, L_7, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		SWIGPendingException_Set_m2A770FCDB68242E723C1E3A5CA7B4B86B969155C(L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper__cctor_mC03977EF4F677D23618BA72DEE999DAD6233D0CE (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGExceptionHelper__cctor_mC03977EF4F677D23618BA72DEE999DAD6233D0CE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * L_0 = (ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 *)il2cpp_codegen_object_new(ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m512E847FCB55576B6340A2669679D2D5179DDE8D(L_0, NULL, (intptr_t)((intptr_t)SWIGExceptionHelper_SetPendingApplicationException_m1EB96681AA0CAEE7C648B0488735AC4B34644625_RuntimeMethod_var), /*hidden argument*/NULL);
		((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->set_applicationDelegate_0(L_0);
		ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * L_1 = (ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 *)il2cpp_codegen_object_new(ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m512E847FCB55576B6340A2669679D2D5179DDE8D(L_1, NULL, (intptr_t)((intptr_t)SWIGExceptionHelper_SetPendingArithmeticException_m6FD83BB7EC4D0C5A758FC33434AFF95000BB2145_RuntimeMethod_var), /*hidden argument*/NULL);
		((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->set_arithmeticDelegate_1(L_1);
		ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * L_2 = (ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 *)il2cpp_codegen_object_new(ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m512E847FCB55576B6340A2669679D2D5179DDE8D(L_2, NULL, (intptr_t)((intptr_t)SWIGExceptionHelper_SetPendingDivideByZeroException_mC428FC6EAD280918627E7AD8285B5EC490842944_RuntimeMethod_var), /*hidden argument*/NULL);
		((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->set_divideByZeroDelegate_2(L_2);
		ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * L_3 = (ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 *)il2cpp_codegen_object_new(ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m512E847FCB55576B6340A2669679D2D5179DDE8D(L_3, NULL, (intptr_t)((intptr_t)SWIGExceptionHelper_SetPendingIndexOutOfRangeException_m8AC3EB8E22CAB0B37997524B3D8EB61E7C4B9888_RuntimeMethod_var), /*hidden argument*/NULL);
		((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->set_indexOutOfRangeDelegate_3(L_3);
		ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * L_4 = (ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 *)il2cpp_codegen_object_new(ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m512E847FCB55576B6340A2669679D2D5179DDE8D(L_4, NULL, (intptr_t)((intptr_t)SWIGExceptionHelper_SetPendingInvalidCastException_m3B7ACF97DC9FB2B9F197C4E3168B9C4F8A4D0719_RuntimeMethod_var), /*hidden argument*/NULL);
		((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->set_invalidCastDelegate_4(L_4);
		ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * L_5 = (ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 *)il2cpp_codegen_object_new(ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m512E847FCB55576B6340A2669679D2D5179DDE8D(L_5, NULL, (intptr_t)((intptr_t)SWIGExceptionHelper_SetPendingInvalidOperationException_mECD029C557A14DBA585161CF1E3EDD1C3201813D_RuntimeMethod_var), /*hidden argument*/NULL);
		((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->set_invalidOperationDelegate_5(L_5);
		ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * L_6 = (ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 *)il2cpp_codegen_object_new(ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m512E847FCB55576B6340A2669679D2D5179DDE8D(L_6, NULL, (intptr_t)((intptr_t)SWIGExceptionHelper_SetPendingIOException_mE61B638B09F96D19FA24D9B2BD85230650FC59B2_RuntimeMethod_var), /*hidden argument*/NULL);
		((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->set_ioDelegate_6(L_6);
		ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * L_7 = (ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 *)il2cpp_codegen_object_new(ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m512E847FCB55576B6340A2669679D2D5179DDE8D(L_7, NULL, (intptr_t)((intptr_t)SWIGExceptionHelper_SetPendingNullReferenceException_mB61F8433F7B8D82845D1635AC67E6132F1F3A7B9_RuntimeMethod_var), /*hidden argument*/NULL);
		((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->set_nullReferenceDelegate_7(L_7);
		ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * L_8 = (ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 *)il2cpp_codegen_object_new(ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m512E847FCB55576B6340A2669679D2D5179DDE8D(L_8, NULL, (intptr_t)((intptr_t)SWIGExceptionHelper_SetPendingOutOfMemoryException_mD766FD3AB3B0EF9E768367C0199FC00A9362119E_RuntimeMethod_var), /*hidden argument*/NULL);
		((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->set_outOfMemoryDelegate_8(L_8);
		ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * L_9 = (ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 *)il2cpp_codegen_object_new(ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m512E847FCB55576B6340A2669679D2D5179DDE8D(L_9, NULL, (intptr_t)((intptr_t)SWIGExceptionHelper_SetPendingOverflowException_m4F7B5FA76EE51D9CB219856AB845929CD819FDF2_RuntimeMethod_var), /*hidden argument*/NULL);
		((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->set_overflowDelegate_9(L_9);
		ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * L_10 = (ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 *)il2cpp_codegen_object_new(ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0_il2cpp_TypeInfo_var);
		ExceptionDelegate__ctor_m512E847FCB55576B6340A2669679D2D5179DDE8D(L_10, NULL, (intptr_t)((intptr_t)SWIGExceptionHelper_SetPendingSystemException_mB83BE9B10C217AA022F9C626585DCE16A1E3BF43_RuntimeMethod_var), /*hidden argument*/NULL);
		((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->set_systemDelegate_10(L_10);
		ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * L_11 = (ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 *)il2cpp_codegen_object_new(ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4_il2cpp_TypeInfo_var);
		ExceptionArgumentDelegate__ctor_mC3898DBFD267903C5001A38AD119C7DA5FE561F2(L_11, NULL, (intptr_t)((intptr_t)SWIGExceptionHelper_SetPendingArgumentException_mCC2449972A52DACB566318E5D82883A1AE0FCC77_RuntimeMethod_var), /*hidden argument*/NULL);
		((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->set_argumentDelegate_11(L_11);
		ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * L_12 = (ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 *)il2cpp_codegen_object_new(ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4_il2cpp_TypeInfo_var);
		ExceptionArgumentDelegate__ctor_mC3898DBFD267903C5001A38AD119C7DA5FE561F2(L_12, NULL, (intptr_t)((intptr_t)SWIGExceptionHelper_SetPendingArgumentNullException_mDDF072DC234116293DF4DD4CAE04931816995032_RuntimeMethod_var), /*hidden argument*/NULL);
		((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->set_argumentNullDelegate_12(L_12);
		ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * L_13 = (ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 *)il2cpp_codegen_object_new(ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4_il2cpp_TypeInfo_var);
		ExceptionArgumentDelegate__ctor_mC3898DBFD267903C5001A38AD119C7DA5FE561F2(L_13, NULL, (intptr_t)((intptr_t)SWIGExceptionHelper_SetPendingArgumentOutOfRangeException_m64A3753D6C93082BF5431CA3B095125235597BA7_RuntimeMethod_var), /*hidden argument*/NULL);
		((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->set_argumentOutOfRangeDelegate_13(L_13);
		ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * L_14 = ((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->get_applicationDelegate_0();
		ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * L_15 = ((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->get_arithmeticDelegate_1();
		ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * L_16 = ((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->get_divideByZeroDelegate_2();
		ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * L_17 = ((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->get_indexOutOfRangeDelegate_3();
		ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * L_18 = ((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->get_invalidCastDelegate_4();
		ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * L_19 = ((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->get_invalidOperationDelegate_5();
		ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * L_20 = ((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->get_ioDelegate_6();
		ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * L_21 = ((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->get_nullReferenceDelegate_7();
		ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * L_22 = ((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->get_outOfMemoryDelegate_8();
		ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * L_23 = ((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->get_overflowDelegate_9();
		ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * L_24 = ((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->get_systemDelegate_10();
		SWIGExceptionHelper_SWIGRegisterExceptionCallbacks_AuthUtil_mD13FDDDDC03DD49540B6AFFC935A58DE0B534762(L_14, L_15, L_16, L_17, L_18, L_19, L_20, L_21, L_22, L_23, L_24, /*hidden argument*/NULL);
		ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * L_25 = ((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->get_argumentDelegate_11();
		ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * L_26 = ((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->get_argumentNullDelegate_12();
		ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * L_27 = ((SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_StaticFields*)il2cpp_codegen_static_fields_for(SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97_il2cpp_TypeInfo_var))->get_argumentOutOfRangeDelegate_13();
		SWIGExceptionHelper_SWIGRegisterExceptionCallbacksArgument_AuthUtil_m85D92F4B1140B7B2228AB92E057C747B9A07FCA7(L_25, L_26, L_27, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGExceptionHelper__ctor_mFF2CE315200639030B26C32F018A7E7354B81663 (SWIGExceptionHelper_t6E2B0A643DE272F549E4C26AAD44FD6F987C7A97 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 (ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * __this, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(char*, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Marshaling of parameter '___paramName1' to native representation
	char* ____paramName1_marshaled = NULL;
	____paramName1_marshaled = il2cpp_codegen_marshal_string(___paramName1);

	// Native function invocation
	il2cppPInvokeFunc(____message0_marshaled, ____paramName1_marshaled);

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

	// Marshaling cleanup of parameter '___paramName1' native representation
	il2cpp_codegen_marshal_free(____paramName1_marshaled);
	____paramName1_marshaled = NULL;

}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExceptionArgumentDelegate__ctor_mC3898DBFD267903C5001A38AD119C7DA5FE561F2 (ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate::Invoke(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExceptionArgumentDelegate_Invoke_m6928DBD3A8C7DD5AEF69A157C7833B46B927DA6D (ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * __this, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (String_t*, String_t*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___message0, ___paramName1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, String_t*, String_t*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___message0, ___paramName1, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< String_t* >::Invoke(targetMethod, ___message0, ___paramName1);
					else
						GenericVirtActionInvoker1< String_t* >::Invoke(targetMethod, ___message0, ___paramName1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___message0, ___paramName1);
					else
						VirtActionInvoker1< String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___message0, ___paramName1);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___paramName1) - 1), targetMethod);
				}
				typedef void (*FunctionPointerType) (String_t*, String_t*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___message0, ___paramName1, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker2< String_t*, String_t* >::Invoke(targetMethod, targetThis, ___message0, ___paramName1);
					else
						GenericVirtActionInvoker2< String_t*, String_t* >::Invoke(targetMethod, targetThis, ___message0, ___paramName1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker2< String_t*, String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___message0, ___paramName1);
					else
						VirtActionInvoker2< String_t*, String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___message0, ___paramName1);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, String_t*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___message0) - 1), ___paramName1, targetMethod);
				}
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (String_t*, String_t*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___message0, ___paramName1, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, String_t*, String_t*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___message0, ___paramName1, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ExceptionArgumentDelegate_BeginInvoke_mBF7ADADE9B2D349A22A925C4C1728A92B154C42D (ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * __this, String_t* ___message0, String_t* ___paramName1, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___message0;
	__d_args[1] = ___paramName1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionArgumentDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExceptionArgumentDelegate_EndInvoke_m70375B45334E455A99D59904D6A945A04B6886EF (ExceptionArgumentDelegate_tAC217EB266CFFF0EDF6F83330B0B6489EC3BB3A4 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 (ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * __this, String_t* ___message0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Native function invocation
	il2cppPInvokeFunc(____message0_marshaled);

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExceptionDelegate__ctor_m512E847FCB55576B6340A2669679D2D5179DDE8D (ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate::Invoke(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExceptionDelegate_Invoke_m797B0B4E66994FAF224C4EB881F468E142D745B7 (ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * __this, String_t* ___message0, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (String_t*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___message0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, String_t*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___message0, targetMethod);
			}
		}
		else if (___parameterCount != 1)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, ___message0);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, ___message0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___message0);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___message0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (String_t*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___message0, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< String_t* >::Invoke(targetMethod, targetThis, ___message0);
					else
						GenericVirtActionInvoker1< String_t* >::Invoke(targetMethod, targetThis, ___message0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___message0);
					else
						VirtActionInvoker1< String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___message0);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___message0) - 1), targetMethod);
				}
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (String_t*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___message0, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, String_t*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___message0, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ExceptionDelegate_BeginInvoke_mAC113B823C70AA10A3D5344072B3FDD0101EA845 (ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * __this, String_t* ___message0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___message0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGExceptionHelper_ExceptionDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExceptionDelegate_EndInvoke_mD9FB06B5011432C53792DCFFC3AA5F23D16703EE (ExceptionDelegate_tBE0D8272ED1099F6076364057FA31DE6FC09F4A0 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Firebase.Auth.AuthUtilPINVOKE_SWIGPendingException::get_Pending()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SWIGPendingException_get_Pending_m746EAD61F79AF2A98C4350069D4507EAC1D251DE (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGPendingException_get_Pending_m746EAD61F79AF2A98C4350069D4507EAC1D251DE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	{
		V_0 = (bool)0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		int32_t L_0 = ((SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var))->get_numExceptionsPending_1();
		V_1 = (bool)((((int32_t)L_0) > ((int32_t)0))? 1 : 0);
		bool L_1 = V_1;
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		Exception_t * L_2 = ((SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var))->get_pendingException_0();
		V_2 = (bool)((!(((RuntimeObject*)(Exception_t *)L_2) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_3 = V_2;
		if (!L_3)
		{
			goto IL_001d;
		}
	}
	{
		V_0 = (bool)1;
	}

IL_001d:
	{
		bool L_4 = V_0;
		V_3 = L_4;
		goto IL_0021;
	}

IL_0021:
	{
		bool L_5 = V_3;
		return L_5;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGPendingException::Set(System.Exception)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGPendingException_Set_m2A770FCDB68242E723C1E3A5CA7B4B86B969155C (Exception_t * ___e0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGPendingException_Set_m2A770FCDB68242E723C1E3A5CA7B4B86B969155C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	RuntimeObject * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		Exception_t * L_0 = ((SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var))->get_pendingException_0();
		V_0 = (bool)((!(((RuntimeObject*)(Exception_t *)L_0) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		Exception_t * L_2 = ((SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var))->get_pendingException_0();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_2);
		String_t* L_4 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(_stringLiteral519E5A114736C3C9A5709C059C9D5F69D5683AB2, L_3, _stringLiteralE7064F0B80F61DBC65915311032D27BAA569AE2A, /*hidden argument*/NULL);
		Exception_t * L_5 = ___e0;
		ApplicationException_t664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74 * L_6 = (ApplicationException_t664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74 *)il2cpp_codegen_object_new(ApplicationException_t664823C3E0D3E1E7C7FA1C0DB4E19E98E9811C74_il2cpp_TypeInfo_var);
		ApplicationException__ctor_m0AF8988654AD1CA2DCA8EC12E6D46563E4EE703F(L_6, L_4, L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, SWIGPendingException_Set_m2A770FCDB68242E723C1E3A5CA7B4B86B969155C_RuntimeMethod_var);
	}

IL_002d:
	{
		Exception_t * L_7 = ___e0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		((SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var))->set_pendingException_0(L_7);
		RuntimeObject * L_8 = ((SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var))->get_exceptionsLock_2();
		V_1 = L_8;
		RuntimeObject * L_9 = V_1;
		Monitor_Enter_m903755FCC479745619842CCDBF5E6355319FA102(L_9, /*hidden argument*/NULL);
	}

IL_0040:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		int32_t L_10 = ((SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var))->get_numExceptionsPending_1();
		((SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var))->set_numExceptionsPending_1(((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1)));
		IL2CPP_LEAVE(0x58, FINALLY_0050);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0050;
	}

FINALLY_0050:
	{ // begin finally (depth: 1)
		RuntimeObject * L_11 = V_1;
		Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_11, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(80)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(80)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x58, IL_0058)
	}

IL_0058:
	{
		return;
	}
}
// System.Exception Firebase.Auth.AuthUtilPINVOKE_SWIGPendingException::Retrieve()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Exception_t * SWIGPendingException_Retrieve_m47C7FCF8ED6E8C49646820EE0691C3EDE0CFFDCD (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGPendingException_Retrieve_m47C7FCF8ED6E8C49646820EE0691C3EDE0CFFDCD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	RuntimeObject * V_3 = NULL;
	Exception_t * V_4 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		V_0 = (Exception_t *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		int32_t L_0 = ((SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var))->get_numExceptionsPending_1();
		V_1 = (bool)((((int32_t)L_0) > ((int32_t)0))? 1 : 0);
		bool L_1 = V_1;
		if (!L_1)
		{
			goto IL_0050;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		Exception_t * L_2 = ((SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var))->get_pendingException_0();
		V_2 = (bool)((!(((RuntimeObject*)(Exception_t *)L_2) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_3 = V_2;
		if (!L_3)
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		Exception_t * L_4 = ((SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var))->get_pendingException_0();
		V_0 = L_4;
		((SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var))->set_pendingException_0((Exception_t *)NULL);
		RuntimeObject * L_5 = ((SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var))->get_exceptionsLock_2();
		V_3 = L_5;
		RuntimeObject * L_6 = V_3;
		Monitor_Enter_m903755FCC479745619842CCDBF5E6355319FA102(L_6, /*hidden argument*/NULL);
	}

IL_0036:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		int32_t L_7 = ((SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var))->get_numExceptionsPending_1();
		((SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var))->set_numExceptionsPending_1(((int32_t)il2cpp_codegen_subtract((int32_t)L_7, (int32_t)1)));
		IL2CPP_LEAVE(0x4E, FINALLY_0046);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0046;
	}

FINALLY_0046:
	{ // begin finally (depth: 1)
		RuntimeObject * L_8 = V_3;
		Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(70)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(70)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
	}

IL_004e:
	{
	}

IL_004f:
	{
	}

IL_0050:
	{
		Exception_t * L_9 = V_0;
		V_4 = L_9;
		goto IL_0055;
	}

IL_0055:
	{
		Exception_t * L_10 = V_4;
		return L_10;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGPendingException::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGPendingException__cctor_mC42B091F09F3F6365C73742A66AD460617B4B7EB (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGPendingException__cctor_mC42B091F09F3F6365C73742A66AD460617B4B7EB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_ThreadStaticFields*)il2cpp_codegen_get_thread_static_data(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var))->set_pendingException_0((Exception_t *)NULL);
		((SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var))->set_numExceptionsPending_1(0);
		((SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var))->set_exceptionsLock_2(NULL);
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(L_0, /*hidden argument*/NULL);
		((SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_StaticFields*)il2cpp_codegen_static_fields_for(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var))->set_exceptionsLock_2(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C" char* DEFAULT_CALL ReversePInvokeWrapper_SWIGStringHelper_CreateString_m8CFC15A5CFF34D0E23227F3DB4BD483E6B495134(char* ___cString0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___cString0' to managed representation
	String_t* ____cString0_unmarshaled = NULL;
	____cString0_unmarshaled = il2cpp_codegen_marshal_string_result(___cString0);

	// Managed method invocation
	String_t* returnValue = SWIGStringHelper_CreateString_m8CFC15A5CFF34D0E23227F3DB4BD483E6B495134(____cString0_unmarshaled, NULL);

	// Marshaling of return value back from managed representation
	char* _returnValue_marshaled = NULL;
	_returnValue_marshaled = il2cpp_codegen_marshal_string(returnValue);

	return _returnValue_marshaled;
}
IL2CPP_EXTERN_C void DEFAULT_CALL SWIGRegisterStringCallback_AuthUtil(Il2CppMethodPointer);
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGStringHelper::SWIGRegisterStringCallback_AuthUtil(Firebase.Auth.AuthUtilPINVOKE_SWIGStringHelper_SWIGStringDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGStringHelper_SWIGRegisterStringCallback_AuthUtil_mCF54A64D405E0EE541CB782629AE4D3EFFF01E69 (SWIGStringDelegate_tF943E218400FF77BFF8F047D209AA7CA94915E00 * ___stringDelegate0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer);

	// Marshaling of parameter '___stringDelegate0' to native representation
	Il2CppMethodPointer ____stringDelegate0_marshaled = NULL;
	____stringDelegate0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___stringDelegate0));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SWIGRegisterStringCallback_AuthUtil)(____stringDelegate0_marshaled);

}
// System.String Firebase.Auth.AuthUtilPINVOKE_SWIGStringHelper::CreateString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SWIGStringHelper_CreateString_m8CFC15A5CFF34D0E23227F3DB4BD483E6B495134 (String_t* ___cString0, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___cString0;
		V_0 = L_0;
		goto IL_0005;
	}

IL_0005:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGStringHelper::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGStringHelper__cctor_mE675A6A15898F4EF6BD38CADB60C065692FF609E (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIGStringHelper__cctor_mE675A6A15898F4EF6BD38CADB60C065692FF609E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SWIGStringDelegate_tF943E218400FF77BFF8F047D209AA7CA94915E00 * L_0 = (SWIGStringDelegate_tF943E218400FF77BFF8F047D209AA7CA94915E00 *)il2cpp_codegen_object_new(SWIGStringDelegate_tF943E218400FF77BFF8F047D209AA7CA94915E00_il2cpp_TypeInfo_var);
		SWIGStringDelegate__ctor_m34FFAA789252BBCC866BE5712BD2072DEC39AA75(L_0, NULL, (intptr_t)((intptr_t)SWIGStringHelper_CreateString_m8CFC15A5CFF34D0E23227F3DB4BD483E6B495134_RuntimeMethod_var), /*hidden argument*/NULL);
		((SWIGStringHelper_t0D99651DC671811558BDB3D299B82F879F5E731B_StaticFields*)il2cpp_codegen_static_fields_for(SWIGStringHelper_t0D99651DC671811558BDB3D299B82F879F5E731B_il2cpp_TypeInfo_var))->set_stringDelegate_0(L_0);
		SWIGStringDelegate_tF943E218400FF77BFF8F047D209AA7CA94915E00 * L_1 = ((SWIGStringHelper_t0D99651DC671811558BDB3D299B82F879F5E731B_StaticFields*)il2cpp_codegen_static_fields_for(SWIGStringHelper_t0D99651DC671811558BDB3D299B82F879F5E731B_il2cpp_TypeInfo_var))->get_stringDelegate_0();
		SWIGStringHelper_SWIGRegisterStringCallback_AuthUtil_mCF54A64D405E0EE541CB782629AE4D3EFFF01E69(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGStringHelper::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGStringHelper__ctor_m22B2245AE72469FC2016E3B70329E4117E38AF96 (SWIGStringHelper_t0D99651DC671811558BDB3D299B82F879F5E731B * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  String_t* DelegatePInvokeWrapper_SWIGStringDelegate_tF943E218400FF77BFF8F047D209AA7CA94915E00 (SWIGStringDelegate_tF943E218400FF77BFF8F047D209AA7CA94915E00 * __this, String_t* ___message0, const RuntimeMethod* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Native function invocation
	char* returnValue = il2cppPInvokeFunc(____message0_marshaled);

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

	return _returnValue_unmarshaled;
}
// System.Void Firebase.Auth.AuthUtilPINVOKE_SWIGStringHelper_SWIGStringDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIGStringDelegate__ctor_m34FFAA789252BBCC866BE5712BD2072DEC39AA75 (SWIGStringDelegate_tF943E218400FF77BFF8F047D209AA7CA94915E00 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.String Firebase.Auth.AuthUtilPINVOKE_SWIGStringHelper_SWIGStringDelegate::Invoke(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SWIGStringDelegate_Invoke_mCB669A3F17E62E6075E1CCAD17E777E903F5739C (SWIGStringDelegate_tF943E218400FF77BFF8F047D209AA7CA94915E00 * __this, String_t* ___message0, const RuntimeMethod* method)
{
	String_t* result = NULL;
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef String_t* (*FunctionPointerType) (String_t*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___message0, targetMethod);
			}
			else
			{
				// closed
				typedef String_t* (*FunctionPointerType) (void*, String_t*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___message0, targetMethod);
			}
		}
		else if (___parameterCount != 1)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker0< String_t* >::Invoke(targetMethod, ___message0);
					else
						result = GenericVirtFuncInvoker0< String_t* >::Invoke(targetMethod, ___message0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker0< String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___message0);
					else
						result = VirtFuncInvoker0< String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___message0);
				}
			}
			else
			{
				typedef String_t* (*FunctionPointerType) (String_t*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___message0, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker1< String_t*, String_t* >::Invoke(targetMethod, targetThis, ___message0);
					else
						result = GenericVirtFuncInvoker1< String_t*, String_t* >::Invoke(targetMethod, targetThis, ___message0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker1< String_t*, String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___message0);
					else
						result = VirtFuncInvoker1< String_t*, String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___message0);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef String_t* (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(___message0) - 1), targetMethod);
				}
				if (targetThis == NULL)
				{
					typedef String_t* (*FunctionPointerType) (String_t*, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___message0, targetMethod);
				}
				else
				{
					typedef String_t* (*FunctionPointerType) (void*, String_t*, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___message0, targetMethod);
				}
			}
		}
	}
	return result;
}
// System.IAsyncResult Firebase.Auth.AuthUtilPINVOKE_SWIGStringHelper_SWIGStringDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* SWIGStringDelegate_BeginInvoke_mA37707372B07564EB2B162155D8F88A43C9F89BF (SWIGStringDelegate_tF943E218400FF77BFF8F047D209AA7CA94915E00 * __this, String_t* ___message0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___message0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.String Firebase.Auth.AuthUtilPINVOKE_SWIGStringHelper_SWIGStringDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SWIGStringDelegate_EndInvoke_m70160AEB75A18A0445E89204A64C3CFEF21F767E (SWIGStringDelegate_tF943E218400FF77BFF8F047D209AA7CA94915E00 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (String_t*)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Auth.Credential::.ctor(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Credential__ctor_mBC968251CEBE306DF7CD89C85E74D1B1D451CFC7 (Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		bool L_0 = ___cMemoryOwn1;
		__this->set_swigCMemOwn_1(L_0);
		intptr_t L_1 = ___cPtr0;
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_2;
		memset((&L_2), 0, sizeof(L_2));
		HandleRef__ctor_mCD64E7F91766FE7602EE34A8CEEF6C1EF4C943BB((&L_2), __this, (intptr_t)L_1, /*hidden argument*/NULL);
		__this->set_swigCPtr_0(L_2);
		return;
	}
}
// System.Runtime.InteropServices.HandleRef Firebase.Auth.Credential::getCPtr(Firebase.Auth.Credential)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HandleRef_t876E76124F400D12395BF61D562162AB6822204A  Credential_getCPtr_m892678A92D70544624773536D6E26DDEDFE63E42 (Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Credential_getCPtr_m892678A92D70544624773536D6E26DDEDFE63E42_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  V_0;
	memset((&V_0), 0, sizeof(V_0));
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  G_B3_0;
	memset((&G_B3_0), 0, sizeof(G_B3_0));
	{
		Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * L_0 = ___obj0;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * L_1 = ___obj0;
		NullCheck(L_1);
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_2 = L_1->get_swigCPtr_0();
		G_B3_0 = L_2;
		goto IL_0017;
	}

IL_000c:
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_3;
		memset((&L_3), 0, sizeof(L_3));
		HandleRef__ctor_mCD64E7F91766FE7602EE34A8CEEF6C1EF4C943BB((&L_3), NULL, (intptr_t)(0), /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_0017:
	{
		V_0 = G_B3_0;
		goto IL_001a;
	}

IL_001a:
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_4 = V_0;
		return L_4;
	}
}
// System.Void Firebase.Auth.Credential::Finalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Credential_Finalize_mD6808D5E0D70311F7639F026600E220CF64ED657 (Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		Credential_Dispose_m78B018A97AF219BCDE1DCA819A2E94EA8BF85FC5(__this, (bool)0, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x14, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m4015B7D3A44DE125C5FE34D7276CD4697C06F380(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x14, IL_0014)
	}

IL_0014:
	{
		return;
	}
}
// System.Void Firebase.Auth.Credential::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Credential_Dispose_m76866FF791102DEEC0E64363C978654CA2E0DD87 (Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Credential_Dispose_m76866FF791102DEEC0E64363C978654CA2E0DD87_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Credential_Dispose_m78B018A97AF219BCDE1DCA819A2E94EA8BF85FC5(__this, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GC_tC1D7BD74E8F44ECCEF5CD2B5D84BFF9AAE02D01D_il2cpp_TypeInfo_var);
		GC_SuppressFinalize_m037319A9B95A5BA437E806DE592802225EE5B425(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.Credential::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Credential_Dispose_m78B018A97AF219BCDE1DCA819A2E94EA8BF85FC5 (Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * __this, bool ___disposing0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Credential_Dispose_m78B018A97AF219BCDE1DCA819A2E94EA8BF85FC5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_il2cpp_TypeInfo_var);
		RuntimeObject * L_0 = ((FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_il2cpp_TypeInfo_var))->get_disposeLock_2();
		V_0 = L_0;
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_m903755FCC479745619842CCDBF5E6355319FA102(L_1, /*hidden argument*/NULL);
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			HandleRef_t876E76124F400D12395BF61D562162AB6822204A * L_2 = __this->get_address_of_swigCPtr_0();
			intptr_t L_3 = HandleRef_get_Handle_m095712F299EA36BF75975343C726FA160EE38277_inline((HandleRef_t876E76124F400D12395BF61D562162AB6822204A *)L_2, /*hidden argument*/NULL);
			bool L_4 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_3, (intptr_t)(0), /*hidden argument*/NULL);
			V_1 = L_4;
			bool L_5 = V_1;
			if (!L_5)
			{
				goto IL_005a;
			}
		}

IL_0028:
		{
			bool L_6 = __this->get_swigCMemOwn_1();
			V_2 = L_6;
			bool L_7 = V_2;
			if (!L_7)
			{
				goto IL_0048;
			}
		}

IL_0033:
		{
			__this->set_swigCMemOwn_1((bool)0);
			HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_8 = __this->get_swigCPtr_0();
			IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
			AuthUtilPINVOKE_delete_Credential_m02588ECD12EF7FBADE68921F2AC6F2DBA0E37C88(L_8, /*hidden argument*/NULL);
		}

IL_0048:
		{
			HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_9;
			memset((&L_9), 0, sizeof(L_9));
			HandleRef__ctor_mCD64E7F91766FE7602EE34A8CEEF6C1EF4C943BB((&L_9), NULL, (intptr_t)(0), /*hidden argument*/NULL);
			__this->set_swigCPtr_0(L_9);
		}

IL_005a:
		{
			IL2CPP_RUNTIME_CLASS_INIT(GC_tC1D7BD74E8F44ECCEF5CD2B5D84BFF9AAE02D01D_il2cpp_TypeInfo_var);
			GC_SuppressFinalize_m037319A9B95A5BA437E806DE592802225EE5B425(__this, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x6C, FINALLY_0064);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0064;
	}

FINALLY_0064:
	{ // begin finally (depth: 1)
		RuntimeObject * L_10 = V_0;
		Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_10, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(100)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(100)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x6C, IL_006c)
	}

IL_006c:
	{
		return;
	}
}
// System.Boolean Firebase.Auth.Credential::IsValid()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Credential_IsValid_m301CF4DEDE9449F85400AECDABF195D322DEA039 (Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Credential_IsValid_m301CF4DEDE9449F85400AECDABF195D322DEA039_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_0();
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		bool L_1 = AuthUtilPINVOKE_Credential_IsValid_m590E8B8C5882B73A5D6C253E2E6591D789A61081(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_1 = L_2;
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_4 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, Credential_IsValid_m301CF4DEDE9449F85400AECDABF195D322DEA039_RuntimeMethod_var);
	}

IL_001c:
	{
		bool L_5 = V_0;
		V_2 = L_5;
		goto IL_0020;
	}

IL_0020:
	{
		bool L_6 = V_2;
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Auth.FacebookAuthProvider::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FacebookAuthProvider__cctor_mF5D3E8AA0DA69CEDDE05F5547497DC789089ECF6 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookAuthProvider__cctor_mF5D3E8AA0DA69CEDDE05F5547497DC789089ECF6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LogUtil_t09D9F776A2032DC33A29AA0E2D4E4E508B8508C4_il2cpp_TypeInfo_var);
		LogUtil_InitializeLogging_m3DC0D9A5186F8E0E1972761477DB5461FFCEC354(/*hidden argument*/NULL);
		return;
	}
}
// Firebase.Auth.Credential Firebase.Auth.FacebookAuthProvider::GetCredential(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * FacebookAuthProvider_GetCredential_m29842AFC31DCAFC30F2E1D492DD575A83512C3F5 (String_t* ___accessToken0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookAuthProvider_GetCredential_m29842AFC31DCAFC30F2E1D492DD575A83512C3F5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * V_0 = NULL;
	bool V_1 = false;
	Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * V_2 = NULL;
	{
		String_t* L_0 = ___accessToken0;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		intptr_t L_1 = AuthUtilPINVOKE_FacebookAuthProvider_GetCredential_m6C17A5533AB95072B52FEFFEF1BC520C8B3B89BF(L_0, /*hidden argument*/NULL);
		Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * L_2 = (Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 *)il2cpp_codegen_object_new(Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630_il2cpp_TypeInfo_var);
		Credential__ctor_mBC968251CEBE306DF7CD89C85E74D1B1D451CFC7(L_2, (intptr_t)L_1, (bool)1, /*hidden argument*/NULL);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_3 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_1 = L_3;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_5 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, FacebookAuthProvider_GetCredential_m29842AFC31DCAFC30F2E1D492DD575A83512C3F5_RuntimeMethod_var);
	}

IL_001d:
	{
		Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * L_6 = V_0;
		V_2 = L_6;
		goto IL_0021;
	}

IL_0021:
	{
		Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * L_7 = V_2;
		return L_7;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_FirebaseAuth_StateChangedFunction_m3EB4E147700973B35AAB4C934E401A47B0098CB7(intptr_t ___appCPtr0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Managed method invocation
	FirebaseAuth_StateChangedFunction_m3EB4E147700973B35AAB4C934E401A47B0098CB7(___appCPtr0, NULL);

}
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_FirebaseAuth_IdTokenChangedFunction_m4867AD7FBED6DB5C1529D485866EB591AFC67BBF(intptr_t ___appCPtr0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Managed method invocation
	FirebaseAuth_IdTokenChangedFunction_m4867AD7FBED6DB5C1529D485866EB591AFC67BBF(___appCPtr0, NULL);

}
// System.Void Firebase.Auth.FirebaseAuth::.ctor(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth__ctor_m5A11B7E7ACB1FF9296510387F4A203804EC9B5D0 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method)
{
	{
		__this->set_persistentLoaded_9((bool)0);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		bool L_0 = ___cMemoryOwn1;
		__this->set_swigCMemOwn_1(L_0);
		intptr_t L_1 = ___cPtr0;
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_2;
		memset((&L_2), 0, sizeof(L_2));
		HandleRef__ctor_mCD64E7F91766FE7602EE34A8CEEF6C1EF4C943BB((&L_2), __this, (intptr_t)L_1, /*hidden argument*/NULL);
		__this->set_swigCPtr_0(L_2);
		return;
	}
}
// System.Runtime.InteropServices.HandleRef Firebase.Auth.FirebaseAuth::getCPtr(Firebase.Auth.FirebaseAuth)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HandleRef_t876E76124F400D12395BF61D562162AB6822204A  FirebaseAuth_getCPtr_m43139A9D3674A9DFF8AED10B43D345217CE378C9 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_getCPtr_m43139A9D3674A9DFF8AED10B43D345217CE378C9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  V_0;
	memset((&V_0), 0, sizeof(V_0));
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  G_B3_0;
	memset((&G_B3_0), 0, sizeof(G_B3_0));
	{
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_0 = ___obj0;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_1 = ___obj0;
		NullCheck(L_1);
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_2 = L_1->get_swigCPtr_0();
		G_B3_0 = L_2;
		goto IL_0017;
	}

IL_000c:
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_3;
		memset((&L_3), 0, sizeof(L_3));
		HandleRef__ctor_mCD64E7F91766FE7602EE34A8CEEF6C1EF4C943BB((&L_3), NULL, (intptr_t)(0), /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_0017:
	{
		V_0 = G_B3_0;
		goto IL_001a;
	}

IL_001a:
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_4 = V_0;
		return L_4;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::Finalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_Finalize_m5E134A851F020A7EA755620983EF8D2879DCE270 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		FirebaseAuth_Dispose_m31BB65B92A235E510C6FBC6B250BFE0D5D4D4A18(__this, (bool)0, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x14, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m4015B7D3A44DE125C5FE34D7276CD4697C06F380(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x14, IL_0014)
	}

IL_0014:
	{
		return;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_Dispose_mDAE8E88D96D92B5D08A8E3165DFA4F3F4DCF30DA (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_Dispose_mDAE8E88D96D92B5D08A8E3165DFA4F3F4DCF30DA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FirebaseAuth_Dispose_m31BB65B92A235E510C6FBC6B250BFE0D5D4D4A18(__this, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GC_tC1D7BD74E8F44ECCEF5CD2B5D84BFF9AAE02D01D_il2cpp_TypeInfo_var);
		GC_SuppressFinalize_m037319A9B95A5BA437E806DE592802225EE5B425(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_Dispose_m31BB65B92A235E510C6FBC6B250BFE0D5D4D4A18 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, bool ___disposing0, const RuntimeMethod* method)
{
	{
		FirebaseAuth_DisposeInternal_m5DD53E6F76144CC4B417157E6C3475F7704117DC(__this, /*hidden argument*/NULL);
		return;
	}
}
// Firebase.Auth.FirebaseAuth Firebase.Auth.FirebaseAuth::ProxyFromAppCPtr(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * FirebaseAuth_ProxyFromAppCPtr_mB07E3A48DDE2BFB10C206669CC172BAC2FE69A15 (intptr_t ___appCPtr0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_ProxyFromAppCPtr_mB07E3A48DDE2BFB10C206669CC172BAC2FE69A15_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * V_0 = NULL;
	FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * V_1 = NULL;
	bool V_2 = false;
	FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
		Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * L_0 = ((FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var))->get_appCPtrToAuth_10();
		V_0 = L_0;
		Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * L_1 = V_0;
		Monitor_Enter_m903755FCC479745619842CCDBF5E6355319FA102(L_1, /*hidden argument*/NULL);
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
			Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * L_2 = ((FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var))->get_appCPtrToAuth_10();
			intptr_t L_3 = ___appCPtr0;
			NullCheck(L_2);
			bool L_4 = Dictionary_2_TryGetValue_mBF4E69D81E01649E50C6DFD9BFE384903C7B81C8(L_2, (intptr_t)L_3, (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D **)(&V_1), /*hidden argument*/Dictionary_2_TryGetValue_mBF4E69D81E01649E50C6DFD9BFE384903C7B81C8_RuntimeMethod_var);
			V_2 = L_4;
			bool L_5 = V_2;
			if (!L_5)
			{
				goto IL_0025;
			}
		}

IL_0020:
		{
			FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_6 = V_1;
			V_3 = L_6;
			IL2CPP_LEAVE(0x34, FINALLY_0028);
		}

IL_0025:
		{
			IL2CPP_LEAVE(0x30, FINALLY_0028);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0028;
	}

FINALLY_0028:
	{ // begin finally (depth: 1)
		Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * L_7 = V_0;
		Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_7, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(40)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(40)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x34, IL_0034)
		IL2CPP_JUMP_TBL(0x30, IL_0030)
	}

IL_0030:
	{
		V_3 = (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D *)NULL;
		goto IL_0034;
	}

IL_0034:
	{
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_8 = V_3;
		return L_8;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::ThrowIfNull()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_ThrowIfNull_m927509F7D041820C618B0061E723697AEDEFC3EC (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_ThrowIfNull_m927509F7D041820C618B0061E723697AEDEFC3EC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A * L_0 = __this->get_address_of_swigCPtr_0();
		intptr_t L_1 = HandleRef_get_Handle_m095712F299EA36BF75975343C726FA160EE38277_inline((HandleRef_t876E76124F400D12395BF61D562162AB6822204A *)L_0, /*hidden argument*/NULL);
		bool L_2 = IntPtr_op_Equality_mEE8D9FD2DFE312BBAA8B4ED3BF7976B3142A5934((intptr_t)L_1, (intptr_t)(0), /*hidden argument*/NULL);
		V_0 = L_2;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0021;
		}
	}
	{
		NullReferenceException_t204B194BC4DDA3259AF5A8633EA248AE5977ABDC * L_4 = (NullReferenceException_t204B194BC4DDA3259AF5A8633EA248AE5977ABDC *)il2cpp_codegen_object_new(NullReferenceException_t204B194BC4DDA3259AF5A8633EA248AE5977ABDC_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m7D46E331C349DD29CBA488C9B6A950A3E7DD5CAE(L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, FirebaseAuth_ThrowIfNull_m927509F7D041820C618B0061E723697AEDEFC3EC_RuntimeMethod_var);
	}

IL_0021:
	{
		return;
	}
}
// Firebase.Auth.FirebaseAuth Firebase.Auth.FirebaseAuth::GetAuth(Firebase.FirebaseApp)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * FirebaseAuth_GetAuth_mEFC135E9B1D37E4739D3E2A8D30A1C80DDE3F157 (FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * ___app0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_GetAuth_mEFC135E9B1D37E4739D3E2A8D30A1C80DDE3F157_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * V_0 = NULL;
	Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * V_1 = NULL;
	U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683 * V_2 = NULL;
	intptr_t V_3;
	memset((&V_3), 0, sizeof(V_3));
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  V_4;
	memset((&V_4), 0, sizeof(V_4));
	bool V_5 = false;
	FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * V_6 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * L_0 = (U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass19_0__ctor_mACC4F54E5C54C1F4E1983D751939FEE59B3EAE4D(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * L_1 = V_0;
		FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * L_2 = ___app0;
		NullCheck(L_1);
		L_1->set_app_1(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
		Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * L_3 = ((FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var))->get_appCPtrToAuth_10();
		V_1 = L_3;
		Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * L_4 = V_1;
		Monitor_Enter_m903755FCC479745619842CCDBF5E6355319FA102(L_4, /*hidden argument*/NULL);
	}

IL_001b:
	try
	{ // begin try (depth: 1)
		{
			U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683 * L_5 = (U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683_il2cpp_TypeInfo_var);
			U3CU3Ec__DisplayClass19_1__ctor_m1C29623F09C98E18908C68A9AAEA1F77D1CC820F(L_5, /*hidden argument*/NULL);
			V_2 = L_5;
			U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683 * L_6 = V_2;
			U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * L_7 = V_0;
			NullCheck(L_6);
			L_6->set_CSU24U3CU3E8__locals1_1(L_7);
			U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683 * L_8 = V_2;
			NullCheck(L_8);
			U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * L_9 = L_8->get_CSU24U3CU3E8__locals1_1();
			NullCheck(L_9);
			FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * L_10 = L_9->get_app_1();
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_il2cpp_TypeInfo_var);
			HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_11 = FirebaseApp_getCPtr_mF922A07C69469D0EB5A268A9278C495805BDCA4F(L_10, /*hidden argument*/NULL);
			V_4 = L_11;
			intptr_t L_12 = HandleRef_get_Handle_m095712F299EA36BF75975343C726FA160EE38277_inline((HandleRef_t876E76124F400D12395BF61D562162AB6822204A *)(&V_4), /*hidden argument*/NULL);
			V_3 = (intptr_t)L_12;
			U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683 * L_13 = V_2;
			NullCheck(L_13);
			U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * L_14 = L_13->get_CSU24U3CU3E8__locals1_1();
			intptr_t L_15 = V_3;
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
			FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_16 = FirebaseAuth_ProxyFromAppCPtr_mB07E3A48DDE2BFB10C206669CC172BAC2FE69A15((intptr_t)L_15, /*hidden argument*/NULL);
			NullCheck(L_14);
			L_14->set_auth_0(L_16);
			U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683 * L_17 = V_2;
			NullCheck(L_17);
			U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * L_18 = L_17->get_CSU24U3CU3E8__locals1_1();
			NullCheck(L_18);
			FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_19 = L_18->get_auth_0();
			V_5 = (bool)((!(((RuntimeObject*)(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D *)L_19) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
			bool L_20 = V_5;
			if (!L_20)
			{
				goto IL_008c;
			}
		}

IL_0068:
		{
			U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683 * L_21 = V_2;
			NullCheck(L_21);
			U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * L_22 = L_21->get_CSU24U3CU3E8__locals1_1();
			NullCheck(L_22);
			FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * L_23 = L_22->get_app_1();
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
			FirebaseAuth_LogHeartbeatInternal_m39BE7DCAB1FCAC3F93F7366A80B69B5ADE4CC145(L_23, /*hidden argument*/NULL);
			U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683 * L_24 = V_2;
			NullCheck(L_24);
			U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * L_25 = L_24->get_CSU24U3CU3E8__locals1_1();
			NullCheck(L_25);
			FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_26 = L_25->get_auth_0();
			V_6 = L_26;
			IL2CPP_LEAVE(0x175, FINALLY_0163);
		}

IL_008c:
		{
			U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683 * L_27 = V_2;
			Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_28 = (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 *)il2cpp_codegen_object_new(Action_t591D2A86165F896B4B800BB5C25CE18672A55579_il2cpp_TypeInfo_var);
			Action__ctor_m570E96B2A0C48BC1DC6788460316191F24572760(L_28, L_27, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass19_1_U3CGetAuthU3Eb__0_m50656295764711C10015E4E049FAD60671EC162A_RuntimeMethod_var), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_il2cpp_TypeInfo_var);
			FirebaseApp_TranslateDllNotFoundException_mC6F04F02034B21A8A258012156CA7F263FB594F6(L_28, /*hidden argument*/NULL);
			U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683 * L_29 = V_2;
			NullCheck(L_29);
			U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * L_30 = L_29->get_CSU24U3CU3E8__locals1_1();
			NullCheck(L_30);
			FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_31 = L_30->get_auth_0();
			U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683 * L_32 = V_2;
			NullCheck(L_32);
			U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * L_33 = L_32->get_CSU24U3CU3E8__locals1_1();
			NullCheck(L_33);
			FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_34 = L_33->get_auth_0();
			StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294 * L_35 = (StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294 *)il2cpp_codegen_object_new(StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294_il2cpp_TypeInfo_var);
			StateChangedDelegate__ctor_m5A465ECFE2F3056DE914C988625D85656F431F1C(L_35, NULL, (intptr_t)((intptr_t)FirebaseAuth_StateChangedFunction_m3EB4E147700973B35AAB4C934E401A47B0098CB7_RuntimeMethod_var), /*hidden argument*/NULL);
			intptr_t L_36 = AuthUtil_CreateAuthStateListener_m5B798BC3C73CF1288B517705947C8F968F49C828(L_34, L_35, /*hidden argument*/NULL);
			NullCheck(L_31);
			L_31->set_authStateListener_4((intptr_t)L_36);
			U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683 * L_37 = V_2;
			NullCheck(L_37);
			U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * L_38 = L_37->get_CSU24U3CU3E8__locals1_1();
			NullCheck(L_38);
			FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_39 = L_38->get_auth_0();
			U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683 * L_40 = V_2;
			NullCheck(L_40);
			U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * L_41 = L_40->get_CSU24U3CU3E8__locals1_1();
			NullCheck(L_41);
			FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_42 = L_41->get_auth_0();
			StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294 * L_43 = (StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294 *)il2cpp_codegen_object_new(StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294_il2cpp_TypeInfo_var);
			StateChangedDelegate__ctor_m5A465ECFE2F3056DE914C988625D85656F431F1C(L_43, NULL, (intptr_t)((intptr_t)FirebaseAuth_IdTokenChangedFunction_m4867AD7FBED6DB5C1529D485866EB591AFC67BBF_RuntimeMethod_var), /*hidden argument*/NULL);
			intptr_t L_44 = AuthUtil_CreateIdTokenListener_m48BB536BB8AFB937461FEB96BB910727FAD27187(L_42, L_43, /*hidden argument*/NULL);
			NullCheck(L_39);
			L_39->set_idTokenListener_5((intptr_t)L_44);
			U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683 * L_45 = V_2;
			NullCheck(L_45);
			U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * L_46 = L_45->get_CSU24U3CU3E8__locals1_1();
			NullCheck(L_46);
			FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_47 = L_46->get_auth_0();
			U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683 * L_48 = V_2;
			NullCheck(L_48);
			U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * L_49 = L_48->get_CSU24U3CU3E8__locals1_1();
			NullCheck(L_49);
			FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * L_50 = L_49->get_app_1();
			NullCheck(L_47);
			L_47->set_appProxy_2(L_50);
			U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683 * L_51 = V_2;
			NullCheck(L_51);
			U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * L_52 = L_51->get_CSU24U3CU3E8__locals1_1();
			NullCheck(L_52);
			FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_53 = L_52->get_auth_0();
			intptr_t L_54 = V_3;
			NullCheck(L_53);
			L_53->set_appCPtr_3((intptr_t)L_54);
			U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683 * L_55 = V_2;
			NullCheck(L_55);
			U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * L_56 = L_55->get_CSU24U3CU3E8__locals1_1();
			NullCheck(L_56);
			FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * L_57 = L_56->get_app_1();
			U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683 * L_58 = V_2;
			NullCheck(L_58);
			U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * L_59 = L_58->get_CSU24U3CU3E8__locals1_1();
			NullCheck(L_59);
			FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_60 = L_59->get_auth_0();
			EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_61 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
			EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_61, L_60, (intptr_t)((intptr_t)FirebaseAuth_OnAppDisposed_m432A4ECCD62471F891EC24B73F0BE81794DCA4EB_RuntimeMethod_var), /*hidden argument*/NULL);
			NullCheck(L_57);
			FirebaseApp_add_AppDisposed_m1CA5C0D303E4A5536DFDBE3356A900B3B966D4B6(L_57, L_61, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
			Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * L_62 = ((FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var))->get_appCPtrToAuth_10();
			intptr_t L_63 = V_3;
			U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683 * L_64 = V_2;
			NullCheck(L_64);
			U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * L_65 = L_64->get_CSU24U3CU3E8__locals1_1();
			NullCheck(L_65);
			FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_66 = L_65->get_auth_0();
			NullCheck(L_62);
			Dictionary_2_set_Item_mABDE8D3E6C60B55410DDB2AA6FE39F9CD5D31BC9(L_62, (intptr_t)L_63, L_66, /*hidden argument*/Dictionary_2_set_Item_mABDE8D3E6C60B55410DDB2AA6FE39F9CD5D31BC9_RuntimeMethod_var);
			IL2CPP_LEAVE(0x16B, FINALLY_0163);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0163;
	}

FINALLY_0163:
	{ // begin finally (depth: 1)
		Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * L_67 = V_1;
		Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_67, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(355)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(355)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x175, IL_0175)
		IL2CPP_JUMP_TBL(0x16B, IL_016b)
	}

IL_016b:
	{
		U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * L_68 = V_0;
		NullCheck(L_68);
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_69 = L_68->get_auth_0();
		V_6 = L_69;
		goto IL_0175;
	}

IL_0175:
	{
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_70 = V_6;
		return L_70;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::OnAppDisposed(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_OnAppDisposed_m432A4ECCD62471F891EC24B73F0BE81794DCA4EB (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, RuntimeObject * ___sender0, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___eventArgs1, const RuntimeMethod* method)
{
	{
		FirebaseAuth_Dispose_mDAE8E88D96D92B5D08A8E3165DFA4F3F4DCF30DA(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::DisposeInternal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_DisposeInternal_m5DD53E6F76144CC4B417157E6C3475F7704117DC (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_DisposeInternal_m5DD53E6F76144CC4B417157E6C3475F7704117DC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
		Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * L_0 = ((FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var))->get_appCPtrToAuth_10();
		V_0 = L_0;
		Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * L_1 = V_0;
		Monitor_Enter_m903755FCC479745619842CCDBF5E6355319FA102(L_1, /*hidden argument*/NULL);
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_il2cpp_TypeInfo_var);
			RuntimeObject * L_2 = ((FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_il2cpp_TypeInfo_var))->get_disposeLock_2();
			V_1 = L_2;
			RuntimeObject * L_3 = V_1;
			Monitor_Enter_m903755FCC479745619842CCDBF5E6355319FA102(L_3, /*hidden argument*/NULL);
		}

IL_001c:
		try
		{ // begin try (depth: 2)
			{
				IL2CPP_RUNTIME_CLASS_INIT(GC_tC1D7BD74E8F44ECCEF5CD2B5D84BFF9AAE02D01D_il2cpp_TypeInfo_var);
				GC_SuppressFinalize_m037319A9B95A5BA437E806DE592802225EE5B425(__this, /*hidden argument*/NULL);
				HandleRef_t876E76124F400D12395BF61D562162AB6822204A * L_4 = __this->get_address_of_swigCPtr_0();
				intptr_t L_5 = HandleRef_get_Handle_m095712F299EA36BF75975343C726FA160EE38277_inline((HandleRef_t876E76124F400D12395BF61D562162AB6822204A *)L_4, /*hidden argument*/NULL);
				bool L_6 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_5, (intptr_t)(0), /*hidden argument*/NULL);
				V_2 = L_6;
				bool L_7 = V_2;
				if (!L_7)
				{
					goto IL_011e;
				}
			}

IL_0040:
			{
				IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
				Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * L_8 = ((FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var))->get_appCPtrToAuth_10();
				intptr_t L_9 = __this->get_appCPtr_3();
				NullCheck(L_8);
				Dictionary_2_Remove_m11C98448049706AEDFE36C6B229DC8C1DA5AD74F(L_8, (intptr_t)L_9, /*hidden argument*/Dictionary_2_Remove_m11C98448049706AEDFE36C6B229DC8C1DA5AD74F_RuntimeMethod_var);
				FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * L_10 = __this->get_appProxy_2();
				EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_11 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
				EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_11, __this, (intptr_t)((intptr_t)FirebaseAuth_OnAppDisposed_m432A4ECCD62471F891EC24B73F0BE81794DCA4EB_RuntimeMethod_var), /*hidden argument*/NULL);
				NullCheck(L_10);
				FirebaseApp_remove_AppDisposed_mE9EBC4903F93C997C038F8B12940A3D658624EA9(L_10, L_11, /*hidden argument*/NULL);
				__this->set_appProxy_2((FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 *)NULL);
				__this->set_appCPtr_3((intptr_t)(0));
				FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_12 = __this->get_currentUser_6();
				V_3 = (bool)((!(((RuntimeObject*)(FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *)L_12) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
				bool L_13 = V_3;
				if (!L_13)
				{
					goto IL_009e;
				}
			}

IL_0089:
			{
				FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_14 = __this->get_currentUser_6();
				NullCheck(L_14);
				UserInfoInterface_Dispose_mE1B360122F9E6726A8FE40382AE6BD8683EAD61B(L_14, /*hidden argument*/NULL);
				__this->set_currentUser_6((FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *)NULL);
			}

IL_009e:
			{
				intptr_t L_15 = __this->get_authStateListener_4();
				bool L_16 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_15, (intptr_t)(0), /*hidden argument*/NULL);
				V_4 = L_16;
				bool L_17 = V_4;
				if (!L_17)
				{
					goto IL_00ce;
				}
			}

IL_00b4:
			{
				intptr_t L_18 = __this->get_authStateListener_4();
				AuthUtil_DestroyAuthStateListener_m37102E0B2C69B71C1517E7771899467C04916BC7(__this, (intptr_t)L_18, /*hidden argument*/NULL);
				__this->set_authStateListener_4((intptr_t)(0));
			}

IL_00ce:
			{
				intptr_t L_19 = __this->get_idTokenListener_5();
				bool L_20 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_19, (intptr_t)(0), /*hidden argument*/NULL);
				V_5 = L_20;
				bool L_21 = V_5;
				if (!L_21)
				{
					goto IL_00fe;
				}
			}

IL_00e4:
			{
				intptr_t L_22 = __this->get_idTokenListener_5();
				AuthUtil_DestroyIdTokenListener_m57CA413B20CBFF946AA457063051AF94A03ECF22(__this, (intptr_t)L_22, /*hidden argument*/NULL);
				__this->set_idTokenListener_5((intptr_t)(0));
			}

IL_00fe:
			{
				IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
				FirebaseAuth_ReleaseReferenceInternal_mDCB0650BAC51CBB644F738FF72AF2B18D2F3862B(__this, /*hidden argument*/NULL);
				__this->set_swigCMemOwn_1((bool)0);
				HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_23;
				memset((&L_23), 0, sizeof(L_23));
				HandleRef__ctor_mCD64E7F91766FE7602EE34A8CEEF6C1EF4C943BB((&L_23), NULL, (intptr_t)(0), /*hidden argument*/NULL);
				__this->set_swigCPtr_0(L_23);
			}

IL_011e:
			{
				IL2CPP_LEAVE(0x129, FINALLY_0121);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0121;
		}

FINALLY_0121:
		{ // begin finally (depth: 2)
			RuntimeObject * L_24 = V_1;
			Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_24, /*hidden argument*/NULL);
			IL2CPP_END_FINALLY(289)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(289)
		{
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
			IL2CPP_JUMP_TBL(0x129, IL_0129)
		}

IL_0129:
		{
			IL2CPP_LEAVE(0x134, FINALLY_012c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_012c;
	}

FINALLY_012c:
	{ // begin finally (depth: 1)
		Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * L_25 = V_0;
		Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_25, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(300)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(300)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x134, IL_0134)
	}

IL_0134:
	{
		return;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::ForwardStateChange(System.IntPtr,System.Action`1<Firebase.Auth.FirebaseAuth>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_ForwardStateChange_m493936DA530F8A8DE041E7F2C8E1409B0924BE5C (intptr_t ___appCPtr0, Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * ___stateChangeClosure1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_ForwardStateChange_m493936DA530F8A8DE041E7F2C8E1409B0924BE5C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass22_0_t0B9E0A71713E1E0BCBE5C32D0A17C7786CF35818 * V_0 = NULL;
	Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * V_1 = NULL;
	U3CU3Ec__DisplayClass22_1_tE096CD1670F7DFF2DC0A9E52B629F80ED0B6ED1B * V_2 = NULL;
	bool V_3 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		U3CU3Ec__DisplayClass22_0_t0B9E0A71713E1E0BCBE5C32D0A17C7786CF35818 * L_0 = (U3CU3Ec__DisplayClass22_0_t0B9E0A71713E1E0BCBE5C32D0A17C7786CF35818 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass22_0_t0B9E0A71713E1E0BCBE5C32D0A17C7786CF35818_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass22_0__ctor_m3362924530F3A7340346D7C34BC4B0E66A70A061(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass22_0_t0B9E0A71713E1E0BCBE5C32D0A17C7786CF35818 * L_1 = V_0;
		Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * L_2 = ___stateChangeClosure1;
		NullCheck(L_1);
		L_1->set_stateChangeClosure_0(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
		Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * L_3 = ((FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var))->get_appCPtrToAuth_10();
		V_1 = L_3;
		Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * L_4 = V_1;
		Monitor_Enter_m903755FCC479745619842CCDBF5E6355319FA102(L_4, /*hidden argument*/NULL);
	}

IL_001b:
	try
	{ // begin try (depth: 1)
		{
			U3CU3Ec__DisplayClass22_1_tE096CD1670F7DFF2DC0A9E52B629F80ED0B6ED1B * L_5 = (U3CU3Ec__DisplayClass22_1_tE096CD1670F7DFF2DC0A9E52B629F80ED0B6ED1B *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass22_1_tE096CD1670F7DFF2DC0A9E52B629F80ED0B6ED1B_il2cpp_TypeInfo_var);
			U3CU3Ec__DisplayClass22_1__ctor_mE1858D9A2B9F52467C9D68DF38C2EFDCC99DE22D(L_5, /*hidden argument*/NULL);
			V_2 = L_5;
			U3CU3Ec__DisplayClass22_1_tE096CD1670F7DFF2DC0A9E52B629F80ED0B6ED1B * L_6 = V_2;
			U3CU3Ec__DisplayClass22_0_t0B9E0A71713E1E0BCBE5C32D0A17C7786CF35818 * L_7 = V_0;
			NullCheck(L_6);
			L_6->set_CSU24U3CU3E8__locals1_1(L_7);
			U3CU3Ec__DisplayClass22_1_tE096CD1670F7DFF2DC0A9E52B629F80ED0B6ED1B * L_8 = V_2;
			intptr_t L_9 = ___appCPtr0;
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
			FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_10 = FirebaseAuth_ProxyFromAppCPtr_mB07E3A48DDE2BFB10C206669CC172BAC2FE69A15((intptr_t)L_9, /*hidden argument*/NULL);
			NullCheck(L_8);
			L_8->set_auth_0(L_10);
			U3CU3Ec__DisplayClass22_1_tE096CD1670F7DFF2DC0A9E52B629F80ED0B6ED1B * L_11 = V_2;
			NullCheck(L_11);
			FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_12 = L_11->get_auth_0();
			V_3 = (bool)((!(((RuntimeObject*)(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D *)L_12) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
			bool L_13 = V_3;
			if (!L_13)
			{
				goto IL_0054;
			}
		}

IL_0042:
		{
			U3CU3Ec__DisplayClass22_1_tE096CD1670F7DFF2DC0A9E52B629F80ED0B6ED1B * L_14 = V_2;
			Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_15 = (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 *)il2cpp_codegen_object_new(Action_t591D2A86165F896B4B800BB5C25CE18672A55579_il2cpp_TypeInfo_var);
			Action__ctor_m570E96B2A0C48BC1DC6788460316191F24572760(L_15, L_14, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass22_1_U3CForwardStateChangeU3Eb__0_m4958CF79DC4F966EB6B44A3100894727D07F5D49_RuntimeMethod_var), /*hidden argument*/NULL);
			ExceptionAggregator_Wrap_m93A9BD0DC888F1D02E77860288095CB49D4CFFC2(L_15, /*hidden argument*/NULL);
		}

IL_0054:
		{
			IL2CPP_LEAVE(0x5F, FINALLY_0057);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0057;
	}

FINALLY_0057:
	{ // begin finally (depth: 1)
		Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * L_16 = V_1;
		Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(87)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(87)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x5F, IL_005f)
	}

IL_005f:
	{
		return;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::StateChangedFunction(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_StateChangedFunction_m3EB4E147700973B35AAB4C934E401A47B0098CB7 (intptr_t ___appCPtr0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_StateChangedFunction_m3EB4E147700973B35AAB4C934E401A47B0098CB7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * G_B2_0 = NULL;
	intptr_t G_B2_1;
	memset((&G_B2_1), 0, sizeof(G_B2_1));
	Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * G_B1_0 = NULL;
	intptr_t G_B1_1;
	memset((&G_B1_1), 0, sizeof(G_B1_1));
	{
		intptr_t L_0 = ___appCPtr0;
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_il2cpp_TypeInfo_var);
		Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * L_1 = ((U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_il2cpp_TypeInfo_var))->get_U3CU3E9__23_0_1();
		Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = L_0;
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_il2cpp_TypeInfo_var);
		U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF * L_3 = ((U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * L_4 = (Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E *)il2cpp_codegen_object_new(Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E_il2cpp_TypeInfo_var);
		Action_1__ctor_m4089BBB88624AEE2FB2C8D495020C7FB36BDB2D3(L_4, L_3, (intptr_t)((intptr_t)U3CU3Ec_U3CStateChangedFunctionU3Eb__23_0_m4DF5A2B9C4EF839583FBDD03461848F5EF489F4E_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m4089BBB88624AEE2FB2C8D495020C7FB36BDB2D3_RuntimeMethod_var);
		Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * L_5 = L_4;
		((U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_il2cpp_TypeInfo_var))->set_U3CU3E9__23_0_1(L_5);
		G_B2_0 = L_5;
		G_B2_1 = G_B1_1;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
		FirebaseAuth_ForwardStateChange_m493936DA530F8A8DE041E7F2C8E1409B0924BE5C((intptr_t)G_B2_1, G_B2_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::IdTokenChangedFunction(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_IdTokenChangedFunction_m4867AD7FBED6DB5C1529D485866EB591AFC67BBF (intptr_t ___appCPtr0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_IdTokenChangedFunction_m4867AD7FBED6DB5C1529D485866EB591AFC67BBF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * G_B2_0 = NULL;
	intptr_t G_B2_1;
	memset((&G_B2_1), 0, sizeof(G_B2_1));
	Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * G_B1_0 = NULL;
	intptr_t G_B1_1;
	memset((&G_B1_1), 0, sizeof(G_B1_1));
	{
		intptr_t L_0 = ___appCPtr0;
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_il2cpp_TypeInfo_var);
		Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * L_1 = ((U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_il2cpp_TypeInfo_var))->get_U3CU3E9__24_0_2();
		Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = L_0;
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_il2cpp_TypeInfo_var);
		U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF * L_3 = ((U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * L_4 = (Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E *)il2cpp_codegen_object_new(Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E_il2cpp_TypeInfo_var);
		Action_1__ctor_m4089BBB88624AEE2FB2C8D495020C7FB36BDB2D3(L_4, L_3, (intptr_t)((intptr_t)U3CU3Ec_U3CIdTokenChangedFunctionU3Eb__24_0_mBDB096D23092A8E0948E7EEABA9ECE55DD9C92FB_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m4089BBB88624AEE2FB2C8D495020C7FB36BDB2D3_RuntimeMethod_var);
		Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * L_5 = L_4;
		((U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_il2cpp_TypeInfo_var))->set_U3CU3E9__24_0_2(L_5);
		G_B2_0 = L_5;
		G_B2_1 = G_B1_1;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
		FirebaseAuth_ForwardStateChange_m493936DA530F8A8DE041E7F2C8E1409B0924BE5C((intptr_t)G_B2_1, G_B2_0, /*hidden argument*/NULL);
		return;
	}
}
// Firebase.Auth.FirebaseAuth Firebase.Auth.FirebaseAuth::get_DefaultInstance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * FirebaseAuth_get_DefaultInstance_mA307D08C178B8C5A8EA7BE0F3222668A544FA228 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_get_DefaultInstance_mA307D08C178B8C5A8EA7BE0F3222668A544FA228_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_il2cpp_TypeInfo_var);
		FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * L_0 = FirebaseApp_get_DefaultInstance_m46ACEAEC916D0AC3014302498F51607462EE5C63(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_1 = FirebaseAuth_GetAuth_mEFC135E9B1D37E4739D3E2A8D30A1C80DDE3F157(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_2 = V_0;
		return L_2;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::add_StateChanged(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_add_StateChanged_mFA3F7A1FE096865FF349DBD64644371C0C8CCB34 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_add_StateChanged_mFA3F7A1FE096865FF349DBD64644371C0C8CCB34_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_0 = ___value0;
		FirebaseAuth_add_stateChangedImpl_m1387CE8F8F0F4796FFDF97ADD352101FEE55335C(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = __this->get_persistentLoaded_9();
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_3 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var);
		EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * L_4 = ((EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields*)il2cpp_codegen_static_fields_for(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var))->get_Empty_0();
		NullCheck(L_3);
		EventHandler_Invoke_mD23D5EFEA562A05C5EACDD3E91EEDD2BF6C22800(L_3, __this, L_4, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::remove_StateChanged(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_remove_StateChanged_mF960C837B4BD78DAF5716BC2348EF9702AD2B736 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method)
{
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_0 = ___value0;
		FirebaseAuth_remove_stateChangedImpl_m6BC5FB994A66D606B5783D721FE3D17FD3D5B4D2(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::add_stateChangedImpl(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_add_stateChangedImpl_m1387CE8F8F0F4796FFDF97ADD352101FEE55335C (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_add_stateChangedImpl_m1387CE8F8F0F4796FFDF97ADD352101FEE55335C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_1 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_2 = NULL;
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_0 = __this->get_stateChangedImpl_7();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_2 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)CastclassSealed((RuntimeObject*)L_4, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var));
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** L_5 = __this->get_address_of_stateChangedImpl_7();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_6 = V_2;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_7 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_8 = InterlockedCompareExchangeImpl<EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *>((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C **)L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_9 = V_0;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_9) == ((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::remove_stateChangedImpl(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_remove_stateChangedImpl_m6BC5FB994A66D606B5783D721FE3D17FD3D5B4D2 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_remove_stateChangedImpl_m6BC5FB994A66D606B5783D721FE3D17FD3D5B4D2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_1 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_2 = NULL;
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_0 = __this->get_stateChangedImpl_7();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_2 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)CastclassSealed((RuntimeObject*)L_4, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var));
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** L_5 = __this->get_address_of_stateChangedImpl_7();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_6 = V_2;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_7 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_8 = InterlockedCompareExchangeImpl<EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *>((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C **)L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_9 = V_0;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_9) == ((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// Firebase.Auth.FirebaseUser Firebase.Auth.FirebaseAuth::UpdateCurrentUser(Firebase.Auth.FirebaseUser)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * FirebaseAuth_UpdateCurrentUser_mAA765D9A52031913DB1D3BBFD50DF4A4F5C4D4CF (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * ___proxy0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_UpdateCurrentUser_mAA765D9A52031913DB1D3BBFD50DF4A4F5C4D4CF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  V_4;
	memset((&V_4), 0, sizeof(V_4));
	bool V_5 = false;
	FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * V_6 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
		Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * L_0 = ((FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var))->get_appCPtrToAuth_10();
		V_0 = L_0;
		Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * L_1 = V_0;
		Monitor_Enter_m903755FCC479745619842CCDBF5E6355319FA102(L_1, /*hidden argument*/NULL);
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_2 = ___proxy0;
			V_1 = (bool)((((RuntimeObject*)(FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *)L_2) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
			bool L_3 = V_1;
			if (!L_3)
			{
				goto IL_0022;
			}
		}

IL_0017:
		{
			__this->set_currentUser_6((FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *)NULL);
			goto IL_007d;
		}

IL_0022:
		{
			FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_4 = __this->get_currentUser_6();
			V_2 = (bool)((((RuntimeObject*)(FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *)L_4) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
			bool L_5 = V_2;
			if (!L_5)
			{
				goto IL_003a;
			}
		}

IL_002f:
		{
			FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_6 = ___proxy0;
			__this->set_currentUser_6(L_6);
			goto IL_007d;
		}

IL_003a:
		{
			FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_7 = __this->get_currentUser_6();
			HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_8 = FirebaseUser_getCPtr_mE5B6DFE6A8DE44DC9F744D57A7863DBC40F3284D(L_7, /*hidden argument*/NULL);
			V_4 = L_8;
			intptr_t L_9 = HandleRef_get_Handle_m095712F299EA36BF75975343C726FA160EE38277_inline((HandleRef_t876E76124F400D12395BF61D562162AB6822204A *)(&V_4), /*hidden argument*/NULL);
			FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_10 = ___proxy0;
			HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_11 = FirebaseUser_getCPtr_mE5B6DFE6A8DE44DC9F744D57A7863DBC40F3284D(L_10, /*hidden argument*/NULL);
			V_4 = L_11;
			intptr_t L_12 = HandleRef_get_Handle_m095712F299EA36BF75975343C726FA160EE38277_inline((HandleRef_t876E76124F400D12395BF61D562162AB6822204A *)(&V_4), /*hidden argument*/NULL);
			bool L_13 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_9, (intptr_t)L_12, /*hidden argument*/NULL);
			V_3 = L_13;
			bool L_14 = V_3;
			if (!L_14)
			{
				goto IL_007c;
			}
		}

IL_0067:
		{
			FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_15 = __this->get_currentUser_6();
			NullCheck(L_15);
			UserInfoInterface_Dispose_mE1B360122F9E6726A8FE40382AE6BD8683EAD61B(L_15, /*hidden argument*/NULL);
			FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_16 = ___proxy0;
			__this->set_currentUser_6(L_16);
		}

IL_007c:
		{
		}

IL_007d:
		{
			FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_17 = __this->get_currentUser_6();
			V_5 = (bool)((!(((RuntimeObject*)(FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *)L_17) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
			bool L_18 = V_5;
			if (!L_18)
			{
				goto IL_0098;
			}
		}

IL_008c:
		{
			FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_19 = __this->get_currentUser_6();
			NullCheck(L_19);
			L_19->set_authProxy_3(__this);
		}

IL_0098:
		{
			IL2CPP_LEAVE(0xA3, FINALLY_009b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_009b;
	}

FINALLY_009b:
	{ // begin finally (depth: 1)
		Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * L_20 = V_0;
		Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_20, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(155)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(155)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0xA3, IL_00a3)
	}

IL_00a3:
	{
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_21 = __this->get_currentUser_6();
		V_6 = L_21;
		goto IL_00ad;
	}

IL_00ad:
	{
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_22 = V_6;
		return L_22;
	}
}
// Firebase.Auth.FirebaseUser Firebase.Auth.FirebaseAuth::get_CurrentUser()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * FirebaseAuth_get_CurrentUser_m7BE23C546ECA1E95C9E383EE434BFCF22A92CFFF (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_get_CurrentUser_m7BE23C546ECA1E95C9E383EE434BFCF22A92CFFF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * V_0 = NULL;
	FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * V_1 = NULL;
	FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * G_B3_0 = NULL;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A * L_0 = __this->get_address_of_swigCPtr_0();
		intptr_t L_1 = HandleRef_get_Handle_m095712F299EA36BF75975343C726FA160EE38277_inline((HandleRef_t876E76124F400D12395BF61D562162AB6822204A *)L_0, /*hidden argument*/NULL);
		bool L_2 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_1, (intptr_t)(0), /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_001b;
		}
	}
	{
		G_B3_0 = ((FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *)(NULL));
		goto IL_0021;
	}

IL_001b:
	{
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_3 = FirebaseAuth_get_CurrentUserInternal_mBA35503BDA1894FCF3E6A586BB0B68E34F148C74(__this, /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_0021:
	{
		V_0 = G_B3_0;
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_4 = V_0;
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_5 = FirebaseAuth_UpdateCurrentUser_mAA765D9A52031913DB1D3BBFD50DF4A4F5C4D4CF(__this, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_002c;
	}

IL_002c:
	{
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_6 = V_1;
		return L_6;
	}
}
// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Firebase.Auth.FirebaseAuth::SignInWithCredentialAsync(Firebase.Auth.Credential)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * FirebaseAuth_SignInWithCredentialAsync_m0C8097559DE43C62ED1AAC3D6105B155B02C948F (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * ___credential0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_SignInWithCredentialAsync_m0C8097559DE43C62ED1AAC3D6105B155B02C948F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass48_0_t0332496AFB260910C4258BB3F40976B2A20A0312 * V_0 = NULL;
	Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * V_1 = NULL;
	{
		U3CU3Ec__DisplayClass48_0_t0332496AFB260910C4258BB3F40976B2A20A0312 * L_0 = (U3CU3Ec__DisplayClass48_0_t0332496AFB260910C4258BB3F40976B2A20A0312 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass48_0_t0332496AFB260910C4258BB3F40976B2A20A0312_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass48_0__ctor_m2347AE3FAE11B4DFFD2348CD75D799A145AFEDCC(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass48_0_t0332496AFB260910C4258BB3F40976B2A20A0312 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		FirebaseAuth_ThrowIfNull_m927509F7D041820C618B0061E723697AEDEFC3EC(__this, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass48_0_t0332496AFB260910C4258BB3F40976B2A20A0312 * L_2 = V_0;
		TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * L_3 = (TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED *)il2cpp_codegen_object_new(TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED_il2cpp_TypeInfo_var);
		TaskCompletionSource_1__ctor_m886A4697ACB84BF43814F0D64469B76A08FDFAC5(L_3, /*hidden argument*/TaskCompletionSource_1__ctor_m886A4697ACB84BF43814F0D64469B76A08FDFAC5_RuntimeMethod_var);
		NullCheck(L_2);
		L_2->set_taskCompletionSource_1(L_3);
		Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * L_4 = ___credential0;
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_5 = FirebaseAuth_SignInWithCredentialInternalAsync_mE39D174D69D03D600340190C92DCBE0390FEF28B(__this, L_4, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass48_0_t0332496AFB260910C4258BB3F40976B2A20A0312 * L_6 = V_0;
		Action_1_t5D2D131226D740682F598DD09436558292112781 * L_7 = (Action_1_t5D2D131226D740682F598DD09436558292112781 *)il2cpp_codegen_object_new(Action_1_t5D2D131226D740682F598DD09436558292112781_il2cpp_TypeInfo_var);
		Action_1__ctor_mE56C97A498D5E1CFF95301DEEF78027CD25448E1(L_7, L_6, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass48_0_U3CSignInWithCredentialAsyncU3Eb__0_m732B3D2DD97881C4CEEF6D96D047F28D1DE67CAE_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mE56C97A498D5E1CFF95301DEEF78027CD25448E1_RuntimeMethod_var);
		NullCheck(L_5);
		Task_1_ContinueWith_mF2A8F0D71FCEA9B5D7B2A79A5DCF193DA524B01E(L_5, L_7, /*hidden argument*/Task_1_ContinueWith_mF2A8F0D71FCEA9B5D7B2A79A5DCF193DA524B01E_RuntimeMethod_var);
		U3CU3Ec__DisplayClass48_0_t0332496AFB260910C4258BB3F40976B2A20A0312 * L_8 = V_0;
		NullCheck(L_8);
		TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * L_9 = L_8->get_taskCompletionSource_1();
		NullCheck(L_9);
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_10 = TaskCompletionSource_1_get_Task_m003148330C7CDC741C0872FC50DA059683141623_inline(L_9, /*hidden argument*/TaskCompletionSource_1_get_Task_m003148330C7CDC741C0872FC50DA059683141623_RuntimeMethod_var);
		V_1 = L_10;
		goto IL_0047;
	}

IL_0047:
	{
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_11 = V_1;
		return L_11;
	}
}
// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Firebase.Auth.FirebaseAuth::SignInAnonymouslyAsync()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * FirebaseAuth_SignInAnonymouslyAsync_m6963AC820328CD24A672D8800D1B51BD98FF0995 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_SignInAnonymouslyAsync_m6963AC820328CD24A672D8800D1B51BD98FF0995_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass50_0_t3165B97C90B070A7EAE8BAE6F1E43B1CFD2F46C2 * V_0 = NULL;
	Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * V_1 = NULL;
	{
		U3CU3Ec__DisplayClass50_0_t3165B97C90B070A7EAE8BAE6F1E43B1CFD2F46C2 * L_0 = (U3CU3Ec__DisplayClass50_0_t3165B97C90B070A7EAE8BAE6F1E43B1CFD2F46C2 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass50_0_t3165B97C90B070A7EAE8BAE6F1E43B1CFD2F46C2_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass50_0__ctor_m91E489361C82BCFD50E78CF12F492893906F7B5A(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass50_0_t3165B97C90B070A7EAE8BAE6F1E43B1CFD2F46C2 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		FirebaseAuth_ThrowIfNull_m927509F7D041820C618B0061E723697AEDEFC3EC(__this, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass50_0_t3165B97C90B070A7EAE8BAE6F1E43B1CFD2F46C2 * L_2 = V_0;
		TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * L_3 = (TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED *)il2cpp_codegen_object_new(TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED_il2cpp_TypeInfo_var);
		TaskCompletionSource_1__ctor_m886A4697ACB84BF43814F0D64469B76A08FDFAC5(L_3, /*hidden argument*/TaskCompletionSource_1__ctor_m886A4697ACB84BF43814F0D64469B76A08FDFAC5_RuntimeMethod_var);
		NullCheck(L_2);
		L_2->set_taskCompletionSource_1(L_3);
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_4 = FirebaseAuth_SignInAnonymouslyInternalAsync_m03786609BE942FA6FEE38405709D18F9F501291C(__this, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass50_0_t3165B97C90B070A7EAE8BAE6F1E43B1CFD2F46C2 * L_5 = V_0;
		Action_1_t5D2D131226D740682F598DD09436558292112781 * L_6 = (Action_1_t5D2D131226D740682F598DD09436558292112781 *)il2cpp_codegen_object_new(Action_1_t5D2D131226D740682F598DD09436558292112781_il2cpp_TypeInfo_var);
		Action_1__ctor_mE56C97A498D5E1CFF95301DEEF78027CD25448E1(L_6, L_5, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass50_0_U3CSignInAnonymouslyAsyncU3Eb__0_m1D34F38B75501F86230E71CD29B059F63C572E4A_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mE56C97A498D5E1CFF95301DEEF78027CD25448E1_RuntimeMethod_var);
		NullCheck(L_4);
		Task_1_ContinueWith_mF2A8F0D71FCEA9B5D7B2A79A5DCF193DA524B01E(L_4, L_6, /*hidden argument*/Task_1_ContinueWith_mF2A8F0D71FCEA9B5D7B2A79A5DCF193DA524B01E_RuntimeMethod_var);
		U3CU3Ec__DisplayClass50_0_t3165B97C90B070A7EAE8BAE6F1E43B1CFD2F46C2 * L_7 = V_0;
		NullCheck(L_7);
		TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * L_8 = L_7->get_taskCompletionSource_1();
		NullCheck(L_8);
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_9 = TaskCompletionSource_1_get_Task_m003148330C7CDC741C0872FC50DA059683141623_inline(L_8, /*hidden argument*/TaskCompletionSource_1_get_Task_m003148330C7CDC741C0872FC50DA059683141623_RuntimeMethod_var);
		V_1 = L_9;
		goto IL_0046;
	}

IL_0046:
	{
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_10 = V_1;
		return L_10;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::CompleteFirebaseUserTask(System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>,System.Threading.Tasks.TaskCompletionSource`1<Firebase.Auth.FirebaseUser>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_CompleteFirebaseUserTask_mD79D870B498E279B1654740125368DD6A6FB8BFF (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * ___task0, TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * ___taskCompletionSource1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_CompleteFirebaseUserTask_mD79D870B498E279B1654740125368DD6A6FB8BFF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	{
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_0 = ___task0;
		NullCheck(L_0);
		bool L_1 = Task_get_IsCanceled_m42A47FCA2C6F33308A08C92C8489E802448F6F42(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0016;
		}
	}
	{
		TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * L_3 = ___taskCompletionSource1;
		NullCheck(L_3);
		TaskCompletionSource_1_SetCanceled_mE99DCDD6E5327656A075BA8D5EC8EAEF2F99AAB6(L_3, /*hidden argument*/TaskCompletionSource_1_SetCanceled_mE99DCDD6E5327656A075BA8D5EC8EAEF2F99AAB6_RuntimeMethod_var);
		goto IL_0046;
	}

IL_0016:
	{
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_4 = ___task0;
		NullCheck(L_4);
		bool L_5 = Task_get_IsFaulted_m7337D2694F4BF380C5B8893B4A924D7F0E762A48(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		bool L_6 = V_1;
		if (!L_6)
		{
			goto IL_0031;
		}
	}
	{
		TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * L_7 = ___taskCompletionSource1;
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_8 = ___task0;
		NullCheck(L_8);
		AggregateException_t9217B9E89DF820D5632411F2BD92F444B17BD60E * L_9 = Task_get_Exception_mA61AAD3E52CBEB631D1956217B521456E7960B95(L_8, /*hidden argument*/NULL);
		TaskCompletionSourceCompat_1_SetException_m60E20FA51C992BE0F3CD0F9BA9C20887A1204F1D(L_7, L_9, /*hidden argument*/TaskCompletionSourceCompat_1_SetException_m60E20FA51C992BE0F3CD0F9BA9C20887A1204F1D_RuntimeMethod_var);
		goto IL_0046;
	}

IL_0031:
	{
		TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * L_10 = ___taskCompletionSource1;
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_11 = ___task0;
		NullCheck(L_11);
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_12 = Task_1_get_Result_m547A389B49652DF34BED42F12292E2483FC2059D(L_11, /*hidden argument*/Task_1_get_Result_m547A389B49652DF34BED42F12292E2483FC2059D_RuntimeMethod_var);
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_13 = FirebaseAuth_UpdateCurrentUser_mAA765D9A52031913DB1D3BBFD50DF4A4F5C4D4CF(__this, L_12, /*hidden argument*/NULL);
		NullCheck(L_10);
		TaskCompletionSource_1_SetResult_mE9B14A44C05A19AB24B379A0CB6C4EC6F21BF410(L_10, L_13, /*hidden argument*/TaskCompletionSource_1_SetResult_mE9B14A44C05A19AB24B379A0CB6C4EC6F21BF410_RuntimeMethod_var);
	}

IL_0046:
	{
		return;
	}
}
// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Firebase.Auth.FirebaseAuth::SignInWithCredentialInternalAsync(Firebase.Auth.Credential)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * FirebaseAuth_SignInWithCredentialInternalAsync_mE39D174D69D03D600340190C92DCBE0390FEF28B (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * ___credential0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_SignInWithCredentialInternalAsync_mE39D174D69D03D600340190C92DCBE0390FEF28B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * V_2 = NULL;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_0();
		Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * L_1 = ___credential0;
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_2 = Credential_getCPtr_m892678A92D70544624773536D6E26DDEDFE63E42(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		intptr_t L_3 = AuthUtilPINVOKE_FirebaseAuth_SignInWithCredentialInternal_m286B534BC062DE3FD14436764BCE057923965B67(L_0, L_2, /*hidden argument*/NULL);
		V_0 = (intptr_t)L_3;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_4 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_6 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, FirebaseAuth_SignInWithCredentialInternalAsync_mE39D174D69D03D600340190C92DCBE0390FEF28B_RuntimeMethod_var);
	}

IL_0022:
	{
		intptr_t L_7 = V_0;
		Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * L_8 = (Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 *)il2cpp_codegen_object_new(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var);
		Future_User__ctor_mF7A226B0665A25144444D5F951A9C29D7F7D755A(L_8, (intptr_t)L_7, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var);
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_9 = Future_User_GetTask_mF54DC9C5FC9DD22F21D53B3E36FC8BE82023A506(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		goto IL_0031;
	}

IL_0031:
	{
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_10 = V_2;
		return L_10;
	}
}
// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Firebase.Auth.FirebaseAuth::SignInAnonymouslyInternalAsync()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * FirebaseAuth_SignInAnonymouslyInternalAsync_m03786609BE942FA6FEE38405709D18F9F501291C (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_SignInAnonymouslyInternalAsync_m03786609BE942FA6FEE38405709D18F9F501291C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * V_2 = NULL;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_0();
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		intptr_t L_1 = AuthUtilPINVOKE_FirebaseAuth_SignInAnonymouslyInternal_m769C6AFE4B52A39BFFB28D8298F6664514DB39C6(L_0, /*hidden argument*/NULL);
		V_0 = (intptr_t)L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_1 = L_2;
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_4 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, FirebaseAuth_SignInAnonymouslyInternalAsync_m03786609BE942FA6FEE38405709D18F9F501291C_RuntimeMethod_var);
	}

IL_001c:
	{
		intptr_t L_5 = V_0;
		Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * L_6 = (Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 *)il2cpp_codegen_object_new(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var);
		Future_User__ctor_mF7A226B0665A25144444D5F951A9C29D7F7D755A(L_6, (intptr_t)L_5, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var);
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_7 = Future_User_GetTask_mF54DC9C5FC9DD22F21D53B3E36FC8BE82023A506(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		goto IL_002b;
	}

IL_002b:
	{
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_8 = V_2;
		return L_8;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::SignOut()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_SignOut_m6849FBD2DC9A11FD9BE593450D195BDA0F8B4C0D (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_SignOut_m6849FBD2DC9A11FD9BE593450D195BDA0F8B4C0D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_0();
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		AuthUtilPINVOKE_FirebaseAuth_SignOut_m03EDFAB11CF0A459DD7CA6A3E097790BA3D91458(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_1 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_3 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, FirebaseAuth_SignOut_m6849FBD2DC9A11FD9BE593450D195BDA0F8B4C0D_RuntimeMethod_var);
	}

IL_001c:
	{
		return;
	}
}
// Firebase.Auth.FirebaseAuth Firebase.Auth.FirebaseAuth::GetAuthInternal(Firebase.FirebaseApp,Firebase.InitResult&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * FirebaseAuth_GetAuthInternal_mC4406E480123387B5EBE17F44BBB61A469EE059D (FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * ___app0, int32_t* ___init_result_out1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_GetAuthInternal_mC4406E480123387B5EBE17F44BBB61A469EE059D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	intptr_t V_1;
	memset((&V_1), 0, sizeof(V_1));
	FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * V_2 = NULL;
	bool V_3 = false;
	FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * V_4 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * G_B4_0 = NULL;
	{
		V_0 = 0;
	}

IL_0003:
	try
	{ // begin try (depth: 1)
		{
			FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * L_0 = ___app0;
			IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_il2cpp_TypeInfo_var);
			HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_1 = FirebaseApp_getCPtr_mF922A07C69469D0EB5A268A9278C495805BDCA4F(L_0, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
			intptr_t L_2 = AuthUtilPINVOKE_FirebaseAuth_GetAuthInternal_m9CA73CC68A18B4B86AFFB925E37E8F6A0DAE72AA(L_1, (int32_t*)(&V_0), /*hidden argument*/NULL);
			V_1 = (intptr_t)L_2;
			intptr_t L_3 = V_1;
			bool L_4 = IntPtr_op_Equality_mEE8D9FD2DFE312BBAA8B4ED3BF7976B3142A5934((intptr_t)L_3, (intptr_t)(0), /*hidden argument*/NULL);
			if (L_4)
			{
				goto IL_0028;
			}
		}

IL_001f:
		{
			intptr_t L_5 = V_1;
			FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_6 = (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D *)il2cpp_codegen_object_new(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
			FirebaseAuth__ctor_m5A11B7E7ACB1FF9296510387F4A203804EC9B5D0(L_6, (intptr_t)L_5, (bool)1, /*hidden argument*/NULL);
			G_B4_0 = L_6;
			goto IL_0029;
		}

IL_0028:
		{
			G_B4_0 = ((FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D *)(NULL));
		}

IL_0029:
		{
			V_2 = G_B4_0;
			IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
			bool L_7 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
			V_3 = L_7;
			bool L_8 = V_3;
			if (!L_8)
			{
				goto IL_0039;
			}
		}

IL_0033:
		{
			IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
			Exception_t * L_9 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, FirebaseAuth_GetAuthInternal_mC4406E480123387B5EBE17F44BBB61A469EE059D_RuntimeMethod_var);
		}

IL_0039:
		{
			FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_10 = V_2;
			V_4 = L_10;
			IL2CPP_LEAVE(0x44, FINALLY_003e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_003e;
	}

FINALLY_003e:
	{ // begin finally (depth: 1)
		int32_t* L_11 = ___init_result_out1;
		int32_t L_12 = V_0;
		*((int32_t*)L_11) = (int32_t)L_12;
		IL2CPP_END_FINALLY(62)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(62)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x44, IL_0044)
	}

IL_0044:
	{
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_13 = V_4;
		return L_13;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::LogHeartbeatInternal(Firebase.FirebaseApp)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_LogHeartbeatInternal_m39BE7DCAB1FCAC3F93F7366A80B69B5ADE4CC145 (FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * ___app0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_LogHeartbeatInternal_m39BE7DCAB1FCAC3F93F7366A80B69B5ADE4CC145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * L_0 = ___app0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_il2cpp_TypeInfo_var);
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_1 = FirebaseApp_getCPtr_mF922A07C69469D0EB5A268A9278C495805BDCA4F(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		AuthUtilPINVOKE_FirebaseAuth_LogHeartbeatInternal_m40FB5B32F8D0549946D5FA26631C4D88338C6D5C(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_0 = L_2;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_4 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, FirebaseAuth_LogHeartbeatInternal_m39BE7DCAB1FCAC3F93F7366A80B69B5ADE4CC145_RuntimeMethod_var);
	}

IL_001c:
	{
		return;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::ReleaseReferenceInternal(Firebase.Auth.FirebaseAuth)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth_ReleaseReferenceInternal_mDCB0650BAC51CBB644F738FF72AF2B18D2F3862B (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * ___instance0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_ReleaseReferenceInternal_mDCB0650BAC51CBB644F738FF72AF2B18D2F3862B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_0 = ___instance0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_1 = FirebaseAuth_getCPtr_m43139A9D3674A9DFF8AED10B43D345217CE378C9(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		AuthUtilPINVOKE_FirebaseAuth_ReleaseReferenceInternal_m77FAC25E87B2644CA0EF2B9BFF0201C2F38349D9(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_0 = L_2;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_4 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, FirebaseAuth_ReleaseReferenceInternal_mDCB0650BAC51CBB644F738FF72AF2B18D2F3862B_RuntimeMethod_var);
	}

IL_001c:
	{
		return;
	}
}
// Firebase.Auth.FirebaseUser Firebase.Auth.FirebaseAuth::get_CurrentUserInternal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * FirebaseAuth_get_CurrentUserInternal_mBA35503BDA1894FCF3E6A586BB0B68E34F148C74 (FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth_get_CurrentUserInternal_mBA35503BDA1894FCF3E6A586BB0B68E34F148C74_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * V_1 = NULL;
	bool V_2 = false;
	FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * V_3 = NULL;
	FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * G_B3_0 = NULL;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_0();
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		intptr_t L_1 = AuthUtilPINVOKE_FirebaseAuth_CurrentUserInternal_get_m1646DA22D3C6243710AFD36C183194346B494495(L_0, /*hidden argument*/NULL);
		V_0 = (intptr_t)L_1;
		intptr_t L_2 = V_0;
		bool L_3 = IntPtr_op_Equality_mEE8D9FD2DFE312BBAA8B4ED3BF7976B3142A5934((intptr_t)L_2, (intptr_t)(0), /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0023;
		}
	}
	{
		intptr_t L_4 = V_0;
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_5 = (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *)il2cpp_codegen_object_new(FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD_il2cpp_TypeInfo_var);
		FirebaseUser__ctor_m19BF183B130EA0D6A400A715A62A07877B7F15D5(L_5, (intptr_t)L_4, (bool)0, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		goto IL_0024;
	}

IL_0023:
	{
		G_B3_0 = ((FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *)(NULL));
	}

IL_0024:
	{
		V_1 = G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_6 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_2 = L_6;
		bool L_7 = V_2;
		if (!L_7)
		{
			goto IL_0034;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_8 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, FirebaseAuth_get_CurrentUserInternal_mBA35503BDA1894FCF3E6A586BB0B68E34F148C74_RuntimeMethod_var);
	}

IL_0034:
	{
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_9 = V_1;
		V_3 = L_9;
		goto IL_0038;
	}

IL_0038:
	{
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_10 = V_3;
		return L_10;
	}
}
// System.Void Firebase.Auth.FirebaseAuth::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseAuth__cctor_m9714CC491B97AB4DE8C0D5763ACB8BAEB9E80936 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseAuth__cctor_m9714CC491B97AB4DE8C0D5763ACB8BAEB9E80936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * L_0 = (Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D *)il2cpp_codegen_object_new(Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mD0C4E66AB396EE592D08876CA7D94D68B67B90A1(L_0, /*hidden argument*/Dictionary_2__ctor_mD0C4E66AB396EE592D08876CA7D94D68B67B90A1_RuntimeMethod_var);
		((FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var))->set_appCPtrToAuth_10(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Auth.FirebaseAuth_<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m07998BEFD2CCB1494ECB6037ACA9B180B78E0086 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__cctor_m07998BEFD2CCB1494ECB6037ACA9B180B78E0086_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF * L_0 = (U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF *)il2cpp_codegen_object_new(U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m17B1FF4992C7A229A6F9003C935994D8A8687BCB(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Firebase.Auth.FirebaseAuth_<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m17B1FF4992C7A229A6F9003C935994D8A8687BCB (U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.FirebaseAuth_<>c::<StateChangedFunction>b__23_0(Firebase.Auth.FirebaseAuth)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CStateChangedFunctionU3Eb__23_0_m4DF5A2B9C4EF839583FBDD03461848F5EF489F4E (U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF * __this, FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * ___auth0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CStateChangedFunctionU3Eb__23_0_m4DF5A2B9C4EF839583FBDD03461848F5EF489F4E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_0 = ___auth0;
		NullCheck(L_0);
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_1 = L_0->get_stateChangedImpl_7();
		V_0 = (bool)((!(((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_1) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0048;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
		Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * L_3 = ((FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var))->get_appCPtrToAuth_10();
		V_1 = L_3;
		Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * L_4 = V_1;
		Monitor_Enter_m903755FCC479745619842CCDBF5E6355319FA102(L_4, /*hidden argument*/NULL);
	}

IL_001c:
	try
	{ // begin try (depth: 1)
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_5 = ___auth0;
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_6 = ___auth0;
		NullCheck(L_6);
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_7 = FirebaseAuth_get_CurrentUserInternal_mBA35503BDA1894FCF3E6A586BB0B68E34F148C74(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		FirebaseAuth_UpdateCurrentUser_mAA765D9A52031913DB1D3BBFD50DF4A4F5C4D4CF(L_5, L_7, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x35, FINALLY_002d);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_002d;
	}

FINALLY_002d:
	{ // begin finally (depth: 1)
		Dictionary_2_t4ABB4EC6FA8AFC9CCEDBB1AFBB438C22DF2A8F8D * L_8 = V_1;
		Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_8, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(45)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(45)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x35, IL_0035)
	}

IL_0035:
	{
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_9 = ___auth0;
		NullCheck(L_9);
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_10 = L_9->get_stateChangedImpl_7();
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_11 = ___auth0;
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var);
		EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * L_12 = ((EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields*)il2cpp_codegen_static_fields_for(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var))->get_Empty_0();
		NullCheck(L_10);
		EventHandler_Invoke_mD23D5EFEA562A05C5EACDD3E91EEDD2BF6C22800(L_10, L_11, L_12, /*hidden argument*/NULL);
	}

IL_0048:
	{
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_13 = ___auth0;
		NullCheck(L_13);
		L_13->set_persistentLoaded_9((bool)1);
		return;
	}
}
// System.Void Firebase.Auth.FirebaseAuth_<>c::<IdTokenChangedFunction>b__24_0(Firebase.Auth.FirebaseAuth)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CIdTokenChangedFunctionU3Eb__24_0_mBDB096D23092A8E0948E7EEABA9ECE55DD9C92FB (U3CU3Ec_t459EC0FCC1906361B3F18BA471CE2612A2DDD6BF * __this, FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * ___auth0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3CIdTokenChangedFunctionU3Eb__24_0_mBDB096D23092A8E0948E7EEABA9ECE55DD9C92FB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_0 = ___auth0;
		NullCheck(L_0);
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_1 = L_0->get_idTokenChangedImpl_8();
		V_0 = (bool)((!(((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_1) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_3 = ___auth0;
		NullCheck(L_3);
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_4 = L_3->get_idTokenChangedImpl_8();
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_5 = ___auth0;
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var);
		EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * L_6 = ((EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields*)il2cpp_codegen_static_fields_for(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var))->get_Empty_0();
		NullCheck(L_4);
		EventHandler_Invoke_mD23D5EFEA562A05C5EACDD3E91EEDD2BF6C22800(L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Auth.FirebaseAuth_<>c__DisplayClass19_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass19_0__ctor_mACC4F54E5C54C1F4E1983D751939FEE59B3EAE4D (U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Auth.FirebaseAuth_<>c__DisplayClass19_1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass19_1__ctor_m1C29623F09C98E18908C68A9AAEA1F77D1CC820F (U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.FirebaseAuth_<>c__DisplayClass19_1::<GetAuth>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass19_1_U3CGetAuthU3Eb__0_m50656295764711C10015E4E049FAD60671EC162A (U3CU3Ec__DisplayClass19_1_tFCB6870D96BA003A96F2144D7F6B8FC2BD2EA683 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass19_1_U3CGetAuthU3Eb__0_m50656295764711C10015E4E049FAD60671EC162A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * L_0 = __this->get_CSU24U3CU3E8__locals1_1();
		U3CU3Ec__DisplayClass19_0_tA150031212EAA038B5C6AB0F61C6A2887726F3C8 * L_1 = __this->get_CSU24U3CU3E8__locals1_1();
		NullCheck(L_1);
		FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986 * L_2 = L_1->get_app_1();
		int32_t* L_3 = __this->get_address_of_init_result_0();
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D_il2cpp_TypeInfo_var);
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_4 = FirebaseAuth_GetAuthInternal_mC4406E480123387B5EBE17F44BBB61A469EE059D(L_2, (int32_t*)L_3, /*hidden argument*/NULL);
		NullCheck(L_0);
		L_0->set_auth_0(L_4);
		int32_t L_5 = __this->get_init_result_0();
		V_0 = (bool)((!(((uint32_t)L_5) <= ((uint32_t)0)))? 1 : 0);
		bool L_6 = V_0;
		if (!L_6)
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_7 = __this->get_init_result_0();
		InitializationException_t4950F6485639018AF98C04DD016B37F1745F8098 * L_8 = (InitializationException_t4950F6485639018AF98C04DD016B37F1745F8098 *)il2cpp_codegen_object_new(InitializationException_t4950F6485639018AF98C04DD016B37F1745F8098_il2cpp_TypeInfo_var);
		InitializationException__ctor_m3BBD8A30C4B1757E8686AB762DBA7C7B6CB81591(L_8, L_7, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, U3CU3Ec__DisplayClass19_1_U3CGetAuthU3Eb__0_m50656295764711C10015E4E049FAD60671EC162A_RuntimeMethod_var);
	}

IL_003c:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Auth.FirebaseAuth_<>c__DisplayClass22_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass22_0__ctor_m3362924530F3A7340346D7C34BC4B0E66A70A061 (U3CU3Ec__DisplayClass22_0_t0B9E0A71713E1E0BCBE5C32D0A17C7786CF35818 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Auth.FirebaseAuth_<>c__DisplayClass22_1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass22_1__ctor_mE1858D9A2B9F52467C9D68DF38C2EFDCC99DE22D (U3CU3Ec__DisplayClass22_1_tE096CD1670F7DFF2DC0A9E52B629F80ED0B6ED1B * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.FirebaseAuth_<>c__DisplayClass22_1::<ForwardStateChange>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass22_1_U3CForwardStateChangeU3Eb__0_m4958CF79DC4F966EB6B44A3100894727D07F5D49 (U3CU3Ec__DisplayClass22_1_tE096CD1670F7DFF2DC0A9E52B629F80ED0B6ED1B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass22_1_U3CForwardStateChangeU3Eb__0_m4958CF79DC4F966EB6B44A3100894727D07F5D49_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec__DisplayClass22_0_t0B9E0A71713E1E0BCBE5C32D0A17C7786CF35818 * L_0 = __this->get_CSU24U3CU3E8__locals1_1();
		NullCheck(L_0);
		Action_1_t58EFACDCC4662D3610B5E45396453DC911F6075E * L_1 = L_0->get_stateChangeClosure_0();
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_2 = __this->get_auth_0();
		NullCheck(L_1);
		Action_1_Invoke_mF026A39A2533644C539653019481EDEAE3F2BB04(L_1, L_2, /*hidden argument*/Action_1_Invoke_mF026A39A2533644C539653019481EDEAE3F2BB04_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Auth.FirebaseAuth_<>c__DisplayClass48_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass48_0__ctor_m2347AE3FAE11B4DFFD2348CD75D799A145AFEDCC (U3CU3Ec__DisplayClass48_0_t0332496AFB260910C4258BB3F40976B2A20A0312 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.FirebaseAuth_<>c__DisplayClass48_0::<SignInWithCredentialAsync>b__0(System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass48_0_U3CSignInWithCredentialAsyncU3Eb__0_m732B3D2DD97881C4CEEF6D96D047F28D1DE67CAE (U3CU3Ec__DisplayClass48_0_t0332496AFB260910C4258BB3F40976B2A20A0312 * __this, Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * ___task0, const RuntimeMethod* method)
{
	{
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_0 = __this->get_U3CU3E4__this_0();
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_1 = ___task0;
		TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * L_2 = __this->get_taskCompletionSource_1();
		NullCheck(L_0);
		FirebaseAuth_CompleteFirebaseUserTask_mD79D870B498E279B1654740125368DD6A6FB8BFF(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Auth.FirebaseAuth_<>c__DisplayClass50_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass50_0__ctor_m91E489361C82BCFD50E78CF12F492893906F7B5A (U3CU3Ec__DisplayClass50_0_t3165B97C90B070A7EAE8BAE6F1E43B1CFD2F46C2 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.FirebaseAuth_<>c__DisplayClass50_0::<SignInAnonymouslyAsync>b__0(System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass50_0_U3CSignInAnonymouslyAsyncU3Eb__0_m1D34F38B75501F86230E71CD29B059F63C572E4A (U3CU3Ec__DisplayClass50_0_t3165B97C90B070A7EAE8BAE6F1E43B1CFD2F46C2 * __this, Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * ___task0, const RuntimeMethod* method)
{
	{
		FirebaseAuth_t9B0A298AD7ADB475D859ED2A272768F5FD77C05D * L_0 = __this->get_U3CU3E4__this_0();
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_1 = ___task0;
		TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * L_2 = __this->get_taskCompletionSource_1();
		NullCheck(L_0);
		FirebaseAuth_CompleteFirebaseUserTask_mD79D870B498E279B1654740125368DD6A6FB8BFF(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294 (StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294 * __this, intptr_t ___authCPtr0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(intptr_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___authCPtr0);

}
// System.Void Firebase.Auth.FirebaseAuth_StateChangedDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StateChangedDelegate__ctor_m5A465ECFE2F3056DE914C988625D85656F431F1C (StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Firebase.Auth.FirebaseAuth_StateChangedDelegate::Invoke(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StateChangedDelegate_Invoke_m76E4AE64047B20E5DBA21576DB6C0B780E2C2729 (StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294 * __this, intptr_t ___authCPtr0, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (intptr_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___authCPtr0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, intptr_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___authCPtr0, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< intptr_t >::Invoke(targetMethod, targetThis, ___authCPtr0);
					else
						GenericVirtActionInvoker1< intptr_t >::Invoke(targetMethod, targetThis, ___authCPtr0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< intptr_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___authCPtr0);
					else
						VirtActionInvoker1< intptr_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___authCPtr0);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(&___authCPtr0) - 1), targetMethod);
				}
				typedef void (*FunctionPointerType) (void*, intptr_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___authCPtr0, targetMethod);
			}
		}
	}
}
// System.IAsyncResult Firebase.Auth.FirebaseAuth_StateChangedDelegate::BeginInvoke(System.IntPtr,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* StateChangedDelegate_BeginInvoke_m57C156934DDB33999507D45527F6E84DF9F6BA35 (StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294 * __this, intptr_t ___authCPtr0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StateChangedDelegate_BeginInvoke_m57C156934DDB33999507D45527F6E84DF9F6BA35_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(IntPtr_t_il2cpp_TypeInfo_var, &___authCPtr0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void Firebase.Auth.FirebaseAuth_StateChangedDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StateChangedDelegate_EndInvoke_mD4D5CA766ED9C02ED543990F366C99E187DA5E3A (StateChangedDelegate_t732EE0792B94844C10E7F1518EAC60571B611294 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Auth.FirebaseUser::.ctor(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseUser__ctor_m19BF183B130EA0D6A400A715A62A07877B7F15D5 (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseUser__ctor_m19BF183B130EA0D6A400A715A62A07877B7F15D5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		intptr_t L_0 = ___cPtr0;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		intptr_t L_1 = AuthUtilPINVOKE_FirebaseUser_SWIGUpcast_m2E75C4601FD7E308456F2F0BF75ECB73F92D0E44((intptr_t)L_0, /*hidden argument*/NULL);
		bool L_2 = ___cMemoryOwn1;
		UserInfoInterface__ctor_m7E75B0EBDFD46089D31A77871A8B74520EEBC04C(__this, (intptr_t)L_1, L_2, /*hidden argument*/NULL);
		intptr_t L_3 = ___cPtr0;
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_4;
		memset((&L_4), 0, sizeof(L_4));
		HandleRef__ctor_mCD64E7F91766FE7602EE34A8CEEF6C1EF4C943BB((&L_4), __this, (intptr_t)L_3, /*hidden argument*/NULL);
		__this->set_swigCPtr_2(L_4);
		return;
	}
}
// System.Runtime.InteropServices.HandleRef Firebase.Auth.FirebaseUser::getCPtr(Firebase.Auth.FirebaseUser)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HandleRef_t876E76124F400D12395BF61D562162AB6822204A  FirebaseUser_getCPtr_mE5B6DFE6A8DE44DC9F744D57A7863DBC40F3284D (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseUser_getCPtr_mE5B6DFE6A8DE44DC9F744D57A7863DBC40F3284D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  V_0;
	memset((&V_0), 0, sizeof(V_0));
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  G_B3_0;
	memset((&G_B3_0), 0, sizeof(G_B3_0));
	{
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_0 = ___obj0;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_1 = ___obj0;
		NullCheck(L_1);
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_2 = L_1->get_swigCPtr_2();
		G_B3_0 = L_2;
		goto IL_0017;
	}

IL_000c:
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_3;
		memset((&L_3), 0, sizeof(L_3));
		HandleRef__ctor_mCD64E7F91766FE7602EE34A8CEEF6C1EF4C943BB((&L_3), NULL, (intptr_t)(0), /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_0017:
	{
		V_0 = G_B3_0;
		goto IL_001a;
	}

IL_001a:
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_4 = V_0;
		return L_4;
	}
}
// System.Void Firebase.Auth.FirebaseUser::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FirebaseUser_Dispose_m347979011C289888965AA04311F9CD818CB5994A (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * __this, bool ___disposing0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseUser_Dispose_m347979011C289888965AA04311F9CD818CB5994A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_il2cpp_TypeInfo_var);
		RuntimeObject * L_0 = ((FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_il2cpp_TypeInfo_var))->get_disposeLock_2();
		V_0 = L_0;
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_m903755FCC479745619842CCDBF5E6355319FA102(L_1, /*hidden argument*/NULL);
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			HandleRef_t876E76124F400D12395BF61D562162AB6822204A * L_2 = __this->get_address_of_swigCPtr_2();
			intptr_t L_3 = HandleRef_get_Handle_m095712F299EA36BF75975343C726FA160EE38277_inline((HandleRef_t876E76124F400D12395BF61D562162AB6822204A *)L_2, /*hidden argument*/NULL);
			bool L_4 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_3, (intptr_t)(0), /*hidden argument*/NULL);
			V_1 = L_4;
			bool L_5 = V_1;
			if (!L_5)
			{
				goto IL_005a;
			}
		}

IL_0028:
		{
			bool L_6 = ((UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C *)__this)->get_swigCMemOwn_1();
			V_2 = L_6;
			bool L_7 = V_2;
			if (!L_7)
			{
				goto IL_0048;
			}
		}

IL_0033:
		{
			((UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C *)__this)->set_swigCMemOwn_1((bool)0);
			HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_8 = __this->get_swigCPtr_2();
			IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
			AuthUtilPINVOKE_delete_FirebaseUser_m21C9A5C887AD120B4228AC2E15E7EA499F745A9F(L_8, /*hidden argument*/NULL);
		}

IL_0048:
		{
			HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_9;
			memset((&L_9), 0, sizeof(L_9));
			HandleRef__ctor_mCD64E7F91766FE7602EE34A8CEEF6C1EF4C943BB((&L_9), NULL, (intptr_t)(0), /*hidden argument*/NULL);
			__this->set_swigCPtr_2(L_9);
		}

IL_005a:
		{
			IL2CPP_RUNTIME_CLASS_INIT(GC_tC1D7BD74E8F44ECCEF5CD2B5D84BFF9AAE02D01D_il2cpp_TypeInfo_var);
			GC_SuppressFinalize_m037319A9B95A5BA437E806DE592802225EE5B425(__this, /*hidden argument*/NULL);
			bool L_10 = ___disposing0;
			UserInfoInterface_Dispose_m8532EF4241914613A205B8E2E1720C39FD85C1AA(__this, L_10, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x74, FINALLY_006c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_006c;
	}

FINALLY_006c:
	{ // begin finally (depth: 1)
		RuntimeObject * L_11 = V_0;
		Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_11, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(108)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(108)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x74, IL_0074)
	}

IL_0074:
	{
		return;
	}
}
// System.Uri Firebase.Auth.FirebaseUser::get_PhotoUrl()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * FirebaseUser_get_PhotoUrl_mAC0F8F1390483E1FD30D932527542C89E9C389C6 (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseUser_get_PhotoUrl_mAC0F8F1390483E1FD30D932527542C89E9C389C6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * V_0 = NULL;
	{
		String_t* L_0 = FirebaseUser_get_PhotoUrlInternal_mAAE831A4F08E1CD94A486970B5F029F7D1D1216F(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_il2cpp_TypeInfo_var);
		Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * L_1 = FirebaseApp_UrlStringToUri_m09F6D262D86EE9898214D35510E1E8CEBF53D0DA(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * L_2 = V_0;
		return L_2;
	}
}
// System.Threading.Tasks.Task Firebase.Auth.FirebaseUser::UpdateEmailAsync(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * FirebaseUser_UpdateEmailAsync_m8D0B4CA7BE4605CF684B74CEE6C5A2ADA2E1F5A7 (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * __this, String_t* ___email0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseUser_UpdateEmailAsync_m8D0B4CA7BE4605CF684B74CEE6C5A2ADA2E1F5A7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * V_0 = NULL;
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * V_1 = NULL;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_2();
		String_t* L_1 = ___email0;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		intptr_t L_2 = AuthUtilPINVOKE_FirebaseUser_UpdateEmail_mE4BC3966DE87BDA371C7CE7A0F23D91BEC313CCB(L_0, L_1, /*hidden argument*/NULL);
		FutureVoid_tF512775335C40422BAC529CBF374A246FD8ED51D * L_3 = (FutureVoid_tF512775335C40422BAC529CBF374A246FD8ED51D *)il2cpp_codegen_object_new(FutureVoid_tF512775335C40422BAC529CBF374A246FD8ED51D_il2cpp_TypeInfo_var);
		FutureVoid__ctor_mE27D03F52C5F3CE33751637C530DB89045EE97DA(L_3, (intptr_t)L_2, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FutureVoid_tF512775335C40422BAC529CBF374A246FD8ED51D_il2cpp_TypeInfo_var);
		Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * L_4 = FutureVoid_GetTask_m81734F11961D1BC3DD5D3E313B006EA275C06B83(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * L_5 = V_0;
		V_1 = L_5;
		goto IL_001d;
	}

IL_001d:
	{
		Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * L_6 = V_1;
		return L_6;
	}
}
// System.Threading.Tasks.Task Firebase.Auth.FirebaseUser::UpdateUserProfileAsync(Firebase.Auth.UserProfile)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * FirebaseUser_UpdateUserProfileAsync_mACFB318A8FA0A47B70F317197B0C5884D5772D9D (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * __this, UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * ___profile0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseUser_UpdateUserProfileAsync_mACFB318A8FA0A47B70F317197B0C5884D5772D9D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * V_0 = NULL;
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * V_1 = NULL;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_2();
		UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * L_1 = ___profile0;
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_2 = UserProfile_getCPtr_m43D66A8F09ED56F3026B25C5DFEEEA8A3BEB7CEC(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		intptr_t L_3 = AuthUtilPINVOKE_FirebaseUser_UpdateUserProfile_m828FCDD053CA923BA34246E6679C39F6BCBF076C(L_0, L_2, /*hidden argument*/NULL);
		FutureVoid_tF512775335C40422BAC529CBF374A246FD8ED51D * L_4 = (FutureVoid_tF512775335C40422BAC529CBF374A246FD8ED51D *)il2cpp_codegen_object_new(FutureVoid_tF512775335C40422BAC529CBF374A246FD8ED51D_il2cpp_TypeInfo_var);
		FutureVoid__ctor_mE27D03F52C5F3CE33751637C530DB89045EE97DA(L_4, (intptr_t)L_3, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FutureVoid_tF512775335C40422BAC529CBF374A246FD8ED51D_il2cpp_TypeInfo_var);
		Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * L_5 = FutureVoid_GetTask_m81734F11961D1BC3DD5D3E313B006EA275C06B83(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * L_6 = V_0;
		V_1 = L_6;
		goto IL_0022;
	}

IL_0022:
	{
		Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * L_7 = V_1;
		return L_7;
	}
}
// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Firebase.Auth.FirebaseUser::LinkWithCredentialAsync(Firebase.Auth.Credential)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * FirebaseUser_LinkWithCredentialAsync_mC6D4BAE39FF8EE275EB3878D63690AE08F129627 (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * __this, Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * ___credential0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseUser_LinkWithCredentialAsync_mC6D4BAE39FF8EE275EB3878D63690AE08F129627_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * V_2 = NULL;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_2();
		Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * L_1 = ___credential0;
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_2 = Credential_getCPtr_m892678A92D70544624773536D6E26DDEDFE63E42(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		intptr_t L_3 = AuthUtilPINVOKE_FirebaseUser_LinkWithCredential_mBC153FDCFAC9320B9628591B105955EFCCA5E03F(L_0, L_2, /*hidden argument*/NULL);
		V_0 = (intptr_t)L_3;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_4 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_6 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, FirebaseUser_LinkWithCredentialAsync_mC6D4BAE39FF8EE275EB3878D63690AE08F129627_RuntimeMethod_var);
	}

IL_0022:
	{
		intptr_t L_7 = V_0;
		Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * L_8 = (Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 *)il2cpp_codegen_object_new(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var);
		Future_User__ctor_mF7A226B0665A25144444D5F951A9C29D7F7D755A(L_8, (intptr_t)L_7, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var);
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_9 = Future_User_GetTask_mF54DC9C5FC9DD22F21D53B3E36FC8BE82023A506(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		goto IL_0031;
	}

IL_0031:
	{
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_10 = V_2;
		return L_10;
	}
}
// System.String Firebase.Auth.FirebaseUser::get_DisplayName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FirebaseUser_get_DisplayName_mC9A77732AD5665647AB92F193A19C10E67EE34AB (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseUser_get_DisplayName_mC9A77732AD5665647AB92F193A19C10E67EE34AB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	String_t* V_2 = NULL;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_2();
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		String_t* L_1 = AuthUtilPINVOKE_FirebaseUser_DisplayName_get_mDD546C27F066D7A7C9F9612FB9CC18C6E8D9E2BF(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_1 = L_2;
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_4 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, FirebaseUser_get_DisplayName_mC9A77732AD5665647AB92F193A19C10E67EE34AB_RuntimeMethod_var);
	}

IL_001c:
	{
		String_t* L_5 = V_0;
		V_2 = L_5;
		goto IL_0020;
	}

IL_0020:
	{
		String_t* L_6 = V_2;
		return L_6;
	}
}
// System.String Firebase.Auth.FirebaseUser::get_Email()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FirebaseUser_get_Email_m824D4D6FADD78BE6C4C6E1FF3FC2645D5F4ADF06 (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseUser_get_Email_m824D4D6FADD78BE6C4C6E1FF3FC2645D5F4ADF06_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	String_t* V_2 = NULL;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_2();
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		String_t* L_1 = AuthUtilPINVOKE_FirebaseUser_Email_get_m1C74E1D469EF27F32E2810738DBF9CA1BA23582D(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_1 = L_2;
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_4 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, FirebaseUser_get_Email_m824D4D6FADD78BE6C4C6E1FF3FC2645D5F4ADF06_RuntimeMethod_var);
	}

IL_001c:
	{
		String_t* L_5 = V_0;
		V_2 = L_5;
		goto IL_0020;
	}

IL_0020:
	{
		String_t* L_6 = V_2;
		return L_6;
	}
}
// System.Boolean Firebase.Auth.FirebaseUser::get_IsAnonymous()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FirebaseUser_get_IsAnonymous_m66E9F5CD028313FE66A48CD0A2C334AC35908902 (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseUser_get_IsAnonymous_m66E9F5CD028313FE66A48CD0A2C334AC35908902_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_2();
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		bool L_1 = AuthUtilPINVOKE_FirebaseUser_IsAnonymous_get_m62129D05B6F8B37AF44E526484F3F44094D907D8(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_1 = L_2;
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_4 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, FirebaseUser_get_IsAnonymous_m66E9F5CD028313FE66A48CD0A2C334AC35908902_RuntimeMethod_var);
	}

IL_001c:
	{
		bool L_5 = V_0;
		V_2 = L_5;
		goto IL_0020;
	}

IL_0020:
	{
		bool L_6 = V_2;
		return L_6;
	}
}
// System.String Firebase.Auth.FirebaseUser::get_PhotoUrlInternal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FirebaseUser_get_PhotoUrlInternal_mAAE831A4F08E1CD94A486970B5F029F7D1D1216F (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseUser_get_PhotoUrlInternal_mAAE831A4F08E1CD94A486970B5F029F7D1D1216F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	String_t* V_2 = NULL;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_2();
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		String_t* L_1 = AuthUtilPINVOKE_FirebaseUser_PhotoUrlInternal_get_m12B7DDC9E668B2B40694C4DEB10A2C4EAC81FD00(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_1 = L_2;
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_4 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, FirebaseUser_get_PhotoUrlInternal_mAAE831A4F08E1CD94A486970B5F029F7D1D1216F_RuntimeMethod_var);
	}

IL_001c:
	{
		String_t* L_5 = V_0;
		V_2 = L_5;
		goto IL_0020;
	}

IL_0020:
	{
		String_t* L_6 = V_2;
		return L_6;
	}
}
// System.Collections.Generic.IEnumerable`1<Firebase.Auth.IUserInfo> Firebase.Auth.FirebaseUser::get_ProviderData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* FirebaseUser_get_ProviderData_mE5D70ADC73F94F71D686939C53A8984CCEC682D0 (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseUser_get_ProviderData_mE5D70ADC73F94F71D686939C53A8984CCEC682D0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * V_0 = NULL;
	List_1_tDAC656203301889991AA49AC3FC48456D0214689 * V_1 = NULL;
	UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A * V_2 = NULL;
	RuntimeObject* V_3 = NULL;
	RuntimeObject* V_4 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_2();
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		intptr_t L_1 = AuthUtilPINVOKE_FirebaseUser_ProviderData_get_mD388B7BE41C3645C7464C3C0DAA81290CF2CED47(L_0, /*hidden argument*/NULL);
		UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * L_2 = (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 *)il2cpp_codegen_object_new(UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422_il2cpp_TypeInfo_var);
		UserInfoInterfaceList__ctor_m969BCAE5B0171B9F24C435611C2D57CEF93C4F54(L_2, (intptr_t)L_1, (bool)0, /*hidden argument*/NULL);
		V_0 = L_2;
		List_1_tDAC656203301889991AA49AC3FC48456D0214689 * L_3 = (List_1_tDAC656203301889991AA49AC3FC48456D0214689 *)il2cpp_codegen_object_new(List_1_tDAC656203301889991AA49AC3FC48456D0214689_il2cpp_TypeInfo_var);
		List_1__ctor_m93A21423354207CBD75E21125B75C91D9C17FE60(L_3, /*hidden argument*/List_1__ctor_m93A21423354207CBD75E21125B75C91D9C17FE60_RuntimeMethod_var);
		V_1 = L_3;
		UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * L_4 = V_0;
		NullCheck(L_4);
		UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A * L_5 = UserInfoInterfaceList_GetEnumerator_m34C89390756E8DCA0D4FDE9EC4020AF7AFBB4021(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0034;
		}

IL_0023:
		{
			UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A * L_6 = V_2;
			NullCheck(L_6);
			UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * L_7 = UserInfoInterfaceListEnumerator_get_Current_mE95C0E6329FA074B3BB193C170A0B46DD8A6A720(L_6, /*hidden argument*/NULL);
			V_3 = L_7;
			List_1_tDAC656203301889991AA49AC3FC48456D0214689 * L_8 = V_1;
			RuntimeObject* L_9 = V_3;
			NullCheck(L_8);
			List_1_Add_m52A7A5A55C7C1AB245A219997212D466AFA9176D(L_8, L_9, /*hidden argument*/List_1_Add_m52A7A5A55C7C1AB245A219997212D466AFA9176D_RuntimeMethod_var);
		}

IL_0034:
		{
			UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A * L_10 = V_2;
			NullCheck(L_10);
			bool L_11 = UserInfoInterfaceListEnumerator_MoveNext_mB0E76F84C535182420B86E45C821E100E902F0A9(L_10, /*hidden argument*/NULL);
			if (L_11)
			{
				goto IL_0023;
			}
		}

IL_003c:
		{
			IL2CPP_LEAVE(0x49, FINALLY_003e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_003e;
	}

FINALLY_003e:
	{ // begin finally (depth: 1)
		{
			UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A * L_12 = V_2;
			if (!L_12)
			{
				goto IL_0048;
			}
		}

IL_0041:
		{
			UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A * L_13 = V_2;
			NullCheck(L_13);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var, L_13);
		}

IL_0048:
		{
			IL2CPP_END_FINALLY(62)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(62)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x49, IL_0049)
	}

IL_0049:
	{
		List_1_tDAC656203301889991AA49AC3FC48456D0214689 * L_14 = V_1;
		V_4 = (RuntimeObject*)L_14;
		goto IL_004e;
	}

IL_004e:
	{
		RuntimeObject* L_15 = V_4;
		return L_15;
	}
}
// System.String Firebase.Auth.FirebaseUser::get_UserId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* FirebaseUser_get_UserId_m1A1850E055379A8AED15B80FE8DDDA60EE4F42DD (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FirebaseUser_get_UserId_m1A1850E055379A8AED15B80FE8DDDA60EE4F42DD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	String_t* V_2 = NULL;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_2();
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		String_t* L_1 = AuthUtilPINVOKE_FirebaseUser_UserId_get_mA5AC5F6494A1F4A6A3F0DBC90E0D4D1006F5CF5A(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_1 = L_2;
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_4 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, FirebaseUser_get_UserId_m1A1850E055379A8AED15B80FE8DDDA60EE4F42DD_RuntimeMethod_var);
	}

IL_001c:
	{
		String_t* L_5 = V_0;
		V_2 = L_5;
		goto IL_0020;
	}

IL_0020:
	{
		String_t* L_6 = V_2;
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_Future_User_SWIG_CompletionDispatcher_m9608732E8FCF7D93C2A70B63BA750D3231DCAD47(int32_t ___key0)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Managed method invocation
	Future_User_SWIG_CompletionDispatcher_m9608732E8FCF7D93C2A70B63BA750D3231DCAD47(___key0, NULL);

}
// System.Void Firebase.Auth.Future_User::.ctor(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Future_User__ctor_mF7A226B0665A25144444D5F951A9C29D7F7D755A (Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Future_User__ctor_mF7A226B0665A25144444D5F951A9C29D7F7D755A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_callbackData_6((intptr_t)(0));
		__this->set_SWIG_CompletionCB_7((SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2 *)NULL);
		intptr_t L_0 = ___cPtr0;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		intptr_t L_1 = AuthUtilPINVOKE_Future_User_SWIGUpcast_m9F5C5F23C6908B207633FFC69E6B33329CCDD5FC((intptr_t)L_0, /*hidden argument*/NULL);
		bool L_2 = ___cMemoryOwn1;
		FutureBase__ctor_mC05DD84980454E9B8210087E01926E5D56FC017F(__this, (intptr_t)L_1, L_2, /*hidden argument*/NULL);
		intptr_t L_3 = ___cPtr0;
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_4;
		memset((&L_4), 0, sizeof(L_4));
		HandleRef__ctor_mCD64E7F91766FE7602EE34A8CEEF6C1EF4C943BB((&L_4), __this, (intptr_t)L_3, /*hidden argument*/NULL);
		__this->set_swigCPtr_2(L_4);
		return;
	}
}
// System.Void Firebase.Auth.Future_User::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Future_User_Dispose_m82E8E0011FC005AB27FDF878D851C18D22FB19E1 (Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * __this, bool ___disposing0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Future_User_Dispose_m82E8E0011FC005AB27FDF878D851C18D22FB19E1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_il2cpp_TypeInfo_var);
		RuntimeObject * L_0 = ((FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_il2cpp_TypeInfo_var))->get_disposeLock_2();
		V_0 = L_0;
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_m903755FCC479745619842CCDBF5E6355319FA102(L_1, /*hidden argument*/NULL);
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			HandleRef_t876E76124F400D12395BF61D562162AB6822204A * L_2 = __this->get_address_of_swigCPtr_2();
			intptr_t L_3 = HandleRef_get_Handle_m095712F299EA36BF75975343C726FA160EE38277_inline((HandleRef_t876E76124F400D12395BF61D562162AB6822204A *)L_2, /*hidden argument*/NULL);
			bool L_4 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_3, (intptr_t)(0), /*hidden argument*/NULL);
			V_1 = L_4;
			bool L_5 = V_1;
			if (!L_5)
			{
				goto IL_0066;
			}
		}

IL_0028:
		{
			Future_User_SetCompletionData_m7B693E6CE4A587D18797FE4A21D00A4C823DB7BC(__this, (intptr_t)(0), /*hidden argument*/NULL);
			bool L_6 = ((FutureBase_t75BACF0EF2911C731F998E85086202A1A2F55397 *)__this)->get_swigCMemOwn_1();
			V_2 = L_6;
			bool L_7 = V_2;
			if (!L_7)
			{
				goto IL_0054;
			}
		}

IL_003f:
		{
			((FutureBase_t75BACF0EF2911C731F998E85086202A1A2F55397 *)__this)->set_swigCMemOwn_1((bool)0);
			HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_8 = __this->get_swigCPtr_2();
			IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
			AuthUtilPINVOKE_delete_Future_User_m09EF8AC9392014A450B9A39F9237AB703460AE42(L_8, /*hidden argument*/NULL);
		}

IL_0054:
		{
			HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_9;
			memset((&L_9), 0, sizeof(L_9));
			HandleRef__ctor_mCD64E7F91766FE7602EE34A8CEEF6C1EF4C943BB((&L_9), NULL, (intptr_t)(0), /*hidden argument*/NULL);
			__this->set_swigCPtr_2(L_9);
		}

IL_0066:
		{
			IL2CPP_RUNTIME_CLASS_INIT(GC_tC1D7BD74E8F44ECCEF5CD2B5D84BFF9AAE02D01D_il2cpp_TypeInfo_var);
			GC_SuppressFinalize_m037319A9B95A5BA437E806DE592802225EE5B425(__this, /*hidden argument*/NULL);
			bool L_10 = ___disposing0;
			FutureBase_Dispose_mD60CE0048F80E6E0FF167F3D088F949F10217BBF(__this, L_10, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x80, FINALLY_0078);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0078;
	}

FINALLY_0078:
	{ // begin finally (depth: 1)
		RuntimeObject * L_11 = V_0;
		Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_11, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(120)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(120)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x80, IL_0080)
	}

IL_0080:
	{
		return;
	}
}
// System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser> Firebase.Auth.Future_User::GetTask(Firebase.Auth.Future_User)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * Future_User_GetTask_mF54DC9C5FC9DD22F21D53B3E36FC8BE82023A506 (Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * ___fu0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Future_User_GetTask_mF54DC9C5FC9DD22F21D53B3E36FC8BE82023A506_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass4_0_t89802218D77EADC8D4FDB0DD3CF8FC5447E97B8D * V_0 = NULL;
	bool V_1 = false;
	Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * V_2 = NULL;
	bool V_3 = false;
	{
		U3CU3Ec__DisplayClass4_0_t89802218D77EADC8D4FDB0DD3CF8FC5447E97B8D * L_0 = (U3CU3Ec__DisplayClass4_0_t89802218D77EADC8D4FDB0DD3CF8FC5447E97B8D *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass4_0_t89802218D77EADC8D4FDB0DD3CF8FC5447E97B8D_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass4_0__ctor_m69741D9DAF70E435DFBFF0CFFA06B08796349300(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass4_0_t89802218D77EADC8D4FDB0DD3CF8FC5447E97B8D * L_1 = V_0;
		Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * L_2 = ___fu0;
		NullCheck(L_1);
		L_1->set_fu_0(L_2);
		U3CU3Ec__DisplayClass4_0_t89802218D77EADC8D4FDB0DD3CF8FC5447E97B8D * L_3 = V_0;
		TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * L_4 = (TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED *)il2cpp_codegen_object_new(TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED_il2cpp_TypeInfo_var);
		TaskCompletionSource_1__ctor_m886A4697ACB84BF43814F0D64469B76A08FDFAC5(L_4, /*hidden argument*/TaskCompletionSource_1__ctor_m886A4697ACB84BF43814F0D64469B76A08FDFAC5_RuntimeMethod_var);
		NullCheck(L_3);
		L_3->set_tcs_1(L_4);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		bool L_5 = SWIGPendingException_get_Pending_m746EAD61F79AF2A98C4350069D4507EAC1D251DE(/*hidden argument*/NULL);
		V_1 = L_5;
		bool L_6 = V_1;
		if (!L_6)
		{
			goto IL_0042;
		}
	}
	{
		U3CU3Ec__DisplayClass4_0_t89802218D77EADC8D4FDB0DD3CF8FC5447E97B8D * L_7 = V_0;
		NullCheck(L_7);
		TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * L_8 = L_7->get_tcs_1();
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_t6F011B01EFC65C1B9C34AAAA97E0FB08AE300B48_il2cpp_TypeInfo_var);
		Exception_t * L_9 = SWIGPendingException_Retrieve_m47C7FCF8ED6E8C49646820EE0691C3EDE0CFFDCD(/*hidden argument*/NULL);
		NullCheck(L_8);
		TaskCompletionSource_1_SetException_m0F8FFC0B85AFA0B942C0966332B7E3275CF42A1B(L_8, L_9, /*hidden argument*/TaskCompletionSource_1_SetException_m0F8FFC0B85AFA0B942C0966332B7E3275CF42A1B_RuntimeMethod_var);
		U3CU3Ec__DisplayClass4_0_t89802218D77EADC8D4FDB0DD3CF8FC5447E97B8D * L_10 = V_0;
		NullCheck(L_10);
		TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * L_11 = L_10->get_tcs_1();
		NullCheck(L_11);
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_12 = TaskCompletionSource_1_get_Task_m003148330C7CDC741C0872FC50DA059683141623_inline(L_11, /*hidden argument*/TaskCompletionSource_1_get_Task_m003148330C7CDC741C0872FC50DA059683141623_RuntimeMethod_var);
		V_2 = L_12;
		goto IL_00a0;
	}

IL_0042:
	{
		U3CU3Ec__DisplayClass4_0_t89802218D77EADC8D4FDB0DD3CF8FC5447E97B8D * L_13 = V_0;
		NullCheck(L_13);
		Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * L_14 = L_13->get_fu_0();
		NullCheck(L_14);
		int32_t L_15 = FutureBase_status_mD675FFD3FAE10D36169C16590EE3C6F1861DD136(L_14, /*hidden argument*/NULL);
		V_3 = (bool)((((int32_t)L_15) == ((int32_t)2))? 1 : 0);
		bool L_16 = V_3;
		if (!L_16)
		{
			goto IL_007a;
		}
	}
	{
		U3CU3Ec__DisplayClass4_0_t89802218D77EADC8D4FDB0DD3CF8FC5447E97B8D * L_17 = V_0;
		NullCheck(L_17);
		TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * L_18 = L_17->get_tcs_1();
		FirebaseException_t947F51F8FDF9CB2D145DC24E92160DC67BF12AEF * L_19 = (FirebaseException_t947F51F8FDF9CB2D145DC24E92160DC67BF12AEF *)il2cpp_codegen_object_new(FirebaseException_t947F51F8FDF9CB2D145DC24E92160DC67BF12AEF_il2cpp_TypeInfo_var);
		FirebaseException__ctor_mB31E783657F428BA72B055DF8A4CE6EA6996996C(L_19, 0, _stringLiteralF3368570EE5DB7FA93890C049EB03DD764108E6C, /*hidden argument*/NULL);
		NullCheck(L_18);
		TaskCompletionSource_1_SetException_m0F8FFC0B85AFA0B942C0966332B7E3275CF42A1B(L_18, L_19, /*hidden argument*/TaskCompletionSource_1_SetException_m0F8FFC0B85AFA0B942C0966332B7E3275CF42A1B_RuntimeMethod_var);
		U3CU3Ec__DisplayClass4_0_t89802218D77EADC8D4FDB0DD3CF8FC5447E97B8D * L_20 = V_0;
		NullCheck(L_20);
		TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * L_21 = L_20->get_tcs_1();
		NullCheck(L_21);
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_22 = TaskCompletionSource_1_get_Task_m003148330C7CDC741C0872FC50DA059683141623_inline(L_21, /*hidden argument*/TaskCompletionSource_1_get_Task_m003148330C7CDC741C0872FC50DA059683141623_RuntimeMethod_var);
		V_2 = L_22;
		goto IL_00a0;
	}

IL_007a:
	{
		U3CU3Ec__DisplayClass4_0_t89802218D77EADC8D4FDB0DD3CF8FC5447E97B8D * L_23 = V_0;
		NullCheck(L_23);
		Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * L_24 = L_23->get_fu_0();
		U3CU3Ec__DisplayClass4_0_t89802218D77EADC8D4FDB0DD3CF8FC5447E97B8D * L_25 = V_0;
		Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327 * L_26 = (Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327 *)il2cpp_codegen_object_new(Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327_il2cpp_TypeInfo_var);
		Action__ctor_m9AE0C8C97132B6FFDB6D84FDC9A8802717222F39(L_26, L_25, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass4_0_U3CGetTaskU3Eb__0_mC165A76D4A3E3CA94F84D8F613ECE4E75D2EF7DF_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_24);
		Future_User_SetOnCompletionCallback_mA4F7DBBBBBA68E9A54C2231034CCE80415DD80A6(L_24, L_26, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass4_0_t89802218D77EADC8D4FDB0DD3CF8FC5447E97B8D * L_27 = V_0;
		NullCheck(L_27);
		TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * L_28 = L_27->get_tcs_1();
		NullCheck(L_28);
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_29 = TaskCompletionSource_1_get_Task_m003148330C7CDC741C0872FC50DA059683141623_inline(L_28, /*hidden argument*/TaskCompletionSource_1_get_Task_m003148330C7CDC741C0872FC50DA059683141623_RuntimeMethod_var);
		V_2 = L_29;
		goto IL_00a0;
	}

IL_00a0:
	{
		Task_1_t60117F676E45AB5E7AAC53C1E70159F6D04119D7 * L_30 = V_2;
		return L_30;
	}
}
// System.Void Firebase.Auth.Future_User::ThrowIfDisposed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Future_User_ThrowIfDisposed_m963BF98F552F3F37DFEC0AEB1DF07207809835DE (Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Future_User_ThrowIfDisposed_m963BF98F552F3F37DFEC0AEB1DF07207809835DE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A * L_0 = __this->get_address_of_swigCPtr_2();
		intptr_t L_1 = HandleRef_get_Handle_m095712F299EA36BF75975343C726FA160EE38277_inline((HandleRef_t876E76124F400D12395BF61D562162AB6822204A *)L_0, /*hidden argument*/NULL);
		bool L_2 = IntPtr_op_Equality_mEE8D9FD2DFE312BBAA8B4ED3BF7976B3142A5934((intptr_t)L_1, (intptr_t)(0), /*hidden argument*/NULL);
		V_0 = L_2;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0026;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_4 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_4, _stringLiteralFB1AE274028B6974E9FDF9D77F5D12C65C8CD4D4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, Future_User_ThrowIfDisposed_m963BF98F552F3F37DFEC0AEB1DF07207809835DE_RuntimeMethod_var);
	}

IL_0026:
	{
		return;
	}
}
// System.Void Firebase.Auth.Future_User::SetOnCompletionCallback(Firebase.Auth.Future_User_Action)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Future_User_SetOnCompletionCallback_mA4F7DBBBBBA68E9A54C2231034CCE80415DD80A6 (Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * __this, Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327 * ___userCompletionCallback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Future_User_SetOnCompletionCallback_mA4F7DBBBBBA68E9A54C2231034CCE80415DD80A6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	RuntimeObject * V_2 = NULL;
	bool V_3 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		Future_User_ThrowIfDisposed_m963BF98F552F3F37DFEC0AEB1DF07207809835DE(__this, /*hidden argument*/NULL);
		SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2 * L_0 = __this->get_SWIG_CompletionCB_7();
		V_1 = (bool)((((RuntimeObject*)(SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2 *)L_0) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_1 = V_1;
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2 * L_2 = (SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2 *)il2cpp_codegen_object_new(SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2_il2cpp_TypeInfo_var);
		SWIG_CompletionDelegate__ctor_m9FCE4E269635690B019F5FF509AFF343FA13DBDC(L_2, NULL, (intptr_t)((intptr_t)Future_User_SWIG_CompletionDispatcher_m9608732E8FCF7D93C2A70B63BA750D3231DCAD47_RuntimeMethod_var), /*hidden argument*/NULL);
		__this->set_SWIG_CompletionCB_7(L_2);
	}

IL_0029:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var);
		RuntimeObject * L_3 = ((Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_StaticFields*)il2cpp_codegen_static_fields_for(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var))->get_CallbackLock_5();
		V_2 = L_3;
		RuntimeObject * L_4 = V_2;
		Monitor_Enter_m903755FCC479745619842CCDBF5E6355319FA102(L_4, /*hidden argument*/NULL);
	}

IL_0036:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var);
			Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A * L_5 = ((Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_StaticFields*)il2cpp_codegen_static_fields_for(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var))->get_Callbacks_3();
			V_3 = (bool)((((RuntimeObject*)(Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A *)L_5) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
			bool L_6 = V_3;
			if (!L_6)
			{
				goto IL_004f;
			}
		}

IL_0043:
		{
			Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A * L_7 = (Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A *)il2cpp_codegen_object_new(Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A_il2cpp_TypeInfo_var);
			Dictionary_2__ctor_m0CBFF732991FC69E50314435AC3E598F357091AE(L_7, /*hidden argument*/Dictionary_2__ctor_m0CBFF732991FC69E50314435AC3E598F357091AE_RuntimeMethod_var);
			IL2CPP_RUNTIME_CLASS_INIT(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var);
			((Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_StaticFields*)il2cpp_codegen_static_fields_for(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var))->set_Callbacks_3(L_7);
		}

IL_004f:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var);
			int32_t L_8 = ((Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_StaticFields*)il2cpp_codegen_static_fields_for(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var))->get_CallbackIndex_4();
			int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
			((Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_StaticFields*)il2cpp_codegen_static_fields_for(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var))->set_CallbackIndex_4(L_9);
			V_0 = L_9;
			Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A * L_10 = ((Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_StaticFields*)il2cpp_codegen_static_fields_for(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var))->get_Callbacks_3();
			int32_t L_11 = V_0;
			Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327 * L_12 = ___userCompletionCallback0;
			NullCheck(L_10);
			Dictionary_2_set_Item_m076684AAD9154CBFD0ACA5E778B83C7D2443BA8F(L_10, L_11, L_12, /*hidden argument*/Dictionary_2_set_Item_m076684AAD9154CBFD0ACA5E778B83C7D2443BA8F_RuntimeMethod_var);
			IL2CPP_LEAVE(0x75, FINALLY_006d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_006d;
	}

FINALLY_006d:
	{ // begin finally (depth: 1)
		RuntimeObject * L_13 = V_2;
		Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_13, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(109)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(109)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x75, IL_0075)
	}

IL_0075:
	{
		SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2 * L_14 = __this->get_SWIG_CompletionCB_7();
		int32_t L_15 = V_0;
		intptr_t L_16 = Future_User_SWIG_OnCompletion_mD2853E95978B586313F3D904FEDA65F0DC04B44A(__this, L_14, L_15, /*hidden argument*/NULL);
		Future_User_SetCompletionData_m7B693E6CE4A587D18797FE4A21D00A4C823DB7BC(__this, (intptr_t)L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.Future_User::SetCompletionData(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Future_User_SetCompletionData_m7B693E6CE4A587D18797FE4A21D00A4C823DB7BC (Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * __this, intptr_t ___data0, const RuntimeMethod* method)
{
	{
		Future_User_ThrowIfDisposed_m963BF98F552F3F37DFEC0AEB1DF07207809835DE(__this, /*hidden argument*/NULL);
		intptr_t L_0 = __this->get_callbackData_6();
		Future_User_SWIG_FreeCompletionData_mE5A33CAC1849B25DD160DC042A980BFDB6F7D45A(__this, (intptr_t)L_0, /*hidden argument*/NULL);
		intptr_t L_1 = ___data0;
		__this->set_callbackData_6((intptr_t)L_1);
		return;
	}
}
// System.Void Firebase.Auth.Future_User::SWIG_CompletionDispatcher(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Future_User_SWIG_CompletionDispatcher_m9608732E8FCF7D93C2A70B63BA750D3231DCAD47 (int32_t ___key0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Future_User_SWIG_CompletionDispatcher_m9608732E8FCF7D93C2A70B63BA750D3231DCAD47_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327 * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	bool V_2 = false;
	bool V_3 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	int32_t G_B4_0 = 0;
	{
		V_0 = (Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327 *)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var);
		RuntimeObject * L_0 = ((Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_StaticFields*)il2cpp_codegen_static_fields_for(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var))->get_CallbackLock_5();
		V_1 = L_0;
		RuntimeObject * L_1 = V_1;
		Monitor_Enter_m903755FCC479745619842CCDBF5E6355319FA102(L_1, /*hidden argument*/NULL);
	}

IL_0010:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var);
			Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A * L_2 = ((Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_StaticFields*)il2cpp_codegen_static_fields_for(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var))->get_Callbacks_3();
			if (!L_2)
			{
				goto IL_0027;
			}
		}

IL_0018:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var);
			Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A * L_3 = ((Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_StaticFields*)il2cpp_codegen_static_fields_for(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var))->get_Callbacks_3();
			int32_t L_4 = ___key0;
			NullCheck(L_3);
			bool L_5 = Dictionary_2_TryGetValue_m2C05B94C421C439F201A3C24298D040A66655D23(L_3, L_4, (Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327 **)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m2C05B94C421C439F201A3C24298D040A66655D23_RuntimeMethod_var);
			G_B4_0 = ((int32_t)(L_5));
			goto IL_0028;
		}

IL_0027:
		{
			G_B4_0 = 0;
		}

IL_0028:
		{
			V_2 = (bool)G_B4_0;
			bool L_6 = V_2;
			if (!L_6)
			{
				goto IL_003a;
			}
		}

IL_002c:
		{
			IL2CPP_RUNTIME_CLASS_INIT(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var);
			Dictionary_2_t2F8C97E68EF2E5475D7792FFEB21B22C000F4E3A * L_7 = ((Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_StaticFields*)il2cpp_codegen_static_fields_for(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var))->get_Callbacks_3();
			int32_t L_8 = ___key0;
			NullCheck(L_7);
			Dictionary_2_Remove_m8D084F34E8CA42993A2B124C27B12C0520B916BD(L_7, L_8, /*hidden argument*/Dictionary_2_Remove_m8D084F34E8CA42993A2B124C27B12C0520B916BD_RuntimeMethod_var);
		}

IL_003a:
		{
			IL2CPP_LEAVE(0x45, FINALLY_003d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_003d;
	}

FINALLY_003d:
	{ // begin finally (depth: 1)
		RuntimeObject * L_9 = V_1;
		Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_9, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(61)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(61)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x45, IL_0045)
	}

IL_0045:
	{
		Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327 * L_10 = V_0;
		V_3 = (bool)((!(((RuntimeObject*)(Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327 *)L_10) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_11 = V_3;
		if (!L_11)
		{
			goto IL_0054;
		}
	}
	{
		Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327 * L_12 = V_0;
		NullCheck(L_12);
		Action_Invoke_m8B7DBC95C4F8A5255426959F447C3FDDC7DFB2B5(L_12, /*hidden argument*/NULL);
	}

IL_0054:
	{
		return;
	}
}
// System.IntPtr Firebase.Auth.Future_User::SWIG_OnCompletion(Firebase.Auth.Future_User_SWIG_CompletionDelegate,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t Future_User_SWIG_OnCompletion_mD2853E95978B586313F3D904FEDA65F0DC04B44A (Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * __this, SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2 * ___cs_callback0, int32_t ___cs_key1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Future_User_SWIG_OnCompletion_mD2853E95978B586313F3D904FEDA65F0DC04B44A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	bool V_1 = false;
	intptr_t V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_2();
		SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2 * L_1 = ___cs_callback0;
		int32_t L_2 = ___cs_key1;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		intptr_t L_3 = AuthUtilPINVOKE_Future_User_SWIG_OnCompletion_mA4865E7D9C1E147E26B7FD5F81078FA99F7ADF32(L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = (intptr_t)L_3;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_4 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_6 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, Future_User_SWIG_OnCompletion_mD2853E95978B586313F3D904FEDA65F0DC04B44A_RuntimeMethod_var);
	}

IL_001e:
	{
		intptr_t L_7 = V_0;
		V_2 = (intptr_t)L_7;
		goto IL_0022;
	}

IL_0022:
	{
		intptr_t L_8 = V_2;
		return (intptr_t)L_8;
	}
}
// System.Void Firebase.Auth.Future_User::SWIG_FreeCompletionData(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Future_User_SWIG_FreeCompletionData_mE5A33CAC1849B25DD160DC042A980BFDB6F7D45A (Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * __this, intptr_t ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Future_User_SWIG_FreeCompletionData_mE5A33CAC1849B25DD160DC042A980BFDB6F7D45A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_2();
		intptr_t L_1 = ___data0;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		AuthUtilPINVOKE_Future_User_SWIG_FreeCompletionData_m4E8D0B09D212ED4E404E86B97439D0AF51879DB1(L_0, (intptr_t)L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_0 = L_2;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_4 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, Future_User_SWIG_FreeCompletionData_mE5A33CAC1849B25DD160DC042A980BFDB6F7D45A_RuntimeMethod_var);
	}

IL_001d:
	{
		return;
	}
}
// Firebase.Auth.FirebaseUser Firebase.Auth.Future_User::GetResult()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * Future_User_GetResult_mE1A953E51D10BE4DD15137A78F45FFAA9D566902 (Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Future_User_GetResult_mE1A953E51D10BE4DD15137A78F45FFAA9D566902_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * V_1 = NULL;
	bool V_2 = false;
	FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * V_3 = NULL;
	FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * G_B3_0 = NULL;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_2();
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		intptr_t L_1 = AuthUtilPINVOKE_Future_User_GetResult_m36CFE956376E3A4DC187BCC9D6F2732417615FE9(L_0, /*hidden argument*/NULL);
		V_0 = (intptr_t)L_1;
		intptr_t L_2 = V_0;
		bool L_3 = IntPtr_op_Equality_mEE8D9FD2DFE312BBAA8B4ED3BF7976B3142A5934((intptr_t)L_2, (intptr_t)(0), /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0023;
		}
	}
	{
		intptr_t L_4 = V_0;
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_5 = (FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *)il2cpp_codegen_object_new(FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD_il2cpp_TypeInfo_var);
		FirebaseUser__ctor_m19BF183B130EA0D6A400A715A62A07877B7F15D5(L_5, (intptr_t)L_4, (bool)0, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		goto IL_0024;
	}

IL_0023:
	{
		G_B3_0 = ((FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD *)(NULL));
	}

IL_0024:
	{
		V_1 = G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_6 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_2 = L_6;
		bool L_7 = V_2;
		if (!L_7)
		{
			goto IL_0034;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_8 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, Future_User_GetResult_mE1A953E51D10BE4DD15137A78F45FFAA9D566902_RuntimeMethod_var);
	}

IL_0034:
	{
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_9 = V_1;
		V_3 = L_9;
		goto IL_0038;
	}

IL_0038:
	{
		FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_10 = V_3;
		return L_10;
	}
}
// System.Void Firebase.Auth.Future_User::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Future_User__cctor_m4576D93855EFC88AA0CD018753370EBD1F2BE0EA (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Future_User__cctor_m4576D93855EFC88AA0CD018753370EBD1F2BE0EA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_StaticFields*)il2cpp_codegen_static_fields_for(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var))->set_CallbackIndex_4(0);
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(L_0, /*hidden argument*/NULL);
		((Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_StaticFields*)il2cpp_codegen_static_fields_for(Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01_il2cpp_TypeInfo_var))->set_CallbackLock_5(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Auth.Future_User_<>c__DisplayClass4_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass4_0__ctor_m69741D9DAF70E435DFBFF0CFFA06B08796349300 (U3CU3Ec__DisplayClass4_0_t89802218D77EADC8D4FDB0DD3CF8FC5447E97B8D * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.Future_User_<>c__DisplayClass4_0::<GetTask>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass4_0_U3CGetTaskU3Eb__0_mC165A76D4A3E3CA94F84D8F613ECE4E75D2EF7DF (U3CU3Ec__DisplayClass4_0_t89802218D77EADC8D4FDB0DD3CF8FC5447E97B8D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass4_0_U3CGetTaskU3Eb__0_mC165A76D4A3E3CA94F84D8F613ECE4E75D2EF7DF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	bool V_2 = false;
	Exception_t * V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		{
			Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * L_0 = __this->get_fu_0();
			NullCheck(L_0);
			int32_t L_1 = FutureBase_status_mD675FFD3FAE10D36169C16590EE3C6F1861DD136(L_0, /*hidden argument*/NULL);
			V_0 = (bool)((((int32_t)L_1) == ((int32_t)2))? 1 : 0);
			bool L_2 = V_0;
			if (!L_2)
			{
				goto IL_0024;
			}
		}

IL_0014:
		{
			TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * L_3 = __this->get_tcs_1();
			NullCheck(L_3);
			TaskCompletionSource_1_SetCanceled_mE99DCDD6E5327656A075BA8D5EC8EAEF2F99AAB6(L_3, /*hidden argument*/TaskCompletionSource_1_SetCanceled_mE99DCDD6E5327656A075BA8D5EC8EAEF2F99AAB6_RuntimeMethod_var);
			goto IL_0074;
		}

IL_0024:
		{
			Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * L_4 = __this->get_fu_0();
			NullCheck(L_4);
			int32_t L_5 = FutureBase_error_m70CFE004FF942C34ED11D221D0B6033AD40C1825(L_4, /*hidden argument*/NULL);
			V_1 = L_5;
			int32_t L_6 = V_1;
			V_2 = (bool)((!(((uint32_t)L_6) <= ((uint32_t)0)))? 1 : 0);
			bool L_7 = V_2;
			if (!L_7)
			{
				goto IL_005a;
			}
		}

IL_0039:
		{
			TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * L_8 = __this->get_tcs_1();
			int32_t L_9 = V_1;
			Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * L_10 = __this->get_fu_0();
			NullCheck(L_10);
			String_t* L_11 = FutureBase_error_message_m12EFFA84132AB25450D3D3F349A414EF597CCCE3(L_10, /*hidden argument*/NULL);
			FirebaseException_t947F51F8FDF9CB2D145DC24E92160DC67BF12AEF * L_12 = (FirebaseException_t947F51F8FDF9CB2D145DC24E92160DC67BF12AEF *)il2cpp_codegen_object_new(FirebaseException_t947F51F8FDF9CB2D145DC24E92160DC67BF12AEF_il2cpp_TypeInfo_var);
			FirebaseException__ctor_mB31E783657F428BA72B055DF8A4CE6EA6996996C(L_12, L_9, L_11, /*hidden argument*/NULL);
			NullCheck(L_8);
			TaskCompletionSource_1_SetException_m0F8FFC0B85AFA0B942C0966332B7E3275CF42A1B(L_8, L_12, /*hidden argument*/TaskCompletionSource_1_SetException_m0F8FFC0B85AFA0B942C0966332B7E3275CF42A1B_RuntimeMethod_var);
			goto IL_0073;
		}

IL_005a:
		{
			TaskCompletionSource_1_tE4AEEAA7BE7106BC1A0974A234533E62519B63ED * L_13 = __this->get_tcs_1();
			Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * L_14 = __this->get_fu_0();
			NullCheck(L_14);
			FirebaseUser_tE668B8A077FD10A63889DAF0F03DFB46E1CAF7AD * L_15 = Future_User_GetResult_mE1A953E51D10BE4DD15137A78F45FFAA9D566902(L_14, /*hidden argument*/NULL);
			NullCheck(L_13);
			TaskCompletionSource_1_SetResult_mE9B14A44C05A19AB24B379A0CB6C4EC6F21BF410(L_13, L_15, /*hidden argument*/TaskCompletionSource_1_SetResult_mE9B14A44C05A19AB24B379A0CB6C4EC6F21BF410_RuntimeMethod_var);
		}

IL_0073:
		{
		}

IL_0074:
		{
			goto IL_008e;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0077;
		throw e;
	}

CATCH_0077:
	{ // begin catch(System.Exception)
		V_3 = ((Exception_t *)__exception_local);
		Exception_t * L_16 = V_3;
		String_t* L_17 = String_Format_m0ACDD8B34764E4040AED0B3EEB753567E4576BFA(_stringLiteral8DE36E4005BD5F6F460D1074B3B6D78F7EB671CD, L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LogUtil_t09D9F776A2032DC33A29AA0E2D4E4E508B8508C4_il2cpp_TypeInfo_var);
		LogUtil_LogMessage_mE0D4736B7C636462B2AD246F65EFAF5F1785822F(4, L_17, /*hidden argument*/NULL);
		goto IL_008e;
	} // end catch (depth: 1)

IL_008e:
	{
		Future_User_t72140E090FDB28C9CB2F37BAF6FCA374EA90FC01 * L_18 = __this->get_fu_0();
		NullCheck(L_18);
		FutureBase_Dispose_mE480130D78254414AA6A59E2FA63B9AFC4AC01BC(L_18, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327 (Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327 * __this, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void Firebase.Auth.Future_User_Action::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action__ctor_m9AE0C8C97132B6FFDB6D84FDC9A8802717222F39 (Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Firebase.Auth.Future_User_Action::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_Invoke_m8B7DBC95C4F8A5255426959F447C3FDDC7DFB2B5 (Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327 * __this, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 0)
			{
				// open
				typedef void (*FunctionPointerType) (const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, targetThis);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, targetThis);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
			}
		}
	}
}
// System.IAsyncResult Firebase.Auth.Future_User_Action::BeginInvoke(System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Action_BeginInvoke_m153941CDA9F036DD9B908B6294DAFAB3A9C763E4 (Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327 * __this, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void Firebase.Auth.Future_User_Action::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_EndInvoke_m49FAC74EBAB03687FE2F0001B201C163432F6DBC (Action_t16C9D12B5531159C33289A8E94CF0FD07D14C327 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2 (SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc(___index0);

}
// System.Void Firebase.Auth.Future_User_SWIG_CompletionDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIG_CompletionDelegate__ctor_m9FCE4E269635690B019F5FF509AFF343FA13DBDC (SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Firebase.Auth.Future_User_SWIG_CompletionDelegate::Invoke(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIG_CompletionDelegate_Invoke_mDEC298EADEA6875C62E559FACECB441130831BE7 (SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (int32_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___index0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, int32_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___index0, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< int32_t >::Invoke(targetMethod, targetThis, ___index0);
					else
						GenericVirtActionInvoker1< int32_t >::Invoke(targetMethod, targetThis, ___index0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___index0);
					else
						VirtActionInvoker1< int32_t >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___index0);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(&___index0) - 1), targetMethod);
				}
				typedef void (*FunctionPointerType) (void*, int32_t, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___index0, targetMethod);
			}
		}
	}
}
// System.IAsyncResult Firebase.Auth.Future_User_SWIG_CompletionDelegate::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* SWIG_CompletionDelegate_BeginInvoke_m1F1D032BA7DC7797D7B332090F05CF043978EA1F (SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2 * __this, int32_t ___index0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SWIG_CompletionDelegate_BeginInvoke_m1F1D032BA7DC7797D7B332090F05CF043978EA1F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &___index0);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void Firebase.Auth.Future_User_SWIG_CompletionDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SWIG_CompletionDelegate_EndInvoke_m78A511D0EE58036E4D5A03C2690C362F49A84CE4 (SWIG_CompletionDelegate_t51B732F1A323FC19A0139557D167E0FD33CA3FA2 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Auth.PlayGamesAuthProvider::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayGamesAuthProvider__cctor_m8EE6103C650CD01A16F0ECFB31FD4E50E6E17CBA (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesAuthProvider__cctor_m8EE6103C650CD01A16F0ECFB31FD4E50E6E17CBA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LogUtil_t09D9F776A2032DC33A29AA0E2D4E4E508B8508C4_il2cpp_TypeInfo_var);
		LogUtil_InitializeLogging_m3DC0D9A5186F8E0E1972761477DB5461FFCEC354(/*hidden argument*/NULL);
		return;
	}
}
// Firebase.Auth.Credential Firebase.Auth.PlayGamesAuthProvider::GetCredential(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * PlayGamesAuthProvider_GetCredential_mA75F27769EA5F8148F14D040FB1C9783DF4ABC33 (String_t* ___serverAuthCode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayGamesAuthProvider_GetCredential_mA75F27769EA5F8148F14D040FB1C9783DF4ABC33_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * V_0 = NULL;
	bool V_1 = false;
	Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * V_2 = NULL;
	{
		String_t* L_0 = ___serverAuthCode0;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		intptr_t L_1 = AuthUtilPINVOKE_PlayGamesAuthProvider_GetCredential_mF15F71E21D192B4940648E872A8F7F8E8713A0CA(L_0, /*hidden argument*/NULL);
		Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * L_2 = (Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 *)il2cpp_codegen_object_new(Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630_il2cpp_TypeInfo_var);
		Credential__ctor_mBC968251CEBE306DF7CD89C85E74D1B1D451CFC7(L_2, (intptr_t)L_1, (bool)1, /*hidden argument*/NULL);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_3 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_1 = L_3;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_5 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, PlayGamesAuthProvider_GetCredential_mA75F27769EA5F8148F14D040FB1C9783DF4ABC33_RuntimeMethod_var);
	}

IL_001d:
	{
		Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * L_6 = V_0;
		V_2 = L_6;
		goto IL_0021;
	}

IL_0021:
	{
		Credential_t0A0989C623B024DA6E2FEA77E7DA5EE985CD6630 * L_7 = V_2;
		return L_7;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Auth.UserInfoInterface::.ctor(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterface__ctor_m7E75B0EBDFD46089D31A77871A8B74520EEBC04C (UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		bool L_0 = ___cMemoryOwn1;
		__this->set_swigCMemOwn_1(L_0);
		intptr_t L_1 = ___cPtr0;
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_2;
		memset((&L_2), 0, sizeof(L_2));
		HandleRef__ctor_mCD64E7F91766FE7602EE34A8CEEF6C1EF4C943BB((&L_2), __this, (intptr_t)L_1, /*hidden argument*/NULL);
		__this->set_swigCPtr_0(L_2);
		return;
	}
}
// System.Runtime.InteropServices.HandleRef Firebase.Auth.UserInfoInterface::getCPtr(Firebase.Auth.UserInfoInterface)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HandleRef_t876E76124F400D12395BF61D562162AB6822204A  UserInfoInterface_getCPtr_m15659A46894D5F64CC2EE953C4F5D97DCB243680 (UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterface_getCPtr_m15659A46894D5F64CC2EE953C4F5D97DCB243680_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  V_0;
	memset((&V_0), 0, sizeof(V_0));
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  G_B3_0;
	memset((&G_B3_0), 0, sizeof(G_B3_0));
	{
		UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * L_0 = ___obj0;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * L_1 = ___obj0;
		NullCheck(L_1);
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_2 = L_1->get_swigCPtr_0();
		G_B3_0 = L_2;
		goto IL_0017;
	}

IL_000c:
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_3;
		memset((&L_3), 0, sizeof(L_3));
		HandleRef__ctor_mCD64E7F91766FE7602EE34A8CEEF6C1EF4C943BB((&L_3), NULL, (intptr_t)(0), /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_0017:
	{
		V_0 = G_B3_0;
		goto IL_001a;
	}

IL_001a:
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_4 = V_0;
		return L_4;
	}
}
// System.Void Firebase.Auth.UserInfoInterface::Finalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterface_Finalize_m9DFDC97F9560711EAD0F29882ACDB60CCDA56D66 (UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void Firebase.Auth.UserInfoInterface::Dispose(System.Boolean) */, __this, (bool)0);
		IL2CPP_LEAVE(0x14, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m4015B7D3A44DE125C5FE34D7276CD4697C06F380(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x14, IL_0014)
	}

IL_0014:
	{
		return;
	}
}
// System.Void Firebase.Auth.UserInfoInterface::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterface_Dispose_mE1B360122F9E6726A8FE40382AE6BD8683EAD61B (UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterface_Dispose_mE1B360122F9E6726A8FE40382AE6BD8683EAD61B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void Firebase.Auth.UserInfoInterface::Dispose(System.Boolean) */, __this, (bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(GC_tC1D7BD74E8F44ECCEF5CD2B5D84BFF9AAE02D01D_il2cpp_TypeInfo_var);
		GC_SuppressFinalize_m037319A9B95A5BA437E806DE592802225EE5B425(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.UserInfoInterface::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterface_Dispose_m8532EF4241914613A205B8E2E1720C39FD85C1AA (UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * __this, bool ___disposing0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterface_Dispose_m8532EF4241914613A205B8E2E1720C39FD85C1AA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_il2cpp_TypeInfo_var);
		RuntimeObject * L_0 = ((FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_il2cpp_TypeInfo_var))->get_disposeLock_2();
		V_0 = L_0;
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_m903755FCC479745619842CCDBF5E6355319FA102(L_1, /*hidden argument*/NULL);
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			HandleRef_t876E76124F400D12395BF61D562162AB6822204A * L_2 = __this->get_address_of_swigCPtr_0();
			intptr_t L_3 = HandleRef_get_Handle_m095712F299EA36BF75975343C726FA160EE38277_inline((HandleRef_t876E76124F400D12395BF61D562162AB6822204A *)L_2, /*hidden argument*/NULL);
			bool L_4 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_3, (intptr_t)(0), /*hidden argument*/NULL);
			V_1 = L_4;
			bool L_5 = V_1;
			if (!L_5)
			{
				goto IL_005a;
			}
		}

IL_0028:
		{
			bool L_6 = __this->get_swigCMemOwn_1();
			V_2 = L_6;
			bool L_7 = V_2;
			if (!L_7)
			{
				goto IL_0048;
			}
		}

IL_0033:
		{
			__this->set_swigCMemOwn_1((bool)0);
			HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_8 = __this->get_swigCPtr_0();
			IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
			AuthUtilPINVOKE_delete_UserInfoInterface_m6EECB261E56A9D7F8BB61C69988423AD41B16747(L_8, /*hidden argument*/NULL);
		}

IL_0048:
		{
			HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_9;
			memset((&L_9), 0, sizeof(L_9));
			HandleRef__ctor_mCD64E7F91766FE7602EE34A8CEEF6C1EF4C943BB((&L_9), NULL, (intptr_t)(0), /*hidden argument*/NULL);
			__this->set_swigCPtr_0(L_9);
		}

IL_005a:
		{
			IL2CPP_RUNTIME_CLASS_INIT(GC_tC1D7BD74E8F44ECCEF5CD2B5D84BFF9AAE02D01D_il2cpp_TypeInfo_var);
			GC_SuppressFinalize_m037319A9B95A5BA437E806DE592802225EE5B425(__this, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x6C, FINALLY_0064);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0064;
	}

FINALLY_0064:
	{ // begin finally (depth: 1)
		RuntimeObject * L_10 = V_0;
		Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_10, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(100)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(100)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x6C, IL_006c)
	}

IL_006c:
	{
		return;
	}
}
// System.Uri Firebase.Auth.UserInfoInterface::get_PhotoUrl()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * UserInfoInterface_get_PhotoUrl_m459FA1AD2C0AB62EFEF0B264F4ED9532CC15484C (UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterface_get_PhotoUrl_m459FA1AD2C0AB62EFEF0B264F4ED9532CC15484C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * V_0 = NULL;
	{
		String_t* L_0 = UserInfoInterface_get_PhotoUrlInternal_m9F70731EE318CA6D5F39817B161185E198BAC024(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_il2cpp_TypeInfo_var);
		Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * L_1 = FirebaseApp_UrlStringToUri_m09F6D262D86EE9898214D35510E1E8CEBF53D0DA(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * L_2 = V_0;
		return L_2;
	}
}
// System.String Firebase.Auth.UserInfoInterface::get_UserId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UserInfoInterface_get_UserId_m3A02719809CE964AFB9ABECF26FFCF81D2415C9D (UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterface_get_UserId_m3A02719809CE964AFB9ABECF26FFCF81D2415C9D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	String_t* V_2 = NULL;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_0();
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		String_t* L_1 = AuthUtilPINVOKE_UserInfoInterface_UserId_get_m3B7A2BC624181BC02ACF3481BF437903B1EFC954(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_1 = L_2;
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_4 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, UserInfoInterface_get_UserId_m3A02719809CE964AFB9ABECF26FFCF81D2415C9D_RuntimeMethod_var);
	}

IL_001c:
	{
		String_t* L_5 = V_0;
		V_2 = L_5;
		goto IL_0020;
	}

IL_0020:
	{
		String_t* L_6 = V_2;
		return L_6;
	}
}
// System.String Firebase.Auth.UserInfoInterface::get_Email()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UserInfoInterface_get_Email_m02D9D14CC54BD35802499A248FD7382F9E95C6AC (UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterface_get_Email_m02D9D14CC54BD35802499A248FD7382F9E95C6AC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	String_t* V_2 = NULL;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_0();
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		String_t* L_1 = AuthUtilPINVOKE_UserInfoInterface_Email_get_m49D0AB8130C3E528C1D44B66CC7D681B2F2E804B(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_1 = L_2;
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_4 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, UserInfoInterface_get_Email_m02D9D14CC54BD35802499A248FD7382F9E95C6AC_RuntimeMethod_var);
	}

IL_001c:
	{
		String_t* L_5 = V_0;
		V_2 = L_5;
		goto IL_0020;
	}

IL_0020:
	{
		String_t* L_6 = V_2;
		return L_6;
	}
}
// System.String Firebase.Auth.UserInfoInterface::get_DisplayName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UserInfoInterface_get_DisplayName_m3DCD08821470A2241296F8AE5E40E28935E61832 (UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterface_get_DisplayName_m3DCD08821470A2241296F8AE5E40E28935E61832_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	String_t* V_2 = NULL;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_0();
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		String_t* L_1 = AuthUtilPINVOKE_UserInfoInterface_DisplayName_get_m40F5D1957524C394F0A8022F47B399931BE4DFF7(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_1 = L_2;
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_4 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, UserInfoInterface_get_DisplayName_m3DCD08821470A2241296F8AE5E40E28935E61832_RuntimeMethod_var);
	}

IL_001c:
	{
		String_t* L_5 = V_0;
		V_2 = L_5;
		goto IL_0020;
	}

IL_0020:
	{
		String_t* L_6 = V_2;
		return L_6;
	}
}
// System.String Firebase.Auth.UserInfoInterface::get_PhotoUrlInternal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UserInfoInterface_get_PhotoUrlInternal_m9F70731EE318CA6D5F39817B161185E198BAC024 (UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterface_get_PhotoUrlInternal_m9F70731EE318CA6D5F39817B161185E198BAC024_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	String_t* V_2 = NULL;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_0();
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		String_t* L_1 = AuthUtilPINVOKE_UserInfoInterface_PhotoUrlInternal_get_mA1C69290B7452547D58BA4675DAE501090CCA5B7(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_1 = L_2;
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_4 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, UserInfoInterface_get_PhotoUrlInternal_m9F70731EE318CA6D5F39817B161185E198BAC024_RuntimeMethod_var);
	}

IL_001c:
	{
		String_t* L_5 = V_0;
		V_2 = L_5;
		goto IL_0020;
	}

IL_0020:
	{
		String_t* L_6 = V_2;
		return L_6;
	}
}
// System.String Firebase.Auth.UserInfoInterface::get_ProviderId()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UserInfoInterface_get_ProviderId_mA71AA9527D83D2FBACFBCB38ACACA341F197660C (UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterface_get_ProviderId_mA71AA9527D83D2FBACFBCB38ACACA341F197660C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	String_t* V_2 = NULL;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_0();
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		String_t* L_1 = AuthUtilPINVOKE_UserInfoInterface_ProviderId_get_mB0AC6D00DCC186EB3D315809B0727D666F24C9B0(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_1 = L_2;
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_4 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, UserInfoInterface_get_ProviderId_mA71AA9527D83D2FBACFBCB38ACACA341F197660C_RuntimeMethod_var);
	}

IL_001c:
	{
		String_t* L_5 = V_0;
		V_2 = L_5;
		goto IL_0020;
	}

IL_0020:
	{
		String_t* L_6 = V_2;
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Auth.UserInfoInterfaceList::.ctor(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterfaceList__ctor_m969BCAE5B0171B9F24C435611C2D57CEF93C4F54 (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		bool L_0 = ___cMemoryOwn1;
		__this->set_swigCMemOwn_1(L_0);
		intptr_t L_1 = ___cPtr0;
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_2;
		memset((&L_2), 0, sizeof(L_2));
		HandleRef__ctor_mCD64E7F91766FE7602EE34A8CEEF6C1EF4C943BB((&L_2), __this, (intptr_t)L_1, /*hidden argument*/NULL);
		__this->set_swigCPtr_0(L_2);
		return;
	}
}
// System.Void Firebase.Auth.UserInfoInterfaceList::Finalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterfaceList_Finalize_mD536EF30C4A7836DC7AE6F949107D1F770E34BB6 (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		VirtActionInvoker1< bool >::Invoke(19 /* System.Void Firebase.Auth.UserInfoInterfaceList::Dispose(System.Boolean) */, __this, (bool)0);
		IL2CPP_LEAVE(0x14, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m4015B7D3A44DE125C5FE34D7276CD4697C06F380(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x14, IL_0014)
	}

IL_0014:
	{
		return;
	}
}
// System.Void Firebase.Auth.UserInfoInterfaceList::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterfaceList_Dispose_mC25B3B40D335F63D189E23B81DEEA426C0E76350 (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterfaceList_Dispose_mC25B3B40D335F63D189E23B81DEEA426C0E76350_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VirtActionInvoker1< bool >::Invoke(19 /* System.Void Firebase.Auth.UserInfoInterfaceList::Dispose(System.Boolean) */, __this, (bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(GC_tC1D7BD74E8F44ECCEF5CD2B5D84BFF9AAE02D01D_il2cpp_TypeInfo_var);
		GC_SuppressFinalize_m037319A9B95A5BA437E806DE592802225EE5B425(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.UserInfoInterfaceList::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterfaceList_Dispose_m6903C9BF5E1050559288FD30150C273778B4DC05 (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, bool ___disposing0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterfaceList_Dispose_m6903C9BF5E1050559288FD30150C273778B4DC05_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_il2cpp_TypeInfo_var);
		RuntimeObject * L_0 = ((FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_il2cpp_TypeInfo_var))->get_disposeLock_2();
		V_0 = L_0;
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_m903755FCC479745619842CCDBF5E6355319FA102(L_1, /*hidden argument*/NULL);
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			HandleRef_t876E76124F400D12395BF61D562162AB6822204A * L_2 = __this->get_address_of_swigCPtr_0();
			intptr_t L_3 = HandleRef_get_Handle_m095712F299EA36BF75975343C726FA160EE38277_inline((HandleRef_t876E76124F400D12395BF61D562162AB6822204A *)L_2, /*hidden argument*/NULL);
			bool L_4 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_3, (intptr_t)(0), /*hidden argument*/NULL);
			V_1 = L_4;
			bool L_5 = V_1;
			if (!L_5)
			{
				goto IL_005a;
			}
		}

IL_0028:
		{
			bool L_6 = __this->get_swigCMemOwn_1();
			V_2 = L_6;
			bool L_7 = V_2;
			if (!L_7)
			{
				goto IL_0048;
			}
		}

IL_0033:
		{
			__this->set_swigCMemOwn_1((bool)0);
			HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_8 = __this->get_swigCPtr_0();
			IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
			AuthUtilPINVOKE_delete_UserInfoInterfaceList_mF4C1DD6531A015AC1E86A763A37E9E2EDC5B1A51(L_8, /*hidden argument*/NULL);
		}

IL_0048:
		{
			HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_9;
			memset((&L_9), 0, sizeof(L_9));
			HandleRef__ctor_mCD64E7F91766FE7602EE34A8CEEF6C1EF4C943BB((&L_9), NULL, (intptr_t)(0), /*hidden argument*/NULL);
			__this->set_swigCPtr_0(L_9);
		}

IL_005a:
		{
			IL2CPP_RUNTIME_CLASS_INIT(GC_tC1D7BD74E8F44ECCEF5CD2B5D84BFF9AAE02D01D_il2cpp_TypeInfo_var);
			GC_SuppressFinalize_m037319A9B95A5BA437E806DE592802225EE5B425(__this, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x6C, FINALLY_0064);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0064;
	}

FINALLY_0064:
	{ // begin finally (depth: 1)
		RuntimeObject * L_10 = V_0;
		Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_10, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(100)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(100)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x6C, IL_006c)
	}

IL_006c:
	{
		return;
	}
}
// System.Boolean Firebase.Auth.UserInfoInterfaceList::get_IsReadOnly()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UserInfoInterfaceList_get_IsReadOnly_m97CCE213B0867EC6B07AF2E0AFCFE5D33DC82844 (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		goto IL_0005;
	}

IL_0005:
	{
		bool L_0 = V_0;
		return L_0;
	}
}
// Firebase.Auth.UserInfoInterface Firebase.Auth.UserInfoInterfaceList::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * UserInfoInterfaceList_get_Item_m2CF8D552EFD6768C39D2579B76356659F2C570DD (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * V_0 = NULL;
	{
		int32_t L_0 = ___index0;
		UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * L_1 = UserInfoInterfaceList_getitem_mFD9B0EB9CA5CA925534712BDF225DEE3B2439CEA(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000b;
	}

IL_000b:
	{
		UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * L_2 = V_0;
		return L_2;
	}
}
// System.Void Firebase.Auth.UserInfoInterfaceList::set_Item(System.Int32,Firebase.Auth.UserInfoInterface)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterfaceList_set_Item_mA5E90884FF1A01C43CCE62ABD40CD296DC000ED5 (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, int32_t ___index0, UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * ___value1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * L_1 = ___value1;
		UserInfoInterfaceList_setitem_m87AC7CBE94A90F753BCF80E9658D7FD0C9A8F80F(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Firebase.Auth.UserInfoInterfaceList::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UserInfoInterfaceList_get_Count_mE191B3C8A1BDBE827045B98CAC48447631613074 (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		uint32_t L_0 = UserInfoInterfaceList_size_m370AE0FA3028E3D76252D636A6274D1FBD12BC6B(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void Firebase.Auth.UserInfoInterfaceList::CopyTo(Firebase.Auth.UserInfoInterface[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterfaceList_CopyTo_m11CCDBDABE905725AC2B4D5FBCF19AA926C4F25A (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, UserInfoInterfaceU5BU5D_tD810A76761E9D37FB729A09C273728E21098926B* ___array0, int32_t ___arrayIndex1, const RuntimeMethod* method)
{
	{
		UserInfoInterfaceU5BU5D_tD810A76761E9D37FB729A09C273728E21098926B* L_0 = ___array0;
		int32_t L_1 = ___arrayIndex1;
		int32_t L_2 = UserInfoInterfaceList_get_Count_mE191B3C8A1BDBE827045B98CAC48447631613074(__this, /*hidden argument*/NULL);
		UserInfoInterfaceList_CopyTo_mA6612EBF0827029C8CBF0CEE383D0FFB11D32B9E(__this, 0, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.UserInfoInterfaceList::CopyTo(System.Int32,Firebase.Auth.UserInfoInterface[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterfaceList_CopyTo_mA6612EBF0827029C8CBF0CEE383D0FFB11D32B9E (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, int32_t ___index0, UserInfoInterfaceU5BU5D_tD810A76761E9D37FB729A09C273728E21098926B* ___array1, int32_t ___arrayIndex2, int32_t ___count3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterfaceList_CopyTo_mA6612EBF0827029C8CBF0CEE383D0FFB11D32B9E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	int32_t V_6 = 0;
	bool V_7 = false;
	int32_t G_B13_0 = 0;
	{
		UserInfoInterfaceU5BU5D_tD810A76761E9D37FB729A09C273728E21098926B* L_0 = ___array1;
		V_0 = (bool)((((RuntimeObject*)(UserInfoInterfaceU5BU5D_tD810A76761E9D37FB729A09C273728E21098926B*)L_0) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, _stringLiteral19EDC1210777BA4D45049C29280D9CC5E1064C25, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, UserInfoInterfaceList_CopyTo_mA6612EBF0827029C8CBF0CEE383D0FFB11D32B9E_RuntimeMethod_var);
	}

IL_0014:
	{
		int32_t L_3 = ___index0;
		V_1 = (bool)((((int32_t)L_3) < ((int32_t)0))? 1 : 0);
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_002c;
		}
	}
	{
		ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * L_5 = (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m300CE4D04A068C209FD858101AC361C1B600B5AE(L_5, _stringLiteralE540CDD1328B2B21E29A95405C301B9313B7C346, _stringLiteral4BD075D15756473773D5A51697A6E781E4129F82, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, UserInfoInterfaceList_CopyTo_mA6612EBF0827029C8CBF0CEE383D0FFB11D32B9E_RuntimeMethod_var);
	}

IL_002c:
	{
		int32_t L_6 = ___arrayIndex2;
		V_2 = (bool)((((int32_t)L_6) < ((int32_t)0))? 1 : 0);
		bool L_7 = V_2;
		if (!L_7)
		{
			goto IL_0044;
		}
	}
	{
		ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * L_8 = (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m300CE4D04A068C209FD858101AC361C1B600B5AE(L_8, _stringLiteralFA5342C4F12AD1A860B71DA5AD002761768999C3, _stringLiteral4BD075D15756473773D5A51697A6E781E4129F82, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, UserInfoInterfaceList_CopyTo_mA6612EBF0827029C8CBF0CEE383D0FFB11D32B9E_RuntimeMethod_var);
	}

IL_0044:
	{
		int32_t L_9 = ___count3;
		V_3 = (bool)((((int32_t)L_9) < ((int32_t)0))? 1 : 0);
		bool L_10 = V_3;
		if (!L_10)
		{
			goto IL_005d;
		}
	}
	{
		ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * L_11 = (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m300CE4D04A068C209FD858101AC361C1B600B5AE(L_11, _stringLiteralEE9F38E186BA06F57B7B74D7E626B94E13CE2556, _stringLiteral4BD075D15756473773D5A51697A6E781E4129F82, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11, UserInfoInterfaceList_CopyTo_mA6612EBF0827029C8CBF0CEE383D0FFB11D32B9E_RuntimeMethod_var);
	}

IL_005d:
	{
		UserInfoInterfaceU5BU5D_tD810A76761E9D37FB729A09C273728E21098926B* L_12 = ___array1;
		NullCheck((RuntimeArray *)(RuntimeArray *)L_12);
		int32_t L_13 = Array_get_Rank_m38145B59D67D75F9896A3F8CDA9B966641AE99E1((RuntimeArray *)(RuntimeArray *)L_12, /*hidden argument*/NULL);
		V_4 = (bool)((((int32_t)L_13) > ((int32_t)1))? 1 : 0);
		bool L_14 = V_4;
		if (!L_14)
		{
			goto IL_007c;
		}
	}
	{
		ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * L_15 = (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 *)il2cpp_codegen_object_new(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m26DC3463C6F3C98BF33EA39598DD2B32F0249CA8(L_15, _stringLiteral75214116684BF6C2BF7B4CD15219AD3BD7A9A2A5, _stringLiteral19EDC1210777BA4D45049C29280D9CC5E1064C25, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15, UserInfoInterfaceList_CopyTo_mA6612EBF0827029C8CBF0CEE383D0FFB11D32B9E_RuntimeMethod_var);
	}

IL_007c:
	{
		int32_t L_16 = ___index0;
		int32_t L_17 = ___count3;
		int32_t L_18 = UserInfoInterfaceList_get_Count_mE191B3C8A1BDBE827045B98CAC48447631613074(__this, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)L_17))) > ((int32_t)L_18)))
		{
			goto IL_0093;
		}
	}
	{
		int32_t L_19 = ___arrayIndex2;
		int32_t L_20 = ___count3;
		UserInfoInterfaceU5BU5D_tD810A76761E9D37FB729A09C273728E21098926B* L_21 = ___array1;
		NullCheck(L_21);
		G_B13_0 = ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)L_20))) > ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_21)->max_length))))))? 1 : 0);
		goto IL_0094;
	}

IL_0093:
	{
		G_B13_0 = 1;
	}

IL_0094:
	{
		V_5 = (bool)G_B13_0;
		bool L_22 = V_5;
		if (!L_22)
		{
			goto IL_00a5;
		}
	}
	{
		ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 * L_23 = (ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1 *)il2cpp_codegen_object_new(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m9A85EF7FEFEC21DDD525A67E831D77278E5165B7(L_23, _stringLiteral8DF36EA2B52717B5109B7D5B88A7A8F713B3640C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_23, UserInfoInterfaceList_CopyTo_mA6612EBF0827029C8CBF0CEE383D0FFB11D32B9E_RuntimeMethod_var);
	}

IL_00a5:
	{
		V_6 = 0;
		goto IL_00c5;
	}

IL_00aa:
	{
		UserInfoInterfaceU5BU5D_tD810A76761E9D37FB729A09C273728E21098926B* L_24 = ___array1;
		int32_t L_25 = ___index0;
		int32_t L_26 = V_6;
		UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * L_27 = UserInfoInterfaceList_getitemcopy_m886AA8C76520C49BFC25FEE8AFC1C2767BBA2A00(__this, ((int32_t)il2cpp_codegen_add((int32_t)L_25, (int32_t)L_26)), /*hidden argument*/NULL);
		int32_t L_28 = ___arrayIndex2;
		int32_t L_29 = V_6;
		NullCheck((RuntimeArray *)(RuntimeArray *)L_24);
		Array_SetValue_m3C6811CE9C45D1E461404B5D2FBD4EC1A054FDCA((RuntimeArray *)(RuntimeArray *)L_24, L_27, ((int32_t)il2cpp_codegen_add((int32_t)L_28, (int32_t)L_29)), /*hidden argument*/NULL);
		int32_t L_30 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)1));
	}

IL_00c5:
	{
		int32_t L_31 = V_6;
		int32_t L_32 = ___count3;
		V_7 = (bool)((((int32_t)L_31) < ((int32_t)L_32))? 1 : 0);
		bool L_33 = V_7;
		if (L_33)
		{
			goto IL_00aa;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<Firebase.Auth.UserInfoInterface> Firebase.Auth.UserInfoInterfaceList::global::System.Collections.Generic.IEnumerable<Firebase.Auth.UserInfoInterface>.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* UserInfoInterfaceList_globalU3AU3ASystem_Collections_Generic_IEnumerableU3CFirebase_Auth_UserInfoInterfaceU3E_GetEnumerator_mCDDED8A7BA953D28567C3DE977F584DC759DE4C3 (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterfaceList_globalU3AU3ASystem_Collections_Generic_IEnumerableU3CFirebase_Auth_UserInfoInterfaceU3E_GetEnumerator_mCDDED8A7BA953D28567C3DE977F584DC759DE4C3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A * L_0 = (UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A *)il2cpp_codegen_object_new(UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A_il2cpp_TypeInfo_var);
		UserInfoInterfaceListEnumerator__ctor_mB52655D936C618ADDC4D506EF165F8ADAFAF350E(L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// System.Collections.IEnumerator Firebase.Auth.UserInfoInterfaceList::global::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* UserInfoInterfaceList_globalU3AU3ASystem_Collections_IEnumerable_GetEnumerator_m0D58B068226E88BDF4B38BF06648A191AC0E75F9 (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterfaceList_globalU3AU3ASystem_Collections_IEnumerable_GetEnumerator_m0D58B068226E88BDF4B38BF06648A191AC0E75F9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A * L_0 = (UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A *)il2cpp_codegen_object_new(UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A_il2cpp_TypeInfo_var);
		UserInfoInterfaceListEnumerator__ctor_mB52655D936C618ADDC4D506EF165F8ADAFAF350E(L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// Firebase.Auth.UserInfoInterfaceList_UserInfoInterfaceListEnumerator Firebase.Auth.UserInfoInterfaceList::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A * UserInfoInterfaceList_GetEnumerator_m34C89390756E8DCA0D4FDE9EC4020AF7AFBB4021 (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterfaceList_GetEnumerator_m34C89390756E8DCA0D4FDE9EC4020AF7AFBB4021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A * V_0 = NULL;
	{
		UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A * L_0 = (UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A *)il2cpp_codegen_object_new(UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A_il2cpp_TypeInfo_var);
		UserInfoInterfaceListEnumerator__ctor_mB52655D936C618ADDC4D506EF165F8ADAFAF350E(L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A * L_1 = V_0;
		return L_1;
	}
}
// System.Void Firebase.Auth.UserInfoInterfaceList::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterfaceList_Clear_mF43F449B72C454169BE9001F0BDB87A43B9BC711 (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterfaceList_Clear_mF43F449B72C454169BE9001F0BDB87A43B9BC711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_0();
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		AuthUtilPINVOKE_UserInfoInterfaceList_Clear_m0477B48FDCFEEC92FC007E3F43EC65647F9E240A(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_1 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_3 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, UserInfoInterfaceList_Clear_mF43F449B72C454169BE9001F0BDB87A43B9BC711_RuntimeMethod_var);
	}

IL_001c:
	{
		return;
	}
}
// System.Void Firebase.Auth.UserInfoInterfaceList::Add(Firebase.Auth.UserInfoInterface)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterfaceList_Add_m272A420019BB6431562C0EE4CF84E86280F2BBD9 (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterfaceList_Add_m272A420019BB6431562C0EE4CF84E86280F2BBD9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_0();
		UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * L_1 = ___x0;
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_2 = UserInfoInterface_getCPtr_m15659A46894D5F64CC2EE953C4F5D97DCB243680(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		AuthUtilPINVOKE_UserInfoInterfaceList_Add_m235699A22C7864EC52A0A694E8549F1C4C1AE089(L_0, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_3 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_0 = L_3;
		bool L_4 = V_0;
		if (!L_4)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_5 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, UserInfoInterfaceList_Add_m272A420019BB6431562C0EE4CF84E86280F2BBD9_RuntimeMethod_var);
	}

IL_0022:
	{
		return;
	}
}
// System.UInt32 Firebase.Auth.UserInfoInterfaceList::size()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t UserInfoInterfaceList_size_m370AE0FA3028E3D76252D636A6274D1FBD12BC6B (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterfaceList_size_m370AE0FA3028E3D76252D636A6274D1FBD12BC6B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	uint32_t V_2 = 0;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_0();
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		uint32_t L_1 = AuthUtilPINVOKE_UserInfoInterfaceList_size_m4022FD61AA1F01B2682E30DD2660D39144EE3875(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_1 = L_2;
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_4 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, UserInfoInterfaceList_size_m370AE0FA3028E3D76252D636A6274D1FBD12BC6B_RuntimeMethod_var);
	}

IL_001c:
	{
		uint32_t L_5 = V_0;
		V_2 = L_5;
		goto IL_0020;
	}

IL_0020:
	{
		uint32_t L_6 = V_2;
		return L_6;
	}
}
// Firebase.Auth.UserInfoInterface Firebase.Auth.UserInfoInterfaceList::getitemcopy(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * UserInfoInterfaceList_getitemcopy_m886AA8C76520C49BFC25FEE8AFC1C2767BBA2A00 (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterfaceList_getitemcopy_m886AA8C76520C49BFC25FEE8AFC1C2767BBA2A00_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * V_1 = NULL;
	bool V_2 = false;
	UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * V_3 = NULL;
	UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * G_B3_0 = NULL;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_0();
		int32_t L_1 = ___index0;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		intptr_t L_2 = AuthUtilPINVOKE_UserInfoInterfaceList_getitemcopy_mB07EF1F7557FF0370E3ACE819AEE2612D4CB524D(L_0, L_1, /*hidden argument*/NULL);
		V_0 = (intptr_t)L_2;
		intptr_t L_3 = V_0;
		bool L_4 = IntPtr_op_Equality_mEE8D9FD2DFE312BBAA8B4ED3BF7976B3142A5934((intptr_t)L_3, (intptr_t)(0), /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0024;
		}
	}
	{
		intptr_t L_5 = V_0;
		UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * L_6 = (UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C *)il2cpp_codegen_object_new(UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C_il2cpp_TypeInfo_var);
		UserInfoInterface__ctor_m7E75B0EBDFD46089D31A77871A8B74520EEBC04C(L_6, (intptr_t)L_5, (bool)0, /*hidden argument*/NULL);
		G_B3_0 = L_6;
		goto IL_0025;
	}

IL_0024:
	{
		G_B3_0 = ((UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C *)(NULL));
	}

IL_0025:
	{
		V_1 = G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_7 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_2 = L_7;
		bool L_8 = V_2;
		if (!L_8)
		{
			goto IL_0035;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_9 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, UserInfoInterfaceList_getitemcopy_m886AA8C76520C49BFC25FEE8AFC1C2767BBA2A00_RuntimeMethod_var);
	}

IL_0035:
	{
		UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * L_10 = V_1;
		V_3 = L_10;
		goto IL_0039;
	}

IL_0039:
	{
		UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * L_11 = V_3;
		return L_11;
	}
}
// Firebase.Auth.UserInfoInterface Firebase.Auth.UserInfoInterfaceList::getitem(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * UserInfoInterfaceList_getitem_mFD9B0EB9CA5CA925534712BDF225DEE3B2439CEA (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterfaceList_getitem_mFD9B0EB9CA5CA925534712BDF225DEE3B2439CEA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * V_1 = NULL;
	bool V_2 = false;
	UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * V_3 = NULL;
	UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * G_B3_0 = NULL;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_0();
		int32_t L_1 = ___index0;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		intptr_t L_2 = AuthUtilPINVOKE_UserInfoInterfaceList_getitem_m57D02F5BE29828EB28F2B8B3DC5591207B34BEE9(L_0, L_1, /*hidden argument*/NULL);
		V_0 = (intptr_t)L_2;
		intptr_t L_3 = V_0;
		bool L_4 = IntPtr_op_Equality_mEE8D9FD2DFE312BBAA8B4ED3BF7976B3142A5934((intptr_t)L_3, (intptr_t)(0), /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0024;
		}
	}
	{
		intptr_t L_5 = V_0;
		UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * L_6 = (UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C *)il2cpp_codegen_object_new(UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C_il2cpp_TypeInfo_var);
		UserInfoInterface__ctor_m7E75B0EBDFD46089D31A77871A8B74520EEBC04C(L_6, (intptr_t)L_5, (bool)0, /*hidden argument*/NULL);
		G_B3_0 = L_6;
		goto IL_0025;
	}

IL_0024:
	{
		G_B3_0 = ((UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C *)(NULL));
	}

IL_0025:
	{
		V_1 = G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_7 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_2 = L_7;
		bool L_8 = V_2;
		if (!L_8)
		{
			goto IL_0035;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_9 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, UserInfoInterfaceList_getitem_mFD9B0EB9CA5CA925534712BDF225DEE3B2439CEA_RuntimeMethod_var);
	}

IL_0035:
	{
		UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * L_10 = V_1;
		V_3 = L_10;
		goto IL_0039;
	}

IL_0039:
	{
		UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * L_11 = V_3;
		return L_11;
	}
}
// System.Void Firebase.Auth.UserInfoInterfaceList::setitem(System.Int32,Firebase.Auth.UserInfoInterface)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterfaceList_setitem_m87AC7CBE94A90F753BCF80E9658D7FD0C9A8F80F (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, int32_t ___index0, UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * ___val1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterfaceList_setitem_m87AC7CBE94A90F753BCF80E9658D7FD0C9A8F80F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_0();
		int32_t L_1 = ___index0;
		UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * L_2 = ___val1;
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_3 = UserInfoInterface_getCPtr_m15659A46894D5F64CC2EE953C4F5D97DCB243680(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		AuthUtilPINVOKE_UserInfoInterfaceList_setitem_mCB2A5B2BF72BD7E40187C4B07E9CC8FA9344A441(L_0, L_1, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_4 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_0 = L_4;
		bool L_5 = V_0;
		if (!L_5)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_6 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, UserInfoInterfaceList_setitem_m87AC7CBE94A90F753BCF80E9658D7FD0C9A8F80F_RuntimeMethod_var);
	}

IL_0023:
	{
		return;
	}
}
// System.Void Firebase.Auth.UserInfoInterfaceList::Insert(System.Int32,Firebase.Auth.UserInfoInterface)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterfaceList_Insert_mEF354596EFD206D50AE0F69B9A76E1A6B4DC1878 (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, int32_t ___index0, UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * ___x1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterfaceList_Insert_mEF354596EFD206D50AE0F69B9A76E1A6B4DC1878_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_0();
		int32_t L_1 = ___index0;
		UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * L_2 = ___x1;
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_3 = UserInfoInterface_getCPtr_m15659A46894D5F64CC2EE953C4F5D97DCB243680(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		AuthUtilPINVOKE_UserInfoInterfaceList_Insert_mF8231269D3DB49CE00D815C59748E6D77C5E8A6D(L_0, L_1, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_4 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_0 = L_4;
		bool L_5 = V_0;
		if (!L_5)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_6 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, UserInfoInterfaceList_Insert_mEF354596EFD206D50AE0F69B9A76E1A6B4DC1878_RuntimeMethod_var);
	}

IL_0023:
	{
		return;
	}
}
// System.Void Firebase.Auth.UserInfoInterfaceList::RemoveAt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterfaceList_RemoveAt_mBE2A2F80ACBA832F24F7B066A861019BFC1F4E0B (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterfaceList_RemoveAt_mBE2A2F80ACBA832F24F7B066A861019BFC1F4E0B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_0();
		int32_t L_1 = ___index0;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		AuthUtilPINVOKE_UserInfoInterfaceList_RemoveAt_m3E7CC088E5D8F62308D0A7193D34DE0DA9215179(L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_2 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_0 = L_2;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_4 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, UserInfoInterfaceList_RemoveAt_mBE2A2F80ACBA832F24F7B066A861019BFC1F4E0B_RuntimeMethod_var);
	}

IL_001d:
	{
		return;
	}
}
// System.Boolean Firebase.Auth.UserInfoInterfaceList::Contains(Firebase.Auth.UserInfoInterface)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UserInfoInterfaceList_Contains_m3D0CA1C4D5D72A7D59BE69C5067AAE50CF4063F8 (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterfaceList_Contains_m3D0CA1C4D5D72A7D59BE69C5067AAE50CF4063F8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_0();
		UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * L_1 = ___value0;
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_2 = UserInfoInterface_getCPtr_m15659A46894D5F64CC2EE953C4F5D97DCB243680(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		bool L_3 = AuthUtilPINVOKE_UserInfoInterfaceList_Contains_mACD9BBB51A6B1007C9107D484C7963ACC01C1803(L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_4 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_6 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, UserInfoInterfaceList_Contains_m3D0CA1C4D5D72A7D59BE69C5067AAE50CF4063F8_RuntimeMethod_var);
	}

IL_0022:
	{
		bool L_7 = V_0;
		V_2 = L_7;
		goto IL_0026;
	}

IL_0026:
	{
		bool L_8 = V_2;
		return L_8;
	}
}
// System.Int32 Firebase.Auth.UserInfoInterfaceList::IndexOf(Firebase.Auth.UserInfoInterface)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UserInfoInterfaceList_IndexOf_m814BFA29319E2C0C125EE6E87B0FD742C890CB86 (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterfaceList_IndexOf_m814BFA29319E2C0C125EE6E87B0FD742C890CB86_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t V_2 = 0;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_0();
		UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * L_1 = ___value0;
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_2 = UserInfoInterface_getCPtr_m15659A46894D5F64CC2EE953C4F5D97DCB243680(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		int32_t L_3 = AuthUtilPINVOKE_UserInfoInterfaceList_IndexOf_mC0DFA98501EF7C4F2D513ACF7B513877B71E69D3(L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_4 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_6 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, UserInfoInterfaceList_IndexOf_m814BFA29319E2C0C125EE6E87B0FD742C890CB86_RuntimeMethod_var);
	}

IL_0022:
	{
		int32_t L_7 = V_0;
		V_2 = L_7;
		goto IL_0026;
	}

IL_0026:
	{
		int32_t L_8 = V_2;
		return L_8;
	}
}
// System.Boolean Firebase.Auth.UserInfoInterfaceList::Remove(Firebase.Auth.UserInfoInterface)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UserInfoInterfaceList_Remove_m6AE184FAF9D7EC2568042FF0304127885EC5F422 (UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * __this, UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterfaceList_Remove_m6AE184FAF9D7EC2568042FF0304127885EC5F422_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_0 = __this->get_swigCPtr_0();
		UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * L_1 = ___value0;
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_2 = UserInfoInterface_getCPtr_m15659A46894D5F64CC2EE953C4F5D97DCB243680(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		bool L_3 = AuthUtilPINVOKE_UserInfoInterfaceList_Remove_m58DD7F22CAFF88AABC98EBB2B4003720BCDEE7BC(L_0, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_4 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_6 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, UserInfoInterfaceList_Remove_m6AE184FAF9D7EC2568042FF0304127885EC5F422_RuntimeMethod_var);
	}

IL_0022:
	{
		bool L_7 = V_0;
		V_2 = L_7;
		goto IL_0026;
	}

IL_0026:
	{
		bool L_8 = V_2;
		return L_8;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Auth.UserInfoInterfaceList_UserInfoInterfaceListEnumerator::.ctor(Firebase.Auth.UserInfoInterfaceList)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterfaceListEnumerator__ctor_mB52655D936C618ADDC4D506EF165F8ADAFAF350E (UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A * __this, UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * ___collection0, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * L_0 = ___collection0;
		__this->set_collectionRef_0(L_0);
		__this->set_currentIndex_1((-1));
		__this->set_currentObject_2(NULL);
		UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * L_1 = __this->get_collectionRef_0();
		NullCheck(L_1);
		int32_t L_2 = UserInfoInterfaceList_get_Count_mE191B3C8A1BDBE827045B98CAC48447631613074(L_1, /*hidden argument*/NULL);
		__this->set_currentSize_3(L_2);
		return;
	}
}
// Firebase.Auth.UserInfoInterface Firebase.Auth.UserInfoInterfaceList_UserInfoInterfaceListEnumerator::get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * UserInfoInterfaceListEnumerator_get_Current_mE95C0E6329FA074B3BB193C170A0B46DD8A6A720 (UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterfaceListEnumerator_get_Current_mE95C0E6329FA074B3BB193C170A0B46DD8A6A720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * V_3 = NULL;
	{
		int32_t L_0 = __this->get_currentIndex_1();
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)(-1)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_2 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m72027D5F1D513C25C05137E203EEED8FD8297706(L_2, _stringLiteralB738C54097BC7E491A6F86FB293C8585DDE5AF46, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, UserInfoInterfaceListEnumerator_get_Current_mE95C0E6329FA074B3BB193C170A0B46DD8A6A720_RuntimeMethod_var);
	}

IL_0019:
	{
		int32_t L_3 = __this->get_currentIndex_1();
		int32_t L_4 = __this->get_currentSize_3();
		V_1 = (bool)((((int32_t)L_3) > ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)1))))? 1 : 0);
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_0038;
		}
	}
	{
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_6 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m72027D5F1D513C25C05137E203EEED8FD8297706(L_6, _stringLiteralABB6C9CBB2EDF2CD0153266C346AC751C2957845, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, UserInfoInterfaceListEnumerator_get_Current_mE95C0E6329FA074B3BB193C170A0B46DD8A6A720_RuntimeMethod_var);
	}

IL_0038:
	{
		RuntimeObject * L_7 = __this->get_currentObject_2();
		V_2 = (bool)((((RuntimeObject*)(RuntimeObject *)L_7) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_8 = V_2;
		if (!L_8)
		{
			goto IL_0050;
		}
	}
	{
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_9 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m72027D5F1D513C25C05137E203EEED8FD8297706(L_9, _stringLiteralE53B9A10A9CA094221A6C1DB46D0C244AB081420, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9, UserInfoInterfaceListEnumerator_get_Current_mE95C0E6329FA074B3BB193C170A0B46DD8A6A720_RuntimeMethod_var);
	}

IL_0050:
	{
		RuntimeObject * L_10 = __this->get_currentObject_2();
		V_3 = ((UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C *)CastclassClass((RuntimeObject*)L_10, UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C_il2cpp_TypeInfo_var));
		goto IL_005e;
	}

IL_005e:
	{
		UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * L_11 = V_3;
		return L_11;
	}
}
// System.Object Firebase.Auth.UserInfoInterfaceList_UserInfoInterfaceListEnumerator::global::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * UserInfoInterfaceListEnumerator_globalU3AU3ASystem_Collections_IEnumerator_get_Current_mA488DE8E00C60FC4C370D97E7084C9609FF0368E (UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * L_0 = UserInfoInterfaceListEnumerator_get_Current_mE95C0E6329FA074B3BB193C170A0B46DD8A6A720(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// System.Boolean Firebase.Auth.UserInfoInterfaceList_UserInfoInterfaceListEnumerator::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UserInfoInterfaceListEnumerator_MoveNext_mB0E76F84C535182420B86E45C821E100E902F0A9 (UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	int32_t G_B3_0 = 0;
	{
		UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * L_0 = __this->get_collectionRef_0();
		NullCheck(L_0);
		int32_t L_1 = UserInfoInterfaceList_get_Count_mE191B3C8A1BDBE827045B98CAC48447631613074(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = __this->get_currentIndex_1();
		int32_t L_3 = V_0;
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)1))) >= ((int32_t)L_3)))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_4 = V_0;
		int32_t L_5 = __this->get_currentSize_3();
		G_B3_0 = ((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0);
		goto IL_0024;
	}

IL_0023:
	{
		G_B3_0 = 0;
	}

IL_0024:
	{
		V_1 = (bool)G_B3_0;
		bool L_6 = V_1;
		V_2 = L_6;
		bool L_7 = V_2;
		if (!L_7)
		{
			goto IL_0053;
		}
	}
	{
		int32_t L_8 = __this->get_currentIndex_1();
		__this->set_currentIndex_1(((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1)));
		UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * L_9 = __this->get_collectionRef_0();
		int32_t L_10 = __this->get_currentIndex_1();
		NullCheck(L_9);
		UserInfoInterface_t5AC6393084AD9C8B7C8ABA654F8372EDAB9F520C * L_11 = UserInfoInterfaceList_get_Item_m2CF8D552EFD6768C39D2579B76356659F2C570DD(L_9, L_10, /*hidden argument*/NULL);
		__this->set_currentObject_2(L_11);
		goto IL_005c;
	}

IL_0053:
	{
		__this->set_currentObject_2(NULL);
	}

IL_005c:
	{
		bool L_12 = V_1;
		V_3 = L_12;
		goto IL_0060;
	}

IL_0060:
	{
		bool L_13 = V_3;
		return L_13;
	}
}
// System.Void Firebase.Auth.UserInfoInterfaceList_UserInfoInterfaceListEnumerator::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterfaceListEnumerator_Reset_m89E71EC357585556E719F9C537D6A4954D0804E4 (UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserInfoInterfaceListEnumerator_Reset_m89E71EC357585556E719F9C537D6A4954D0804E4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		__this->set_currentIndex_1((-1));
		__this->set_currentObject_2(NULL);
		UserInfoInterfaceList_tB10A7FC89542F08CA09321A791F37BC0314FE422 * L_0 = __this->get_collectionRef_0();
		NullCheck(L_0);
		int32_t L_1 = UserInfoInterfaceList_get_Count_mE191B3C8A1BDBE827045B98CAC48447631613074(L_0, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_currentSize_3();
		V_0 = (bool)((((int32_t)((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_4 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m72027D5F1D513C25C05137E203EEED8FD8297706(L_4, _stringLiteralE53B9A10A9CA094221A6C1DB46D0C244AB081420, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, UserInfoInterfaceListEnumerator_Reset_m89E71EC357585556E719F9C537D6A4954D0804E4_RuntimeMethod_var);
	}

IL_0035:
	{
		return;
	}
}
// System.Void Firebase.Auth.UserInfoInterfaceList_UserInfoInterfaceListEnumerator::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserInfoInterfaceListEnumerator_Dispose_mA619C0FC99C494EE549F5868CBDCDE2E02F5CF03 (UserInfoInterfaceListEnumerator_t7621A4553DA658C3F7CFFB869D21499393FE800A * __this, const RuntimeMethod* method)
{
	{
		__this->set_currentIndex_1((-1));
		__this->set_currentObject_2(NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Firebase.Auth.UserProfile::.ctor(System.IntPtr,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserProfile__ctor_m3F41707B2949CA717EB71681388E002A3106801A (UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * __this, intptr_t ___cPtr0, bool ___cMemoryOwn1, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		bool L_0 = ___cMemoryOwn1;
		__this->set_swigCMemOwn_1(L_0);
		intptr_t L_1 = ___cPtr0;
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_2;
		memset((&L_2), 0, sizeof(L_2));
		HandleRef__ctor_mCD64E7F91766FE7602EE34A8CEEF6C1EF4C943BB((&L_2), __this, (intptr_t)L_1, /*hidden argument*/NULL);
		__this->set_swigCPtr_0(L_2);
		return;
	}
}
// System.Runtime.InteropServices.HandleRef Firebase.Auth.UserProfile::getCPtr(Firebase.Auth.UserProfile)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HandleRef_t876E76124F400D12395BF61D562162AB6822204A  UserProfile_getCPtr_m43D66A8F09ED56F3026B25C5DFEEEA8A3BEB7CEC (UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserProfile_getCPtr_m43D66A8F09ED56F3026B25C5DFEEEA8A3BEB7CEC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  V_0;
	memset((&V_0), 0, sizeof(V_0));
	HandleRef_t876E76124F400D12395BF61D562162AB6822204A  G_B3_0;
	memset((&G_B3_0), 0, sizeof(G_B3_0));
	{
		UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * L_0 = ___obj0;
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * L_1 = ___obj0;
		NullCheck(L_1);
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_2 = L_1->get_swigCPtr_0();
		G_B3_0 = L_2;
		goto IL_0017;
	}

IL_000c:
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_3;
		memset((&L_3), 0, sizeof(L_3));
		HandleRef__ctor_mCD64E7F91766FE7602EE34A8CEEF6C1EF4C943BB((&L_3), NULL, (intptr_t)(0), /*hidden argument*/NULL);
		G_B3_0 = L_3;
	}

IL_0017:
	{
		V_0 = G_B3_0;
		goto IL_001a;
	}

IL_001a:
	{
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_4 = V_0;
		return L_4;
	}
}
// System.Void Firebase.Auth.UserProfile::Finalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserProfile_Finalize_m47BE7F05B7BA57E259D3FECCCBBC7AEEE0351DB1 (UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		UserProfile_Dispose_m5BC283516965B4FC98A8AF63957F03D035DD94C3(__this, (bool)0, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x14, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m4015B7D3A44DE125C5FE34D7276CD4697C06F380(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x14, IL_0014)
	}

IL_0014:
	{
		return;
	}
}
// System.Void Firebase.Auth.UserProfile::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserProfile_Dispose_m8B47F62EA95EDA7AF6819F157ED0C3043944380C (UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserProfile_Dispose_m8B47F62EA95EDA7AF6819F157ED0C3043944380C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UserProfile_Dispose_m5BC283516965B4FC98A8AF63957F03D035DD94C3(__this, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GC_tC1D7BD74E8F44ECCEF5CD2B5D84BFF9AAE02D01D_il2cpp_TypeInfo_var);
		GC_SuppressFinalize_m037319A9B95A5BA437E806DE592802225EE5B425(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.UserProfile::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserProfile_Dispose_m5BC283516965B4FC98A8AF63957F03D035DD94C3 (UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * __this, bool ___disposing0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserProfile_Dispose_m5BC283516965B4FC98A8AF63957F03D035DD94C3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_il2cpp_TypeInfo_var);
		RuntimeObject * L_0 = ((FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_StaticFields*)il2cpp_codegen_static_fields_for(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_il2cpp_TypeInfo_var))->get_disposeLock_2();
		V_0 = L_0;
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_m903755FCC479745619842CCDBF5E6355319FA102(L_1, /*hidden argument*/NULL);
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			HandleRef_t876E76124F400D12395BF61D562162AB6822204A * L_2 = __this->get_address_of_swigCPtr_0();
			intptr_t L_3 = HandleRef_get_Handle_m095712F299EA36BF75975343C726FA160EE38277_inline((HandleRef_t876E76124F400D12395BF61D562162AB6822204A *)L_2, /*hidden argument*/NULL);
			bool L_4 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_3, (intptr_t)(0), /*hidden argument*/NULL);
			V_1 = L_4;
			bool L_5 = V_1;
			if (!L_5)
			{
				goto IL_005a;
			}
		}

IL_0028:
		{
			bool L_6 = __this->get_swigCMemOwn_1();
			V_2 = L_6;
			bool L_7 = V_2;
			if (!L_7)
			{
				goto IL_0048;
			}
		}

IL_0033:
		{
			__this->set_swigCMemOwn_1((bool)0);
			HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_8 = __this->get_swigCPtr_0();
			IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
			AuthUtilPINVOKE_delete_UserProfile_mEBF4B6D168A27F787149E7D71C5F6DA18AA6104E(L_8, /*hidden argument*/NULL);
		}

IL_0048:
		{
			HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_9;
			memset((&L_9), 0, sizeof(L_9));
			HandleRef__ctor_mCD64E7F91766FE7602EE34A8CEEF6C1EF4C943BB((&L_9), NULL, (intptr_t)(0), /*hidden argument*/NULL);
			__this->set_swigCPtr_0(L_9);
		}

IL_005a:
		{
			IL2CPP_RUNTIME_CLASS_INIT(GC_tC1D7BD74E8F44ECCEF5CD2B5D84BFF9AAE02D01D_il2cpp_TypeInfo_var);
			GC_SuppressFinalize_m037319A9B95A5BA437E806DE592802225EE5B425(__this, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x6C, FINALLY_0064);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0064;
	}

FINALLY_0064:
	{ // begin finally (depth: 1)
		RuntimeObject * L_10 = V_0;
		Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_10, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(100)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(100)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x6C, IL_006c)
	}

IL_006c:
	{
		return;
	}
}
// System.Void Firebase.Auth.UserProfile::set_PhotoUrl(System.Uri)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserProfile_set_PhotoUrl_mCDA8A14D63367925AD4D59808B472B88086D977A (UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * __this, Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserProfile_set_PhotoUrl_mCDA8A14D63367925AD4D59808B472B88086D977A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(FirebaseApp_t20651D18EC581E16B23A78CC38A27A2991409986_il2cpp_TypeInfo_var);
		String_t* L_1 = FirebaseApp_UriToUrlString_m2BB38FE415141D88AE4884175162ADA40561AE26(L_0, /*hidden argument*/NULL);
		UserProfile_set_PhotoUrlInternal_mED2EA1583FC42D01FE71BA105537EDF6E2019363(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Firebase.Auth.UserProfile::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserProfile__ctor_m4747BEB722B1243AD8AAA126F205FD8A08D8C809 (UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserProfile__ctor_m4747BEB722B1243AD8AAA126F205FD8A08D8C809_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		intptr_t L_0 = AuthUtilPINVOKE_new_UserProfile_mF862E0AB1127CCB19BEA8E520C8EC4E4AD85FF1C(/*hidden argument*/NULL);
		UserProfile__ctor_m3F41707B2949CA717EB71681388E002A3106801A(__this, (intptr_t)L_0, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_1 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_3 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, UserProfile__ctor_m4747BEB722B1243AD8AAA126F205FD8A08D8C809_RuntimeMethod_var);
	}

IL_001d:
	{
		return;
	}
}
// System.Void Firebase.Auth.UserProfile::set_DisplayName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserProfile_set_DisplayName_m0D8F5FA21420F08A93200DDA53AF9452882A6557 (UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserProfile_set_DisplayName_m0D8F5FA21420F08A93200DDA53AF9452882A6557_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = ___value0;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000b;
		}
	}
	{
		G_B2_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
	}

IL_000b:
	{
		___value0 = G_B2_0;
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_2 = __this->get_swigCPtr_0();
		String_t* L_3 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		AuthUtilPINVOKE_UserProfile_DisplayName_set_m2E3AA6A6AFEED65380B898B3BFD7FDD033CB5B59(L_2, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_4 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_0 = L_4;
		bool L_5 = V_0;
		if (!L_5)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_6 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, UserProfile_set_DisplayName_m0D8F5FA21420F08A93200DDA53AF9452882A6557_RuntimeMethod_var);
	}

IL_0029:
	{
		return;
	}
}
// System.Void Firebase.Auth.UserProfile::set_PhotoUrlInternal(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UserProfile_set_PhotoUrlInternal_mED2EA1583FC42D01FE71BA105537EDF6E2019363 (UserProfile_t68F1CAE261BF30FF6F0A90FB904FAF92B696AB31 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UserProfile_set_PhotoUrlInternal_mED2EA1583FC42D01FE71BA105537EDF6E2019363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	{
		String_t* L_0 = ___value0;
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000b;
		}
	}
	{
		G_B2_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
	}

IL_000b:
	{
		___value0 = G_B2_0;
		HandleRef_t876E76124F400D12395BF61D562162AB6822204A  L_2 = __this->get_swigCPtr_0();
		String_t* L_3 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(AuthUtilPINVOKE_t678489E027B3710A4C225DDA284EB2B3103FB00B_il2cpp_TypeInfo_var);
		AuthUtilPINVOKE_UserProfile_PhotoUrlInternal_set_mA5391563F2C002A4D053CF4BC28C2B7548E699B0(L_2, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		bool L_4 = SWIGPendingException_get_Pending_m368C58F7DD3C0618543462CD68806FE1EF98FCDC(/*hidden argument*/NULL);
		V_0 = L_4;
		bool L_5 = V_0;
		if (!L_5)
		{
			goto IL_0029;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SWIGPendingException_tBD01F554C16083D77763863FC641E4E5F3FDA6F2_il2cpp_TypeInfo_var);
		Exception_t * L_6 = SWIGPendingException_Retrieve_m56CE138BBD1EB5BCCF31BD13DFB22AB78FBA8DB1(/*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, UserProfile_set_PhotoUrlInternal_mED2EA1583FC42D01FE71BA105537EDF6E2019363_RuntimeMethod_var);
	}

IL_0029:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR intptr_t HandleRef_get_Handle_m095712F299EA36BF75975343C726FA160EE38277_inline (HandleRef_t876E76124F400D12395BF61D562162AB6822204A * __this, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = __this->get_m_handle_1();
		return (intptr_t)L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09 * TaskCompletionSource_1_get_Task_mF1CB937DAB68DC43DA33893E95F48EFB07F385A0_gshared_inline (TaskCompletionSource_1_t3ECF7B56E418D2A3FFC4E551B9DA6A7E38C7D304 * __this, const RuntimeMethod* method)
{
	{
		Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09 * L_0 = (Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09 *)__this->get_m_task_0();
		return (Task_1_tA56001ED5270173CA1432EDFCD84EABB1024BC09 *)L_0;
	}
}
