﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"



extern const RuntimeMethod* PInvoke_NativeMessageHandlerCallback_m5D5E84BB3C9785BE4384D272156E122275A93DE4_RuntimeMethod_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void AppleAuth.AppleAuthLoginArgs::.ctor(AppleAuth.Enums.LoginOptions,System.String,System.String)
extern void AppleAuthLoginArgs__ctor_m7E3D2AA118703B59AEDF336421336EE1B6545D54_AdjustorThunk (void);
// 0x00000002 System.Void AppleAuth.AppleAuthManager::.cctor()
extern void AppleAuthManager__cctor_m56F15734DA0AA48CE41060C15FE448DCC1730FF8 (void);
// 0x00000003 System.Boolean AppleAuth.AppleAuthManager::get_IsCurrentPlatformSupported()
extern void AppleAuthManager_get_IsCurrentPlatformSupported_m4B3FB905E225F53F9FB03208A10FA86EB8C6CBB4 (void);
// 0x00000004 System.Void AppleAuth.AppleAuthManager::.ctor(AppleAuth.Interfaces.IPayloadDeserializer)
extern void AppleAuthManager__ctor_m4597D08EC5F35812DB3BAF881BF569B77C15EC87 (void);
// 0x00000005 System.Void AppleAuth.AppleAuthManager::QuickLogin(System.Action`1<AppleAuth.Interfaces.ICredential>,System.Action`1<AppleAuth.Interfaces.IAppleError>)
extern void AppleAuthManager_QuickLogin_m9CD8DC1EBE373F3669F4A837783FAC62A0B26740 (void);
// 0x00000006 System.Void AppleAuth.AppleAuthManager::QuickLogin(AppleAuth.AppleAuthQuickLoginArgs,System.Action`1<AppleAuth.Interfaces.ICredential>,System.Action`1<AppleAuth.Interfaces.IAppleError>)
extern void AppleAuthManager_QuickLogin_m5BE7B5C2C7728FDA2B0D5EB11862FD07762A4AC6 (void);
// 0x00000007 System.Void AppleAuth.AppleAuthManager::LoginWithAppleId(AppleAuth.Enums.LoginOptions,System.Action`1<AppleAuth.Interfaces.ICredential>,System.Action`1<AppleAuth.Interfaces.IAppleError>)
extern void AppleAuthManager_LoginWithAppleId_m8D2D704CC0ACCE5C96983E180B637E431B8ACBA9 (void);
// 0x00000008 System.Void AppleAuth.AppleAuthManager::LoginWithAppleId(AppleAuth.AppleAuthLoginArgs,System.Action`1<AppleAuth.Interfaces.ICredential>,System.Action`1<AppleAuth.Interfaces.IAppleError>)
extern void AppleAuthManager_LoginWithAppleId_m3251131E6071E0C1D505B6CAA2DE945C8C5EA099 (void);
// 0x00000009 System.Void AppleAuth.AppleAuthManager::GetCredentialState(System.String,System.Action`1<AppleAuth.Enums.CredentialState>,System.Action`1<AppleAuth.Interfaces.IAppleError>)
extern void AppleAuthManager_GetCredentialState_m2076CC7A70D84B5D140FFC0667EBD95734BC15D9 (void);
// 0x0000000A System.Void AppleAuth.AppleAuthManager::SetCredentialsRevokedCallback(System.Action`1<System.String>)
extern void AppleAuthManager_SetCredentialsRevokedCallback_m601A44468303E8F8AE6DB176D7052B2B7A1A3710 (void);
// 0x0000000B System.Void AppleAuth.AppleAuthManager::Update()
extern void AppleAuthManager_Update_mF049B837ADF2966D780DDDFAC97135FFE05F9B54 (void);
// 0x0000000C System.Void AppleAuth.AppleAuthQuickLoginArgs::.ctor(System.String,System.String)
extern void AppleAuthQuickLoginArgs__ctor_m002ADB6747DBEF52120701B85E6E69C392680819_AdjustorThunk (void);
// 0x0000000D System.Void AppleAuth.IAppleAuthManager::QuickLogin(System.Action`1<AppleAuth.Interfaces.ICredential>,System.Action`1<AppleAuth.Interfaces.IAppleError>)
// 0x0000000E System.Void AppleAuth.IAppleAuthManager::QuickLogin(AppleAuth.AppleAuthQuickLoginArgs,System.Action`1<AppleAuth.Interfaces.ICredential>,System.Action`1<AppleAuth.Interfaces.IAppleError>)
// 0x0000000F System.Void AppleAuth.IAppleAuthManager::LoginWithAppleId(AppleAuth.Enums.LoginOptions,System.Action`1<AppleAuth.Interfaces.ICredential>,System.Action`1<AppleAuth.Interfaces.IAppleError>)
// 0x00000010 System.Void AppleAuth.IAppleAuthManager::LoginWithAppleId(AppleAuth.AppleAuthLoginArgs,System.Action`1<AppleAuth.Interfaces.ICredential>,System.Action`1<AppleAuth.Interfaces.IAppleError>)
// 0x00000011 System.Void AppleAuth.IAppleAuthManager::GetCredentialState(System.String,System.Action`1<AppleAuth.Enums.CredentialState>,System.Action`1<AppleAuth.Interfaces.IAppleError>)
// 0x00000012 System.Void AppleAuth.IAppleAuthManager::SetCredentialsRevokedCallback(System.Action`1<System.String>)
// 0x00000013 System.Void AppleAuth.IAppleAuthManager::Update()
// 0x00000014 System.Int32 AppleAuth.Native.AppleError::get_Code()
extern void AppleError_get_Code_m2DC6FA24A78F376AC7759F74CB97456F98C3E3D5 (void);
// 0x00000015 System.String AppleAuth.Native.AppleError::get_Domain()
extern void AppleError_get_Domain_m5570BBA51B6AC6502731859394EE956DEAC8AFD5 (void);
// 0x00000016 System.String AppleAuth.Native.AppleError::get_LocalizedDescription()
extern void AppleError_get_LocalizedDescription_m0528258B3E61E724A22BAA0CF7446F3FD0848A1E (void);
// 0x00000017 System.String[] AppleAuth.Native.AppleError::get_LocalizedRecoveryOptions()
extern void AppleError_get_LocalizedRecoveryOptions_mAA19861613F1EE020B040A278D75F55AB5B03F97 (void);
// 0x00000018 System.String AppleAuth.Native.AppleError::get_LocalizedRecoverySuggestion()
extern void AppleError_get_LocalizedRecoverySuggestion_m103E894877132C6986B995CE1094E5F665A0D924 (void);
// 0x00000019 System.String AppleAuth.Native.AppleError::get_LocalizedFailureReason()
extern void AppleError_get_LocalizedFailureReason_m452D315F855E29C98CCCC5FAD4AD76439894B2FB (void);
// 0x0000001A System.Void AppleAuth.Native.AppleError::OnBeforeSerialize()
extern void AppleError_OnBeforeSerialize_m672A853DB847E0037955E714AC8E7E23010F2E38 (void);
// 0x0000001B System.Void AppleAuth.Native.AppleError::OnAfterDeserialize()
extern void AppleError_OnAfterDeserialize_m6F38B1635730B512980DD3C80D1BEA4AB8D80AD9 (void);
// 0x0000001C System.Void AppleAuth.Native.AppleError::.ctor()
extern void AppleError__ctor_mCAB1FA6B14D31603B7A898FB89E6B8A063A16F21 (void);
// 0x0000001D System.Byte[] AppleAuth.Native.AppleIDCredential::get_IdentityToken()
extern void AppleIDCredential_get_IdentityToken_mD9A23ADD2B974E3174AE1157B8C52AAAF8472A29 (void);
// 0x0000001E System.Byte[] AppleAuth.Native.AppleIDCredential::get_AuthorizationCode()
extern void AppleIDCredential_get_AuthorizationCode_mE91B95D24E462A4FFAD1679F8F8F8839E0DE838C (void);
// 0x0000001F System.String AppleAuth.Native.AppleIDCredential::get_State()
extern void AppleIDCredential_get_State_mA59F6A34B119CF63FC5BAD01E953DF87C7DAD71E (void);
// 0x00000020 System.String AppleAuth.Native.AppleIDCredential::get_User()
extern void AppleIDCredential_get_User_mCB948350B33024427D5274E190387D8D716BE2FC (void);
// 0x00000021 System.String[] AppleAuth.Native.AppleIDCredential::get_AuthorizedScopes()
extern void AppleIDCredential_get_AuthorizedScopes_mC3D225AC26C5E53B88CC33BE80D7C9C67B97C465 (void);
// 0x00000022 AppleAuth.Interfaces.IPersonName AppleAuth.Native.AppleIDCredential::get_FullName()
extern void AppleIDCredential_get_FullName_m0241AF6E01D81B58698B766FAE28FAF2D63BE612 (void);
// 0x00000023 System.String AppleAuth.Native.AppleIDCredential::get_Email()
extern void AppleIDCredential_get_Email_m1DC5268DB27B573E5A5A0136AD66DB729AD105E6 (void);
// 0x00000024 AppleAuth.Enums.RealUserStatus AppleAuth.Native.AppleIDCredential::get_RealUserStatus()
extern void AppleIDCredential_get_RealUserStatus_m33277B12D4375B895255B7066D941C178147CB05 (void);
// 0x00000025 System.Void AppleAuth.Native.AppleIDCredential::OnBeforeSerialize()
extern void AppleIDCredential_OnBeforeSerialize_mA40DB44EC7DE2EC21B7CDD46F4B10B0A4615B11E (void);
// 0x00000026 System.Void AppleAuth.Native.AppleIDCredential::OnAfterDeserialize()
extern void AppleIDCredential_OnAfterDeserialize_m21F884F713F24E7D7B9ACA8880E4784E9F082DE1 (void);
// 0x00000027 System.Void AppleAuth.Native.AppleIDCredential::.ctor()
extern void AppleIDCredential__ctor_m233E172484F302A79A023E6112756E25D2E237E7 (void);
// 0x00000028 System.Boolean AppleAuth.Native.CredentialStateResponse::get_Success()
extern void CredentialStateResponse_get_Success_m82535AF6717492354D8205926985762AA154C61D (void);
// 0x00000029 AppleAuth.Enums.CredentialState AppleAuth.Native.CredentialStateResponse::get_CredentialState()
extern void CredentialStateResponse_get_CredentialState_m928023D0F25060F95DFECA3F167F7B129D1F9BFE (void);
// 0x0000002A AppleAuth.Interfaces.IAppleError AppleAuth.Native.CredentialStateResponse::get_Error()
extern void CredentialStateResponse_get_Error_m0A2A022847F24D12928ED533DB47FAE49D91E462 (void);
// 0x0000002B System.Void AppleAuth.Native.CredentialStateResponse::OnBeforeSerialize()
extern void CredentialStateResponse_OnBeforeSerialize_mCF51ED02727B0BC8154D07F96D3DB3EA05C4B02B (void);
// 0x0000002C System.Void AppleAuth.Native.CredentialStateResponse::OnAfterDeserialize()
extern void CredentialStateResponse_OnAfterDeserialize_m517F35790E5F8574E84FA626C835ED67B66D3234 (void);
// 0x0000002D System.Void AppleAuth.Native.CredentialStateResponse::.ctor()
extern void CredentialStateResponse__ctor_m14F70BAA0EDD1A6971E8D7E7F45B5F82D38DEA75 (void);
// 0x0000002E AppleAuth.Interfaces.IPersonName AppleAuth.Native.FullPersonName::get_PhoneticRepresentation()
extern void FullPersonName_get_PhoneticRepresentation_m12EAC6FB3B6497A6DB661B72405D4C7BAA7FDCC6 (void);
// 0x0000002F System.Void AppleAuth.Native.FullPersonName::OnAfterDeserialize()
extern void FullPersonName_OnAfterDeserialize_mEA9D5C99803AB1C5CCD7353848CE73E1CD7588D7 (void);
// 0x00000030 System.Void AppleAuth.Native.FullPersonName::.ctor()
extern void FullPersonName__ctor_m6B6C1CCBB5E050F80C876443FB3B6B7EDF81A519 (void);
// 0x00000031 System.Boolean AppleAuth.Native.LoginWithAppleIdResponse::get_Success()
extern void LoginWithAppleIdResponse_get_Success_mD4595AEB2EB43101FB255838FF10DE5DA288AA8A (void);
// 0x00000032 AppleAuth.Interfaces.IAppleError AppleAuth.Native.LoginWithAppleIdResponse::get_Error()
extern void LoginWithAppleIdResponse_get_Error_mA5F4E932F0B21B4D6089465CDB617439F5273BE7 (void);
// 0x00000033 AppleAuth.Interfaces.IAppleIDCredential AppleAuth.Native.LoginWithAppleIdResponse::get_AppleIDCredential()
extern void LoginWithAppleIdResponse_get_AppleIDCredential_mE60828381C62290D6318784BEA6CF5D36900369E (void);
// 0x00000034 AppleAuth.Interfaces.IPasswordCredential AppleAuth.Native.LoginWithAppleIdResponse::get_PasswordCredential()
extern void LoginWithAppleIdResponse_get_PasswordCredential_m9DEAEB1B7F3015EC979C4B3150EAC04A9FD6787D (void);
// 0x00000035 System.Void AppleAuth.Native.LoginWithAppleIdResponse::OnBeforeSerialize()
extern void LoginWithAppleIdResponse_OnBeforeSerialize_m1FF29265303F70312D5CF9B97B3F8A7F0C01A7F0 (void);
// 0x00000036 System.Void AppleAuth.Native.LoginWithAppleIdResponse::OnAfterDeserialize()
extern void LoginWithAppleIdResponse_OnAfterDeserialize_m95F017BF70EDB75F9113FDBBE7D991BD415F6AA0 (void);
// 0x00000037 System.Void AppleAuth.Native.LoginWithAppleIdResponse::.ctor()
extern void LoginWithAppleIdResponse__ctor_mCA177F81AD1DC44AF186230CDC8BCDB1BD489540 (void);
// 0x00000038 System.String AppleAuth.Native.PasswordCredential::get_User()
extern void PasswordCredential_get_User_m9053330B4BCC8C8806CDCDB0AD6697C3819C166F (void);
// 0x00000039 System.String AppleAuth.Native.PasswordCredential::get_Password()
extern void PasswordCredential_get_Password_m34CB954A5DFC8322D383FF93585073CB6319342C (void);
// 0x0000003A System.Void AppleAuth.Native.PasswordCredential::OnBeforeSerialize()
extern void PasswordCredential_OnBeforeSerialize_m43919643ADFDED9EE5870C2F34D22D4962167EEA (void);
// 0x0000003B System.Void AppleAuth.Native.PasswordCredential::OnAfterDeserialize()
extern void PasswordCredential_OnAfterDeserialize_mA3F5E0C7A4AC250BB17845D83100B9A5AC02E784 (void);
// 0x0000003C System.Void AppleAuth.Native.PasswordCredential::.ctor()
extern void PasswordCredential__ctor_m4CEA3FF72C5D44D5ADA0F05C9822D1C164734C78 (void);
// 0x0000003D AppleAuth.Interfaces.ICredentialStateResponse AppleAuth.Native.PayloadDeserializer::DeserializeCredentialStateResponse(System.String)
extern void PayloadDeserializer_DeserializeCredentialStateResponse_mB1B47BE54866E19949520EE2A62A56A2E59AE563 (void);
// 0x0000003E AppleAuth.Interfaces.ILoginWithAppleIdResponse AppleAuth.Native.PayloadDeserializer::DeserializeLoginWithAppleIdResponse(System.String)
extern void PayloadDeserializer_DeserializeLoginWithAppleIdResponse_m7C6E5F892CD5579916521D37323B5E6943E5F9F0 (void);
// 0x0000003F System.Void AppleAuth.Native.PayloadDeserializer::.ctor()
extern void PayloadDeserializer__ctor_m3DD225AE33F56326A8DF099FAEDDD75F65469D31 (void);
// 0x00000040 System.String AppleAuth.Native.PersonName::get_NamePrefix()
extern void PersonName_get_NamePrefix_m53B3E50F11C940E5A3D239FC7AE7B3AFF9C7608C (void);
// 0x00000041 System.String AppleAuth.Native.PersonName::get_GivenName()
extern void PersonName_get_GivenName_mE6FBDCBA9F1E094F9AF211E494817201B4DF91B6 (void);
// 0x00000042 System.String AppleAuth.Native.PersonName::get_MiddleName()
extern void PersonName_get_MiddleName_mCA9994B19EF2B5794EED00F890EB8AFC973A74A1 (void);
// 0x00000043 System.String AppleAuth.Native.PersonName::get_FamilyName()
extern void PersonName_get_FamilyName_mDE8C71F0B60B6534105B8377857565A5ABD50B27 (void);
// 0x00000044 System.String AppleAuth.Native.PersonName::get_NameSuffix()
extern void PersonName_get_NameSuffix_m01EAB5B6F3D4A3DE331CF2EDC543C1A796D5C0D7 (void);
// 0x00000045 System.String AppleAuth.Native.PersonName::get_Nickname()
extern void PersonName_get_Nickname_mC7A33C610502A12950954A11F59D54E41D48FA30 (void);
// 0x00000046 AppleAuth.Interfaces.IPersonName AppleAuth.Native.PersonName::get_PhoneticRepresentation()
extern void PersonName_get_PhoneticRepresentation_m4E7221EC4D7934C7D4816159993943B47818AE6C (void);
// 0x00000047 System.Void AppleAuth.Native.PersonName::OnBeforeSerialize()
extern void PersonName_OnBeforeSerialize_m841722B7A1DCA5782A61621F0A62B16668BB5A20 (void);
// 0x00000048 System.Void AppleAuth.Native.PersonName::OnAfterDeserialize()
extern void PersonName_OnAfterDeserialize_m32D4307910B89D3E33FBBE420CBA93641F8F0ACD (void);
// 0x00000049 System.Void AppleAuth.Native.PersonName::.ctor()
extern void PersonName__ctor_m320D2DECA0176EE689309F89168BC54311BDD69C (void);
// 0x0000004A System.Void AppleAuth.Native.SerializationTools::FixSerializationForString(System.String&)
extern void SerializationTools_FixSerializationForString_m8171A047B8FCDC62B846791D70C2DA21418D8650 (void);
// 0x0000004B System.Void AppleAuth.Native.SerializationTools::FixSerializationForArray(T[]&)
// 0x0000004C System.Void AppleAuth.Native.SerializationTools::FixSerializationForObject(T&,System.Boolean)
// 0x0000004D System.Byte[] AppleAuth.Native.SerializationTools::GetBytesFromBase64String(System.String,System.String)
extern void SerializationTools_GetBytesFromBase64String_m861B8FE0EC904AC192A9D2DD8D5CC6B51A7F2CAD (void);
// 0x0000004E System.Int32 AppleAuth.Interfaces.IAppleError::get_Code()
// 0x0000004F System.String AppleAuth.Interfaces.IAppleError::get_Domain()
// 0x00000050 System.String AppleAuth.Interfaces.IAppleError::get_LocalizedDescription()
// 0x00000051 System.String[] AppleAuth.Interfaces.IAppleError::get_LocalizedRecoveryOptions()
// 0x00000052 System.String AppleAuth.Interfaces.IAppleError::get_LocalizedRecoverySuggestion()
// 0x00000053 System.String AppleAuth.Interfaces.IAppleError::get_LocalizedFailureReason()
// 0x00000054 System.Byte[] AppleAuth.Interfaces.IAppleIDCredential::get_IdentityToken()
// 0x00000055 System.Byte[] AppleAuth.Interfaces.IAppleIDCredential::get_AuthorizationCode()
// 0x00000056 System.String AppleAuth.Interfaces.IAppleIDCredential::get_State()
// 0x00000057 System.String[] AppleAuth.Interfaces.IAppleIDCredential::get_AuthorizedScopes()
// 0x00000058 AppleAuth.Interfaces.IPersonName AppleAuth.Interfaces.IAppleIDCredential::get_FullName()
// 0x00000059 System.String AppleAuth.Interfaces.IAppleIDCredential::get_Email()
// 0x0000005A AppleAuth.Enums.RealUserStatus AppleAuth.Interfaces.IAppleIDCredential::get_RealUserStatus()
// 0x0000005B System.String AppleAuth.Interfaces.ICredential::get_User()
// 0x0000005C System.Boolean AppleAuth.Interfaces.ICredentialStateResponse::get_Success()
// 0x0000005D AppleAuth.Enums.CredentialState AppleAuth.Interfaces.ICredentialStateResponse::get_CredentialState()
// 0x0000005E AppleAuth.Interfaces.IAppleError AppleAuth.Interfaces.ICredentialStateResponse::get_Error()
// 0x0000005F System.Boolean AppleAuth.Interfaces.ILoginWithAppleIdResponse::get_Success()
// 0x00000060 AppleAuth.Interfaces.IAppleError AppleAuth.Interfaces.ILoginWithAppleIdResponse::get_Error()
// 0x00000061 AppleAuth.Interfaces.IAppleIDCredential AppleAuth.Interfaces.ILoginWithAppleIdResponse::get_AppleIDCredential()
// 0x00000062 AppleAuth.Interfaces.IPasswordCredential AppleAuth.Interfaces.ILoginWithAppleIdResponse::get_PasswordCredential()
// 0x00000063 System.String AppleAuth.Interfaces.IPasswordCredential::get_Password()
// 0x00000064 AppleAuth.Interfaces.ICredentialStateResponse AppleAuth.Interfaces.IPayloadDeserializer::DeserializeCredentialStateResponse(System.String)
// 0x00000065 AppleAuth.Interfaces.ILoginWithAppleIdResponse AppleAuth.Interfaces.IPayloadDeserializer::DeserializeLoginWithAppleIdResponse(System.String)
// 0x00000066 System.String AppleAuth.Interfaces.IPersonName::get_NamePrefix()
// 0x00000067 System.String AppleAuth.Interfaces.IPersonName::get_GivenName()
// 0x00000068 System.String AppleAuth.Interfaces.IPersonName::get_MiddleName()
// 0x00000069 System.String AppleAuth.Interfaces.IPersonName::get_FamilyName()
// 0x0000006A System.String AppleAuth.Interfaces.IPersonName::get_NameSuffix()
// 0x0000006B System.String AppleAuth.Interfaces.IPersonName::get_Nickname()
// 0x0000006C AppleAuth.Interfaces.IPersonName AppleAuth.Interfaces.IPersonName::get_PhoneticRepresentation()
// 0x0000006D AppleAuth.Enums.AuthorizationErrorCode AppleAuth.Extensions.AppleErrorExtensions::GetAuthorizationErrorCode(AppleAuth.Interfaces.IAppleError)
extern void AppleErrorExtensions_GetAuthorizationErrorCode_m657BF77C7E6EE94F9643A7720C0F4B749652424B (void);
// 0x0000006E System.String AppleAuth.Extensions.PersonNameExtensions::ToLocalizedString(AppleAuth.Interfaces.IPersonName,AppleAuth.Enums.PersonNameFormatterStyle,System.Boolean)
extern void PersonNameExtensions_ToLocalizedString_mDE02E6128882D1255EF7753E1ADBDD7CD6A2E06A (void);
// 0x0000006F System.String AppleAuth.Extensions.PersonNameExtensions::JsonStringForPersonName(AppleAuth.Interfaces.IPersonName)
extern void PersonNameExtensions_JsonStringForPersonName_m60F3E8CB4AEEFF4A5741EAB787BB034F9F71672B (void);
// 0x00000070 System.Void AppleAuth.Extensions.PersonNameExtensions::TryAddKeyValue(System.String,System.String,System.String,System.Text.StringBuilder)
extern void PersonNameExtensions_TryAddKeyValue_m3BBD8554A10DC6B6D8BD30500D245EFEF1854C40 (void);
// 0x00000071 System.Void AppleAuth.AppleAuthManager_CallbackHandler::add__nativeCredentialsRevoked(System.Action`1<System.String>)
extern void CallbackHandler_add__nativeCredentialsRevoked_m76EF11A8538E68C4EF86007907748718B46C0A2D (void);
// 0x00000072 System.Void AppleAuth.AppleAuthManager_CallbackHandler::remove__nativeCredentialsRevoked(System.Action`1<System.String>)
extern void CallbackHandler_remove__nativeCredentialsRevoked_mD4A0DE080A0F2633EBCF99808C748D76F21F622A (void);
// 0x00000073 System.Void AppleAuth.AppleAuthManager_CallbackHandler::add_NativeCredentialsRevoked(System.Action`1<System.String>)
extern void CallbackHandler_add_NativeCredentialsRevoked_mAFC47A0B52BC9DCACB3C8B36748CB8FB54CEC208 (void);
// 0x00000074 System.Void AppleAuth.AppleAuthManager_CallbackHandler::remove_NativeCredentialsRevoked(System.Action`1<System.String>)
extern void CallbackHandler_remove_NativeCredentialsRevoked_m8E5E2588E004C4838F26030762E98561A8FBBAE3 (void);
// 0x00000075 System.Void AppleAuth.AppleAuthManager_CallbackHandler::ScheduleCallback(System.UInt32,System.String)
extern void CallbackHandler_ScheduleCallback_m085D44C88C7A14F9F29E1170A8FCA2BE13B857F5 (void);
// 0x00000076 System.Void AppleAuth.AppleAuthManager_CallbackHandler::ExecutePendingCallbacks()
extern void CallbackHandler_ExecutePendingCallbacks_m5AD24B967D8C51AECFE59C5BF02342F57952AC3C (void);
// 0x00000077 System.UInt32 AppleAuth.AppleAuthManager_CallbackHandler::AddMessageCallback(System.Boolean,System.Action`1<System.String>)
extern void CallbackHandler_AddMessageCallback_m0B95A78A2EB0A42E2D6EE2186A042C3DC406C65D (void);
// 0x00000078 System.Void AppleAuth.AppleAuthManager_CallbackHandler::RemoveMessageCallback(System.UInt32)
extern void CallbackHandler_RemoveMessageCallback_m54E5CE8453DA1146E7E132B646E96FB964C72C7B (void);
// 0x00000079 System.Void AppleAuth.AppleAuthManager_CallbackHandler::.cctor()
extern void CallbackHandler__cctor_m232A322ACC97DEA243EC9A2BF85855C293E13238 (void);
// 0x0000007A System.Void AppleAuth.AppleAuthManager_PInvoke::NativeMessageHandlerCallback(System.UInt32,System.String)
extern void PInvoke_NativeMessageHandlerCallback_m5D5E84BB3C9785BE4384D272156E122275A93DE4 (void);
// 0x0000007B System.Boolean AppleAuth.AppleAuthManager_PInvoke::AppleAuth_IsCurrentPlatformSupported()
extern void PInvoke_AppleAuth_IsCurrentPlatformSupported_mF4AB0EB44BE680301E902157841A2E8BE29B9998 (void);
// 0x0000007C System.Void AppleAuth.AppleAuthManager_PInvoke::AppleAuth_SetupNativeMessageHandlerCallback(AppleAuth.AppleAuthManager_PInvoke_NativeMessageHandlerCallbackDelegate)
extern void PInvoke_AppleAuth_SetupNativeMessageHandlerCallback_m2D140AC7EC65E41051360B692A205C2B1345CA90 (void);
// 0x0000007D System.Void AppleAuth.AppleAuthManager_PInvoke::AppleAuth_GetCredentialState(System.UInt32,System.String)
extern void PInvoke_AppleAuth_GetCredentialState_mB2184459930094EC4BF2948375B4D18B069A2D28 (void);
// 0x0000007E System.Void AppleAuth.AppleAuthManager_PInvoke::AppleAuth_LoginWithAppleId(System.UInt32,System.Int32,System.String,System.String)
extern void PInvoke_AppleAuth_LoginWithAppleId_m955A59F158F91E47BC0A0950F24B1564DF623E9E (void);
// 0x0000007F System.Void AppleAuth.AppleAuthManager_PInvoke::AppleAuth_QuickLogin(System.UInt32,System.String,System.String)
extern void PInvoke_AppleAuth_QuickLogin_m2BDD63FE86A5B848DD047E60AFBD031AB00060A4 (void);
// 0x00000080 System.Void AppleAuth.AppleAuthManager_PInvoke::AppleAuth_RegisterCredentialsRevokedCallbackId(System.UInt32)
extern void PInvoke_AppleAuth_RegisterCredentialsRevokedCallbackId_m65B9651E2B15431DD6289C68DD8787F9EDAF8EF8 (void);
// 0x00000081 System.Void AppleAuth.AppleAuthManager_PInvoke::AppleAuth_LogMessage(System.String)
extern void PInvoke_AppleAuth_LogMessage_m9620530B053E06324B4C16569F6605A7BACACEC9 (void);
// 0x00000082 System.Void AppleAuth.AppleAuthManager_<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_mFE19321C75D3BBE3F40E7B55C1F2D6D2194B34B1 (void);
// 0x00000083 System.Void AppleAuth.AppleAuthManager_<>c__DisplayClass7_0::<QuickLogin>b__0(System.String)
extern void U3CU3Ec__DisplayClass7_0_U3CQuickLoginU3Eb__0_m6B15B778A870666871156FA6266C6B86B6EDEF69 (void);
// 0x00000084 System.Void AppleAuth.AppleAuthManager_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_mECE874027375E819AF084618F941EC8554006642 (void);
// 0x00000085 System.Void AppleAuth.AppleAuthManager_<>c__DisplayClass9_0::<LoginWithAppleId>b__0(System.String)
extern void U3CU3Ec__DisplayClass9_0_U3CLoginWithAppleIdU3Eb__0_mA214B2AB4BFCF620062979C4C7935B3BF70815DB (void);
// 0x00000086 System.Void AppleAuth.AppleAuthManager_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m1A51C03108292B70E58D92C883CFA153B037FF47 (void);
// 0x00000087 System.Void AppleAuth.AppleAuthManager_<>c__DisplayClass10_0::<GetCredentialState>b__0(System.String)
extern void U3CU3Ec__DisplayClass10_0_U3CGetCredentialStateU3Eb__0_m99A6F544599A6034B2E04CC297223F607D6859F1 (void);
// 0x00000088 System.String AppleAuth.Extensions.PersonNameExtensions_PInvoke::AppleAuth_GetPersonNameUsingFormatter(System.String,System.Int32,System.Boolean)
extern void PInvoke_AppleAuth_GetPersonNameUsingFormatter_m579639B679D05DD5E952F09B075C0689E8879762 (void);
// 0x00000089 System.Void AppleAuth.AppleAuthManager_CallbackHandler_Entry::.ctor(System.Boolean,System.Action`1<System.String>)
extern void Entry__ctor_m211294F29613E18A310B5A2CBF5DB141F0C5164F (void);
// 0x0000008A System.Void AppleAuth.AppleAuthManager_CallbackHandler_<>c::.cctor()
extern void U3CU3Ec__cctor_m68C6CA84594FBF39B20762F63FD6E4C01368FB86 (void);
// 0x0000008B System.Void AppleAuth.AppleAuthManager_CallbackHandler_<>c::.ctor()
extern void U3CU3Ec__ctor_mBD4C3476C866706F3C68BD2C7B862E65E3CFB7D6 (void);
// 0x0000008C System.Void AppleAuth.AppleAuthManager_CallbackHandler_<>c::<add_NativeCredentialsRevoked>b__12_0(System.String)
extern void U3CU3Ec_U3Cadd_NativeCredentialsRevokedU3Eb__12_0_m5724726AA513DABA2EA1B4A0806D72D71DEE9D41 (void);
// 0x0000008D System.Void AppleAuth.AppleAuthManager_CallbackHandler_<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_m955A36C7D7F760FD5656A05D0BF5CB140DF78210 (void);
// 0x0000008E System.Void AppleAuth.AppleAuthManager_CallbackHandler_<>c__DisplayClass14_1::.ctor()
extern void U3CU3Ec__DisplayClass14_1__ctor_mAEFE6646216AE52125F79843CDF4695B36111113 (void);
// 0x0000008F System.Void AppleAuth.AppleAuthManager_CallbackHandler_<>c__DisplayClass14_1::<ScheduleCallback>b__0()
extern void U3CU3Ec__DisplayClass14_1_U3CScheduleCallbackU3Eb__0_m5C8FE466DDC61501D84CF97573FEA1C8B7689192 (void);
// 0x00000090 System.Void AppleAuth.AppleAuthManager_PInvoke_NativeMessageHandlerCallbackDelegate::.ctor(System.Object,System.IntPtr)
extern void NativeMessageHandlerCallbackDelegate__ctor_mA999B2608865CF7F48403396ED87EEDDDB0BD602 (void);
// 0x00000091 System.Void AppleAuth.AppleAuthManager_PInvoke_NativeMessageHandlerCallbackDelegate::Invoke(System.UInt32,System.String)
extern void NativeMessageHandlerCallbackDelegate_Invoke_mDCD2998A4CBAD6F8C9FEF4F3E3BB3BBA23178F69 (void);
// 0x00000092 System.IAsyncResult AppleAuth.AppleAuthManager_PInvoke_NativeMessageHandlerCallbackDelegate::BeginInvoke(System.UInt32,System.String,System.AsyncCallback,System.Object)
extern void NativeMessageHandlerCallbackDelegate_BeginInvoke_mC28322C6AC144EA2EBE5EF1C26C448B4449D7F3C (void);
// 0x00000093 System.Void AppleAuth.AppleAuthManager_PInvoke_NativeMessageHandlerCallbackDelegate::EndInvoke(System.IAsyncResult)
extern void NativeMessageHandlerCallbackDelegate_EndInvoke_m55CAB28C03D0D9E03FEAB0A759F8260A8A828CC5 (void);
static Il2CppMethodPointer s_methodPointers[147] = 
{
	AppleAuthLoginArgs__ctor_m7E3D2AA118703B59AEDF336421336EE1B6545D54_AdjustorThunk,
	AppleAuthManager__cctor_m56F15734DA0AA48CE41060C15FE448DCC1730FF8,
	AppleAuthManager_get_IsCurrentPlatformSupported_m4B3FB905E225F53F9FB03208A10FA86EB8C6CBB4,
	AppleAuthManager__ctor_m4597D08EC5F35812DB3BAF881BF569B77C15EC87,
	AppleAuthManager_QuickLogin_m9CD8DC1EBE373F3669F4A837783FAC62A0B26740,
	AppleAuthManager_QuickLogin_m5BE7B5C2C7728FDA2B0D5EB11862FD07762A4AC6,
	AppleAuthManager_LoginWithAppleId_m8D2D704CC0ACCE5C96983E180B637E431B8ACBA9,
	AppleAuthManager_LoginWithAppleId_m3251131E6071E0C1D505B6CAA2DE945C8C5EA099,
	AppleAuthManager_GetCredentialState_m2076CC7A70D84B5D140FFC0667EBD95734BC15D9,
	AppleAuthManager_SetCredentialsRevokedCallback_m601A44468303E8F8AE6DB176D7052B2B7A1A3710,
	AppleAuthManager_Update_mF049B837ADF2966D780DDDFAC97135FFE05F9B54,
	AppleAuthQuickLoginArgs__ctor_m002ADB6747DBEF52120701B85E6E69C392680819_AdjustorThunk,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AppleError_get_Code_m2DC6FA24A78F376AC7759F74CB97456F98C3E3D5,
	AppleError_get_Domain_m5570BBA51B6AC6502731859394EE956DEAC8AFD5,
	AppleError_get_LocalizedDescription_m0528258B3E61E724A22BAA0CF7446F3FD0848A1E,
	AppleError_get_LocalizedRecoveryOptions_mAA19861613F1EE020B040A278D75F55AB5B03F97,
	AppleError_get_LocalizedRecoverySuggestion_m103E894877132C6986B995CE1094E5F665A0D924,
	AppleError_get_LocalizedFailureReason_m452D315F855E29C98CCCC5FAD4AD76439894B2FB,
	AppleError_OnBeforeSerialize_m672A853DB847E0037955E714AC8E7E23010F2E38,
	AppleError_OnAfterDeserialize_m6F38B1635730B512980DD3C80D1BEA4AB8D80AD9,
	AppleError__ctor_mCAB1FA6B14D31603B7A898FB89E6B8A063A16F21,
	AppleIDCredential_get_IdentityToken_mD9A23ADD2B974E3174AE1157B8C52AAAF8472A29,
	AppleIDCredential_get_AuthorizationCode_mE91B95D24E462A4FFAD1679F8F8F8839E0DE838C,
	AppleIDCredential_get_State_mA59F6A34B119CF63FC5BAD01E953DF87C7DAD71E,
	AppleIDCredential_get_User_mCB948350B33024427D5274E190387D8D716BE2FC,
	AppleIDCredential_get_AuthorizedScopes_mC3D225AC26C5E53B88CC33BE80D7C9C67B97C465,
	AppleIDCredential_get_FullName_m0241AF6E01D81B58698B766FAE28FAF2D63BE612,
	AppleIDCredential_get_Email_m1DC5268DB27B573E5A5A0136AD66DB729AD105E6,
	AppleIDCredential_get_RealUserStatus_m33277B12D4375B895255B7066D941C178147CB05,
	AppleIDCredential_OnBeforeSerialize_mA40DB44EC7DE2EC21B7CDD46F4B10B0A4615B11E,
	AppleIDCredential_OnAfterDeserialize_m21F884F713F24E7D7B9ACA8880E4784E9F082DE1,
	AppleIDCredential__ctor_m233E172484F302A79A023E6112756E25D2E237E7,
	CredentialStateResponse_get_Success_m82535AF6717492354D8205926985762AA154C61D,
	CredentialStateResponse_get_CredentialState_m928023D0F25060F95DFECA3F167F7B129D1F9BFE,
	CredentialStateResponse_get_Error_m0A2A022847F24D12928ED533DB47FAE49D91E462,
	CredentialStateResponse_OnBeforeSerialize_mCF51ED02727B0BC8154D07F96D3DB3EA05C4B02B,
	CredentialStateResponse_OnAfterDeserialize_m517F35790E5F8574E84FA626C835ED67B66D3234,
	CredentialStateResponse__ctor_m14F70BAA0EDD1A6971E8D7E7F45B5F82D38DEA75,
	FullPersonName_get_PhoneticRepresentation_m12EAC6FB3B6497A6DB661B72405D4C7BAA7FDCC6,
	FullPersonName_OnAfterDeserialize_mEA9D5C99803AB1C5CCD7353848CE73E1CD7588D7,
	FullPersonName__ctor_m6B6C1CCBB5E050F80C876443FB3B6B7EDF81A519,
	LoginWithAppleIdResponse_get_Success_mD4595AEB2EB43101FB255838FF10DE5DA288AA8A,
	LoginWithAppleIdResponse_get_Error_mA5F4E932F0B21B4D6089465CDB617439F5273BE7,
	LoginWithAppleIdResponse_get_AppleIDCredential_mE60828381C62290D6318784BEA6CF5D36900369E,
	LoginWithAppleIdResponse_get_PasswordCredential_m9DEAEB1B7F3015EC979C4B3150EAC04A9FD6787D,
	LoginWithAppleIdResponse_OnBeforeSerialize_m1FF29265303F70312D5CF9B97B3F8A7F0C01A7F0,
	LoginWithAppleIdResponse_OnAfterDeserialize_m95F017BF70EDB75F9113FDBBE7D991BD415F6AA0,
	LoginWithAppleIdResponse__ctor_mCA177F81AD1DC44AF186230CDC8BCDB1BD489540,
	PasswordCredential_get_User_m9053330B4BCC8C8806CDCDB0AD6697C3819C166F,
	PasswordCredential_get_Password_m34CB954A5DFC8322D383FF93585073CB6319342C,
	PasswordCredential_OnBeforeSerialize_m43919643ADFDED9EE5870C2F34D22D4962167EEA,
	PasswordCredential_OnAfterDeserialize_mA3F5E0C7A4AC250BB17845D83100B9A5AC02E784,
	PasswordCredential__ctor_m4CEA3FF72C5D44D5ADA0F05C9822D1C164734C78,
	PayloadDeserializer_DeserializeCredentialStateResponse_mB1B47BE54866E19949520EE2A62A56A2E59AE563,
	PayloadDeserializer_DeserializeLoginWithAppleIdResponse_m7C6E5F892CD5579916521D37323B5E6943E5F9F0,
	PayloadDeserializer__ctor_m3DD225AE33F56326A8DF099FAEDDD75F65469D31,
	PersonName_get_NamePrefix_m53B3E50F11C940E5A3D239FC7AE7B3AFF9C7608C,
	PersonName_get_GivenName_mE6FBDCBA9F1E094F9AF211E494817201B4DF91B6,
	PersonName_get_MiddleName_mCA9994B19EF2B5794EED00F890EB8AFC973A74A1,
	PersonName_get_FamilyName_mDE8C71F0B60B6534105B8377857565A5ABD50B27,
	PersonName_get_NameSuffix_m01EAB5B6F3D4A3DE331CF2EDC543C1A796D5C0D7,
	PersonName_get_Nickname_mC7A33C610502A12950954A11F59D54E41D48FA30,
	PersonName_get_PhoneticRepresentation_m4E7221EC4D7934C7D4816159993943B47818AE6C,
	PersonName_OnBeforeSerialize_m841722B7A1DCA5782A61621F0A62B16668BB5A20,
	PersonName_OnAfterDeserialize_m32D4307910B89D3E33FBBE420CBA93641F8F0ACD,
	PersonName__ctor_m320D2DECA0176EE689309F89168BC54311BDD69C,
	SerializationTools_FixSerializationForString_m8171A047B8FCDC62B846791D70C2DA21418D8650,
	NULL,
	NULL,
	SerializationTools_GetBytesFromBase64String_m861B8FE0EC904AC192A9D2DD8D5CC6B51A7F2CAD,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AppleErrorExtensions_GetAuthorizationErrorCode_m657BF77C7E6EE94F9643A7720C0F4B749652424B,
	PersonNameExtensions_ToLocalizedString_mDE02E6128882D1255EF7753E1ADBDD7CD6A2E06A,
	PersonNameExtensions_JsonStringForPersonName_m60F3E8CB4AEEFF4A5741EAB787BB034F9F71672B,
	PersonNameExtensions_TryAddKeyValue_m3BBD8554A10DC6B6D8BD30500D245EFEF1854C40,
	CallbackHandler_add__nativeCredentialsRevoked_m76EF11A8538E68C4EF86007907748718B46C0A2D,
	CallbackHandler_remove__nativeCredentialsRevoked_mD4A0DE080A0F2633EBCF99808C748D76F21F622A,
	CallbackHandler_add_NativeCredentialsRevoked_mAFC47A0B52BC9DCACB3C8B36748CB8FB54CEC208,
	CallbackHandler_remove_NativeCredentialsRevoked_m8E5E2588E004C4838F26030762E98561A8FBBAE3,
	CallbackHandler_ScheduleCallback_m085D44C88C7A14F9F29E1170A8FCA2BE13B857F5,
	CallbackHandler_ExecutePendingCallbacks_m5AD24B967D8C51AECFE59C5BF02342F57952AC3C,
	CallbackHandler_AddMessageCallback_m0B95A78A2EB0A42E2D6EE2186A042C3DC406C65D,
	CallbackHandler_RemoveMessageCallback_m54E5CE8453DA1146E7E132B646E96FB964C72C7B,
	CallbackHandler__cctor_m232A322ACC97DEA243EC9A2BF85855C293E13238,
	PInvoke_NativeMessageHandlerCallback_m5D5E84BB3C9785BE4384D272156E122275A93DE4,
	PInvoke_AppleAuth_IsCurrentPlatformSupported_mF4AB0EB44BE680301E902157841A2E8BE29B9998,
	PInvoke_AppleAuth_SetupNativeMessageHandlerCallback_m2D140AC7EC65E41051360B692A205C2B1345CA90,
	PInvoke_AppleAuth_GetCredentialState_mB2184459930094EC4BF2948375B4D18B069A2D28,
	PInvoke_AppleAuth_LoginWithAppleId_m955A59F158F91E47BC0A0950F24B1564DF623E9E,
	PInvoke_AppleAuth_QuickLogin_m2BDD63FE86A5B848DD047E60AFBD031AB00060A4,
	PInvoke_AppleAuth_RegisterCredentialsRevokedCallbackId_m65B9651E2B15431DD6289C68DD8787F9EDAF8EF8,
	PInvoke_AppleAuth_LogMessage_m9620530B053E06324B4C16569F6605A7BACACEC9,
	U3CU3Ec__DisplayClass7_0__ctor_mFE19321C75D3BBE3F40E7B55C1F2D6D2194B34B1,
	U3CU3Ec__DisplayClass7_0_U3CQuickLoginU3Eb__0_m6B15B778A870666871156FA6266C6B86B6EDEF69,
	U3CU3Ec__DisplayClass9_0__ctor_mECE874027375E819AF084618F941EC8554006642,
	U3CU3Ec__DisplayClass9_0_U3CLoginWithAppleIdU3Eb__0_mA214B2AB4BFCF620062979C4C7935B3BF70815DB,
	U3CU3Ec__DisplayClass10_0__ctor_m1A51C03108292B70E58D92C883CFA153B037FF47,
	U3CU3Ec__DisplayClass10_0_U3CGetCredentialStateU3Eb__0_m99A6F544599A6034B2E04CC297223F607D6859F1,
	PInvoke_AppleAuth_GetPersonNameUsingFormatter_m579639B679D05DD5E952F09B075C0689E8879762,
	Entry__ctor_m211294F29613E18A310B5A2CBF5DB141F0C5164F,
	U3CU3Ec__cctor_m68C6CA84594FBF39B20762F63FD6E4C01368FB86,
	U3CU3Ec__ctor_mBD4C3476C866706F3C68BD2C7B862E65E3CFB7D6,
	U3CU3Ec_U3Cadd_NativeCredentialsRevokedU3Eb__12_0_m5724726AA513DABA2EA1B4A0806D72D71DEE9D41,
	U3CU3Ec__DisplayClass14_0__ctor_m955A36C7D7F760FD5656A05D0BF5CB140DF78210,
	U3CU3Ec__DisplayClass14_1__ctor_mAEFE6646216AE52125F79843CDF4695B36111113,
	U3CU3Ec__DisplayClass14_1_U3CScheduleCallbackU3Eb__0_m5C8FE466DDC61501D84CF97573FEA1C8B7689192,
	NativeMessageHandlerCallbackDelegate__ctor_mA999B2608865CF7F48403396ED87EEDDDB0BD602,
	NativeMessageHandlerCallbackDelegate_Invoke_mDCD2998A4CBAD6F8C9FEF4F3E3BB3BBA23178F69,
	NativeMessageHandlerCallbackDelegate_BeginInvoke_mC28322C6AC144EA2EBE5EF1C26C448B4449D7F3C,
	NativeMessageHandlerCallbackDelegate_EndInvoke_m55CAB28C03D0D9E03FEAB0A759F8260A8A828CC5,
};
static const int32_t s_InvokerIndices[147] = 
{
	345,
	3,
	49,
	26,
	27,
	1259,
	345,
	1260,
	156,
	26,
	23,
	27,
	27,
	1259,
	345,
	1260,
	156,
	26,
	23,
	10,
	14,
	14,
	14,
	14,
	14,
	23,
	23,
	23,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	10,
	23,
	23,
	23,
	102,
	10,
	14,
	23,
	23,
	23,
	14,
	23,
	23,
	102,
	14,
	14,
	14,
	23,
	23,
	23,
	14,
	14,
	23,
	23,
	23,
	28,
	28,
	23,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	23,
	23,
	23,
	17,
	-1,
	-1,
	1,
	10,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	10,
	14,
	102,
	10,
	14,
	102,
	14,
	14,
	14,
	14,
	28,
	28,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	186,
	622,
	0,
	1262,
	111,
	111,
	111,
	111,
	506,
	3,
	1261,
	121,
	3,
	506,
	49,
	111,
	506,
	968,
	1219,
	121,
	111,
	23,
	26,
	23,
	26,
	23,
	26,
	622,
	904,
	3,
	23,
	26,
	23,
	23,
	23,
	163,
	62,
	288,
	26,
};
static const Il2CppTokenIndexMethodTuple s_reversePInvokeIndices[1] = 
{
	{ 0x0600007A, 18,  (void**)&PInvoke_NativeMessageHandlerCallback_m5D5E84BB3C9785BE4384D272156E122275A93DE4_RuntimeMethod_var, 0 },
};
extern const Il2CppCodeGenModule g_AppleAuthCodeGenModule;
const Il2CppCodeGenModule g_AppleAuthCodeGenModule = 
{
	"AppleAuth.dll",
	147,
	s_methodPointers,
	s_InvokerIndices,
	1,
	s_reversePInvokeIndices,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
