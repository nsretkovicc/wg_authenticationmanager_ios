﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericVirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericInterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

// AppleAuth.AppleAuthManager
struct AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C;
// AppleAuth.AppleAuthManager/<>c__DisplayClass10_0
struct U3CU3Ec__DisplayClass10_0_t778A5E2C062F0F876B04E713630BB330A1511837;
// AppleAuth.AppleAuthManager/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_tCF330B8821408AA5001117209552A8D55BB2229A;
// AppleAuth.AppleAuthManager/<>c__DisplayClass9_0
struct U3CU3Ec__DisplayClass9_0_tDC78461B5F5A4E5E4BB40BB960C7C2BE05321689;
// AppleAuth.AppleAuthManager/CallbackHandler/<>c
struct U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2;
// AppleAuth.AppleAuthManager/CallbackHandler/<>c__DisplayClass14_0
struct U3CU3Ec__DisplayClass14_0_t0743B54A2D147CB75027EE3F324C3B375DB67067;
// AppleAuth.AppleAuthManager/CallbackHandler/<>c__DisplayClass14_1
struct U3CU3Ec__DisplayClass14_1_tED700A7605A9EEFA916745644049F0D15DE58F1D;
// AppleAuth.AppleAuthManager/CallbackHandler/Entry
struct Entry_t6FA80886BFD1655BEB628D855A2FAD715839F6BB;
// AppleAuth.AppleAuthManager/PInvoke/NativeMessageHandlerCallbackDelegate
struct NativeMessageHandlerCallbackDelegate_t791982D0270309BB7AEFD64EF9FFA0F7DBFE4A14;
// AppleAuth.Interfaces.IAppleError
struct IAppleError_t7BFB5142BCDD43FA9230E884B829D317FFC7445E;
// AppleAuth.Interfaces.IAppleIDCredential
struct IAppleIDCredential_t9A8611685E0223C124ABE9E60CB0C463E6F27E4E;
// AppleAuth.Interfaces.ICredential
struct ICredential_tA3317C84F5F522C782EC554E6F9AE3AC6D036F44;
// AppleAuth.Interfaces.ICredentialStateResponse
struct ICredentialStateResponse_tF6E3E5E7ED85FCDEF7643474DC53BFB473180ED7;
// AppleAuth.Interfaces.ILoginWithAppleIdResponse
struct ILoginWithAppleIdResponse_t6D8A3D76E4C720155C59442F155E221E8083FA10;
// AppleAuth.Interfaces.IPasswordCredential
struct IPasswordCredential_tC5ACB72704F73E0A1229C3B9B8792031098F1D2E;
// AppleAuth.Interfaces.IPayloadDeserializer
struct IPayloadDeserializer_tE27C18AF93B5B81F79A412F73640C4238FDA5C93;
// AppleAuth.Interfaces.IPersonName
struct IPersonName_tB6E0862B5573622746FCF68C4368BB51DE6F7B7A;
// AppleAuth.Native.AppleError
struct AppleError_t157640890ACABCEB007334DB77D1279DCB27223E;
// AppleAuth.Native.AppleIDCredential
struct AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0;
// AppleAuth.Native.CredentialStateResponse
struct CredentialStateResponse_t293F3810FEEAB306E15098AB47F7FE21607854B7;
// AppleAuth.Native.FullPersonName
struct FullPersonName_t2AC7C2FEA68179788173459B7B32BECF0C41B875;
// AppleAuth.Native.LoginWithAppleIdResponse
struct LoginWithAppleIdResponse_tEADF70491D90DDEE797098E7FFCEC61C231ACE4C;
// AppleAuth.Native.PasswordCredential
struct PasswordCredential_t64C056A6AE5B8356D435709C776795D925E35C40;
// AppleAuth.Native.PayloadDeserializer
struct PayloadDeserializer_t74E3362B6A34F9E80031FCDC097BE517ABE0489E;
// AppleAuth.Native.PersonName
struct PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action[]
struct ActionU5BU5D_tBFE34E95F8D5EAA828CD01E01994E4574F55958B;
// System.Action`1<AppleAuth.Enums.CredentialState>
struct Action_1_tF472B580CE5012B6F8159B65F503A7AD17BDAC88;
// System.Action`1<AppleAuth.Interfaces.IAppleError>
struct Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3;
// System.Action`1<AppleAuth.Interfaces.ICredential>
struct Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F;
// System.Action`1<System.Int32Enum>
struct Action_1_tABA1E3BFA092E3309A0ECC53722E4F9826DCE983;
// System.Action`1<System.Object>
struct Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0;
// System.Action`1<System.String>
struct Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2/Entry<System.UInt32,AppleAuth.AppleAuthManager/CallbackHandler/Entry>[]
struct EntryU5BU5D_t122B21BBDE6829AA331FE13F9994475CA1C5A4BA;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt32,AppleAuth.AppleAuthManager/CallbackHandler/Entry>
struct KeyCollection_tC4A836D57C1B360FAFE4C5CFDE6192258B4450BA;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.UInt32,AppleAuth.AppleAuthManager/CallbackHandler/Entry>
struct ValueCollection_t032C2EC84572334119CDB2711D009811E4B5B227;
// System.Collections.Generic.Dictionary`2<System.UInt32,AppleAuth.AppleAuthManager/CallbackHandler/Entry>
struct Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C;
// System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>
struct Dictionary_2_tA04C0C5C8CC1B93793DBB27FBA9C324693B6755F;
// System.Collections.Generic.IEqualityComparer`1<System.UInt32>
struct IEqualityComparer_1_t666366B838E9C80B34EC9FF26AB5244EDDA804B7;
// System.Collections.Generic.List`1<System.Action>
struct List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Exception
struct Exception_t;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;

IL2CPP_EXTERN_C RuntimeClass* Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_t591D2A86165F896B4B800BB5C25CE18672A55579_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Console_t5C8E87BA271B0DECA837A3BF9093AC3560DB3D5D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Convert_t1C7A851BFB2F0782FD7F72F6AA1DCBB7B53A9C7E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Entry_t6FA80886BFD1655BEB628D855A2FAD715839F6BB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IAppleError_t7BFB5142BCDD43FA9230E884B829D317FFC7445E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ICredentialStateResponse_tF6E3E5E7ED85FCDEF7643474DC53BFB473180ED7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ILoginWithAppleIdResponse_t6D8A3D76E4C720155C59442F155E221E8083FA10_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IPayloadDeserializer_tE27C18AF93B5B81F79A412F73640C4238FDA5C93_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IPersonName_tB6E0862B5573622746FCF68C4368BB51DE6F7B7A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NativeMessageHandlerCallbackDelegate_t791982D0270309BB7AEFD64EF9FFA0F7DBFE4A14_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringBuilder_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass10_0_t778A5E2C062F0F876B04E713630BB330A1511837_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass14_0_t0743B54A2D147CB75027EE3F324C3B375DB67067_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass14_1_tED700A7605A9EEFA916745644049F0D15DE58F1D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass7_0_tCF330B8821408AA5001117209552A8D55BB2229A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass9_0_tDC78461B5F5A4E5E4BB40BB960C7C2BE05321689_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral2AAEC0C992B44185948950AC0ABBF8A01636ABE4;
IL2CPP_EXTERN_C String_t* _stringLiteral2DE8C080871706C72188444F394AB7D4883B5D3A;
IL2CPP_EXTERN_C String_t* _stringLiteral4015416815F6229BA82839676F35279E4BCB0BC7;
IL2CPP_EXTERN_C String_t* _stringLiteral441DEA5D56A75C98B8BD51BA0E048ECE7B587ED5;
IL2CPP_EXTERN_C String_t* _stringLiteral4598F0A2F680EBEB450AF29B388AEE917B8C55A5;
IL2CPP_EXTERN_C String_t* _stringLiteral476822F3F2155A8DC261D2238DCB4B70200E8135;
IL2CPP_EXTERN_C String_t* _stringLiteral5B9ED5B09F24B9884FB9EE944F2D5B8980ED9BF9;
IL2CPP_EXTERN_C String_t* _stringLiteral5DD507D4A1A6B837CF5B71E12253C51AD4289E52;
IL2CPP_EXTERN_C String_t* _stringLiteral60BA4B2DAA4ED4D070FEC06687E249E0E6F9EE45;
IL2CPP_EXTERN_C String_t* _stringLiteral63E86140555073BFEED4D739D25CCEC3567ED623;
IL2CPP_EXTERN_C String_t* _stringLiteral6995E5E626F48FD5709D7896958EF02CDF3923D9;
IL2CPP_EXTERN_C String_t* _stringLiteral7827EF4895EEF02ED0A612C85CD420118258CD9A;
IL2CPP_EXTERN_C String_t* _stringLiteral8120F8F7D255BD92291AAA653E8164F6A6B1FF90;
IL2CPP_EXTERN_C String_t* _stringLiteral8C80B4AF0AC201C596A77E249D674FAC66C5B9B8;
IL2CPP_EXTERN_C String_t* _stringLiteralAB3E73C8E35434A79B5915E1660A0637615D95E3;
IL2CPP_EXTERN_C String_t* _stringLiteralAFEC25D3F526944824BEA6749A38AE4CFE673EE2;
IL2CPP_EXTERN_C String_t* _stringLiteralB858CB282617FB0956D960215C8E84D1CCF909C6;
IL2CPP_EXTERN_C String_t* _stringLiteralBBC69D6340E1166E27C0A4765EF658BF4E6F12A8;
IL2CPP_EXTERN_C String_t* _stringLiteralBCACF7319C9F96F367F1418203A31C525A5E3CD9;
IL2CPP_EXTERN_C String_t* _stringLiteralC2B7DF6201FDD3362399091F0A29550DF3505B6A;
IL2CPP_EXTERN_C String_t* _stringLiteralD7E7549C9E8D32D1A983F1A0B1165651D38EC876;
IL2CPP_EXTERN_C String_t* _stringLiteralE3F56338D22165DC687E508DF1273220C341FAE9;
IL2CPP_EXTERN_C String_t* _stringLiteralFFC8C524A098DC00EE24D0A8DAB38ED9932880F6;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1_Invoke_m6328F763431ED2ACDDB6ED8F0FDEA6194403E79F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1_Invoke_m78B725AD74CD7728202CC5ACC1E6E0F114CD02DE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1_Invoke_m7D166367CD49F79461F74C532A8286A491F75329_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1_Invoke_m7EAD69A6377B20A71F043C663094490F99E5E2AC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CallbackHandler_AddMessageCallback_m0B95A78A2EB0A42E2D6EE2186A042C3DC406C65D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CallbackHandler_RemoveMessageCallback_m54E5CE8453DA1146E7E132B646E96FB964C72C7B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_mEB4CC4CAD43E8C0BA470A4F7FDB71BB47559F441_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_mD9907E4F897C40763F8340258AEDDD9FD3F1FCE9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Remove_m78D417809EE5B205B4A02CF24F81E4096BB6EC3D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_TryGetValue_mCFF58E001B1BF5E1ECA4BBC7ECED52321A3DBA2D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mFE529F4016E0E4A41B5B8B456DEB0F58BC6F7E95_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* JsonUtility_FromJson_TisCredentialStateResponse_t293F3810FEEAB306E15098AB47F7FE21607854B7_m52466496B9EC0733C29DD4EBD0E9A613CB7C0A68_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* JsonUtility_FromJson_TisLoginWithAppleIdResponse_tEADF70491D90DDEE797098E7FFCEC61C231ACE4C_m9AFB93CAD117C86828C186AC2CB41B60B8FE871F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m019DF84155C09D3979798C84CD70DD28E146025B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mA348FA1140766465189459D25B01EB179001DE83_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_RemoveAt_m281DBD9DDEBE448DB0268959927756D4BD4E6FD6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_ToArray_m9DD19D800AE6D84ED0729D5D97CAF84DF317DD38_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m875F64A655E35C2D021756D570AD9C3FBCC2E968_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m3986FAACE5FCFF72B7601E768375D65E038FDEDB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m900B85866FD2558E6CE2725E07AF693C29C19731_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PInvoke_NativeMessageHandlerCallback_m5D5E84BB3C9785BE4384D272156E122275A93DE4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SerializationTools_FixSerializationForArray_TisString_t_m163AA34D516D317C89666B944EEDE14BF8DCF53D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SerializationTools_FixSerializationForObject_TisAppleError_t157640890ACABCEB007334DB77D1279DCB27223E_m5AE3CC0F4C6F2F346DFDECDE2B62C98BBD65DCE7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SerializationTools_FixSerializationForObject_TisAppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0_mE518B6B97BEE8670FFFAE3C4BA679D63F7AB29B0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SerializationTools_FixSerializationForObject_TisFullPersonName_t2AC7C2FEA68179788173459B7B32BECF0C41B875_m0CD7820D8502A3091B822BF4899CDF95314369E4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SerializationTools_FixSerializationForObject_TisInt32_t585191389E07734F19F3156FF88FB3EF4800D102_mF97F93CA0140582AB28C2FBBF667F05E87F95449_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SerializationTools_FixSerializationForObject_TisPasswordCredential_t64C056A6AE5B8356D435709C776795D925E35C40_m2F6D3902FF446FBBEBAC5665578E2EF37BD51601_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SerializationTools_FixSerializationForObject_TisPersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0_mA295ACB04AD3B5BDBC01ECD5D1F9280893829245_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3Cadd_NativeCredentialsRevokedU3Eb__12_0_m5724726AA513DABA2EA1B4A0806D72D71DEE9D41_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass10_0_U3CGetCredentialStateU3Eb__0_m99A6F544599A6034B2E04CC297223F607D6859F1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass14_1_U3CScheduleCallbackU3Eb__0_m5C8FE466DDC61501D84CF97573FEA1C8B7689192_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass7_0_U3CQuickLoginU3Eb__0_m6B15B778A870666871156FA6266C6B86B6EDEF69_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass9_0_U3CLoginWithAppleIdU3Eb__0_mA214B2AB4BFCF620062979C4C7935B3BF70815DB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* AuthorizationErrorCode_t0FFD9BA03D3349238191A90F27789A3DD249672F_0_0_0_var;
IL2CPP_EXTERN_C const uint32_t AppleAuthManager_GetCredentialState_m2076CC7A70D84B5D140FFC0667EBD95734BC15D9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AppleAuthManager_LoginWithAppleId_m3251131E6071E0C1D505B6CAA2DE945C8C5EA099_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AppleAuthManager_QuickLogin_m5BE7B5C2C7728FDA2B0D5EB11862FD07762A4AC6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AppleAuthManager_SetCredentialsRevokedCallback_m601A44468303E8F8AE6DB176D7052B2B7A1A3710_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AppleAuthManager_Update_mF049B837ADF2966D780DDDFAC97135FFE05F9B54_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AppleAuthManager__cctor_m56F15734DA0AA48CE41060C15FE448DCC1730FF8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AppleErrorExtensions_GetAuthorizationErrorCode_m657BF77C7E6EE94F9643A7720C0F4B749652424B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AppleError_OnAfterDeserialize_m6F38B1635730B512980DD3C80D1BEA4AB8D80AD9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t AppleIDCredential_OnAfterDeserialize_m21F884F713F24E7D7B9ACA8880E4784E9F082DE1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CallbackHandler_AddMessageCallback_m0B95A78A2EB0A42E2D6EE2186A042C3DC406C65D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CallbackHandler_ExecutePendingCallbacks_m5AD24B967D8C51AECFE59C5BF02342F57952AC3C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CallbackHandler_RemoveMessageCallback_m54E5CE8453DA1146E7E132B646E96FB964C72C7B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CallbackHandler_ScheduleCallback_m085D44C88C7A14F9F29E1170A8FCA2BE13B857F5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CallbackHandler__cctor_m232A322ACC97DEA243EC9A2BF85855C293E13238_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CallbackHandler_add_NativeCredentialsRevoked_mAFC47A0B52BC9DCACB3C8B36748CB8FB54CEC208_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CallbackHandler_add__nativeCredentialsRevoked_m76EF11A8538E68C4EF86007907748718B46C0A2D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CallbackHandler_remove_NativeCredentialsRevoked_m8E5E2588E004C4838F26030762E98561A8FBBAE3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CallbackHandler_remove__nativeCredentialsRevoked_mD4A0DE080A0F2633EBCF99808C748D76F21F622A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t CredentialStateResponse_OnAfterDeserialize_m517F35790E5F8574E84FA626C835ED67B66D3234_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FullPersonName_OnAfterDeserialize_mEA9D5C99803AB1C5CCD7353848CE73E1CD7588D7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t LoginWithAppleIdResponse_OnAfterDeserialize_m95F017BF70EDB75F9113FDBBE7D991BD415F6AA0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t NativeMessageHandlerCallbackDelegate_BeginInvoke_mC28322C6AC144EA2EBE5EF1C26C448B4449D7F3C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PInvoke_NativeMessageHandlerCallback_m5D5E84BB3C9785BE4384D272156E122275A93DE4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PayloadDeserializer_DeserializeCredentialStateResponse_mB1B47BE54866E19949520EE2A62A56A2E59AE563_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PayloadDeserializer_DeserializeLoginWithAppleIdResponse_m7C6E5F892CD5579916521D37323B5E6943E5F9F0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PersonNameExtensions_JsonStringForPersonName_m60F3E8CB4AEEFF4A5741EAB787BB034F9F71672B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PersonNameExtensions_ToLocalizedString_mDE02E6128882D1255EF7753E1ADBDD7CD6A2E06A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SerializationTools_GetBytesFromBase64String_m861B8FE0EC904AC192A9D2DD8D5CC6B51A7F2CAD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec_U3Cadd_NativeCredentialsRevokedU3Eb__12_0_m5724726AA513DABA2EA1B4A0806D72D71DEE9D41_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass10_0_U3CGetCredentialStateU3Eb__0_m99A6F544599A6034B2E04CC297223F607D6859F1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass14_1_U3CScheduleCallbackU3Eb__0_m5C8FE466DDC61501D84CF97573FEA1C8B7689192_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass7_0_U3CQuickLoginU3Eb__0_m6B15B778A870666871156FA6266C6B86B6EDEF69_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass9_0_U3CLoginWithAppleIdU3Eb__0_mA214B2AB4BFCF620062979C4C7935B3BF70815DB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__cctor_m68C6CA84594FBF39B20762F63FD6E4C01368FB86_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tD0F558965836803BF9CCA4586641DFF822357535 
{
public:

public:
};


// System.Object


// AppleAuth.AppleAuthManager
struct  AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C  : public RuntimeObject
{
public:
	// AppleAuth.Interfaces.IPayloadDeserializer AppleAuth.AppleAuthManager::_payloadDeserializer
	RuntimeObject* ____payloadDeserializer_0;
	// System.Action`1<System.String> AppleAuth.AppleAuthManager::_credentialsRevokedCallback
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ____credentialsRevokedCallback_1;

public:
	inline static int32_t get_offset_of__payloadDeserializer_0() { return static_cast<int32_t>(offsetof(AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C, ____payloadDeserializer_0)); }
	inline RuntimeObject* get__payloadDeserializer_0() const { return ____payloadDeserializer_0; }
	inline RuntimeObject** get_address_of__payloadDeserializer_0() { return &____payloadDeserializer_0; }
	inline void set__payloadDeserializer_0(RuntimeObject* value)
	{
		____payloadDeserializer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____payloadDeserializer_0), (void*)value);
	}

	inline static int32_t get_offset_of__credentialsRevokedCallback_1() { return static_cast<int32_t>(offsetof(AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C, ____credentialsRevokedCallback_1)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get__credentialsRevokedCallback_1() const { return ____credentialsRevokedCallback_1; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of__credentialsRevokedCallback_1() { return &____credentialsRevokedCallback_1; }
	inline void set__credentialsRevokedCallback_1(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		____credentialsRevokedCallback_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____credentialsRevokedCallback_1), (void*)value);
	}
};


// AppleAuth.AppleAuthManager_<>c__DisplayClass10_0
struct  U3CU3Ec__DisplayClass10_0_t778A5E2C062F0F876B04E713630BB330A1511837  : public RuntimeObject
{
public:
	// AppleAuth.AppleAuthManager AppleAuth.AppleAuthManager_<>c__DisplayClass10_0::<>4__this
	AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * ___U3CU3E4__this_0;
	// System.Action`1<AppleAuth.Interfaces.IAppleError> AppleAuth.AppleAuthManager_<>c__DisplayClass10_0::errorCallback
	Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * ___errorCallback_1;
	// System.Action`1<AppleAuth.Enums.CredentialState> AppleAuth.AppleAuthManager_<>c__DisplayClass10_0::successCallback
	Action_1_tF472B580CE5012B6F8159B65F503A7AD17BDAC88 * ___successCallback_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t778A5E2C062F0F876B04E713630BB330A1511837, ___U3CU3E4__this_0)); }
	inline AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_errorCallback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t778A5E2C062F0F876B04E713630BB330A1511837, ___errorCallback_1)); }
	inline Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * get_errorCallback_1() const { return ___errorCallback_1; }
	inline Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 ** get_address_of_errorCallback_1() { return &___errorCallback_1; }
	inline void set_errorCallback_1(Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * value)
	{
		___errorCallback_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___errorCallback_1), (void*)value);
	}

	inline static int32_t get_offset_of_successCallback_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t778A5E2C062F0F876B04E713630BB330A1511837, ___successCallback_2)); }
	inline Action_1_tF472B580CE5012B6F8159B65F503A7AD17BDAC88 * get_successCallback_2() const { return ___successCallback_2; }
	inline Action_1_tF472B580CE5012B6F8159B65F503A7AD17BDAC88 ** get_address_of_successCallback_2() { return &___successCallback_2; }
	inline void set_successCallback_2(Action_1_tF472B580CE5012B6F8159B65F503A7AD17BDAC88 * value)
	{
		___successCallback_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___successCallback_2), (void*)value);
	}
};


// AppleAuth.AppleAuthManager_<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_tCF330B8821408AA5001117209552A8D55BB2229A  : public RuntimeObject
{
public:
	// AppleAuth.AppleAuthManager AppleAuth.AppleAuthManager_<>c__DisplayClass7_0::<>4__this
	AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * ___U3CU3E4__this_0;
	// System.Action`1<AppleAuth.Interfaces.IAppleError> AppleAuth.AppleAuthManager_<>c__DisplayClass7_0::errorCallback
	Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * ___errorCallback_1;
	// System.Action`1<AppleAuth.Interfaces.ICredential> AppleAuth.AppleAuthManager_<>c__DisplayClass7_0::successCallback
	Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * ___successCallback_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_tCF330B8821408AA5001117209552A8D55BB2229A, ___U3CU3E4__this_0)); }
	inline AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_errorCallback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_tCF330B8821408AA5001117209552A8D55BB2229A, ___errorCallback_1)); }
	inline Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * get_errorCallback_1() const { return ___errorCallback_1; }
	inline Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 ** get_address_of_errorCallback_1() { return &___errorCallback_1; }
	inline void set_errorCallback_1(Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * value)
	{
		___errorCallback_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___errorCallback_1), (void*)value);
	}

	inline static int32_t get_offset_of_successCallback_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_tCF330B8821408AA5001117209552A8D55BB2229A, ___successCallback_2)); }
	inline Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * get_successCallback_2() const { return ___successCallback_2; }
	inline Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F ** get_address_of_successCallback_2() { return &___successCallback_2; }
	inline void set_successCallback_2(Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * value)
	{
		___successCallback_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___successCallback_2), (void*)value);
	}
};


// AppleAuth.AppleAuthManager_<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_tDC78461B5F5A4E5E4BB40BB960C7C2BE05321689  : public RuntimeObject
{
public:
	// AppleAuth.AppleAuthManager AppleAuth.AppleAuthManager_<>c__DisplayClass9_0::<>4__this
	AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * ___U3CU3E4__this_0;
	// System.Action`1<AppleAuth.Interfaces.IAppleError> AppleAuth.AppleAuthManager_<>c__DisplayClass9_0::errorCallback
	Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * ___errorCallback_1;
	// System.Action`1<AppleAuth.Interfaces.ICredential> AppleAuth.AppleAuthManager_<>c__DisplayClass9_0::successCallback
	Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * ___successCallback_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_tDC78461B5F5A4E5E4BB40BB960C7C2BE05321689, ___U3CU3E4__this_0)); }
	inline AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_0), (void*)value);
	}

	inline static int32_t get_offset_of_errorCallback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_tDC78461B5F5A4E5E4BB40BB960C7C2BE05321689, ___errorCallback_1)); }
	inline Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * get_errorCallback_1() const { return ___errorCallback_1; }
	inline Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 ** get_address_of_errorCallback_1() { return &___errorCallback_1; }
	inline void set_errorCallback_1(Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * value)
	{
		___errorCallback_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___errorCallback_1), (void*)value);
	}

	inline static int32_t get_offset_of_successCallback_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_tDC78461B5F5A4E5E4BB40BB960C7C2BE05321689, ___successCallback_2)); }
	inline Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * get_successCallback_2() const { return ___successCallback_2; }
	inline Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F ** get_address_of_successCallback_2() { return &___successCallback_2; }
	inline void set_successCallback_2(Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * value)
	{
		___successCallback_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___successCallback_2), (void*)value);
	}
};


// AppleAuth.AppleAuthManager_CallbackHandler
struct  CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC  : public RuntimeObject
{
public:

public:
};

struct CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields
{
public:
	// System.Object AppleAuth.AppleAuthManager_CallbackHandler::SyncLock
	RuntimeObject * ___SyncLock_2;
	// System.Collections.Generic.Dictionary`2<System.UInt32,AppleAuth.AppleAuthManager_CallbackHandler_Entry> AppleAuth.AppleAuthManager_CallbackHandler::CallbackDictionary
	Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C * ___CallbackDictionary_3;
	// System.Collections.Generic.List`1<System.Action> AppleAuth.AppleAuthManager_CallbackHandler::ScheduledActions
	List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * ___ScheduledActions_4;
	// System.UInt32 AppleAuth.AppleAuthManager_CallbackHandler::_callbackId
	uint32_t ____callbackId_5;
	// System.Boolean AppleAuth.AppleAuthManager_CallbackHandler::_initialized
	bool ____initialized_6;
	// System.UInt32 AppleAuth.AppleAuthManager_CallbackHandler::_credentialsRevokedCallbackId
	uint32_t ____credentialsRevokedCallbackId_7;
	// System.Action`1<System.String> AppleAuth.AppleAuthManager_CallbackHandler::_nativeCredentialsRevoked
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ____nativeCredentialsRevoked_8;

public:
	inline static int32_t get_offset_of_SyncLock_2() { return static_cast<int32_t>(offsetof(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields, ___SyncLock_2)); }
	inline RuntimeObject * get_SyncLock_2() const { return ___SyncLock_2; }
	inline RuntimeObject ** get_address_of_SyncLock_2() { return &___SyncLock_2; }
	inline void set_SyncLock_2(RuntimeObject * value)
	{
		___SyncLock_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SyncLock_2), (void*)value);
	}

	inline static int32_t get_offset_of_CallbackDictionary_3() { return static_cast<int32_t>(offsetof(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields, ___CallbackDictionary_3)); }
	inline Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C * get_CallbackDictionary_3() const { return ___CallbackDictionary_3; }
	inline Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C ** get_address_of_CallbackDictionary_3() { return &___CallbackDictionary_3; }
	inline void set_CallbackDictionary_3(Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C * value)
	{
		___CallbackDictionary_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CallbackDictionary_3), (void*)value);
	}

	inline static int32_t get_offset_of_ScheduledActions_4() { return static_cast<int32_t>(offsetof(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields, ___ScheduledActions_4)); }
	inline List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * get_ScheduledActions_4() const { return ___ScheduledActions_4; }
	inline List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 ** get_address_of_ScheduledActions_4() { return &___ScheduledActions_4; }
	inline void set_ScheduledActions_4(List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * value)
	{
		___ScheduledActions_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ScheduledActions_4), (void*)value);
	}

	inline static int32_t get_offset_of__callbackId_5() { return static_cast<int32_t>(offsetof(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields, ____callbackId_5)); }
	inline uint32_t get__callbackId_5() const { return ____callbackId_5; }
	inline uint32_t* get_address_of__callbackId_5() { return &____callbackId_5; }
	inline void set__callbackId_5(uint32_t value)
	{
		____callbackId_5 = value;
	}

	inline static int32_t get_offset_of__initialized_6() { return static_cast<int32_t>(offsetof(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields, ____initialized_6)); }
	inline bool get__initialized_6() const { return ____initialized_6; }
	inline bool* get_address_of__initialized_6() { return &____initialized_6; }
	inline void set__initialized_6(bool value)
	{
		____initialized_6 = value;
	}

	inline static int32_t get_offset_of__credentialsRevokedCallbackId_7() { return static_cast<int32_t>(offsetof(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields, ____credentialsRevokedCallbackId_7)); }
	inline uint32_t get__credentialsRevokedCallbackId_7() const { return ____credentialsRevokedCallbackId_7; }
	inline uint32_t* get_address_of__credentialsRevokedCallbackId_7() { return &____credentialsRevokedCallbackId_7; }
	inline void set__credentialsRevokedCallbackId_7(uint32_t value)
	{
		____credentialsRevokedCallbackId_7 = value;
	}

	inline static int32_t get_offset_of__nativeCredentialsRevoked_8() { return static_cast<int32_t>(offsetof(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields, ____nativeCredentialsRevoked_8)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get__nativeCredentialsRevoked_8() const { return ____nativeCredentialsRevoked_8; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of__nativeCredentialsRevoked_8() { return &____nativeCredentialsRevoked_8; }
	inline void set__nativeCredentialsRevoked_8(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		____nativeCredentialsRevoked_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____nativeCredentialsRevoked_8), (void*)value);
	}
};


// AppleAuth.AppleAuthManager_CallbackHandler_<>c
struct  U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2_StaticFields
{
public:
	// AppleAuth.AppleAuthManager_CallbackHandler_<>c AppleAuth.AppleAuthManager_CallbackHandler_<>c::<>9
	U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2 * ___U3CU3E9_0;
	// System.Action`1<System.String> AppleAuth.AppleAuthManager_CallbackHandler_<>c::<>9__12_0
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___U3CU3E9__12_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2_StaticFields, ___U3CU3E9__12_0_1)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_U3CU3E9__12_0_1() const { return ___U3CU3E9__12_0_1; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_U3CU3E9__12_0_1() { return &___U3CU3E9__12_0_1; }
	inline void set_U3CU3E9__12_0_1(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___U3CU3E9__12_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__12_0_1), (void*)value);
	}
};


// AppleAuth.AppleAuthManager_CallbackHandler_<>c__DisplayClass14_0
struct  U3CU3Ec__DisplayClass14_0_t0743B54A2D147CB75027EE3F324C3B375DB67067  : public RuntimeObject
{
public:
	// System.String AppleAuth.AppleAuthManager_CallbackHandler_<>c__DisplayClass14_0::payload
	String_t* ___payload_0;

public:
	inline static int32_t get_offset_of_payload_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass14_0_t0743B54A2D147CB75027EE3F324C3B375DB67067, ___payload_0)); }
	inline String_t* get_payload_0() const { return ___payload_0; }
	inline String_t** get_address_of_payload_0() { return &___payload_0; }
	inline void set_payload_0(String_t* value)
	{
		___payload_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___payload_0), (void*)value);
	}
};


// AppleAuth.AppleAuthManager_CallbackHandler_<>c__DisplayClass14_1
struct  U3CU3Ec__DisplayClass14_1_tED700A7605A9EEFA916745644049F0D15DE58F1D  : public RuntimeObject
{
public:
	// System.Action`1<System.String> AppleAuth.AppleAuthManager_CallbackHandler_<>c__DisplayClass14_1::callback
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___callback_0;
	// AppleAuth.AppleAuthManager_CallbackHandler_<>c__DisplayClass14_0 AppleAuth.AppleAuthManager_CallbackHandler_<>c__DisplayClass14_1::CSU24<>8__locals1
	U3CU3Ec__DisplayClass14_0_t0743B54A2D147CB75027EE3F324C3B375DB67067 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass14_1_tED700A7605A9EEFA916745644049F0D15DE58F1D, ___callback_0)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___callback_0), (void*)value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass14_1_tED700A7605A9EEFA916745644049F0D15DE58F1D, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass14_0_t0743B54A2D147CB75027EE3F324C3B375DB67067 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass14_0_t0743B54A2D147CB75027EE3F324C3B375DB67067 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass14_0_t0743B54A2D147CB75027EE3F324C3B375DB67067 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CSU24U3CU3E8__locals1_1), (void*)value);
	}
};


// AppleAuth.AppleAuthManager_CallbackHandler_Entry
struct  Entry_t6FA80886BFD1655BEB628D855A2FAD715839F6BB  : public RuntimeObject
{
public:
	// System.Boolean AppleAuth.AppleAuthManager_CallbackHandler_Entry::IsSingleUseCallback
	bool ___IsSingleUseCallback_0;
	// System.Action`1<System.String> AppleAuth.AppleAuthManager_CallbackHandler_Entry::MessageCallback
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___MessageCallback_1;

public:
	inline static int32_t get_offset_of_IsSingleUseCallback_0() { return static_cast<int32_t>(offsetof(Entry_t6FA80886BFD1655BEB628D855A2FAD715839F6BB, ___IsSingleUseCallback_0)); }
	inline bool get_IsSingleUseCallback_0() const { return ___IsSingleUseCallback_0; }
	inline bool* get_address_of_IsSingleUseCallback_0() { return &___IsSingleUseCallback_0; }
	inline void set_IsSingleUseCallback_0(bool value)
	{
		___IsSingleUseCallback_0 = value;
	}

	inline static int32_t get_offset_of_MessageCallback_1() { return static_cast<int32_t>(offsetof(Entry_t6FA80886BFD1655BEB628D855A2FAD715839F6BB, ___MessageCallback_1)); }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * get_MessageCallback_1() const { return ___MessageCallback_1; }
	inline Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 ** get_address_of_MessageCallback_1() { return &___MessageCallback_1; }
	inline void set_MessageCallback_1(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * value)
	{
		___MessageCallback_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MessageCallback_1), (void*)value);
	}
};


// AppleAuth.AppleAuthManager_PInvoke
struct  PInvoke_t60C79ED3B39CAAD243F229FA197F8E1C7325B170  : public RuntimeObject
{
public:

public:
};


// AppleAuth.Extensions.AppleErrorExtensions
struct  AppleErrorExtensions_t05B8507732E07454728BB9FCAD3CEF04DA3A5D7A  : public RuntimeObject
{
public:

public:
};


// AppleAuth.Extensions.PersonNameExtensions
struct  PersonNameExtensions_t4CC9F2614ADC22AC84203F6BFC14C079D3BADD17  : public RuntimeObject
{
public:

public:
};


// AppleAuth.Extensions.PersonNameExtensions_PInvoke
struct  PInvoke_t3D4CDE7C0354B8413F97459C39C730DC79C9BE95  : public RuntimeObject
{
public:

public:
};


// AppleAuth.Native.AppleError
struct  AppleError_t157640890ACABCEB007334DB77D1279DCB27223E  : public RuntimeObject
{
public:
	// System.Int32 AppleAuth.Native.AppleError::_code
	int32_t ____code_0;
	// System.String AppleAuth.Native.AppleError::_domain
	String_t* ____domain_1;
	// System.String AppleAuth.Native.AppleError::_localizedDescription
	String_t* ____localizedDescription_2;
	// System.String[] AppleAuth.Native.AppleError::_localizedRecoveryOptions
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____localizedRecoveryOptions_3;
	// System.String AppleAuth.Native.AppleError::_localizedRecoverySuggestion
	String_t* ____localizedRecoverySuggestion_4;
	// System.String AppleAuth.Native.AppleError::_localizedFailureReason
	String_t* ____localizedFailureReason_5;

public:
	inline static int32_t get_offset_of__code_0() { return static_cast<int32_t>(offsetof(AppleError_t157640890ACABCEB007334DB77D1279DCB27223E, ____code_0)); }
	inline int32_t get__code_0() const { return ____code_0; }
	inline int32_t* get_address_of__code_0() { return &____code_0; }
	inline void set__code_0(int32_t value)
	{
		____code_0 = value;
	}

	inline static int32_t get_offset_of__domain_1() { return static_cast<int32_t>(offsetof(AppleError_t157640890ACABCEB007334DB77D1279DCB27223E, ____domain_1)); }
	inline String_t* get__domain_1() const { return ____domain_1; }
	inline String_t** get_address_of__domain_1() { return &____domain_1; }
	inline void set__domain_1(String_t* value)
	{
		____domain_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____domain_1), (void*)value);
	}

	inline static int32_t get_offset_of__localizedDescription_2() { return static_cast<int32_t>(offsetof(AppleError_t157640890ACABCEB007334DB77D1279DCB27223E, ____localizedDescription_2)); }
	inline String_t* get__localizedDescription_2() const { return ____localizedDescription_2; }
	inline String_t** get_address_of__localizedDescription_2() { return &____localizedDescription_2; }
	inline void set__localizedDescription_2(String_t* value)
	{
		____localizedDescription_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____localizedDescription_2), (void*)value);
	}

	inline static int32_t get_offset_of__localizedRecoveryOptions_3() { return static_cast<int32_t>(offsetof(AppleError_t157640890ACABCEB007334DB77D1279DCB27223E, ____localizedRecoveryOptions_3)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__localizedRecoveryOptions_3() const { return ____localizedRecoveryOptions_3; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__localizedRecoveryOptions_3() { return &____localizedRecoveryOptions_3; }
	inline void set__localizedRecoveryOptions_3(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____localizedRecoveryOptions_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____localizedRecoveryOptions_3), (void*)value);
	}

	inline static int32_t get_offset_of__localizedRecoverySuggestion_4() { return static_cast<int32_t>(offsetof(AppleError_t157640890ACABCEB007334DB77D1279DCB27223E, ____localizedRecoverySuggestion_4)); }
	inline String_t* get__localizedRecoverySuggestion_4() const { return ____localizedRecoverySuggestion_4; }
	inline String_t** get_address_of__localizedRecoverySuggestion_4() { return &____localizedRecoverySuggestion_4; }
	inline void set__localizedRecoverySuggestion_4(String_t* value)
	{
		____localizedRecoverySuggestion_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____localizedRecoverySuggestion_4), (void*)value);
	}

	inline static int32_t get_offset_of__localizedFailureReason_5() { return static_cast<int32_t>(offsetof(AppleError_t157640890ACABCEB007334DB77D1279DCB27223E, ____localizedFailureReason_5)); }
	inline String_t* get__localizedFailureReason_5() const { return ____localizedFailureReason_5; }
	inline String_t** get_address_of__localizedFailureReason_5() { return &____localizedFailureReason_5; }
	inline void set__localizedFailureReason_5(String_t* value)
	{
		____localizedFailureReason_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____localizedFailureReason_5), (void*)value);
	}
};


// AppleAuth.Native.AppleIDCredential
struct  AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0  : public RuntimeObject
{
public:
	// System.String AppleAuth.Native.AppleIDCredential::_base64IdentityToken
	String_t* ____base64IdentityToken_0;
	// System.String AppleAuth.Native.AppleIDCredential::_base64AuthorizationCode
	String_t* ____base64AuthorizationCode_1;
	// System.String AppleAuth.Native.AppleIDCredential::_state
	String_t* ____state_2;
	// System.String AppleAuth.Native.AppleIDCredential::_user
	String_t* ____user_3;
	// System.String[] AppleAuth.Native.AppleIDCredential::_authorizedScopes
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____authorizedScopes_4;
	// System.Boolean AppleAuth.Native.AppleIDCredential::_hasFullName
	bool ____hasFullName_5;
	// AppleAuth.Native.FullPersonName AppleAuth.Native.AppleIDCredential::_fullName
	FullPersonName_t2AC7C2FEA68179788173459B7B32BECF0C41B875 * ____fullName_6;
	// System.String AppleAuth.Native.AppleIDCredential::_email
	String_t* ____email_7;
	// System.Int32 AppleAuth.Native.AppleIDCredential::_realUserStatus
	int32_t ____realUserStatus_8;
	// System.Byte[] AppleAuth.Native.AppleIDCredential::_identityToken
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____identityToken_9;
	// System.Byte[] AppleAuth.Native.AppleIDCredential::_authorizationCode
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____authorizationCode_10;

public:
	inline static int32_t get_offset_of__base64IdentityToken_0() { return static_cast<int32_t>(offsetof(AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0, ____base64IdentityToken_0)); }
	inline String_t* get__base64IdentityToken_0() const { return ____base64IdentityToken_0; }
	inline String_t** get_address_of__base64IdentityToken_0() { return &____base64IdentityToken_0; }
	inline void set__base64IdentityToken_0(String_t* value)
	{
		____base64IdentityToken_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____base64IdentityToken_0), (void*)value);
	}

	inline static int32_t get_offset_of__base64AuthorizationCode_1() { return static_cast<int32_t>(offsetof(AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0, ____base64AuthorizationCode_1)); }
	inline String_t* get__base64AuthorizationCode_1() const { return ____base64AuthorizationCode_1; }
	inline String_t** get_address_of__base64AuthorizationCode_1() { return &____base64AuthorizationCode_1; }
	inline void set__base64AuthorizationCode_1(String_t* value)
	{
		____base64AuthorizationCode_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____base64AuthorizationCode_1), (void*)value);
	}

	inline static int32_t get_offset_of__state_2() { return static_cast<int32_t>(offsetof(AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0, ____state_2)); }
	inline String_t* get__state_2() const { return ____state_2; }
	inline String_t** get_address_of__state_2() { return &____state_2; }
	inline void set__state_2(String_t* value)
	{
		____state_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____state_2), (void*)value);
	}

	inline static int32_t get_offset_of__user_3() { return static_cast<int32_t>(offsetof(AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0, ____user_3)); }
	inline String_t* get__user_3() const { return ____user_3; }
	inline String_t** get_address_of__user_3() { return &____user_3; }
	inline void set__user_3(String_t* value)
	{
		____user_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____user_3), (void*)value);
	}

	inline static int32_t get_offset_of__authorizedScopes_4() { return static_cast<int32_t>(offsetof(AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0, ____authorizedScopes_4)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__authorizedScopes_4() const { return ____authorizedScopes_4; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__authorizedScopes_4() { return &____authorizedScopes_4; }
	inline void set__authorizedScopes_4(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____authorizedScopes_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____authorizedScopes_4), (void*)value);
	}

	inline static int32_t get_offset_of__hasFullName_5() { return static_cast<int32_t>(offsetof(AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0, ____hasFullName_5)); }
	inline bool get__hasFullName_5() const { return ____hasFullName_5; }
	inline bool* get_address_of__hasFullName_5() { return &____hasFullName_5; }
	inline void set__hasFullName_5(bool value)
	{
		____hasFullName_5 = value;
	}

	inline static int32_t get_offset_of__fullName_6() { return static_cast<int32_t>(offsetof(AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0, ____fullName_6)); }
	inline FullPersonName_t2AC7C2FEA68179788173459B7B32BECF0C41B875 * get__fullName_6() const { return ____fullName_6; }
	inline FullPersonName_t2AC7C2FEA68179788173459B7B32BECF0C41B875 ** get_address_of__fullName_6() { return &____fullName_6; }
	inline void set__fullName_6(FullPersonName_t2AC7C2FEA68179788173459B7B32BECF0C41B875 * value)
	{
		____fullName_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____fullName_6), (void*)value);
	}

	inline static int32_t get_offset_of__email_7() { return static_cast<int32_t>(offsetof(AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0, ____email_7)); }
	inline String_t* get__email_7() const { return ____email_7; }
	inline String_t** get_address_of__email_7() { return &____email_7; }
	inline void set__email_7(String_t* value)
	{
		____email_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____email_7), (void*)value);
	}

	inline static int32_t get_offset_of__realUserStatus_8() { return static_cast<int32_t>(offsetof(AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0, ____realUserStatus_8)); }
	inline int32_t get__realUserStatus_8() const { return ____realUserStatus_8; }
	inline int32_t* get_address_of__realUserStatus_8() { return &____realUserStatus_8; }
	inline void set__realUserStatus_8(int32_t value)
	{
		____realUserStatus_8 = value;
	}

	inline static int32_t get_offset_of__identityToken_9() { return static_cast<int32_t>(offsetof(AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0, ____identityToken_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__identityToken_9() const { return ____identityToken_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__identityToken_9() { return &____identityToken_9; }
	inline void set__identityToken_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____identityToken_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____identityToken_9), (void*)value);
	}

	inline static int32_t get_offset_of__authorizationCode_10() { return static_cast<int32_t>(offsetof(AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0, ____authorizationCode_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__authorizationCode_10() const { return ____authorizationCode_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__authorizationCode_10() { return &____authorizationCode_10; }
	inline void set__authorizationCode_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____authorizationCode_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____authorizationCode_10), (void*)value);
	}
};


// AppleAuth.Native.CredentialStateResponse
struct  CredentialStateResponse_t293F3810FEEAB306E15098AB47F7FE21607854B7  : public RuntimeObject
{
public:
	// System.Boolean AppleAuth.Native.CredentialStateResponse::_success
	bool ____success_0;
	// System.Boolean AppleAuth.Native.CredentialStateResponse::_hasCredentialState
	bool ____hasCredentialState_1;
	// System.Boolean AppleAuth.Native.CredentialStateResponse::_hasError
	bool ____hasError_2;
	// System.Int32 AppleAuth.Native.CredentialStateResponse::_credentialState
	int32_t ____credentialState_3;
	// AppleAuth.Native.AppleError AppleAuth.Native.CredentialStateResponse::_error
	AppleError_t157640890ACABCEB007334DB77D1279DCB27223E * ____error_4;

public:
	inline static int32_t get_offset_of__success_0() { return static_cast<int32_t>(offsetof(CredentialStateResponse_t293F3810FEEAB306E15098AB47F7FE21607854B7, ____success_0)); }
	inline bool get__success_0() const { return ____success_0; }
	inline bool* get_address_of__success_0() { return &____success_0; }
	inline void set__success_0(bool value)
	{
		____success_0 = value;
	}

	inline static int32_t get_offset_of__hasCredentialState_1() { return static_cast<int32_t>(offsetof(CredentialStateResponse_t293F3810FEEAB306E15098AB47F7FE21607854B7, ____hasCredentialState_1)); }
	inline bool get__hasCredentialState_1() const { return ____hasCredentialState_1; }
	inline bool* get_address_of__hasCredentialState_1() { return &____hasCredentialState_1; }
	inline void set__hasCredentialState_1(bool value)
	{
		____hasCredentialState_1 = value;
	}

	inline static int32_t get_offset_of__hasError_2() { return static_cast<int32_t>(offsetof(CredentialStateResponse_t293F3810FEEAB306E15098AB47F7FE21607854B7, ____hasError_2)); }
	inline bool get__hasError_2() const { return ____hasError_2; }
	inline bool* get_address_of__hasError_2() { return &____hasError_2; }
	inline void set__hasError_2(bool value)
	{
		____hasError_2 = value;
	}

	inline static int32_t get_offset_of__credentialState_3() { return static_cast<int32_t>(offsetof(CredentialStateResponse_t293F3810FEEAB306E15098AB47F7FE21607854B7, ____credentialState_3)); }
	inline int32_t get__credentialState_3() const { return ____credentialState_3; }
	inline int32_t* get_address_of__credentialState_3() { return &____credentialState_3; }
	inline void set__credentialState_3(int32_t value)
	{
		____credentialState_3 = value;
	}

	inline static int32_t get_offset_of__error_4() { return static_cast<int32_t>(offsetof(CredentialStateResponse_t293F3810FEEAB306E15098AB47F7FE21607854B7, ____error_4)); }
	inline AppleError_t157640890ACABCEB007334DB77D1279DCB27223E * get__error_4() const { return ____error_4; }
	inline AppleError_t157640890ACABCEB007334DB77D1279DCB27223E ** get_address_of__error_4() { return &____error_4; }
	inline void set__error_4(AppleError_t157640890ACABCEB007334DB77D1279DCB27223E * value)
	{
		____error_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____error_4), (void*)value);
	}
};


// AppleAuth.Native.LoginWithAppleIdResponse
struct  LoginWithAppleIdResponse_tEADF70491D90DDEE797098E7FFCEC61C231ACE4C  : public RuntimeObject
{
public:
	// System.Boolean AppleAuth.Native.LoginWithAppleIdResponse::_success
	bool ____success_0;
	// System.Boolean AppleAuth.Native.LoginWithAppleIdResponse::_hasAppleIdCredential
	bool ____hasAppleIdCredential_1;
	// System.Boolean AppleAuth.Native.LoginWithAppleIdResponse::_hasPasswordCredential
	bool ____hasPasswordCredential_2;
	// System.Boolean AppleAuth.Native.LoginWithAppleIdResponse::_hasError
	bool ____hasError_3;
	// AppleAuth.Native.AppleIDCredential AppleAuth.Native.LoginWithAppleIdResponse::_appleIdCredential
	AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0 * ____appleIdCredential_4;
	// AppleAuth.Native.PasswordCredential AppleAuth.Native.LoginWithAppleIdResponse::_passwordCredential
	PasswordCredential_t64C056A6AE5B8356D435709C776795D925E35C40 * ____passwordCredential_5;
	// AppleAuth.Native.AppleError AppleAuth.Native.LoginWithAppleIdResponse::_error
	AppleError_t157640890ACABCEB007334DB77D1279DCB27223E * ____error_6;

public:
	inline static int32_t get_offset_of__success_0() { return static_cast<int32_t>(offsetof(LoginWithAppleIdResponse_tEADF70491D90DDEE797098E7FFCEC61C231ACE4C, ____success_0)); }
	inline bool get__success_0() const { return ____success_0; }
	inline bool* get_address_of__success_0() { return &____success_0; }
	inline void set__success_0(bool value)
	{
		____success_0 = value;
	}

	inline static int32_t get_offset_of__hasAppleIdCredential_1() { return static_cast<int32_t>(offsetof(LoginWithAppleIdResponse_tEADF70491D90DDEE797098E7FFCEC61C231ACE4C, ____hasAppleIdCredential_1)); }
	inline bool get__hasAppleIdCredential_1() const { return ____hasAppleIdCredential_1; }
	inline bool* get_address_of__hasAppleIdCredential_1() { return &____hasAppleIdCredential_1; }
	inline void set__hasAppleIdCredential_1(bool value)
	{
		____hasAppleIdCredential_1 = value;
	}

	inline static int32_t get_offset_of__hasPasswordCredential_2() { return static_cast<int32_t>(offsetof(LoginWithAppleIdResponse_tEADF70491D90DDEE797098E7FFCEC61C231ACE4C, ____hasPasswordCredential_2)); }
	inline bool get__hasPasswordCredential_2() const { return ____hasPasswordCredential_2; }
	inline bool* get_address_of__hasPasswordCredential_2() { return &____hasPasswordCredential_2; }
	inline void set__hasPasswordCredential_2(bool value)
	{
		____hasPasswordCredential_2 = value;
	}

	inline static int32_t get_offset_of__hasError_3() { return static_cast<int32_t>(offsetof(LoginWithAppleIdResponse_tEADF70491D90DDEE797098E7FFCEC61C231ACE4C, ____hasError_3)); }
	inline bool get__hasError_3() const { return ____hasError_3; }
	inline bool* get_address_of__hasError_3() { return &____hasError_3; }
	inline void set__hasError_3(bool value)
	{
		____hasError_3 = value;
	}

	inline static int32_t get_offset_of__appleIdCredential_4() { return static_cast<int32_t>(offsetof(LoginWithAppleIdResponse_tEADF70491D90DDEE797098E7FFCEC61C231ACE4C, ____appleIdCredential_4)); }
	inline AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0 * get__appleIdCredential_4() const { return ____appleIdCredential_4; }
	inline AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0 ** get_address_of__appleIdCredential_4() { return &____appleIdCredential_4; }
	inline void set__appleIdCredential_4(AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0 * value)
	{
		____appleIdCredential_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____appleIdCredential_4), (void*)value);
	}

	inline static int32_t get_offset_of__passwordCredential_5() { return static_cast<int32_t>(offsetof(LoginWithAppleIdResponse_tEADF70491D90DDEE797098E7FFCEC61C231ACE4C, ____passwordCredential_5)); }
	inline PasswordCredential_t64C056A6AE5B8356D435709C776795D925E35C40 * get__passwordCredential_5() const { return ____passwordCredential_5; }
	inline PasswordCredential_t64C056A6AE5B8356D435709C776795D925E35C40 ** get_address_of__passwordCredential_5() { return &____passwordCredential_5; }
	inline void set__passwordCredential_5(PasswordCredential_t64C056A6AE5B8356D435709C776795D925E35C40 * value)
	{
		____passwordCredential_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____passwordCredential_5), (void*)value);
	}

	inline static int32_t get_offset_of__error_6() { return static_cast<int32_t>(offsetof(LoginWithAppleIdResponse_tEADF70491D90DDEE797098E7FFCEC61C231ACE4C, ____error_6)); }
	inline AppleError_t157640890ACABCEB007334DB77D1279DCB27223E * get__error_6() const { return ____error_6; }
	inline AppleError_t157640890ACABCEB007334DB77D1279DCB27223E ** get_address_of__error_6() { return &____error_6; }
	inline void set__error_6(AppleError_t157640890ACABCEB007334DB77D1279DCB27223E * value)
	{
		____error_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____error_6), (void*)value);
	}
};


// AppleAuth.Native.PasswordCredential
struct  PasswordCredential_t64C056A6AE5B8356D435709C776795D925E35C40  : public RuntimeObject
{
public:
	// System.String AppleAuth.Native.PasswordCredential::_user
	String_t* ____user_0;
	// System.String AppleAuth.Native.PasswordCredential::_password
	String_t* ____password_1;

public:
	inline static int32_t get_offset_of__user_0() { return static_cast<int32_t>(offsetof(PasswordCredential_t64C056A6AE5B8356D435709C776795D925E35C40, ____user_0)); }
	inline String_t* get__user_0() const { return ____user_0; }
	inline String_t** get_address_of__user_0() { return &____user_0; }
	inline void set__user_0(String_t* value)
	{
		____user_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____user_0), (void*)value);
	}

	inline static int32_t get_offset_of__password_1() { return static_cast<int32_t>(offsetof(PasswordCredential_t64C056A6AE5B8356D435709C776795D925E35C40, ____password_1)); }
	inline String_t* get__password_1() const { return ____password_1; }
	inline String_t** get_address_of__password_1() { return &____password_1; }
	inline void set__password_1(String_t* value)
	{
		____password_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____password_1), (void*)value);
	}
};


// AppleAuth.Native.PayloadDeserializer
struct  PayloadDeserializer_t74E3362B6A34F9E80031FCDC097BE517ABE0489E  : public RuntimeObject
{
public:

public:
};


// AppleAuth.Native.PersonName
struct  PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0  : public RuntimeObject
{
public:
	// System.String AppleAuth.Native.PersonName::_namePrefix
	String_t* ____namePrefix_0;
	// System.String AppleAuth.Native.PersonName::_givenName
	String_t* ____givenName_1;
	// System.String AppleAuth.Native.PersonName::_middleName
	String_t* ____middleName_2;
	// System.String AppleAuth.Native.PersonName::_familyName
	String_t* ____familyName_3;
	// System.String AppleAuth.Native.PersonName::_nameSuffix
	String_t* ____nameSuffix_4;
	// System.String AppleAuth.Native.PersonName::_nickname
	String_t* ____nickname_5;

public:
	inline static int32_t get_offset_of__namePrefix_0() { return static_cast<int32_t>(offsetof(PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0, ____namePrefix_0)); }
	inline String_t* get__namePrefix_0() const { return ____namePrefix_0; }
	inline String_t** get_address_of__namePrefix_0() { return &____namePrefix_0; }
	inline void set__namePrefix_0(String_t* value)
	{
		____namePrefix_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____namePrefix_0), (void*)value);
	}

	inline static int32_t get_offset_of__givenName_1() { return static_cast<int32_t>(offsetof(PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0, ____givenName_1)); }
	inline String_t* get__givenName_1() const { return ____givenName_1; }
	inline String_t** get_address_of__givenName_1() { return &____givenName_1; }
	inline void set__givenName_1(String_t* value)
	{
		____givenName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____givenName_1), (void*)value);
	}

	inline static int32_t get_offset_of__middleName_2() { return static_cast<int32_t>(offsetof(PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0, ____middleName_2)); }
	inline String_t* get__middleName_2() const { return ____middleName_2; }
	inline String_t** get_address_of__middleName_2() { return &____middleName_2; }
	inline void set__middleName_2(String_t* value)
	{
		____middleName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____middleName_2), (void*)value);
	}

	inline static int32_t get_offset_of__familyName_3() { return static_cast<int32_t>(offsetof(PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0, ____familyName_3)); }
	inline String_t* get__familyName_3() const { return ____familyName_3; }
	inline String_t** get_address_of__familyName_3() { return &____familyName_3; }
	inline void set__familyName_3(String_t* value)
	{
		____familyName_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____familyName_3), (void*)value);
	}

	inline static int32_t get_offset_of__nameSuffix_4() { return static_cast<int32_t>(offsetof(PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0, ____nameSuffix_4)); }
	inline String_t* get__nameSuffix_4() const { return ____nameSuffix_4; }
	inline String_t** get_address_of__nameSuffix_4() { return &____nameSuffix_4; }
	inline void set__nameSuffix_4(String_t* value)
	{
		____nameSuffix_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____nameSuffix_4), (void*)value);
	}

	inline static int32_t get_offset_of__nickname_5() { return static_cast<int32_t>(offsetof(PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0, ____nickname_5)); }
	inline String_t* get__nickname_5() const { return ____nickname_5; }
	inline String_t** get_address_of__nickname_5() { return &____nickname_5; }
	inline void set__nickname_5(String_t* value)
	{
		____nickname_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____nickname_5), (void*)value);
	}
};


// AppleAuth.Native.SerializationTools
struct  SerializationTools_tADF2434B49D586C24E66595F01B9D95C36B1719E  : public RuntimeObject
{
public:

public:
};

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.Dictionary`2<System.UInt32,AppleAuth.AppleAuthManager_CallbackHandler_Entry>
struct  Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___buckets_0;
	// System.Collections.Generic.Dictionary`2_Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t122B21BBDE6829AA331FE13F9994475CA1C5A4BA* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2_KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_tC4A836D57C1B360FAFE4C5CFDE6192258B4450BA * ___keys_7;
	// System.Collections.Generic.Dictionary`2_ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t032C2EC84572334119CDB2711D009811E4B5B227 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C, ___buckets_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C, ___entries_1)); }
	inline EntryU5BU5D_t122B21BBDE6829AA331FE13F9994475CA1C5A4BA* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t122B21BBDE6829AA331FE13F9994475CA1C5A4BA** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t122B21BBDE6829AA331FE13F9994475CA1C5A4BA* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C, ___keys_7)); }
	inline KeyCollection_tC4A836D57C1B360FAFE4C5CFDE6192258B4450BA * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_tC4A836D57C1B360FAFE4C5CFDE6192258B4450BA ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_tC4A836D57C1B360FAFE4C5CFDE6192258B4450BA * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C, ___values_8)); }
	inline ValueCollection_t032C2EC84572334119CDB2711D009811E4B5B227 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t032C2EC84572334119CDB2711D009811E4B5B227 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t032C2EC84572334119CDB2711D009811E4B5B227 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Action>
struct  List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ActionU5BU5D_tBFE34E95F8D5EAA828CD01E01994E4574F55958B* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46, ____items_1)); }
	inline ActionU5BU5D_tBFE34E95F8D5EAA828CD01E01994E4574F55958B* get__items_1() const { return ____items_1; }
	inline ActionU5BU5D_tBFE34E95F8D5EAA828CD01E01994E4574F55958B** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ActionU5BU5D_tBFE34E95F8D5EAA828CD01E01994E4574F55958B* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ActionU5BU5D_tBFE34E95F8D5EAA828CD01E01994E4574F55958B* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46_StaticFields, ____emptyArray_5)); }
	inline ActionU5BU5D_tBFE34E95F8D5EAA828CD01E01994E4574F55958B* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ActionU5BU5D_tBFE34E95F8D5EAA828CD01E01994E4574F55958B** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ActionU5BU5D_tBFE34E95F8D5EAA828CD01E01994E4574F55958B* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct  List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____items_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.String>
struct  List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____items_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_StaticFields, ____emptyArray_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__emptyArray_5() const { return ____emptyArray_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.Text.StringBuilder
struct  StringBuilder_t  : public RuntimeObject
{
public:
	// System.Char[] System.Text.StringBuilder::m_ChunkChars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___m_ChunkChars_0;
	// System.Text.StringBuilder System.Text.StringBuilder::m_ChunkPrevious
	StringBuilder_t * ___m_ChunkPrevious_1;
	// System.Int32 System.Text.StringBuilder::m_ChunkLength
	int32_t ___m_ChunkLength_2;
	// System.Int32 System.Text.StringBuilder::m_ChunkOffset
	int32_t ___m_ChunkOffset_3;
	// System.Int32 System.Text.StringBuilder::m_MaxCapacity
	int32_t ___m_MaxCapacity_4;

public:
	inline static int32_t get_offset_of_m_ChunkChars_0() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkChars_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_m_ChunkChars_0() const { return ___m_ChunkChars_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_m_ChunkChars_0() { return &___m_ChunkChars_0; }
	inline void set_m_ChunkChars_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___m_ChunkChars_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChunkChars_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChunkPrevious_1() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkPrevious_1)); }
	inline StringBuilder_t * get_m_ChunkPrevious_1() const { return ___m_ChunkPrevious_1; }
	inline StringBuilder_t ** get_address_of_m_ChunkPrevious_1() { return &___m_ChunkPrevious_1; }
	inline void set_m_ChunkPrevious_1(StringBuilder_t * value)
	{
		___m_ChunkPrevious_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChunkPrevious_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChunkLength_2() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkLength_2)); }
	inline int32_t get_m_ChunkLength_2() const { return ___m_ChunkLength_2; }
	inline int32_t* get_address_of_m_ChunkLength_2() { return &___m_ChunkLength_2; }
	inline void set_m_ChunkLength_2(int32_t value)
	{
		___m_ChunkLength_2 = value;
	}

	inline static int32_t get_offset_of_m_ChunkOffset_3() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkOffset_3)); }
	inline int32_t get_m_ChunkOffset_3() const { return ___m_ChunkOffset_3; }
	inline int32_t* get_address_of_m_ChunkOffset_3() { return &___m_ChunkOffset_3; }
	inline void set_m_ChunkOffset_3(int32_t value)
	{
		___m_ChunkOffset_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_MaxCapacity_4)); }
	inline int32_t get_m_MaxCapacity_4() const { return ___m_MaxCapacity_4; }
	inline int32_t* get_address_of_m_MaxCapacity_4() { return &___m_MaxCapacity_4; }
	inline void set_m_MaxCapacity_4(int32_t value)
	{
		___m_MaxCapacity_4 = value;
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// AppleAuth.AppleAuthQuickLoginArgs
struct  AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A 
{
public:
	// System.String AppleAuth.AppleAuthQuickLoginArgs::Nonce
	String_t* ___Nonce_0;
	// System.String AppleAuth.AppleAuthQuickLoginArgs::State
	String_t* ___State_1;

public:
	inline static int32_t get_offset_of_Nonce_0() { return static_cast<int32_t>(offsetof(AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A, ___Nonce_0)); }
	inline String_t* get_Nonce_0() const { return ___Nonce_0; }
	inline String_t** get_address_of_Nonce_0() { return &___Nonce_0; }
	inline void set_Nonce_0(String_t* value)
	{
		___Nonce_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Nonce_0), (void*)value);
	}

	inline static int32_t get_offset_of_State_1() { return static_cast<int32_t>(offsetof(AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A, ___State_1)); }
	inline String_t* get_State_1() const { return ___State_1; }
	inline String_t** get_address_of_State_1() { return &___State_1; }
	inline void set_State_1(String_t* value)
	{
		___State_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___State_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of AppleAuth.AppleAuthQuickLoginArgs
struct AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A_marshaled_pinvoke
{
	char* ___Nonce_0;
	char* ___State_1;
};
// Native definition for COM marshalling of AppleAuth.AppleAuthQuickLoginArgs
struct AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A_marshaled_com
{
	Il2CppChar* ___Nonce_0;
	Il2CppChar* ___State_1;
};

// AppleAuth.Native.FullPersonName
struct  FullPersonName_t2AC7C2FEA68179788173459B7B32BECF0C41B875  : public PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0
{
public:
	// System.Boolean AppleAuth.Native.FullPersonName::_hasPhoneticRepresentation
	bool ____hasPhoneticRepresentation_6;
	// AppleAuth.Native.PersonName AppleAuth.Native.FullPersonName::_phoneticRepresentation
	PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0 * ____phoneticRepresentation_7;

public:
	inline static int32_t get_offset_of__hasPhoneticRepresentation_6() { return static_cast<int32_t>(offsetof(FullPersonName_t2AC7C2FEA68179788173459B7B32BECF0C41B875, ____hasPhoneticRepresentation_6)); }
	inline bool get__hasPhoneticRepresentation_6() const { return ____hasPhoneticRepresentation_6; }
	inline bool* get_address_of__hasPhoneticRepresentation_6() { return &____hasPhoneticRepresentation_6; }
	inline void set__hasPhoneticRepresentation_6(bool value)
	{
		____hasPhoneticRepresentation_6 = value;
	}

	inline static int32_t get_offset_of__phoneticRepresentation_7() { return static_cast<int32_t>(offsetof(FullPersonName_t2AC7C2FEA68179788173459B7B32BECF0C41B875, ____phoneticRepresentation_7)); }
	inline PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0 * get__phoneticRepresentation_7() const { return ____phoneticRepresentation_7; }
	inline PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0 ** get_address_of__phoneticRepresentation_7() { return &____phoneticRepresentation_7; }
	inline void set__phoneticRepresentation_7(PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0 * value)
	{
		____phoneticRepresentation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____phoneticRepresentation_7), (void*)value);
	}
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct  Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.UInt32
struct  UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// AppleAuth.Enums.AuthorizationErrorCode
struct  AuthorizationErrorCode_t0FFD9BA03D3349238191A90F27789A3DD249672F 
{
public:
	// System.Int32 AppleAuth.Enums.AuthorizationErrorCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AuthorizationErrorCode_t0FFD9BA03D3349238191A90F27789A3DD249672F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// AppleAuth.Enums.CredentialState
struct  CredentialState_t67035148011C9DB2CE800949AA1DA32AEB22D089 
{
public:
	// System.Int32 AppleAuth.Enums.CredentialState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CredentialState_t67035148011C9DB2CE800949AA1DA32AEB22D089, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// AppleAuth.Enums.LoginOptions
struct  LoginOptions_t0143F76C42AF6F27BB47BC6AC903FAED0C403519 
{
public:
	// System.Int32 AppleAuth.Enums.LoginOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoginOptions_t0143F76C42AF6F27BB47BC6AC903FAED0C403519, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// AppleAuth.Enums.PersonNameFormatterStyle
struct  PersonNameFormatterStyle_tF5E845067A91D379772C2926040E34D24C90669A 
{
public:
	// System.Int32 AppleAuth.Enums.PersonNameFormatterStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PersonNameFormatterStyle_tF5E845067A91D379772C2926040E34D24C90669A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// AppleAuth.Enums.RealUserStatus
struct  RealUserStatus_t3439EB7A9F2D88C6F5FF8D3F571A4B8C92EF596D 
{
public:
	// System.Int32 AppleAuth.Enums.RealUserStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RealUserStatus_t3439EB7A9F2D88C6F5FF8D3F571A4B8C92EF596D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.Int32Enum
struct  Int32Enum_t6312CE4586C17FE2E2E513D2E7655B574F10FDCD 
{
public:
	// System.Int32 System.Int32Enum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Int32Enum_t6312CE4586C17FE2E2E513D2E7655B574F10FDCD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// AppleAuth.AppleAuthLoginArgs
struct  AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22 
{
public:
	// AppleAuth.Enums.LoginOptions AppleAuth.AppleAuthLoginArgs::Options
	int32_t ___Options_0;
	// System.String AppleAuth.AppleAuthLoginArgs::Nonce
	String_t* ___Nonce_1;
	// System.String AppleAuth.AppleAuthLoginArgs::State
	String_t* ___State_2;

public:
	inline static int32_t get_offset_of_Options_0() { return static_cast<int32_t>(offsetof(AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22, ___Options_0)); }
	inline int32_t get_Options_0() const { return ___Options_0; }
	inline int32_t* get_address_of_Options_0() { return &___Options_0; }
	inline void set_Options_0(int32_t value)
	{
		___Options_0 = value;
	}

	inline static int32_t get_offset_of_Nonce_1() { return static_cast<int32_t>(offsetof(AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22, ___Nonce_1)); }
	inline String_t* get_Nonce_1() const { return ___Nonce_1; }
	inline String_t** get_address_of_Nonce_1() { return &___Nonce_1; }
	inline void set_Nonce_1(String_t* value)
	{
		___Nonce_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Nonce_1), (void*)value);
	}

	inline static int32_t get_offset_of_State_2() { return static_cast<int32_t>(offsetof(AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22, ___State_2)); }
	inline String_t* get_State_2() const { return ___State_2; }
	inline String_t** get_address_of_State_2() { return &___State_2; }
	inline void set_State_2(String_t* value)
	{
		___State_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___State_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of AppleAuth.AppleAuthLoginArgs
struct AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22_marshaled_pinvoke
{
	int32_t ___Options_0;
	char* ___Nonce_1;
	char* ___State_2;
};
// Native definition for COM marshalling of AppleAuth.AppleAuthLoginArgs
struct AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22_marshaled_com
{
	int32_t ___Options_0;
	Il2CppChar* ___Nonce_1;
	Il2CppChar* ___State_2;
};

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// AppleAuth.AppleAuthManager_PInvoke_NativeMessageHandlerCallbackDelegate
struct  NativeMessageHandlerCallbackDelegate_t791982D0270309BB7AEFD64EF9FFA0F7DBFE4A14  : public MulticastDelegate_t
{
public:

public:
};


// System.Action
struct  Action_t591D2A86165F896B4B800BB5C25CE18672A55579  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`1<AppleAuth.Enums.CredentialState>
struct  Action_1_tF472B580CE5012B6F8159B65F503A7AD17BDAC88  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`1<AppleAuth.Interfaces.IAppleError>
struct  Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`1<AppleAuth.Interfaces.ICredential>
struct  Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`1<System.String>
struct  Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0  : public MulticastDelegate_t
{
public:

public:
};


// System.AsyncCallback
struct  AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::Invoke(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1_Invoke_mB86FC1B303E77C41ED0E94FC3592A9CF8DA571D5_gshared (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Void System.Action`1<System.Int32Enum>::Invoke(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1_Invoke_m4DD49F027613D2A2A892F880C23BDFB6573E5C93_gshared (Action_1_tABA1E3BFA092E3309A0ECC53722E4F9826DCE983 * __this, int32_t ___obj0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::TryGetValue(!0,!1&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_m8B422BD91F7A9B7BB719295D3FA0D0DFC6C32048_gshared (Dictionary_2_tA04C0C5C8CC1B93793DBB27FBA9C324693B6755F * __this, uint32_t ___key0, RuntimeObject ** ___value1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::Remove(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_Remove_mBBF21C84D264E876D572E4BC4E48A9A02BE96E6E_gshared (Dictionary_2_tA04C0C5C8CC1B93793DBB27FBA9C324693B6755F * __this, uint32_t ___key0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_RemoveAt_m50D861A91F15E3169935F47FB656C3ED5486E74E_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::Add(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_mFED9C128850F4D30830C563875C93B0BB082269F_gshared (Dictionary_2_tA04C0C5C8CC1B93793DBB27FBA9C324693B6755F * __this, uint32_t ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::ContainsKey(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_ContainsKey_m6F077DB25FAE1DC0B6713986DB1364D165E22FAB_gshared (Dictionary_2_tA04C0C5C8CC1B93793DBB27FBA9C324693B6755F * __this, uint32_t ___key0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m77846496C26983270E51FB59FF324AA096F2EB57_gshared (Dictionary_2_tA04C0C5C8CC1B93793DBB27FBA9C324693B6755F * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<System.Object>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* List_1_ToArray_m801D4DEF3587F60F463F04EEABE5CBE711FE5612_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void AppleAuth.Native.SerializationTools::FixSerializationForArray<System.Object>(T[]&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializationTools_FixSerializationForArray_TisRuntimeObject_m8110B15789387DD72517BA8ED21F2ED3F76298A8_gshared (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** ___originalArray0, const RuntimeMethod* method);
// System.Void AppleAuth.Native.SerializationTools::FixSerializationForObject<System.Object>(T&,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializationTools_FixSerializationForObject_TisRuntimeObject_m69D0A58810708BD4B38F526550C2F8F2C436CD55_gshared (RuntimeObject ** ___originalObject0, bool ___hasOriginalObject1, const RuntimeMethod* method);
// System.Void AppleAuth.Native.SerializationTools::FixSerializationForObject<System.Int32>(T&,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializationTools_FixSerializationForObject_TisInt32_t585191389E07734F19F3156FF88FB3EF4800D102_mF97F93CA0140582AB28C2FBBF667F05E87F95449_gshared (int32_t* ___originalObject0, bool ___hasOriginalObject1, const RuntimeMethod* method);
// !!0 UnityEngine.JsonUtility::FromJson<System.Object>(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * JsonUtility_FromJson_TisRuntimeObject_m38BFFDE45556DDE756B8EDCA5E0775A8320E1296_gshared (String_t* ___json0, const RuntimeMethod* method);

// System.Void AppleAuth.AppleAuthLoginArgs::.ctor(AppleAuth.Enums.LoginOptions,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppleAuthLoginArgs__ctor_m7E3D2AA118703B59AEDF336421336EE1B6545D54 (AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22 * __this, int32_t ___options0, String_t* ___nonce1, String_t* ___state2, const RuntimeMethod* method);
// System.Void AppleAuth.AppleAuthManager/PInvoke::AppleAuth_LogMessage(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PInvoke_AppleAuth_LogMessage_m9620530B053E06324B4C16569F6605A7BACACEC9 (String_t* ___messageCStr0, const RuntimeMethod* method);
// System.Boolean AppleAuth.AppleAuthManager/PInvoke::AppleAuth_IsCurrentPlatformSupported()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PInvoke_AppleAuth_IsCurrentPlatformSupported_mF4AB0EB44BE680301E902157841A2E8BE29B9998 (const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void AppleAuth.AppleAuthManager::QuickLogin(AppleAuth.AppleAuthQuickLoginArgs,System.Action`1<AppleAuth.Interfaces.ICredential>,System.Action`1<AppleAuth.Interfaces.IAppleError>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppleAuthManager_QuickLogin_m5BE7B5C2C7728FDA2B0D5EB11862FD07762A4AC6 (AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * __this, AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A  ___quickLoginArgs0, Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * ___successCallback1, Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * ___errorCallback2, const RuntimeMethod* method);
// System.Void AppleAuth.AppleAuthManager/<>c__DisplayClass7_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass7_0__ctor_mFE19321C75D3BBE3F40E7B55C1F2D6D2194B34B1 (U3CU3Ec__DisplayClass7_0_tCF330B8821408AA5001117209552A8D55BB2229A * __this, const RuntimeMethod* method);
// System.Void System.Action`1<System.String>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared)(__this, ___object0, ___method1, method);
}
// System.UInt32 AppleAuth.AppleAuthManager/CallbackHandler::AddMessageCallback(System.Boolean,System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t CallbackHandler_AddMessageCallback_m0B95A78A2EB0A42E2D6EE2186A042C3DC406C65D (bool ___isSingleUse0, Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___messageCallback1, const RuntimeMethod* method);
// System.Void AppleAuth.AppleAuthManager/PInvoke::AppleAuth_QuickLogin(System.UInt32,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PInvoke_AppleAuth_QuickLogin_m2BDD63FE86A5B848DD047E60AFBD031AB00060A4 (uint32_t ___requestId0, String_t* ___nonceCStr1, String_t* ___stateCStr2, const RuntimeMethod* method);
// System.Void AppleAuth.AppleAuthManager::LoginWithAppleId(AppleAuth.AppleAuthLoginArgs,System.Action`1<AppleAuth.Interfaces.ICredential>,System.Action`1<AppleAuth.Interfaces.IAppleError>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppleAuthManager_LoginWithAppleId_m3251131E6071E0C1D505B6CAA2DE945C8C5EA099 (AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * __this, AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22  ___loginArgs0, Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * ___successCallback1, Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * ___errorCallback2, const RuntimeMethod* method);
// System.Void AppleAuth.AppleAuthManager/<>c__DisplayClass9_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass9_0__ctor_mECE874027375E819AF084618F941EC8554006642 (U3CU3Ec__DisplayClass9_0_tDC78461B5F5A4E5E4BB40BB960C7C2BE05321689 * __this, const RuntimeMethod* method);
// System.Void AppleAuth.AppleAuthManager/PInvoke::AppleAuth_LoginWithAppleId(System.UInt32,System.Int32,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PInvoke_AppleAuth_LoginWithAppleId_m955A59F158F91E47BC0A0950F24B1564DF623E9E (uint32_t ___requestId0, int32_t ___loginOptions1, String_t* ___nonceCStr2, String_t* ___stateCStr3, const RuntimeMethod* method);
// System.Void AppleAuth.AppleAuthManager/<>c__DisplayClass10_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass10_0__ctor_m1A51C03108292B70E58D92C883CFA153B037FF47 (U3CU3Ec__DisplayClass10_0_t778A5E2C062F0F876B04E713630BB330A1511837 * __this, const RuntimeMethod* method);
// System.Void AppleAuth.AppleAuthManager/PInvoke::AppleAuth_GetCredentialState(System.UInt32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PInvoke_AppleAuth_GetCredentialState_mB2184459930094EC4BF2948375B4D18B069A2D28 (uint32_t ___requestId0, String_t* ___userId1, const RuntimeMethod* method);
// System.Void AppleAuth.AppleAuthManager/CallbackHandler::remove_NativeCredentialsRevoked(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CallbackHandler_remove_NativeCredentialsRevoked_m8E5E2588E004C4838F26030762E98561A8FBBAE3 (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___value0, const RuntimeMethod* method);
// System.Void AppleAuth.AppleAuthManager/CallbackHandler::add_NativeCredentialsRevoked(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CallbackHandler_add_NativeCredentialsRevoked_mAFC47A0B52BC9DCACB3C8B36748CB8FB54CEC208 (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___value0, const RuntimeMethod* method);
// System.Void AppleAuth.AppleAuthManager/CallbackHandler::ExecutePendingCallbacks()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CallbackHandler_ExecutePendingCallbacks_m5AD24B967D8C51AECFE59C5BF02342F57952AC3C (const RuntimeMethod* method);
// System.Void System.Action`1<AppleAuth.Interfaces.IAppleError>::Invoke(!0)
inline void Action_1_Invoke_m7D166367CD49F79461F74C532A8286A491F75329 (Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * __this, RuntimeObject* ___obj0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 *, RuntimeObject*, const RuntimeMethod*))Action_1_Invoke_mB86FC1B303E77C41ED0E94FC3592A9CF8DA571D5_gshared)(__this, ___obj0, method);
}
// System.Void System.Action`1<AppleAuth.Enums.CredentialState>::Invoke(!0)
inline void Action_1_Invoke_m7EAD69A6377B20A71F043C663094490F99E5E2AC (Action_1_tF472B580CE5012B6F8159B65F503A7AD17BDAC88 * __this, int32_t ___obj0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_tF472B580CE5012B6F8159B65F503A7AD17BDAC88 *, int32_t, const RuntimeMethod*))Action_1_Invoke_m4DD49F027613D2A2A892F880C23BDFB6573E5C93_gshared)(__this, ___obj0, method);
}
// System.Void System.Action`1<AppleAuth.Interfaces.ICredential>::Invoke(!0)
inline void Action_1_Invoke_m78B725AD74CD7728202CC5ACC1E6E0F114CD02DE (Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * __this, RuntimeObject* ___obj0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F *, RuntimeObject*, const RuntimeMethod*))Action_1_Invoke_mB86FC1B303E77C41ED0E94FC3592A9CF8DA571D5_gshared)(__this, ___obj0, method);
}
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1 (Delegate_t * ___a0, Delegate_t * ___b1, const RuntimeMethod* method);
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D (Delegate_t * ___source0, Delegate_t * ___value1, const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Enter(System.Object,System.Boolean&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Enter_mC5B353DD83A0B0155DF6FBCC4DF5A580C25534C5 (RuntimeObject * ___obj0, bool* ___lockTaken1, const RuntimeMethod* method);
// System.Void AppleAuth.AppleAuthManager/PInvoke::AppleAuth_RegisterCredentialsRevokedCallbackId(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PInvoke_AppleAuth_RegisterCredentialsRevokedCallbackId_m65B9651E2B15431DD6289C68DD8787F9EDAF8EF8 (uint32_t ___callbackId0, const RuntimeMethod* method);
// System.Void AppleAuth.AppleAuthManager/CallbackHandler::add__nativeCredentialsRevoked(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CallbackHandler_add__nativeCredentialsRevoked_m76EF11A8538E68C4EF86007907748718B46C0A2D (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___value0, const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Exit(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2 (RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Void AppleAuth.AppleAuthManager/CallbackHandler::remove__nativeCredentialsRevoked(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CallbackHandler_remove__nativeCredentialsRevoked_mD4A0DE080A0F2633EBCF99808C748D76F21F622A (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___value0, const RuntimeMethod* method);
// System.Void AppleAuth.AppleAuthManager/CallbackHandler::RemoveMessageCallback(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CallbackHandler_RemoveMessageCallback_m54E5CE8453DA1146E7E132B646E96FB964C72C7B (uint32_t ___requestId0, const RuntimeMethod* method);
// System.Void AppleAuth.AppleAuthManager/CallbackHandler/<>c__DisplayClass14_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass14_0__ctor_m955A36C7D7F760FD5656A05D0BF5CB140DF78210 (U3CU3Ec__DisplayClass14_0_t0743B54A2D147CB75027EE3F324C3B375DB67067 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,AppleAuth.AppleAuthManager/CallbackHandler/Entry>::TryGetValue(!0,!1&)
inline bool Dictionary_2_TryGetValue_mCFF58E001B1BF5E1ECA4BBC7ECED52321A3DBA2D (Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C * __this, uint32_t ___key0, Entry_t6FA80886BFD1655BEB628D855A2FAD715839F6BB ** ___value1, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C *, uint32_t, Entry_t6FA80886BFD1655BEB628D855A2FAD715839F6BB **, const RuntimeMethod*))Dictionary_2_TryGetValue_m8B422BD91F7A9B7BB719295D3FA0D0DFC6C32048_gshared)(__this, ___key0, ___value1, method);
}
// System.Void AppleAuth.AppleAuthManager/CallbackHandler/<>c__DisplayClass14_1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass14_1__ctor_mAEFE6646216AE52125F79843CDF4695B36111113 (U3CU3Ec__DisplayClass14_1_tED700A7605A9EEFA916745644049F0D15DE58F1D * __this, const RuntimeMethod* method);
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action__ctor_m570E96B2A0C48BC1DC6788460316191F24572760 (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Action>::Add(!0)
inline void List_1_Add_m019DF84155C09D3979798C84CD70DD28E146025B (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * __this, Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 *, Action_t591D2A86165F896B4B800BB5C25CE18672A55579 *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, ___item0, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,AppleAuth.AppleAuthManager/CallbackHandler/Entry>::Remove(!0)
inline bool Dictionary_2_Remove_m78D417809EE5B205B4A02CF24F81E4096BB6EC3D (Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C * __this, uint32_t ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C *, uint32_t, const RuntimeMethod*))Dictionary_2_Remove_mBBF21C84D264E876D572E4BC4E48A9A02BE96E6E_gshared)(__this, ___key0, method);
}
// !0 System.Collections.Generic.List`1<System.Action>::get_Item(System.Int32)
inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * List_1_get_Item_m900B85866FD2558E6CE2725E07AF693C29C19731_inline (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * (*) (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline)(__this, ___index0, method);
}
// System.Void System.Collections.Generic.List`1<System.Action>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_m281DBD9DDEBE448DB0268959927756D4BD4E6FD6 (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m50D861A91F15E3169935F47FB656C3ED5486E74E_gshared)(__this, ___index0, method);
}
// System.Void System.Action::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_Invoke_mC8D676E5DDF967EC5D23DD0E96FB52AA499817FD (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Action>::get_Count()
inline int32_t List_1_get_Count_m3986FAACE5FCFF72B7601E768375D65E038FDEDB_inline (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline)(__this, method);
}
// System.Void AppleAuth.AppleAuthManager/PInvoke/NativeMessageHandlerCallbackDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeMessageHandlerCallbackDelegate__ctor_mA999B2608865CF7F48403396ED87EEDDDB0BD602 (NativeMessageHandlerCallbackDelegate_t791982D0270309BB7AEFD64EF9FFA0F7DBFE4A14 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void AppleAuth.AppleAuthManager/PInvoke::AppleAuth_SetupNativeMessageHandlerCallback(AppleAuth.AppleAuthManager/PInvoke/NativeMessageHandlerCallbackDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PInvoke_AppleAuth_SetupNativeMessageHandlerCallback_m2D140AC7EC65E41051360B692A205C2B1345CA90 (NativeMessageHandlerCallbackDelegate_t791982D0270309BB7AEFD64EF9FFA0F7DBFE4A14 * ___callback0, const RuntimeMethod* method);
// System.Void System.Exception::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception__ctor_m89BADFF36C3B170013878726E07729D51AA9FBE0 (Exception_t * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void AppleAuth.AppleAuthManager/CallbackHandler/Entry::.ctor(System.Boolean,System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Entry__ctor_m211294F29613E18A310B5A2CBF5DB141F0C5164F (Entry_t6FA80886BFD1655BEB628D855A2FAD715839F6BB * __this, bool ___isSingleUseCallback0, Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___messageCallback1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,AppleAuth.AppleAuthManager/CallbackHandler/Entry>::Add(!0,!1)
inline void Dictionary_2_Add_mEB4CC4CAD43E8C0BA470A4F7FDB71BB47559F441 (Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C * __this, uint32_t ___key0, Entry_t6FA80886BFD1655BEB628D855A2FAD715839F6BB * ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C *, uint32_t, Entry_t6FA80886BFD1655BEB628D855A2FAD715839F6BB *, const RuntimeMethod*))Dictionary_2_Add_mFED9C128850F4D30830C563875C93B0BB082269F_gshared)(__this, ___key0, ___value1, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<System.UInt32,AppleAuth.AppleAuthManager/CallbackHandler/Entry>::ContainsKey(!0)
inline bool Dictionary_2_ContainsKey_mD9907E4F897C40763F8340258AEDDD9FD3F1FCE9 (Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C * __this, uint32_t ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C *, uint32_t, const RuntimeMethod*))Dictionary_2_ContainsKey_m6F077DB25FAE1DC0B6713986DB1364D165E22FAB_gshared)(__this, ___key0, method);
}
// System.String System.String::Concat(System.Object,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m2E1F71C491D2429CC80A28745488FEA947BB7AAC (RuntimeObject * ___arg00, RuntimeObject * ___arg11, RuntimeObject * ___arg22, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.UInt32,AppleAuth.AppleAuthManager/CallbackHandler/Entry>::.ctor()
inline void Dictionary_2__ctor_mFE529F4016E0E4A41B5B8B456DEB0F58BC6F7E95 (Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C *, const RuntimeMethod*))Dictionary_2__ctor_m77846496C26983270E51FB59FF324AA096F2EB57_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Action>::.ctor()
inline void List_1__ctor_m875F64A655E35C2D021756D570AD9C3FBCC2E968 (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void AppleAuth.AppleAuthManager/CallbackHandler/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mBD4C3476C866706F3C68BD2C7B862E65E3CFB7D6 (U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<System.String>::Invoke(!0)
inline void Action_1_Invoke_m6328F763431ED2ACDDB6ED8F0FDEA6194403E79F (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * __this, String_t* ___obj0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *, String_t*, const RuntimeMethod*))Action_1_Invoke_mB86FC1B303E77C41ED0E94FC3592A9CF8DA571D5_gshared)(__this, ___obj0, method);
}
// System.Void AppleAuth.AppleAuthManager/PInvoke::NativeMessageHandlerCallback(System.UInt32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PInvoke_NativeMessageHandlerCallback_m5D5E84BB3C9785BE4384D272156E122275A93DE4 (uint32_t ___requestId0, String_t* ___payload1, const RuntimeMethod* method);
// System.Void AppleAuth.AppleAuthManager/CallbackHandler::ScheduleCallback(System.UInt32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CallbackHandler_ScheduleCallback_m085D44C88C7A14F9F29E1170A8FCA2BE13B857F5 (uint32_t ___requestId0, String_t* ___payload1, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495 (RuntimeObject * ___arg00, RuntimeObject * ___arg11, const RuntimeMethod* method);
// System.Void System.Console::WriteLine(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Console_WriteLine_mA5F7E391799514350980A0DE16983383542CA820 (String_t* ___value0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Void AppleAuth.AppleAuthQuickLoginArgs::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppleAuthQuickLoginArgs__ctor_m002ADB6747DBEF52120701B85E6E69C392680819 (AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A * __this, String_t* ___nonce0, String_t* ___state1, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6 (RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ___handle0, const RuntimeMethod* method);
// System.Boolean System.Enum::IsDefined(System.Type,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enum_IsDefined_mA573B15329CA2AA7C59367D514D2927FC66217E2 (Type_t * ___enumType0, RuntimeObject * ___value1, const RuntimeMethod* method);
// System.String AppleAuth.Extensions.PersonNameExtensions::JsonStringForPersonName(AppleAuth.Interfaces.IPersonName)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PersonNameExtensions_JsonStringForPersonName_m60F3E8CB4AEEFF4A5741EAB787BB034F9F71672B (RuntimeObject* ___personName0, const RuntimeMethod* method);
// System.String AppleAuth.Extensions.PersonNameExtensions/PInvoke::AppleAuth_GetPersonNameUsingFormatter(System.String,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PInvoke_AppleAuth_GetPersonNameUsingFormatter_m579639B679D05DD5E952F09B075C0689E8879762 (String_t* ___payload0, int32_t ___style1, bool ___usePhoneticRepresentation2, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
inline void List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06 (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229 (String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.String>::Add(!0)
inline void List_1_Add_mA348FA1140766465189459D25B01EB179001DE83 (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * __this, String_t* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *, String_t*, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, ___item0, method);
}
// !0[] System.Collections.Generic.List`1<System.String>::ToArray()
inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* List_1_ToArray_m9DD19D800AE6D84ED0729D5D97CAF84DF317DD38 (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * __this, const RuntimeMethod* method)
{
	return ((  StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* (*) (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *, const RuntimeMethod*))List_1_ToArray_m801D4DEF3587F60F463F04EEABE5CBE711FE5612_gshared)(__this, method);
}
// System.String System.String::Join(System.String,System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Join_m49371BED70248F0FCE970CB4F2E39E9A688AAFA4 (String_t* ___separator0, StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___value1, const RuntimeMethod* method);
// System.Void System.Text.StringBuilder::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringBuilder__ctor_mF928376F82E8C8FF3C11842C562DB8CF28B2735E (StringBuilder_t * __this, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260 (StringBuilder_t * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void AppleAuth.Extensions.PersonNameExtensions::TryAddKeyValue(System.String,System.String,System.String,System.Text.StringBuilder)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersonNameExtensions_TryAddKeyValue_m3BBD8554A10DC6B6D8BD30500D245EFEF1854C40 (String_t* ___format0, String_t* ___key1, String_t* ___value2, StringBuilder_t * ___stringBuilder3, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::AppendFormat(System.String,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_AppendFormat_m9DBA7709F546159ABC85BA341965305AB044D1B7 (StringBuilder_t * __this, String_t* ___format0, RuntimeObject * ___arg01, RuntimeObject * ___arg12, const RuntimeMethod* method);
// System.Void AppleAuth.Native.SerializationTools::FixSerializationForString(System.String&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializationTools_FixSerializationForString_m8171A047B8FCDC62B846791D70C2DA21418D8650 (String_t** ___originalString0, const RuntimeMethod* method);
// System.Void AppleAuth.Native.SerializationTools::FixSerializationForArray<System.String>(T[]&)
inline void SerializationTools_FixSerializationForArray_TisString_t_m163AA34D516D317C89666B944EEDE14BF8DCF53D (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** ___originalArray0, const RuntimeMethod* method)
{
	((  void (*) (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E**, const RuntimeMethod*))SerializationTools_FixSerializationForArray_TisRuntimeObject_m8110B15789387DD72517BA8ED21F2ED3F76298A8_gshared)(___originalArray0, method);
}
// System.Void AppleAuth.Native.SerializationTools::FixSerializationForObject<AppleAuth.Native.FullPersonName>(T&,System.Boolean)
inline void SerializationTools_FixSerializationForObject_TisFullPersonName_t2AC7C2FEA68179788173459B7B32BECF0C41B875_m0CD7820D8502A3091B822BF4899CDF95314369E4 (FullPersonName_t2AC7C2FEA68179788173459B7B32BECF0C41B875 ** ___originalObject0, bool ___hasOriginalObject1, const RuntimeMethod* method)
{
	((  void (*) (FullPersonName_t2AC7C2FEA68179788173459B7B32BECF0C41B875 **, bool, const RuntimeMethod*))SerializationTools_FixSerializationForObject_TisRuntimeObject_m69D0A58810708BD4B38F526550C2F8F2C436CD55_gshared)(___originalObject0, ___hasOriginalObject1, method);
}
// System.Byte[] AppleAuth.Native.SerializationTools::GetBytesFromBase64String(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* SerializationTools_GetBytesFromBase64String_m861B8FE0EC904AC192A9D2DD8D5CC6B51A7F2CAD (String_t* ___base64String0, String_t* ___fieldName1, const RuntimeMethod* method);
// System.Void AppleAuth.Native.SerializationTools::FixSerializationForObject<System.Int32>(T&,System.Boolean)
inline void SerializationTools_FixSerializationForObject_TisInt32_t585191389E07734F19F3156FF88FB3EF4800D102_mF97F93CA0140582AB28C2FBBF667F05E87F95449 (int32_t* ___originalObject0, bool ___hasOriginalObject1, const RuntimeMethod* method)
{
	((  void (*) (int32_t*, bool, const RuntimeMethod*))SerializationTools_FixSerializationForObject_TisInt32_t585191389E07734F19F3156FF88FB3EF4800D102_mF97F93CA0140582AB28C2FBBF667F05E87F95449_gshared)(___originalObject0, ___hasOriginalObject1, method);
}
// System.Void AppleAuth.Native.SerializationTools::FixSerializationForObject<AppleAuth.Native.AppleError>(T&,System.Boolean)
inline void SerializationTools_FixSerializationForObject_TisAppleError_t157640890ACABCEB007334DB77D1279DCB27223E_m5AE3CC0F4C6F2F346DFDECDE2B62C98BBD65DCE7 (AppleError_t157640890ACABCEB007334DB77D1279DCB27223E ** ___originalObject0, bool ___hasOriginalObject1, const RuntimeMethod* method)
{
	((  void (*) (AppleError_t157640890ACABCEB007334DB77D1279DCB27223E **, bool, const RuntimeMethod*))SerializationTools_FixSerializationForObject_TisRuntimeObject_m69D0A58810708BD4B38F526550C2F8F2C436CD55_gshared)(___originalObject0, ___hasOriginalObject1, method);
}
// System.Void AppleAuth.Native.PersonName::OnAfterDeserialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersonName_OnAfterDeserialize_m32D4307910B89D3E33FBBE420CBA93641F8F0ACD (PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0 * __this, const RuntimeMethod* method);
// System.Void AppleAuth.Native.SerializationTools::FixSerializationForObject<AppleAuth.Native.PersonName>(T&,System.Boolean)
inline void SerializationTools_FixSerializationForObject_TisPersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0_mA295ACB04AD3B5BDBC01ECD5D1F9280893829245 (PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0 ** ___originalObject0, bool ___hasOriginalObject1, const RuntimeMethod* method)
{
	((  void (*) (PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0 **, bool, const RuntimeMethod*))SerializationTools_FixSerializationForObject_TisRuntimeObject_m69D0A58810708BD4B38F526550C2F8F2C436CD55_gshared)(___originalObject0, ___hasOriginalObject1, method);
}
// System.Void AppleAuth.Native.PersonName::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersonName__ctor_m320D2DECA0176EE689309F89168BC54311BDD69C (PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0 * __this, const RuntimeMethod* method);
// System.Void AppleAuth.Native.SerializationTools::FixSerializationForObject<AppleAuth.Native.AppleIDCredential>(T&,System.Boolean)
inline void SerializationTools_FixSerializationForObject_TisAppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0_mE518B6B97BEE8670FFFAE3C4BA679D63F7AB29B0 (AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0 ** ___originalObject0, bool ___hasOriginalObject1, const RuntimeMethod* method)
{
	((  void (*) (AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0 **, bool, const RuntimeMethod*))SerializationTools_FixSerializationForObject_TisRuntimeObject_m69D0A58810708BD4B38F526550C2F8F2C436CD55_gshared)(___originalObject0, ___hasOriginalObject1, method);
}
// System.Void AppleAuth.Native.SerializationTools::FixSerializationForObject<AppleAuth.Native.PasswordCredential>(T&,System.Boolean)
inline void SerializationTools_FixSerializationForObject_TisPasswordCredential_t64C056A6AE5B8356D435709C776795D925E35C40_m2F6D3902FF446FBBEBAC5665578E2EF37BD51601 (PasswordCredential_t64C056A6AE5B8356D435709C776795D925E35C40 ** ___originalObject0, bool ___hasOriginalObject1, const RuntimeMethod* method)
{
	((  void (*) (PasswordCredential_t64C056A6AE5B8356D435709C776795D925E35C40 **, bool, const RuntimeMethod*))SerializationTools_FixSerializationForObject_TisRuntimeObject_m69D0A58810708BD4B38F526550C2F8F2C436CD55_gshared)(___originalObject0, ___hasOriginalObject1, method);
}
// !!0 UnityEngine.JsonUtility::FromJson<AppleAuth.Native.CredentialStateResponse>(System.String)
inline CredentialStateResponse_t293F3810FEEAB306E15098AB47F7FE21607854B7 * JsonUtility_FromJson_TisCredentialStateResponse_t293F3810FEEAB306E15098AB47F7FE21607854B7_m52466496B9EC0733C29DD4EBD0E9A613CB7C0A68 (String_t* ___json0, const RuntimeMethod* method)
{
	return ((  CredentialStateResponse_t293F3810FEEAB306E15098AB47F7FE21607854B7 * (*) (String_t*, const RuntimeMethod*))JsonUtility_FromJson_TisRuntimeObject_m38BFFDE45556DDE756B8EDCA5E0775A8320E1296_gshared)(___json0, method);
}
// !!0 UnityEngine.JsonUtility::FromJson<AppleAuth.Native.LoginWithAppleIdResponse>(System.String)
inline LoginWithAppleIdResponse_tEADF70491D90DDEE797098E7FFCEC61C231ACE4C * JsonUtility_FromJson_TisLoginWithAppleIdResponse_tEADF70491D90DDEE797098E7FFCEC61C231ACE4C_m9AFB93CAD117C86828C186AC2CB41B60B8FE871F (String_t* ___json0, const RuntimeMethod* method)
{
	return ((  LoginWithAppleIdResponse_tEADF70491D90DDEE797098E7FFCEC61C231ACE4C * (*) (String_t*, const RuntimeMethod*))JsonUtility_FromJson_TisRuntimeObject_m38BFFDE45556DDE756B8EDCA5E0775A8320E1296_gshared)(___json0, method);
}
// System.Byte[] System.Convert::FromBase64String(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* Convert_FromBase64String_m079F788D000703E8018DA39BE9C05F1CBF60B156 (String_t* ___s0, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: AppleAuth.AppleAuthLoginArgs
IL2CPP_EXTERN_C void AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22_marshal_pinvoke(const AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22& unmarshaled, AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22_marshaled_pinvoke& marshaled)
{
	marshaled.___Options_0 = unmarshaled.get_Options_0();
	marshaled.___Nonce_1 = il2cpp_codegen_marshal_string(unmarshaled.get_Nonce_1());
	marshaled.___State_2 = il2cpp_codegen_marshal_string(unmarshaled.get_State_2());
}
IL2CPP_EXTERN_C void AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22_marshal_pinvoke_back(const AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22_marshaled_pinvoke& marshaled, AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22& unmarshaled)
{
	int32_t unmarshaled_Options_temp_0 = 0;
	unmarshaled_Options_temp_0 = marshaled.___Options_0;
	unmarshaled.set_Options_0(unmarshaled_Options_temp_0);
	unmarshaled.set_Nonce_1(il2cpp_codegen_marshal_string_result(marshaled.___Nonce_1));
	unmarshaled.set_State_2(il2cpp_codegen_marshal_string_result(marshaled.___State_2));
}
// Conversion method for clean up from marshalling of: AppleAuth.AppleAuthLoginArgs
IL2CPP_EXTERN_C void AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22_marshal_pinvoke_cleanup(AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___Nonce_1);
	marshaled.___Nonce_1 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___State_2);
	marshaled.___State_2 = NULL;
}
// Conversion methods for marshalling of: AppleAuth.AppleAuthLoginArgs
IL2CPP_EXTERN_C void AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22_marshal_com(const AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22& unmarshaled, AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22_marshaled_com& marshaled)
{
	marshaled.___Options_0 = unmarshaled.get_Options_0();
	marshaled.___Nonce_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_Nonce_1());
	marshaled.___State_2 = il2cpp_codegen_marshal_bstring(unmarshaled.get_State_2());
}
IL2CPP_EXTERN_C void AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22_marshal_com_back(const AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22_marshaled_com& marshaled, AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22& unmarshaled)
{
	int32_t unmarshaled_Options_temp_0 = 0;
	unmarshaled_Options_temp_0 = marshaled.___Options_0;
	unmarshaled.set_Options_0(unmarshaled_Options_temp_0);
	unmarshaled.set_Nonce_1(il2cpp_codegen_marshal_bstring_result(marshaled.___Nonce_1));
	unmarshaled.set_State_2(il2cpp_codegen_marshal_bstring_result(marshaled.___State_2));
}
// Conversion method for clean up from marshalling of: AppleAuth.AppleAuthLoginArgs
IL2CPP_EXTERN_C void AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22_marshal_com_cleanup(AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___Nonce_1);
	marshaled.___Nonce_1 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___State_2);
	marshaled.___State_2 = NULL;
}
// System.Void AppleAuth.AppleAuthLoginArgs::.ctor(AppleAuth.Enums.LoginOptions,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppleAuthLoginArgs__ctor_m7E3D2AA118703B59AEDF336421336EE1B6545D54 (AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22 * __this, int32_t ___options0, String_t* ___nonce1, String_t* ___state2, const RuntimeMethod* method)
{
	{
		// this.Options = options;
		int32_t L_0 = ___options0;
		__this->set_Options_0(L_0);
		// this.Nonce = nonce;
		String_t* L_1 = ___nonce1;
		__this->set_Nonce_1(L_1);
		// this.State = state;
		String_t* L_2 = ___state2;
		__this->set_State_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void AppleAuthLoginArgs__ctor_m7E3D2AA118703B59AEDF336421336EE1B6545D54_AdjustorThunk (RuntimeObject * __this, int32_t ___options0, String_t* ___nonce1, String_t* ___state2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22 * _thisAdjusted = reinterpret_cast<AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22 *>(__this + _offset);
	AppleAuthLoginArgs__ctor_m7E3D2AA118703B59AEDF336421336EE1B6545D54(_thisAdjusted, ___options0, ___nonce1, ___state2, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AppleAuth.AppleAuthManager::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppleAuthManager__cctor_m56F15734DA0AA48CE41060C15FE448DCC1730FF8 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppleAuthManager__cctor_m56F15734DA0AA48CE41060C15FE448DCC1730FF8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// PInvoke.AppleAuth_LogMessage(versionMessage);
		PInvoke_AppleAuth_LogMessage_m9620530B053E06324B4C16569F6605A7BACACEC9(_stringLiteralAB3E73C8E35434A79B5915E1660A0637615D95E3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean AppleAuth.AppleAuthManager::get_IsCurrentPlatformSupported()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool AppleAuthManager_get_IsCurrentPlatformSupported_m4B3FB905E225F53F9FB03208A10FA86EB8C6CBB4 (const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// return PInvoke.AppleAuth_IsCurrentPlatformSupported();
		bool L_0 = PInvoke_AppleAuth_IsCurrentPlatformSupported_mF4AB0EB44BE680301E902157841A2E8BE29B9998(/*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		// }
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void AppleAuth.AppleAuthManager::.ctor(AppleAuth.Interfaces.IPayloadDeserializer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppleAuthManager__ctor_m4597D08EC5F35812DB3BAF881BF569B77C15EC87 (AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * __this, RuntimeObject* ___payloadDeserializer0, const RuntimeMethod* method)
{
	{
		// public AppleAuthManager(IPayloadDeserializer payloadDeserializer)
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// this._payloadDeserializer = payloadDeserializer;
		RuntimeObject* L_0 = ___payloadDeserializer0;
		__this->set__payloadDeserializer_0(L_0);
		// }
		return;
	}
}
// System.Void AppleAuth.AppleAuthManager::QuickLogin(System.Action`1<AppleAuth.Interfaces.ICredential>,System.Action`1<AppleAuth.Interfaces.IAppleError>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppleAuthManager_QuickLogin_m9CD8DC1EBE373F3669F4A837783FAC62A0B26740 (AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * __this, Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * ___successCallback0, Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * ___errorCallback1, const RuntimeMethod* method)
{
	AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// this.QuickLogin(new AppleAuthQuickLoginArgs(), successCallback, errorCallback);
		il2cpp_codegen_initobj((&V_0), sizeof(AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A ));
		AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A  L_0 = V_0;
		Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * L_1 = ___successCallback0;
		Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * L_2 = ___errorCallback1;
		AppleAuthManager_QuickLogin_m5BE7B5C2C7728FDA2B0D5EB11862FD07762A4AC6(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppleAuth.AppleAuthManager::QuickLogin(AppleAuth.AppleAuthQuickLoginArgs,System.Action`1<AppleAuth.Interfaces.ICredential>,System.Action`1<AppleAuth.Interfaces.IAppleError>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppleAuthManager_QuickLogin_m5BE7B5C2C7728FDA2B0D5EB11862FD07762A4AC6 (AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * __this, AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A  ___quickLoginArgs0, Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * ___successCallback1, Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * ___errorCallback2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppleAuthManager_QuickLogin_m5BE7B5C2C7728FDA2B0D5EB11862FD07762A4AC6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass7_0_tCF330B8821408AA5001117209552A8D55BB2229A * V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	uint32_t V_3 = 0;
	{
		U3CU3Ec__DisplayClass7_0_tCF330B8821408AA5001117209552A8D55BB2229A * L_0 = (U3CU3Ec__DisplayClass7_0_tCF330B8821408AA5001117209552A8D55BB2229A *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass7_0_tCF330B8821408AA5001117209552A8D55BB2229A_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass7_0__ctor_mFE19321C75D3BBE3F40E7B55C1F2D6D2194B34B1(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass7_0_tCF330B8821408AA5001117209552A8D55BB2229A * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass7_0_tCF330B8821408AA5001117209552A8D55BB2229A * L_2 = V_0;
		Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * L_3 = ___errorCallback2;
		NullCheck(L_2);
		L_2->set_errorCallback_1(L_3);
		U3CU3Ec__DisplayClass7_0_tCF330B8821408AA5001117209552A8D55BB2229A * L_4 = V_0;
		Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * L_5 = ___successCallback1;
		NullCheck(L_4);
		L_4->set_successCallback_2(L_5);
		// var nonce = quickLoginArgs.Nonce;
		AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A  L_6 = ___quickLoginArgs0;
		String_t* L_7 = L_6.get_Nonce_0();
		V_1 = L_7;
		// var state = quickLoginArgs.State;
		AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A  L_8 = ___quickLoginArgs0;
		String_t* L_9 = L_8.get_State_1();
		V_2 = L_9;
		// var requestId = CallbackHandler.AddMessageCallback(
		//     true,
		//     payload =>
		//     {
		//         var response = this._payloadDeserializer.DeserializeLoginWithAppleIdResponse(payload);
		//         if (response.Error != null)
		//             errorCallback(response.Error);
		//         else if (response.PasswordCredential != null)
		//             successCallback(response.PasswordCredential);
		//         else
		//             successCallback(response.AppleIDCredential);
		//     });
		U3CU3Ec__DisplayClass7_0_tCF330B8821408AA5001117209552A8D55BB2229A * L_10 = V_0;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_11 = (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)il2cpp_codegen_object_new(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0_il2cpp_TypeInfo_var);
		Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B(L_11, L_10, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass7_0_U3CQuickLoginU3Eb__0_m6B15B778A870666871156FA6266C6B86B6EDEF69_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
		uint32_t L_12 = CallbackHandler_AddMessageCallback_m0B95A78A2EB0A42E2D6EE2186A042C3DC406C65D((bool)1, L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		// PInvoke.AppleAuth_QuickLogin(requestId, nonce, state);
		uint32_t L_13 = V_3;
		String_t* L_14 = V_1;
		String_t* L_15 = V_2;
		PInvoke_AppleAuth_QuickLogin_m2BDD63FE86A5B848DD047E60AFBD031AB00060A4(L_13, L_14, L_15, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppleAuth.AppleAuthManager::LoginWithAppleId(AppleAuth.Enums.LoginOptions,System.Action`1<AppleAuth.Interfaces.ICredential>,System.Action`1<AppleAuth.Interfaces.IAppleError>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppleAuthManager_LoginWithAppleId_m8D2D704CC0ACCE5C96983E180B637E431B8ACBA9 (AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * __this, int32_t ___options0, Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * ___successCallback1, Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * ___errorCallback2, const RuntimeMethod* method)
{
	{
		// this.LoginWithAppleId(new AppleAuthLoginArgs(options), successCallback, errorCallback);
		int32_t L_0 = ___options0;
		AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22  L_1;
		memset((&L_1), 0, sizeof(L_1));
		AppleAuthLoginArgs__ctor_m7E3D2AA118703B59AEDF336421336EE1B6545D54((&L_1), L_0, (String_t*)NULL, (String_t*)NULL, /*hidden argument*/NULL);
		Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * L_2 = ___successCallback1;
		Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * L_3 = ___errorCallback2;
		AppleAuthManager_LoginWithAppleId_m3251131E6071E0C1D505B6CAA2DE945C8C5EA099(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppleAuth.AppleAuthManager::LoginWithAppleId(AppleAuth.AppleAuthLoginArgs,System.Action`1<AppleAuth.Interfaces.ICredential>,System.Action`1<AppleAuth.Interfaces.IAppleError>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppleAuthManager_LoginWithAppleId_m3251131E6071E0C1D505B6CAA2DE945C8C5EA099 (AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * __this, AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22  ___loginArgs0, Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * ___successCallback1, Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * ___errorCallback2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppleAuthManager_LoginWithAppleId_m3251131E6071E0C1D505B6CAA2DE945C8C5EA099_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass9_0_tDC78461B5F5A4E5E4BB40BB960C7C2BE05321689 * V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	uint32_t V_4 = 0;
	{
		U3CU3Ec__DisplayClass9_0_tDC78461B5F5A4E5E4BB40BB960C7C2BE05321689 * L_0 = (U3CU3Ec__DisplayClass9_0_tDC78461B5F5A4E5E4BB40BB960C7C2BE05321689 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass9_0_tDC78461B5F5A4E5E4BB40BB960C7C2BE05321689_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass9_0__ctor_mECE874027375E819AF084618F941EC8554006642(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass9_0_tDC78461B5F5A4E5E4BB40BB960C7C2BE05321689 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass9_0_tDC78461B5F5A4E5E4BB40BB960C7C2BE05321689 * L_2 = V_0;
		Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * L_3 = ___errorCallback2;
		NullCheck(L_2);
		L_2->set_errorCallback_1(L_3);
		U3CU3Ec__DisplayClass9_0_tDC78461B5F5A4E5E4BB40BB960C7C2BE05321689 * L_4 = V_0;
		Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * L_5 = ___successCallback1;
		NullCheck(L_4);
		L_4->set_successCallback_2(L_5);
		// var loginOptions = loginArgs.Options;
		AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22  L_6 = ___loginArgs0;
		int32_t L_7 = L_6.get_Options_0();
		V_1 = L_7;
		// var nonce = loginArgs.Nonce;
		AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22  L_8 = ___loginArgs0;
		String_t* L_9 = L_8.get_Nonce_1();
		V_2 = L_9;
		// var state = loginArgs.State;
		AppleAuthLoginArgs_tA1FCEAA44ECE043CF9601C06C2EA583EA182FB22  L_10 = ___loginArgs0;
		String_t* L_11 = L_10.get_State_2();
		V_3 = L_11;
		// var requestId = CallbackHandler.AddMessageCallback(
		//     true,
		//     payload =>
		//     {
		//         var response = this._payloadDeserializer.DeserializeLoginWithAppleIdResponse(payload);
		//         if (response.Error != null)
		//             errorCallback(response.Error);
		//         else
		//             successCallback(response.AppleIDCredential);
		//     });
		U3CU3Ec__DisplayClass9_0_tDC78461B5F5A4E5E4BB40BB960C7C2BE05321689 * L_12 = V_0;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_13 = (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)il2cpp_codegen_object_new(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0_il2cpp_TypeInfo_var);
		Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B(L_13, L_12, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass9_0_U3CLoginWithAppleIdU3Eb__0_mA214B2AB4BFCF620062979C4C7935B3BF70815DB_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
		uint32_t L_14 = CallbackHandler_AddMessageCallback_m0B95A78A2EB0A42E2D6EE2186A042C3DC406C65D((bool)1, L_13, /*hidden argument*/NULL);
		V_4 = L_14;
		// PInvoke.AppleAuth_LoginWithAppleId(requestId, (int)loginOptions, nonce, state);
		uint32_t L_15 = V_4;
		int32_t L_16 = V_1;
		String_t* L_17 = V_2;
		String_t* L_18 = V_3;
		PInvoke_AppleAuth_LoginWithAppleId_m955A59F158F91E47BC0A0950F24B1564DF623E9E(L_15, L_16, L_17, L_18, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppleAuth.AppleAuthManager::GetCredentialState(System.String,System.Action`1<AppleAuth.Enums.CredentialState>,System.Action`1<AppleAuth.Interfaces.IAppleError>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppleAuthManager_GetCredentialState_m2076CC7A70D84B5D140FFC0667EBD95734BC15D9 (AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * __this, String_t* ___userId0, Action_1_tF472B580CE5012B6F8159B65F503A7AD17BDAC88 * ___successCallback1, Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * ___errorCallback2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppleAuthManager_GetCredentialState_m2076CC7A70D84B5D140FFC0667EBD95734BC15D9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass10_0_t778A5E2C062F0F876B04E713630BB330A1511837 * V_0 = NULL;
	uint32_t V_1 = 0;
	{
		U3CU3Ec__DisplayClass10_0_t778A5E2C062F0F876B04E713630BB330A1511837 * L_0 = (U3CU3Ec__DisplayClass10_0_t778A5E2C062F0F876B04E713630BB330A1511837 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass10_0_t778A5E2C062F0F876B04E713630BB330A1511837_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass10_0__ctor_m1A51C03108292B70E58D92C883CFA153B037FF47(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass10_0_t778A5E2C062F0F876B04E713630BB330A1511837 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass10_0_t778A5E2C062F0F876B04E713630BB330A1511837 * L_2 = V_0;
		Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * L_3 = ___errorCallback2;
		NullCheck(L_2);
		L_2->set_errorCallback_1(L_3);
		U3CU3Ec__DisplayClass10_0_t778A5E2C062F0F876B04E713630BB330A1511837 * L_4 = V_0;
		Action_1_tF472B580CE5012B6F8159B65F503A7AD17BDAC88 * L_5 = ___successCallback1;
		NullCheck(L_4);
		L_4->set_successCallback_2(L_5);
		// var requestId = CallbackHandler.AddMessageCallback(
		//     true,
		//     payload =>
		//     {
		//         var response = this._payloadDeserializer.DeserializeCredentialStateResponse(payload);
		//         if (response.Error != null)
		//             errorCallback(response.Error);
		//         else
		//             successCallback(response.CredentialState);
		//     });
		U3CU3Ec__DisplayClass10_0_t778A5E2C062F0F876B04E713630BB330A1511837 * L_6 = V_0;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_7 = (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)il2cpp_codegen_object_new(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0_il2cpp_TypeInfo_var);
		Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B(L_7, L_6, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass10_0_U3CGetCredentialStateU3Eb__0_m99A6F544599A6034B2E04CC297223F607D6859F1_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
		uint32_t L_8 = CallbackHandler_AddMessageCallback_m0B95A78A2EB0A42E2D6EE2186A042C3DC406C65D((bool)1, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		// PInvoke.AppleAuth_GetCredentialState(requestId, userId);
		uint32_t L_9 = V_1;
		String_t* L_10 = ___userId0;
		PInvoke_AppleAuth_GetCredentialState_mB2184459930094EC4BF2948375B4D18B069A2D28(L_9, L_10, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppleAuth.AppleAuthManager::SetCredentialsRevokedCallback(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppleAuthManager_SetCredentialsRevokedCallback_m601A44468303E8F8AE6DB176D7052B2B7A1A3710 (AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * __this, Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___credentialsRevokedCallback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppleAuthManager_SetCredentialsRevokedCallback_m601A44468303E8F8AE6DB176D7052B2B7A1A3710_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	{
		// if (this._credentialsRevokedCallback != null)
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_0 = __this->get__credentialsRevokedCallback_1();
		V_0 = (bool)((!(((RuntimeObject*)(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)L_0) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		// CallbackHandler.NativeCredentialsRevoked -= this._credentialsRevokedCallback;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_2 = __this->get__credentialsRevokedCallback_1();
		IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
		CallbackHandler_remove_NativeCredentialsRevoked_m8E5E2588E004C4838F26030762E98561A8FBBAE3(L_2, /*hidden argument*/NULL);
		// this._credentialsRevokedCallback = null;
		__this->set__credentialsRevokedCallback_1((Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)NULL);
	}

IL_0023:
	{
		// if (credentialsRevokedCallback != null)
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_3 = ___credentialsRevokedCallback0;
		V_1 = (bool)((!(((RuntimeObject*)(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)L_3) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_003b;
		}
	}
	{
		// CallbackHandler.NativeCredentialsRevoked += credentialsRevokedCallback;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_5 = ___credentialsRevokedCallback0;
		IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
		CallbackHandler_add_NativeCredentialsRevoked_mAFC47A0B52BC9DCACB3C8B36748CB8FB54CEC208(L_5, /*hidden argument*/NULL);
		// this._credentialsRevokedCallback = credentialsRevokedCallback;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_6 = ___credentialsRevokedCallback0;
		__this->set__credentialsRevokedCallback_1(L_6);
	}

IL_003b:
	{
		// }
		return;
	}
}
// System.Void AppleAuth.AppleAuthManager::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppleAuthManager_Update_mF049B837ADF2966D780DDDFAC97135FFE05F9B54 (AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppleAuthManager_Update_mF049B837ADF2966D780DDDFAC97135FFE05F9B54_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// CallbackHandler.ExecutePendingCallbacks();
		IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
		CallbackHandler_ExecutePendingCallbacks_m5AD24B967D8C51AECFE59C5BF02342F57952AC3C(/*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AppleAuth.AppleAuthManager_<>c__DisplayClass10_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass10_0__ctor_m1A51C03108292B70E58D92C883CFA153B037FF47 (U3CU3Ec__DisplayClass10_0_t778A5E2C062F0F876B04E713630BB330A1511837 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AppleAuth.AppleAuthManager_<>c__DisplayClass10_0::<GetCredentialState>b__0(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass10_0_U3CGetCredentialStateU3Eb__0_m99A6F544599A6034B2E04CC297223F607D6859F1 (U3CU3Ec__DisplayClass10_0_t778A5E2C062F0F876B04E713630BB330A1511837 * __this, String_t* ___payload0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass10_0_U3CGetCredentialStateU3Eb__0_m99A6F544599A6034B2E04CC297223F607D6859F1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	bool V_1 = false;
	{
		// var response = this._payloadDeserializer.DeserializeCredentialStateResponse(payload);
		AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * L_0 = __this->get_U3CU3E4__this_0();
		NullCheck(L_0);
		RuntimeObject* L_1 = L_0->get__payloadDeserializer_0();
		String_t* L_2 = ___payload0;
		NullCheck(L_1);
		RuntimeObject* L_3 = InterfaceFuncInvoker1< RuntimeObject*, String_t* >::Invoke(0 /* AppleAuth.Interfaces.ICredentialStateResponse AppleAuth.Interfaces.IPayloadDeserializer::DeserializeCredentialStateResponse(System.String) */, IPayloadDeserializer_tE27C18AF93B5B81F79A412F73640C4238FDA5C93_il2cpp_TypeInfo_var, L_1, L_2);
		V_0 = L_3;
		// if (response.Error != null)
		RuntimeObject* L_4 = V_0;
		NullCheck(L_4);
		RuntimeObject* L_5 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(2 /* AppleAuth.Interfaces.IAppleError AppleAuth.Interfaces.ICredentialStateResponse::get_Error() */, ICredentialStateResponse_tF6E3E5E7ED85FCDEF7643474DC53BFB473180ED7_il2cpp_TypeInfo_var, L_4);
		V_1 = (bool)((!(((RuntimeObject*)(RuntimeObject*)L_5) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_6 = V_1;
		if (!L_6)
		{
			goto IL_0034;
		}
	}
	{
		// errorCallback(response.Error);
		Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * L_7 = __this->get_errorCallback_1();
		RuntimeObject* L_8 = V_0;
		NullCheck(L_8);
		RuntimeObject* L_9 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(2 /* AppleAuth.Interfaces.IAppleError AppleAuth.Interfaces.ICredentialStateResponse::get_Error() */, ICredentialStateResponse_tF6E3E5E7ED85FCDEF7643474DC53BFB473180ED7_il2cpp_TypeInfo_var, L_8);
		NullCheck(L_7);
		Action_1_Invoke_m7D166367CD49F79461F74C532A8286A491F75329(L_7, L_9, /*hidden argument*/Action_1_Invoke_m7D166367CD49F79461F74C532A8286A491F75329_RuntimeMethod_var);
		goto IL_0046;
	}

IL_0034:
	{
		// successCallback(response.CredentialState);
		Action_1_tF472B580CE5012B6F8159B65F503A7AD17BDAC88 * L_10 = __this->get_successCallback_2();
		RuntimeObject* L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* AppleAuth.Enums.CredentialState AppleAuth.Interfaces.ICredentialStateResponse::get_CredentialState() */, ICredentialStateResponse_tF6E3E5E7ED85FCDEF7643474DC53BFB473180ED7_il2cpp_TypeInfo_var, L_11);
		NullCheck(L_10);
		Action_1_Invoke_m7EAD69A6377B20A71F043C663094490F99E5E2AC(L_10, L_12, /*hidden argument*/Action_1_Invoke_m7EAD69A6377B20A71F043C663094490F99E5E2AC_RuntimeMethod_var);
	}

IL_0046:
	{
		// });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AppleAuth.AppleAuthManager_<>c__DisplayClass7_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass7_0__ctor_mFE19321C75D3BBE3F40E7B55C1F2D6D2194B34B1 (U3CU3Ec__DisplayClass7_0_tCF330B8821408AA5001117209552A8D55BB2229A * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AppleAuth.AppleAuthManager_<>c__DisplayClass7_0::<QuickLogin>b__0(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass7_0_U3CQuickLoginU3Eb__0_m6B15B778A870666871156FA6266C6B86B6EDEF69 (U3CU3Ec__DisplayClass7_0_tCF330B8821408AA5001117209552A8D55BB2229A * __this, String_t* ___payload0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass7_0_U3CQuickLoginU3Eb__0_m6B15B778A870666871156FA6266C6B86B6EDEF69_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	{
		// var response = this._payloadDeserializer.DeserializeLoginWithAppleIdResponse(payload);
		AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * L_0 = __this->get_U3CU3E4__this_0();
		NullCheck(L_0);
		RuntimeObject* L_1 = L_0->get__payloadDeserializer_0();
		String_t* L_2 = ___payload0;
		NullCheck(L_1);
		RuntimeObject* L_3 = InterfaceFuncInvoker1< RuntimeObject*, String_t* >::Invoke(1 /* AppleAuth.Interfaces.ILoginWithAppleIdResponse AppleAuth.Interfaces.IPayloadDeserializer::DeserializeLoginWithAppleIdResponse(System.String) */, IPayloadDeserializer_tE27C18AF93B5B81F79A412F73640C4238FDA5C93_il2cpp_TypeInfo_var, L_1, L_2);
		V_0 = L_3;
		// if (response.Error != null)
		RuntimeObject* L_4 = V_0;
		NullCheck(L_4);
		RuntimeObject* L_5 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(1 /* AppleAuth.Interfaces.IAppleError AppleAuth.Interfaces.ILoginWithAppleIdResponse::get_Error() */, ILoginWithAppleIdResponse_t6D8A3D76E4C720155C59442F155E221E8083FA10_il2cpp_TypeInfo_var, L_4);
		V_1 = (bool)((!(((RuntimeObject*)(RuntimeObject*)L_5) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_6 = V_1;
		if (!L_6)
		{
			goto IL_0034;
		}
	}
	{
		// errorCallback(response.Error);
		Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * L_7 = __this->get_errorCallback_1();
		RuntimeObject* L_8 = V_0;
		NullCheck(L_8);
		RuntimeObject* L_9 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(1 /* AppleAuth.Interfaces.IAppleError AppleAuth.Interfaces.ILoginWithAppleIdResponse::get_Error() */, ILoginWithAppleIdResponse_t6D8A3D76E4C720155C59442F155E221E8083FA10_il2cpp_TypeInfo_var, L_8);
		NullCheck(L_7);
		Action_1_Invoke_m7D166367CD49F79461F74C532A8286A491F75329(L_7, L_9, /*hidden argument*/Action_1_Invoke_m7D166367CD49F79461F74C532A8286A491F75329_RuntimeMethod_var);
		goto IL_0067;
	}

IL_0034:
	{
		// else if (response.PasswordCredential != null)
		RuntimeObject* L_10 = V_0;
		NullCheck(L_10);
		RuntimeObject* L_11 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(3 /* AppleAuth.Interfaces.IPasswordCredential AppleAuth.Interfaces.ILoginWithAppleIdResponse::get_PasswordCredential() */, ILoginWithAppleIdResponse_t6D8A3D76E4C720155C59442F155E221E8083FA10_il2cpp_TypeInfo_var, L_10);
		V_2 = (bool)((!(((RuntimeObject*)(RuntimeObject*)L_11) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_12 = V_2;
		if (!L_12)
		{
			goto IL_0055;
		}
	}
	{
		// successCallback(response.PasswordCredential);
		Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * L_13 = __this->get_successCallback_2();
		RuntimeObject* L_14 = V_0;
		NullCheck(L_14);
		RuntimeObject* L_15 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(3 /* AppleAuth.Interfaces.IPasswordCredential AppleAuth.Interfaces.ILoginWithAppleIdResponse::get_PasswordCredential() */, ILoginWithAppleIdResponse_t6D8A3D76E4C720155C59442F155E221E8083FA10_il2cpp_TypeInfo_var, L_14);
		NullCheck(L_13);
		Action_1_Invoke_m78B725AD74CD7728202CC5ACC1E6E0F114CD02DE(L_13, L_15, /*hidden argument*/Action_1_Invoke_m78B725AD74CD7728202CC5ACC1E6E0F114CD02DE_RuntimeMethod_var);
		goto IL_0067;
	}

IL_0055:
	{
		// successCallback(response.AppleIDCredential);
		Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * L_16 = __this->get_successCallback_2();
		RuntimeObject* L_17 = V_0;
		NullCheck(L_17);
		RuntimeObject* L_18 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(2 /* AppleAuth.Interfaces.IAppleIDCredential AppleAuth.Interfaces.ILoginWithAppleIdResponse::get_AppleIDCredential() */, ILoginWithAppleIdResponse_t6D8A3D76E4C720155C59442F155E221E8083FA10_il2cpp_TypeInfo_var, L_17);
		NullCheck(L_16);
		Action_1_Invoke_m78B725AD74CD7728202CC5ACC1E6E0F114CD02DE(L_16, L_18, /*hidden argument*/Action_1_Invoke_m78B725AD74CD7728202CC5ACC1E6E0F114CD02DE_RuntimeMethod_var);
	}

IL_0067:
	{
		// });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AppleAuth.AppleAuthManager_<>c__DisplayClass9_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass9_0__ctor_mECE874027375E819AF084618F941EC8554006642 (U3CU3Ec__DisplayClass9_0_tDC78461B5F5A4E5E4BB40BB960C7C2BE05321689 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AppleAuth.AppleAuthManager_<>c__DisplayClass9_0::<LoginWithAppleId>b__0(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass9_0_U3CLoginWithAppleIdU3Eb__0_mA214B2AB4BFCF620062979C4C7935B3BF70815DB (U3CU3Ec__DisplayClass9_0_tDC78461B5F5A4E5E4BB40BB960C7C2BE05321689 * __this, String_t* ___payload0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass9_0_U3CLoginWithAppleIdU3Eb__0_mA214B2AB4BFCF620062979C4C7935B3BF70815DB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	bool V_1 = false;
	{
		// var response = this._payloadDeserializer.DeserializeLoginWithAppleIdResponse(payload);
		AppleAuthManager_t48317ECA7A1EEC4A5D7620C7E22B274CD020AE7C * L_0 = __this->get_U3CU3E4__this_0();
		NullCheck(L_0);
		RuntimeObject* L_1 = L_0->get__payloadDeserializer_0();
		String_t* L_2 = ___payload0;
		NullCheck(L_1);
		RuntimeObject* L_3 = InterfaceFuncInvoker1< RuntimeObject*, String_t* >::Invoke(1 /* AppleAuth.Interfaces.ILoginWithAppleIdResponse AppleAuth.Interfaces.IPayloadDeserializer::DeserializeLoginWithAppleIdResponse(System.String) */, IPayloadDeserializer_tE27C18AF93B5B81F79A412F73640C4238FDA5C93_il2cpp_TypeInfo_var, L_1, L_2);
		V_0 = L_3;
		// if (response.Error != null)
		RuntimeObject* L_4 = V_0;
		NullCheck(L_4);
		RuntimeObject* L_5 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(1 /* AppleAuth.Interfaces.IAppleError AppleAuth.Interfaces.ILoginWithAppleIdResponse::get_Error() */, ILoginWithAppleIdResponse_t6D8A3D76E4C720155C59442F155E221E8083FA10_il2cpp_TypeInfo_var, L_4);
		V_1 = (bool)((!(((RuntimeObject*)(RuntimeObject*)L_5) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_6 = V_1;
		if (!L_6)
		{
			goto IL_0034;
		}
	}
	{
		// errorCallback(response.Error);
		Action_1_t4D3643F92584A6386B6D52772D165BBD29643DD3 * L_7 = __this->get_errorCallback_1();
		RuntimeObject* L_8 = V_0;
		NullCheck(L_8);
		RuntimeObject* L_9 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(1 /* AppleAuth.Interfaces.IAppleError AppleAuth.Interfaces.ILoginWithAppleIdResponse::get_Error() */, ILoginWithAppleIdResponse_t6D8A3D76E4C720155C59442F155E221E8083FA10_il2cpp_TypeInfo_var, L_8);
		NullCheck(L_7);
		Action_1_Invoke_m7D166367CD49F79461F74C532A8286A491F75329(L_7, L_9, /*hidden argument*/Action_1_Invoke_m7D166367CD49F79461F74C532A8286A491F75329_RuntimeMethod_var);
		goto IL_0046;
	}

IL_0034:
	{
		// successCallback(response.AppleIDCredential);
		Action_1_t675F46A6B01AF1F9AAECC11582DEF3D8EF18168F * L_10 = __this->get_successCallback_2();
		RuntimeObject* L_11 = V_0;
		NullCheck(L_11);
		RuntimeObject* L_12 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(2 /* AppleAuth.Interfaces.IAppleIDCredential AppleAuth.Interfaces.ILoginWithAppleIdResponse::get_AppleIDCredential() */, ILoginWithAppleIdResponse_t6D8A3D76E4C720155C59442F155E221E8083FA10_il2cpp_TypeInfo_var, L_11);
		NullCheck(L_10);
		Action_1_Invoke_m78B725AD74CD7728202CC5ACC1E6E0F114CD02DE(L_10, L_12, /*hidden argument*/Action_1_Invoke_m78B725AD74CD7728202CC5ACC1E6E0F114CD02DE_RuntimeMethod_var);
	}

IL_0046:
	{
		// });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AppleAuth.AppleAuthManager_CallbackHandler::add__nativeCredentialsRevoked(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CallbackHandler_add__nativeCredentialsRevoked_m76EF11A8538E68C4EF86007907748718B46C0A2D (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CallbackHandler_add__nativeCredentialsRevoked_m76EF11A8538E68C4EF86007907748718B46C0A2D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * V_0 = NULL;
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * V_1 = NULL;
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_0 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get__nativeCredentialsRevoked_8();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_1 = V_0;
		V_1 = L_1;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_2 = V_1;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)CastclassSealed((RuntimeObject*)L_4, Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_5 = V_2;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_6 = V_1;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_7 = InterlockedCompareExchangeImpl<Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *>((Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 **)(((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get_address_of__nativeCredentialsRevoked_8()), L_5, L_6);
		V_0 = L_7;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_8 = V_0;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)L_8) == ((RuntimeObject*)(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void AppleAuth.AppleAuthManager_CallbackHandler::remove__nativeCredentialsRevoked(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CallbackHandler_remove__nativeCredentialsRevoked_mD4A0DE080A0F2633EBCF99808C748D76F21F622A (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CallbackHandler_remove__nativeCredentialsRevoked_mD4A0DE080A0F2633EBCF99808C748D76F21F622A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * V_0 = NULL;
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * V_1 = NULL;
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * V_2 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_0 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get__nativeCredentialsRevoked_8();
		V_0 = L_0;
	}

IL_0006:
	{
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_1 = V_0;
		V_1 = L_1;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_2 = V_1;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)CastclassSealed((RuntimeObject*)L_4, Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_5 = V_2;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_6 = V_1;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_7 = InterlockedCompareExchangeImpl<Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *>((Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 **)(((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get_address_of__nativeCredentialsRevoked_8()), L_5, L_6);
		V_0 = L_7;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_8 = V_0;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)L_8) == ((RuntimeObject*)(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void AppleAuth.AppleAuthManager_CallbackHandler::add_NativeCredentialsRevoked(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CallbackHandler_add_NativeCredentialsRevoked_mAFC47A0B52BC9DCACB3C8B36748CB8FB54CEC208 (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CallbackHandler_add_NativeCredentialsRevoked_mAFC47A0B52BC9DCACB3C8B36748CB8FB54CEC208_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * G_B4_0 = NULL;
	int32_t G_B4_1 = 0;
	Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	{
		// lock (SyncLock)
		IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
		RuntimeObject * L_0 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get_SyncLock_2();
		V_0 = L_0;
		V_1 = (bool)0;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_1 = V_0;
			Monitor_Enter_mC5B353DD83A0B0155DF6FBCC4DF5A580C25534C5(L_1, (bool*)(&V_1), /*hidden argument*/NULL);
			// if (_nativeCredentialsRevoked == null)
			IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
			Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_2 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get__nativeCredentialsRevoked_8();
			V_2 = (bool)((((RuntimeObject*)(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)L_2) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
			bool L_3 = V_2;
			if (!L_3)
			{
				goto IL_0056;
			}
		}

IL_001f:
		{
			// _credentialsRevokedCallbackId = AddMessageCallback(false, payload => _nativeCredentialsRevoked.Invoke(payload));
			IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2_il2cpp_TypeInfo_var);
			Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_4 = ((U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2_il2cpp_TypeInfo_var))->get_U3CU3E9__12_0_1();
			Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_5 = L_4;
			G_B3_0 = L_5;
			G_B3_1 = 0;
			if (L_5)
			{
				G_B4_0 = L_5;
				G_B4_1 = 0;
				goto IL_0040;
			}
		}

IL_0029:
		{
			IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2_il2cpp_TypeInfo_var);
			U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2 * L_6 = ((U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
			Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_7 = (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)il2cpp_codegen_object_new(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0_il2cpp_TypeInfo_var);
			Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B(L_7, L_6, (intptr_t)((intptr_t)U3CU3Ec_U3Cadd_NativeCredentialsRevokedU3Eb__12_0_m5724726AA513DABA2EA1B4A0806D72D71DEE9D41_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mD9D4E02AEE600236E03EBAB2449B68B482FAC69B_RuntimeMethod_var);
			Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_8 = L_7;
			((U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2_il2cpp_TypeInfo_var))->set_U3CU3E9__12_0_1(L_8);
			G_B4_0 = L_8;
			G_B4_1 = G_B3_1;
		}

IL_0040:
		{
			IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
			uint32_t L_9 = CallbackHandler_AddMessageCallback_m0B95A78A2EB0A42E2D6EE2186A042C3DC406C65D((bool)G_B4_1, G_B4_0, /*hidden argument*/NULL);
			((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->set__credentialsRevokedCallbackId_7(L_9);
			// PInvoke.AppleAuth_RegisterCredentialsRevokedCallbackId(_credentialsRevokedCallbackId);
			uint32_t L_10 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get__credentialsRevokedCallbackId_7();
			PInvoke_AppleAuth_RegisterCredentialsRevokedCallbackId_m65B9651E2B15431DD6289C68DD8787F9EDAF8EF8(L_10, /*hidden argument*/NULL);
		}

IL_0056:
		{
			// _nativeCredentialsRevoked += value;
			Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_11 = ___value0;
			IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
			CallbackHandler_add__nativeCredentialsRevoked_m76EF11A8538E68C4EF86007907748718B46C0A2D(L_11, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x6B, FINALLY_0060);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0060;
	}

FINALLY_0060:
	{ // begin finally (depth: 1)
		{
			bool L_12 = V_1;
			if (!L_12)
			{
				goto IL_006a;
			}
		}

IL_0063:
		{
			RuntimeObject * L_13 = V_0;
			Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_13, /*hidden argument*/NULL);
		}

IL_006a:
		{
			IL2CPP_END_FINALLY(96)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(96)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x6B, IL_006b)
	}

IL_006b:
	{
		// }
		return;
	}
}
// System.Void AppleAuth.AppleAuthManager_CallbackHandler::remove_NativeCredentialsRevoked(System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CallbackHandler_remove_NativeCredentialsRevoked_m8E5E2588E004C4838F26030762E98561A8FBBAE3 (Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CallbackHandler_remove_NativeCredentialsRevoked_m8E5E2588E004C4838F26030762E98561A8FBBAE3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (SyncLock)
		IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
		RuntimeObject * L_0 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get_SyncLock_2();
		V_0 = L_0;
		V_1 = (bool)0;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_1 = V_0;
			Monitor_Enter_mC5B353DD83A0B0155DF6FBCC4DF5A580C25534C5(L_1, (bool*)(&V_1), /*hidden argument*/NULL);
			// _nativeCredentialsRevoked -= value;
			Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_2 = ___value0;
			IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
			CallbackHandler_remove__nativeCredentialsRevoked_mD4A0DE080A0F2633EBCF99808C748D76F21F622A(L_2, /*hidden argument*/NULL);
			// if (_nativeCredentialsRevoked == null)
			Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_3 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get__nativeCredentialsRevoked_8();
			V_2 = (bool)((((RuntimeObject*)(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
			bool L_4 = V_2;
			if (!L_4)
			{
				goto IL_0040;
			}
		}

IL_0026:
		{
			// RemoveMessageCallback(_credentialsRevokedCallbackId);
			IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
			uint32_t L_5 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get__credentialsRevokedCallbackId_7();
			CallbackHandler_RemoveMessageCallback_m54E5CE8453DA1146E7E132B646E96FB964C72C7B(L_5, /*hidden argument*/NULL);
			// _credentialsRevokedCallbackId = 0U;
			((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->set__credentialsRevokedCallbackId_7(0);
			// PInvoke.AppleAuth_RegisterCredentialsRevokedCallbackId(0U);
			PInvoke_AppleAuth_RegisterCredentialsRevokedCallbackId_m65B9651E2B15431DD6289C68DD8787F9EDAF8EF8(0, /*hidden argument*/NULL);
		}

IL_0040:
		{
			IL2CPP_LEAVE(0x4E, FINALLY_0043);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0043;
	}

FINALLY_0043:
	{ // begin finally (depth: 1)
		{
			bool L_6 = V_1;
			if (!L_6)
			{
				goto IL_004d;
			}
		}

IL_0046:
		{
			RuntimeObject * L_7 = V_0;
			Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_7, /*hidden argument*/NULL);
		}

IL_004d:
		{
			IL2CPP_END_FINALLY(67)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(67)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x4E, IL_004e)
	}

IL_004e:
	{
		// }
		return;
	}
}
// System.Void AppleAuth.AppleAuthManager_CallbackHandler::ScheduleCallback(System.UInt32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CallbackHandler_ScheduleCallback_m085D44C88C7A14F9F29E1170A8FCA2BE13B857F5 (uint32_t ___requestId0, String_t* ___payload1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CallbackHandler_ScheduleCallback_m085D44C88C7A14F9F29E1170A8FCA2BE13B857F5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass14_0_t0743B54A2D147CB75027EE3F324C3B375DB67067 * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	bool V_2 = false;
	Entry_t6FA80886BFD1655BEB628D855A2FAD715839F6BB * V_3 = NULL;
	bool V_4 = false;
	U3CU3Ec__DisplayClass14_1_tED700A7605A9EEFA916745644049F0D15DE58F1D * V_5 = NULL;
	bool V_6 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		U3CU3Ec__DisplayClass14_0_t0743B54A2D147CB75027EE3F324C3B375DB67067 * L_0 = (U3CU3Ec__DisplayClass14_0_t0743B54A2D147CB75027EE3F324C3B375DB67067 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass14_0_t0743B54A2D147CB75027EE3F324C3B375DB67067_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass14_0__ctor_m955A36C7D7F760FD5656A05D0BF5CB140DF78210(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass14_0_t0743B54A2D147CB75027EE3F324C3B375DB67067 * L_1 = V_0;
		String_t* L_2 = ___payload1;
		NullCheck(L_1);
		L_1->set_payload_0(L_2);
		// lock (SyncLock)
		IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
		RuntimeObject * L_3 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get_SyncLock_2();
		V_1 = L_3;
		V_2 = (bool)0;
	}

IL_0016:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_4 = V_1;
			Monitor_Enter_mC5B353DD83A0B0155DF6FBCC4DF5A580C25534C5(L_4, (bool*)(&V_2), /*hidden argument*/NULL);
			// var callbackEntry = default(Entry);
			V_3 = (Entry_t6FA80886BFD1655BEB628D855A2FAD715839F6BB *)NULL;
			// if (CallbackDictionary.TryGetValue(requestId, out callbackEntry))
			IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
			Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C * L_5 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get_CallbackDictionary_3();
			uint32_t L_6 = ___requestId0;
			NullCheck(L_5);
			bool L_7 = Dictionary_2_TryGetValue_mCFF58E001B1BF5E1ECA4BBC7ECED52321A3DBA2D(L_5, L_6, (Entry_t6FA80886BFD1655BEB628D855A2FAD715839F6BB **)(&V_3), /*hidden argument*/Dictionary_2_TryGetValue_mCFF58E001B1BF5E1ECA4BBC7ECED52321A3DBA2D_RuntimeMethod_var);
			V_4 = L_7;
			bool L_8 = V_4;
			if (!L_8)
			{
				goto IL_0085;
			}
		}

IL_0035:
		{
			U3CU3Ec__DisplayClass14_1_tED700A7605A9EEFA916745644049F0D15DE58F1D * L_9 = (U3CU3Ec__DisplayClass14_1_tED700A7605A9EEFA916745644049F0D15DE58F1D *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass14_1_tED700A7605A9EEFA916745644049F0D15DE58F1D_il2cpp_TypeInfo_var);
			U3CU3Ec__DisplayClass14_1__ctor_mAEFE6646216AE52125F79843CDF4695B36111113(L_9, /*hidden argument*/NULL);
			V_5 = L_9;
			U3CU3Ec__DisplayClass14_1_tED700A7605A9EEFA916745644049F0D15DE58F1D * L_10 = V_5;
			U3CU3Ec__DisplayClass14_0_t0743B54A2D147CB75027EE3F324C3B375DB67067 * L_11 = V_0;
			NullCheck(L_10);
			L_10->set_CSU24U3CU3E8__locals1_1(L_11);
			// var callback = callbackEntry.MessageCallback;
			U3CU3Ec__DisplayClass14_1_tED700A7605A9EEFA916745644049F0D15DE58F1D * L_12 = V_5;
			Entry_t6FA80886BFD1655BEB628D855A2FAD715839F6BB * L_13 = V_3;
			NullCheck(L_13);
			Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_14 = L_13->get_MessageCallback_1();
			NullCheck(L_12);
			L_12->set_callback_0(L_14);
			// ScheduledActions.Add(() => callback.Invoke(payload));
			IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
			List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * L_15 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get_ScheduledActions_4();
			U3CU3Ec__DisplayClass14_1_tED700A7605A9EEFA916745644049F0D15DE58F1D * L_16 = V_5;
			Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_17 = (Action_t591D2A86165F896B4B800BB5C25CE18672A55579 *)il2cpp_codegen_object_new(Action_t591D2A86165F896B4B800BB5C25CE18672A55579_il2cpp_TypeInfo_var);
			Action__ctor_m570E96B2A0C48BC1DC6788460316191F24572760(L_17, L_16, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass14_1_U3CScheduleCallbackU3Eb__0_m5C8FE466DDC61501D84CF97573FEA1C8B7689192_RuntimeMethod_var), /*hidden argument*/NULL);
			NullCheck(L_15);
			List_1_Add_m019DF84155C09D3979798C84CD70DD28E146025B(L_15, L_17, /*hidden argument*/List_1_Add_m019DF84155C09D3979798C84CD70DD28E146025B_RuntimeMethod_var);
			// if (callbackEntry.IsSingleUseCallback)
			Entry_t6FA80886BFD1655BEB628D855A2FAD715839F6BB * L_18 = V_3;
			NullCheck(L_18);
			bool L_19 = L_18->get_IsSingleUseCallback_0();
			V_6 = L_19;
			bool L_20 = V_6;
			if (!L_20)
			{
				goto IL_0084;
			}
		}

IL_0076:
		{
			// CallbackDictionary.Remove(requestId);
			IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
			Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C * L_21 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get_CallbackDictionary_3();
			uint32_t L_22 = ___requestId0;
			NullCheck(L_21);
			Dictionary_2_Remove_m78D417809EE5B205B4A02CF24F81E4096BB6EC3D(L_21, L_22, /*hidden argument*/Dictionary_2_Remove_m78D417809EE5B205B4A02CF24F81E4096BB6EC3D_RuntimeMethod_var);
		}

IL_0084:
		{
		}

IL_0085:
		{
			IL2CPP_LEAVE(0x93, FINALLY_0088);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0088;
	}

FINALLY_0088:
	{ // begin finally (depth: 1)
		{
			bool L_23 = V_2;
			if (!L_23)
			{
				goto IL_0092;
			}
		}

IL_008b:
		{
			RuntimeObject * L_24 = V_1;
			Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_24, /*hidden argument*/NULL);
		}

IL_0092:
		{
			IL2CPP_END_FINALLY(136)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(136)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x93, IL_0093)
	}

IL_0093:
	{
		// }
		return;
	}
}
// System.Void AppleAuth.AppleAuthManager_CallbackHandler::ExecutePendingCallbacks()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CallbackHandler_ExecutePendingCallbacks_m5AD24B967D8C51AECFE59C5BF02342F57952AC3C (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CallbackHandler_ExecutePendingCallbacks_m5AD24B967D8C51AECFE59C5BF02342F57952AC3C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * V_2 = NULL;
	bool V_3 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (SyncLock)
		IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
		RuntimeObject * L_0 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get_SyncLock_2();
		V_0 = L_0;
		V_1 = (bool)0;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_1 = V_0;
			Monitor_Enter_mC5B353DD83A0B0155DF6FBCC4DF5A580C25534C5(L_1, (bool*)(&V_1), /*hidden argument*/NULL);
			goto IL_0036;
		}

IL_0015:
		{
			// var action = ScheduledActions[0];
			IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
			List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * L_2 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get_ScheduledActions_4();
			NullCheck(L_2);
			Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_3 = List_1_get_Item_m900B85866FD2558E6CE2725E07AF693C29C19731_inline(L_2, 0, /*hidden argument*/List_1_get_Item_m900B85866FD2558E6CE2725E07AF693C29C19731_RuntimeMethod_var);
			V_2 = L_3;
			// ScheduledActions.RemoveAt(0);
			List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * L_4 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get_ScheduledActions_4();
			NullCheck(L_4);
			List_1_RemoveAt_m281DBD9DDEBE448DB0268959927756D4BD4E6FD6(L_4, 0, /*hidden argument*/List_1_RemoveAt_m281DBD9DDEBE448DB0268959927756D4BD4E6FD6_RuntimeMethod_var);
			// action.Invoke();
			Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * L_5 = V_2;
			NullCheck(L_5);
			Action_Invoke_mC8D676E5DDF967EC5D23DD0E96FB52AA499817FD(L_5, /*hidden argument*/NULL);
		}

IL_0036:
		{
			// while (ScheduledActions.Count > 0)
			IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
			List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * L_6 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get_ScheduledActions_4();
			NullCheck(L_6);
			int32_t L_7 = List_1_get_Count_m3986FAACE5FCFF72B7601E768375D65E038FDEDB_inline(L_6, /*hidden argument*/List_1_get_Count_m3986FAACE5FCFF72B7601E768375D65E038FDEDB_RuntimeMethod_var);
			V_3 = (bool)((((int32_t)L_7) > ((int32_t)0))? 1 : 0);
			bool L_8 = V_3;
			if (L_8)
			{
				goto IL_0015;
			}
		}

IL_0047:
		{
			IL2CPP_LEAVE(0x55, FINALLY_004a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_004a;
	}

FINALLY_004a:
	{ // begin finally (depth: 1)
		{
			bool L_9 = V_1;
			if (!L_9)
			{
				goto IL_0054;
			}
		}

IL_004d:
		{
			RuntimeObject * L_10 = V_0;
			Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_10, /*hidden argument*/NULL);
		}

IL_0054:
		{
			IL2CPP_END_FINALLY(74)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(74)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x55, IL_0055)
	}

IL_0055:
	{
		// }
		return;
	}
}
// System.UInt32 AppleAuth.AppleAuthManager_CallbackHandler::AddMessageCallback(System.Boolean,System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR uint32_t CallbackHandler_AddMessageCallback_m0B95A78A2EB0A42E2D6EE2186A042C3DC406C65D (bool ___isSingleUse0, Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___messageCallback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CallbackHandler_AddMessageCallback_m0B95A78A2EB0A42E2D6EE2186A042C3DC406C65D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	RuntimeObject * V_3 = NULL;
	bool V_4 = false;
	Entry_t6FA80886BFD1655BEB628D855A2FAD715839F6BB * V_5 = NULL;
	bool V_6 = false;
	uint32_t V_7 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// if (!_initialized)
		IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
		bool L_0 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get__initialized_6();
		V_1 = (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_1;
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		// PInvoke.AppleAuth_SetupNativeMessageHandlerCallback(PInvoke.NativeMessageHandlerCallback);
		NativeMessageHandlerCallbackDelegate_t791982D0270309BB7AEFD64EF9FFA0F7DBFE4A14 * L_2 = (NativeMessageHandlerCallbackDelegate_t791982D0270309BB7AEFD64EF9FFA0F7DBFE4A14 *)il2cpp_codegen_object_new(NativeMessageHandlerCallbackDelegate_t791982D0270309BB7AEFD64EF9FFA0F7DBFE4A14_il2cpp_TypeInfo_var);
		NativeMessageHandlerCallbackDelegate__ctor_mA999B2608865CF7F48403396ED87EEDDDB0BD602(L_2, NULL, (intptr_t)((intptr_t)PInvoke_NativeMessageHandlerCallback_m5D5E84BB3C9785BE4384D272156E122275A93DE4_RuntimeMethod_var), /*hidden argument*/NULL);
		PInvoke_AppleAuth_SetupNativeMessageHandlerCallback_m2D140AC7EC65E41051360B692A205C2B1345CA90(L_2, /*hidden argument*/NULL);
		// _initialized = true;
		IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
		((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->set__initialized_6((bool)1);
	}

IL_0027:
	{
		// if (messageCallback == null)
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_3 = ___messageCallback1;
		V_2 = (bool)((((RuntimeObject*)(Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)L_3) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_4 = V_2;
		if (!L_4)
		{
			goto IL_003b;
		}
	}
	{
		// throw new Exception("Can't add a null Message Callback.");
		Exception_t * L_5 = (Exception_t *)il2cpp_codegen_object_new(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m89BADFF36C3B170013878726E07729D51AA9FBE0(L_5, _stringLiteral476822F3F2155A8DC261D2238DCB4B70200E8135, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, CallbackHandler_AddMessageCallback_m0B95A78A2EB0A42E2D6EE2186A042C3DC406C65D_RuntimeMethod_var);
	}

IL_003b:
	{
		// var usedCallbackId = default(uint);
		V_0 = 0;
		// lock (SyncLock)
		IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
		RuntimeObject * L_6 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get_SyncLock_2();
		V_3 = L_6;
		V_4 = (bool)0;
	}

IL_0046:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_7 = V_3;
			Monitor_Enter_mC5B353DD83A0B0155DF6FBCC4DF5A580C25534C5(L_7, (bool*)(&V_4), /*hidden argument*/NULL);
			// usedCallbackId = _callbackId;
			IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
			uint32_t L_8 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get__callbackId_5();
			V_0 = L_8;
			// _callbackId += 1;
			uint32_t L_9 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get__callbackId_5();
			((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->set__callbackId_5(((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1)));
			// if (_callbackId >= MaxCallbackId)
			uint32_t L_10 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get__callbackId_5();
			V_6 = (bool)((((int32_t)((!(((uint32_t)L_10) >= ((uint32_t)(-1))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
			bool L_11 = V_6;
			if (!L_11)
			{
				goto IL_0079;
			}
		}

IL_0073:
		{
			// _callbackId = InitialCallbackId;
			IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
			((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->set__callbackId_5(1);
		}

IL_0079:
		{
			// var callbackEntry = new Entry(isSingleUse, messageCallback);
			bool L_12 = ___isSingleUse0;
			Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_13 = ___messageCallback1;
			Entry_t6FA80886BFD1655BEB628D855A2FAD715839F6BB * L_14 = (Entry_t6FA80886BFD1655BEB628D855A2FAD715839F6BB *)il2cpp_codegen_object_new(Entry_t6FA80886BFD1655BEB628D855A2FAD715839F6BB_il2cpp_TypeInfo_var);
			Entry__ctor_m211294F29613E18A310B5A2CBF5DB141F0C5164F(L_14, L_12, L_13, /*hidden argument*/NULL);
			V_5 = L_14;
			// CallbackDictionary.Add(usedCallbackId, callbackEntry);
			IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
			Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C * L_15 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get_CallbackDictionary_3();
			uint32_t L_16 = V_0;
			Entry_t6FA80886BFD1655BEB628D855A2FAD715839F6BB * L_17 = V_5;
			NullCheck(L_15);
			Dictionary_2_Add_mEB4CC4CAD43E8C0BA470A4F7FDB71BB47559F441(L_15, L_16, L_17, /*hidden argument*/Dictionary_2_Add_mEB4CC4CAD43E8C0BA470A4F7FDB71BB47559F441_RuntimeMethod_var);
			IL2CPP_LEAVE(0x9F, FINALLY_0093);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0093;
	}

FINALLY_0093:
	{ // begin finally (depth: 1)
		{
			bool L_18 = V_4;
			if (!L_18)
			{
				goto IL_009e;
			}
		}

IL_0097:
		{
			RuntimeObject * L_19 = V_3;
			Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_19, /*hidden argument*/NULL);
		}

IL_009e:
		{
			IL2CPP_END_FINALLY(147)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(147)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x9F, IL_009f)
	}

IL_009f:
	{
		// return usedCallbackId;
		uint32_t L_20 = V_0;
		V_7 = L_20;
		goto IL_00a4;
	}

IL_00a4:
	{
		// }
		uint32_t L_21 = V_7;
		return L_21;
	}
}
// System.Void AppleAuth.AppleAuthManager_CallbackHandler::RemoveMessageCallback(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CallbackHandler_RemoveMessageCallback_m54E5CE8453DA1146E7E132B646E96FB964C72C7B (uint32_t ___requestId0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CallbackHandler_RemoveMessageCallback_m54E5CE8453DA1146E7E132B646E96FB964C72C7B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// lock (SyncLock)
		IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
		RuntimeObject * L_0 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get_SyncLock_2();
		V_0 = L_0;
		V_1 = (bool)0;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		{
			RuntimeObject * L_1 = V_0;
			Monitor_Enter_mC5B353DD83A0B0155DF6FBCC4DF5A580C25534C5(L_1, (bool*)(&V_1), /*hidden argument*/NULL);
			// if (!CallbackDictionary.ContainsKey(requestId))
			IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
			Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C * L_2 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get_CallbackDictionary_3();
			uint32_t L_3 = ___requestId0;
			NullCheck(L_2);
			bool L_4 = Dictionary_2_ContainsKey_mD9907E4F897C40763F8340258AEDDD9FD3F1FCE9(L_2, L_3, /*hidden argument*/Dictionary_2_ContainsKey_mD9907E4F897C40763F8340258AEDDD9FD3F1FCE9_RuntimeMethod_var);
			V_2 = (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
			bool L_5 = V_2;
			if (!L_5)
			{
				goto IL_0041;
			}
		}

IL_0025:
		{
			// throw new Exception("Callback with id " + requestId + " does not exist and can't be removed");
			uint32_t L_6 = ___requestId0;
			uint32_t L_7 = L_6;
			RuntimeObject * L_8 = Box(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var, &L_7);
			String_t* L_9 = String_Concat_m2E1F71C491D2429CC80A28745488FEA947BB7AAC(_stringLiteral4598F0A2F680EBEB450AF29B388AEE917B8C55A5, L_8, _stringLiteral4015416815F6229BA82839676F35279E4BCB0BC7, /*hidden argument*/NULL);
			Exception_t * L_10 = (Exception_t *)il2cpp_codegen_object_new(Exception_t_il2cpp_TypeInfo_var);
			Exception__ctor_m89BADFF36C3B170013878726E07729D51AA9FBE0(L_10, L_9, /*hidden argument*/NULL);
			IL2CPP_RAISE_MANAGED_EXCEPTION(L_10, CallbackHandler_RemoveMessageCallback_m54E5CE8453DA1146E7E132B646E96FB964C72C7B_RuntimeMethod_var);
		}

IL_0041:
		{
			// CallbackDictionary.Remove(requestId);
			IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
			Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C * L_11 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get_CallbackDictionary_3();
			uint32_t L_12 = ___requestId0;
			NullCheck(L_11);
			Dictionary_2_Remove_m78D417809EE5B205B4A02CF24F81E4096BB6EC3D(L_11, L_12, /*hidden argument*/Dictionary_2_Remove_m78D417809EE5B205B4A02CF24F81E4096BB6EC3D_RuntimeMethod_var);
			IL2CPP_LEAVE(0x5B, FINALLY_0050);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0050;
	}

FINALLY_0050:
	{ // begin finally (depth: 1)
		{
			bool L_13 = V_1;
			if (!L_13)
			{
				goto IL_005a;
			}
		}

IL_0053:
		{
			RuntimeObject * L_14 = V_0;
			Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_14, /*hidden argument*/NULL);
		}

IL_005a:
		{
			IL2CPP_END_FINALLY(80)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(80)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x5B, IL_005b)
	}

IL_005b:
	{
		// }
		return;
	}
}
// System.Void AppleAuth.AppleAuthManager_CallbackHandler::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CallbackHandler__cctor_m232A322ACC97DEA243EC9A2BF85855C293E13238 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CallbackHandler__cctor_m232A322ACC97DEA243EC9A2BF85855C293E13238_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static readonly object SyncLock = new object();
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(L_0, /*hidden argument*/NULL);
		((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->set_SyncLock_2(L_0);
		// private static readonly System.Collections.Generic.Dictionary<uint, Entry> CallbackDictionary = new System.Collections.Generic.Dictionary<uint, Entry>();
		Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C * L_1 = (Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C *)il2cpp_codegen_object_new(Dictionary_2_t84DFA6994500F347F5A612BF1750E864FBF10D3C_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mFE529F4016E0E4A41B5B8B456DEB0F58BC6F7E95(L_1, /*hidden argument*/Dictionary_2__ctor_mFE529F4016E0E4A41B5B8B456DEB0F58BC6F7E95_RuntimeMethod_var);
		((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->set_CallbackDictionary_3(L_1);
		// private static readonly System.Collections.Generic.List<Action> ScheduledActions = new System.Collections.Generic.List<Action>();
		List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 * L_2 = (List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46 *)il2cpp_codegen_object_new(List_1_tF3653E6C83C09A018881AD9B85FEB961C912DF46_il2cpp_TypeInfo_var);
		List_1__ctor_m875F64A655E35C2D021756D570AD9C3FBCC2E968(L_2, /*hidden argument*/List_1__ctor_m875F64A655E35C2D021756D570AD9C3FBCC2E968_RuntimeMethod_var);
		((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->set_ScheduledActions_4(L_2);
		// private static uint _callbackId = InitialCallbackId;
		((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->set__callbackId_5(1);
		// private static bool _initialized = false;
		((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->set__initialized_6((bool)0);
		// private static uint _credentialsRevokedCallbackId = 0U;
		((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->set__credentialsRevokedCallbackId_7(0);
		// private static event Action<string> _nativeCredentialsRevoked = null;
		((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->set__nativeCredentialsRevoked_8((Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 *)NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AppleAuth.AppleAuthManager_CallbackHandler_<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m68C6CA84594FBF39B20762F63FD6E4C01368FB86 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__cctor_m68C6CA84594FBF39B20762F63FD6E4C01368FB86_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2 * L_0 = (U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2 *)il2cpp_codegen_object_new(U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_mBD4C3476C866706F3C68BD2C7B862E65E3CFB7D6(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void AppleAuth.AppleAuthManager_CallbackHandler_<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mBD4C3476C866706F3C68BD2C7B862E65E3CFB7D6 (U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AppleAuth.AppleAuthManager_CallbackHandler_<>c::<add_NativeCredentialsRevoked>b__12_0(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3Cadd_NativeCredentialsRevokedU3Eb__12_0_m5724726AA513DABA2EA1B4A0806D72D71DEE9D41 (U3CU3Ec_tF51AB690E3AD969C00449AEC148967E7F78B92B2 * __this, String_t* ___payload0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec_U3Cadd_NativeCredentialsRevokedU3Eb__12_0_m5724726AA513DABA2EA1B4A0806D72D71DEE9D41_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _credentialsRevokedCallbackId = AddMessageCallback(false, payload => _nativeCredentialsRevoked.Invoke(payload));
		IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_0 = ((CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_StaticFields*)il2cpp_codegen_static_fields_for(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var))->get__nativeCredentialsRevoked_8();
		String_t* L_1 = ___payload0;
		NullCheck(L_0);
		Action_1_Invoke_m6328F763431ED2ACDDB6ED8F0FDEA6194403E79F(L_0, L_1, /*hidden argument*/Action_1_Invoke_m6328F763431ED2ACDDB6ED8F0FDEA6194403E79F_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AppleAuth.AppleAuthManager_CallbackHandler_<>c__DisplayClass14_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass14_0__ctor_m955A36C7D7F760FD5656A05D0BF5CB140DF78210 (U3CU3Ec__DisplayClass14_0_t0743B54A2D147CB75027EE3F324C3B375DB67067 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AppleAuth.AppleAuthManager_CallbackHandler_<>c__DisplayClass14_1::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass14_1__ctor_mAEFE6646216AE52125F79843CDF4695B36111113 (U3CU3Ec__DisplayClass14_1_tED700A7605A9EEFA916745644049F0D15DE58F1D * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AppleAuth.AppleAuthManager_CallbackHandler_<>c__DisplayClass14_1::<ScheduleCallback>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass14_1_U3CScheduleCallbackU3Eb__0_m5C8FE466DDC61501D84CF97573FEA1C8B7689192 (U3CU3Ec__DisplayClass14_1_tED700A7605A9EEFA916745644049F0D15DE58F1D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass14_1_U3CScheduleCallbackU3Eb__0_m5C8FE466DDC61501D84CF97573FEA1C8B7689192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ScheduledActions.Add(() => callback.Invoke(payload));
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_0 = __this->get_callback_0();
		U3CU3Ec__DisplayClass14_0_t0743B54A2D147CB75027EE3F324C3B375DB67067 * L_1 = __this->get_CSU24U3CU3E8__locals1_1();
		NullCheck(L_1);
		String_t* L_2 = L_1->get_payload_0();
		NullCheck(L_0);
		Action_1_Invoke_m6328F763431ED2ACDDB6ED8F0FDEA6194403E79F(L_0, L_2, /*hidden argument*/Action_1_Invoke_m6328F763431ED2ACDDB6ED8F0FDEA6194403E79F_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AppleAuth.AppleAuthManager_CallbackHandler_Entry::.ctor(System.Boolean,System.Action`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Entry__ctor_m211294F29613E18A310B5A2CBF5DB141F0C5164F (Entry_t6FA80886BFD1655BEB628D855A2FAD715839F6BB * __this, bool ___isSingleUseCallback0, Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * ___messageCallback1, const RuntimeMethod* method)
{
	{
		// public Entry(bool isSingleUseCallback, Action<string> messageCallback)
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// this.IsSingleUseCallback = isSingleUseCallback;
		bool L_0 = ___isSingleUseCallback0;
		__this->set_IsSingleUseCallback_0(L_0);
		// this.MessageCallback = messageCallback;
		Action_1_t32A9EECF5D4397CC1B9A7C7079870875411B06D0 * L_1 = ___messageCallback1;
		__this->set_MessageCallback_1(L_1);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C" void DEFAULT_CALL ReversePInvokeWrapper_PInvoke_NativeMessageHandlerCallback_m5D5E84BB3C9785BE4384D272156E122275A93DE4(uint32_t ___requestId0, char* ___payload1)
{
	il2cpp::vm::ScopedThreadAttacher _vmThreadHelper;

	// Marshaling of parameter '___payload1' to managed representation
	String_t* ____payload1_unmarshaled = NULL;
	____payload1_unmarshaled = il2cpp_codegen_marshal_string_result(___payload1);

	// Managed method invocation
	PInvoke_NativeMessageHandlerCallback_m5D5E84BB3C9785BE4384D272156E122275A93DE4(___requestId0, ____payload1_unmarshaled, NULL);

}
// System.Void AppleAuth.AppleAuthManager_PInvoke::NativeMessageHandlerCallback(System.UInt32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PInvoke_NativeMessageHandlerCallback_m5D5E84BB3C9785BE4384D272156E122275A93DE4 (uint32_t ___requestId0, String_t* ___payload1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PInvoke_NativeMessageHandlerCallback_m5D5E84BB3C9785BE4384D272156E122275A93DE4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		// CallbackHandler.ScheduleCallback(requestId, payload);
		uint32_t L_0 = ___requestId0;
		String_t* L_1 = ___payload1;
		IL2CPP_RUNTIME_CLASS_INIT(CallbackHandler_t6FA76A255739268432DEAE58953EA3F8A7604AAC_il2cpp_TypeInfo_var);
		CallbackHandler_ScheduleCallback_m085D44C88C7A14F9F29E1170A8FCA2BE13B857F5(L_0, L_1, /*hidden argument*/NULL);
		goto IL_004a;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_000d;
		throw e;
	}

CATCH_000d:
	{ // begin catch(System.Exception)
		// catch (Exception exception)
		V_0 = ((Exception_t *)__exception_local);
		// Console.WriteLine("Received exception while scheduling a callback for request ID " + requestId);
		uint32_t L_2 = ___requestId0;
		uint32_t L_3 = L_2;
		RuntimeObject * L_4 = Box(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var, &L_3);
		String_t* L_5 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteral6995E5E626F48FD5709D7896958EF02CDF3923D9, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t5C8E87BA271B0DECA837A3BF9093AC3560DB3D5D_il2cpp_TypeInfo_var);
		Console_WriteLine_mA5F7E391799514350980A0DE16983383542CA820(L_5, /*hidden argument*/NULL);
		// Console.WriteLine("Detailed payload:\n" + payload);
		String_t* L_6 = ___payload1;
		String_t* L_7 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteral7827EF4895EEF02ED0A612C85CD420118258CD9A, L_6, /*hidden argument*/NULL);
		Console_WriteLine_mA5F7E391799514350980A0DE16983383542CA820(L_7, /*hidden argument*/NULL);
		// Console.WriteLine("Exception: " + exception);
		Exception_t * L_8 = V_0;
		String_t* L_9 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteral441DEA5D56A75C98B8BD51BA0E048ECE7B587ED5, L_8, /*hidden argument*/NULL);
		Console_WriteLine_mA5F7E391799514350980A0DE16983383542CA820(L_9, /*hidden argument*/NULL);
		goto IL_004a;
	} // end catch (depth: 1)

IL_004a:
	{
		// }
		return;
	}
}
IL2CPP_EXTERN_C int32_t DEFAULT_CALL AppleAuth_IsCurrentPlatformSupported();
// System.Boolean AppleAuth.AppleAuthManager_PInvoke::AppleAuth_IsCurrentPlatformSupported()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PInvoke_AppleAuth_IsCurrentPlatformSupported_mF4AB0EB44BE680301E902157841A2E8BE29B9998 (const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(AppleAuth_IsCurrentPlatformSupported)();

	return static_cast<bool>(returnValue);
}
IL2CPP_EXTERN_C void DEFAULT_CALL AppleAuth_SetupNativeMessageHandlerCallback(Il2CppMethodPointer);
// System.Void AppleAuth.AppleAuthManager_PInvoke::AppleAuth_SetupNativeMessageHandlerCallback(AppleAuth.AppleAuthManager_PInvoke_NativeMessageHandlerCallbackDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PInvoke_AppleAuth_SetupNativeMessageHandlerCallback_m2D140AC7EC65E41051360B692A205C2B1345CA90 (NativeMessageHandlerCallbackDelegate_t791982D0270309BB7AEFD64EF9FFA0F7DBFE4A14 * ___callback0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (Il2CppMethodPointer);

	// Marshaling of parameter '___callback0' to native representation
	Il2CppMethodPointer ____callback0_marshaled = NULL;
	____callback0_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<MulticastDelegate_t*>(___callback0));

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(AppleAuth_SetupNativeMessageHandlerCallback)(____callback0_marshaled);

}
IL2CPP_EXTERN_C void DEFAULT_CALL AppleAuth_GetCredentialState(uint32_t, char*);
// System.Void AppleAuth.AppleAuthManager_PInvoke::AppleAuth_GetCredentialState(System.UInt32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PInvoke_AppleAuth_GetCredentialState_mB2184459930094EC4BF2948375B4D18B069A2D28 (uint32_t ___requestId0, String_t* ___userId1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (uint32_t, char*);

	// Marshaling of parameter '___userId1' to native representation
	char* ____userId1_marshaled = NULL;
	____userId1_marshaled = il2cpp_codegen_marshal_string(___userId1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(AppleAuth_GetCredentialState)(___requestId0, ____userId1_marshaled);

	// Marshaling cleanup of parameter '___userId1' native representation
	il2cpp_codegen_marshal_free(____userId1_marshaled);
	____userId1_marshaled = NULL;

}
IL2CPP_EXTERN_C void DEFAULT_CALL AppleAuth_LoginWithAppleId(uint32_t, int32_t, char*, char*);
// System.Void AppleAuth.AppleAuthManager_PInvoke::AppleAuth_LoginWithAppleId(System.UInt32,System.Int32,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PInvoke_AppleAuth_LoginWithAppleId_m955A59F158F91E47BC0A0950F24B1564DF623E9E (uint32_t ___requestId0, int32_t ___loginOptions1, String_t* ___nonceCStr2, String_t* ___stateCStr3, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (uint32_t, int32_t, char*, char*);

	// Marshaling of parameter '___nonceCStr2' to native representation
	char* ____nonceCStr2_marshaled = NULL;
	____nonceCStr2_marshaled = il2cpp_codegen_marshal_string(___nonceCStr2);

	// Marshaling of parameter '___stateCStr3' to native representation
	char* ____stateCStr3_marshaled = NULL;
	____stateCStr3_marshaled = il2cpp_codegen_marshal_string(___stateCStr3);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(AppleAuth_LoginWithAppleId)(___requestId0, ___loginOptions1, ____nonceCStr2_marshaled, ____stateCStr3_marshaled);

	// Marshaling cleanup of parameter '___nonceCStr2' native representation
	il2cpp_codegen_marshal_free(____nonceCStr2_marshaled);
	____nonceCStr2_marshaled = NULL;

	// Marshaling cleanup of parameter '___stateCStr3' native representation
	il2cpp_codegen_marshal_free(____stateCStr3_marshaled);
	____stateCStr3_marshaled = NULL;

}
IL2CPP_EXTERN_C void DEFAULT_CALL AppleAuth_QuickLogin(uint32_t, char*, char*);
// System.Void AppleAuth.AppleAuthManager_PInvoke::AppleAuth_QuickLogin(System.UInt32,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PInvoke_AppleAuth_QuickLogin_m2BDD63FE86A5B848DD047E60AFBD031AB00060A4 (uint32_t ___requestId0, String_t* ___nonceCStr1, String_t* ___stateCStr2, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (uint32_t, char*, char*);

	// Marshaling of parameter '___nonceCStr1' to native representation
	char* ____nonceCStr1_marshaled = NULL;
	____nonceCStr1_marshaled = il2cpp_codegen_marshal_string(___nonceCStr1);

	// Marshaling of parameter '___stateCStr2' to native representation
	char* ____stateCStr2_marshaled = NULL;
	____stateCStr2_marshaled = il2cpp_codegen_marshal_string(___stateCStr2);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(AppleAuth_QuickLogin)(___requestId0, ____nonceCStr1_marshaled, ____stateCStr2_marshaled);

	// Marshaling cleanup of parameter '___nonceCStr1' native representation
	il2cpp_codegen_marshal_free(____nonceCStr1_marshaled);
	____nonceCStr1_marshaled = NULL;

	// Marshaling cleanup of parameter '___stateCStr2' native representation
	il2cpp_codegen_marshal_free(____stateCStr2_marshaled);
	____stateCStr2_marshaled = NULL;

}
IL2CPP_EXTERN_C void DEFAULT_CALL AppleAuth_RegisterCredentialsRevokedCallbackId(uint32_t);
// System.Void AppleAuth.AppleAuthManager_PInvoke::AppleAuth_RegisterCredentialsRevokedCallbackId(System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PInvoke_AppleAuth_RegisterCredentialsRevokedCallbackId_m65B9651E2B15431DD6289C68DD8787F9EDAF8EF8 (uint32_t ___callbackId0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (uint32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(AppleAuth_RegisterCredentialsRevokedCallbackId)(___callbackId0);

}
IL2CPP_EXTERN_C void DEFAULT_CALL AppleAuth_LogMessage(char*);
// System.Void AppleAuth.AppleAuthManager_PInvoke::AppleAuth_LogMessage(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PInvoke_AppleAuth_LogMessage_m9620530B053E06324B4C16569F6605A7BACACEC9 (String_t* ___messageCStr0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___messageCStr0' to native representation
	char* ____messageCStr0_marshaled = NULL;
	____messageCStr0_marshaled = il2cpp_codegen_marshal_string(___messageCStr0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(AppleAuth_LogMessage)(____messageCStr0_marshaled);

	// Marshaling cleanup of parameter '___messageCStr0' native representation
	il2cpp_codegen_marshal_free(____messageCStr0_marshaled);
	____messageCStr0_marshaled = NULL;

}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_NativeMessageHandlerCallbackDelegate_t791982D0270309BB7AEFD64EF9FFA0F7DBFE4A14 (NativeMessageHandlerCallbackDelegate_t791982D0270309BB7AEFD64EF9FFA0F7DBFE4A14 * __this, uint32_t ___requestId0, String_t* ___payload1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(uint32_t, char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___payload1' to native representation
	char* ____payload1_marshaled = NULL;
	____payload1_marshaled = il2cpp_codegen_marshal_string(___payload1);

	// Native function invocation
	il2cppPInvokeFunc(___requestId0, ____payload1_marshaled);

	// Marshaling cleanup of parameter '___payload1' native representation
	il2cpp_codegen_marshal_free(____payload1_marshaled);
	____payload1_marshaled = NULL;

}
// System.Void AppleAuth.AppleAuthManager_PInvoke_NativeMessageHandlerCallbackDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeMessageHandlerCallbackDelegate__ctor_mA999B2608865CF7F48403396ED87EEDDDB0BD602 (NativeMessageHandlerCallbackDelegate_t791982D0270309BB7AEFD64EF9FFA0F7DBFE4A14 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void AppleAuth.AppleAuthManager_PInvoke_NativeMessageHandlerCallbackDelegate::Invoke(System.UInt32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeMessageHandlerCallbackDelegate_Invoke_mDCD2998A4CBAD6F8C9FEF4F3E3BB3BBA23178F69 (NativeMessageHandlerCallbackDelegate_t791982D0270309BB7AEFD64EF9FFA0F7DBFE4A14 * __this, uint32_t ___requestId0, String_t* ___payload1, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (uint32_t, String_t*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___requestId0, ___payload1, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, uint32_t, String_t*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___requestId0, ___payload1, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker2< uint32_t, String_t* >::Invoke(targetMethod, targetThis, ___requestId0, ___payload1);
					else
						GenericVirtActionInvoker2< uint32_t, String_t* >::Invoke(targetMethod, targetThis, ___requestId0, ___payload1);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker2< uint32_t, String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___requestId0, ___payload1);
					else
						VirtActionInvoker2< uint32_t, String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___requestId0, ___payload1);
				}
			}
			else
			{
				if (targetThis == NULL && il2cpp_codegen_class_is_value_type(il2cpp_codegen_method_get_declaring_type(targetMethod)))
				{
					typedef void (*FunctionPointerType) (RuntimeObject*, String_t*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)((reinterpret_cast<RuntimeObject*>(&___requestId0) - 1), ___payload1, targetMethod);
				}
				typedef void (*FunctionPointerType) (void*, uint32_t, String_t*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___requestId0, ___payload1, targetMethod);
			}
		}
	}
}
// System.IAsyncResult AppleAuth.AppleAuthManager_PInvoke_NativeMessageHandlerCallbackDelegate::BeginInvoke(System.UInt32,System.String,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* NativeMessageHandlerCallbackDelegate_BeginInvoke_mC28322C6AC144EA2EBE5EF1C26C448B4449D7F3C (NativeMessageHandlerCallbackDelegate_t791982D0270309BB7AEFD64EF9FFA0F7DBFE4A14 * __this, uint32_t ___requestId0, String_t* ___payload1, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NativeMessageHandlerCallbackDelegate_BeginInvoke_mC28322C6AC144EA2EBE5EF1C26C448B4449D7F3C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_il2cpp_TypeInfo_var, &___requestId0);
	__d_args[1] = ___payload1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void AppleAuth.AppleAuthManager_PInvoke_NativeMessageHandlerCallbackDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeMessageHandlerCallbackDelegate_EndInvoke_m55CAB28C03D0D9E03FEAB0A759F8260A8A828CC5 (NativeMessageHandlerCallbackDelegate_t791982D0270309BB7AEFD64EF9FFA0F7DBFE4A14 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: AppleAuth.AppleAuthQuickLoginArgs
IL2CPP_EXTERN_C void AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A_marshal_pinvoke(const AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A& unmarshaled, AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A_marshaled_pinvoke& marshaled)
{
	marshaled.___Nonce_0 = il2cpp_codegen_marshal_string(unmarshaled.get_Nonce_0());
	marshaled.___State_1 = il2cpp_codegen_marshal_string(unmarshaled.get_State_1());
}
IL2CPP_EXTERN_C void AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A_marshal_pinvoke_back(const AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A_marshaled_pinvoke& marshaled, AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A& unmarshaled)
{
	unmarshaled.set_Nonce_0(il2cpp_codegen_marshal_string_result(marshaled.___Nonce_0));
	unmarshaled.set_State_1(il2cpp_codegen_marshal_string_result(marshaled.___State_1));
}
// Conversion method for clean up from marshalling of: AppleAuth.AppleAuthQuickLoginArgs
IL2CPP_EXTERN_C void AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A_marshal_pinvoke_cleanup(AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___Nonce_0);
	marshaled.___Nonce_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___State_1);
	marshaled.___State_1 = NULL;
}
// Conversion methods for marshalling of: AppleAuth.AppleAuthQuickLoginArgs
IL2CPP_EXTERN_C void AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A_marshal_com(const AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A& unmarshaled, AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A_marshaled_com& marshaled)
{
	marshaled.___Nonce_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_Nonce_0());
	marshaled.___State_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_State_1());
}
IL2CPP_EXTERN_C void AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A_marshal_com_back(const AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A_marshaled_com& marshaled, AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A& unmarshaled)
{
	unmarshaled.set_Nonce_0(il2cpp_codegen_marshal_bstring_result(marshaled.___Nonce_0));
	unmarshaled.set_State_1(il2cpp_codegen_marshal_bstring_result(marshaled.___State_1));
}
// Conversion method for clean up from marshalling of: AppleAuth.AppleAuthQuickLoginArgs
IL2CPP_EXTERN_C void AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A_marshal_com_cleanup(AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___Nonce_0);
	marshaled.___Nonce_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___State_1);
	marshaled.___State_1 = NULL;
}
// System.Void AppleAuth.AppleAuthQuickLoginArgs::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppleAuthQuickLoginArgs__ctor_m002ADB6747DBEF52120701B85E6E69C392680819 (AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A * __this, String_t* ___nonce0, String_t* ___state1, const RuntimeMethod* method)
{
	{
		// this.Nonce = nonce;
		String_t* L_0 = ___nonce0;
		__this->set_Nonce_0(L_0);
		// this.State = state;
		String_t* L_1 = ___state1;
		__this->set_State_1(L_1);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void AppleAuthQuickLoginArgs__ctor_m002ADB6747DBEF52120701B85E6E69C392680819_AdjustorThunk (RuntimeObject * __this, String_t* ___nonce0, String_t* ___state1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A * _thisAdjusted = reinterpret_cast<AppleAuthQuickLoginArgs_tF1C4A36EFE77BDB96319351447A2294EEC5C4F9A *>(__this + _offset);
	AppleAuthQuickLoginArgs__ctor_m002ADB6747DBEF52120701B85E6E69C392680819(_thisAdjusted, ___nonce0, ___state1, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// AppleAuth.Enums.AuthorizationErrorCode AppleAuth.Extensions.AppleErrorExtensions::GetAuthorizationErrorCode(AppleAuth.Interfaces.IAppleError)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AppleErrorExtensions_GetAuthorizationErrorCode_m657BF77C7E6EE94F9643A7720C0F4B749652424B (RuntimeObject* ___error0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppleErrorExtensions_GetAuthorizationErrorCode_m657BF77C7E6EE94F9643A7720C0F4B749652424B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t G_B3_0 = 0;
	{
		// if (error.Domain == "com.apple.AuthenticationServices.AuthorizationError" &&
		//     Enum.IsDefined(typeof(AuthorizationErrorCode), error.Code))
		RuntimeObject* L_0 = ___error0;
		NullCheck(L_0);
		String_t* L_1 = InterfaceFuncInvoker0< String_t* >::Invoke(1 /* System.String AppleAuth.Interfaces.IAppleError::get_Domain() */, IAppleError_t7BFB5142BCDD43FA9230E884B829D317FFC7445E_il2cpp_TypeInfo_var, L_0);
		bool L_2 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_1, _stringLiteral8C80B4AF0AC201C596A77E249D674FAC66C5B9B8, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_3 = { reinterpret_cast<intptr_t> (AuthorizationErrorCode_t0FFD9BA03D3349238191A90F27789A3DD249672F_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_3, /*hidden argument*/NULL);
		RuntimeObject* L_5 = ___error0;
		NullCheck(L_5);
		int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 AppleAuth.Interfaces.IAppleError::get_Code() */, IAppleError_t7BFB5142BCDD43FA9230E884B829D317FFC7445E_il2cpp_TypeInfo_var, L_5);
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_il2cpp_TypeInfo_var);
		bool L_9 = Enum_IsDefined_mA573B15329CA2AA7C59367D514D2927FC66217E2(L_4, L_8, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_9));
		goto IL_0030;
	}

IL_002f:
	{
		G_B3_0 = 0;
	}

IL_0030:
	{
		V_0 = (bool)G_B3_0;
		bool L_10 = V_0;
		if (!L_10)
		{
			goto IL_003e;
		}
	}
	{
		// return (AuthorizationErrorCode)error.Code;
		RuntimeObject* L_11 = ___error0;
		NullCheck(L_11);
		int32_t L_12 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 AppleAuth.Interfaces.IAppleError::get_Code() */, IAppleError_t7BFB5142BCDD43FA9230E884B829D317FFC7445E_il2cpp_TypeInfo_var, L_11);
		V_1 = L_12;
		goto IL_0046;
	}

IL_003e:
	{
		// return AuthorizationErrorCode.Unknown;
		V_1 = ((int32_t)1000);
		goto IL_0046;
	}

IL_0046:
	{
		// }
		int32_t L_13 = V_1;
		return L_13;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String AppleAuth.Extensions.PersonNameExtensions::ToLocalizedString(AppleAuth.Interfaces.IPersonName,AppleAuth.Enums.PersonNameFormatterStyle,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PersonNameExtensions_ToLocalizedString_mDE02E6128882D1255EF7753E1ADBDD7CD6A2E06A (RuntimeObject* ___personName0, int32_t ___style1, bool ___usePhoneticRepresentation2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersonNameExtensions_ToLocalizedString_mDE02E6128882D1255EF7753E1ADBDD7CD6A2E06A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * V_2 = NULL;
	bool V_3 = false;
	String_t* V_4 = NULL;
	bool V_5 = false;
	bool V_6 = false;
	bool V_7 = false;
	bool V_8 = false;
	bool V_9 = false;
	{
		// var jsonString = JsonStringForPersonName(personName);
		RuntimeObject* L_0 = ___personName0;
		String_t* L_1 = PersonNameExtensions_JsonStringForPersonName_m60F3E8CB4AEEFF4A5741EAB787BB034F9F71672B(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// var localizedString = PInvoke.AppleAuth_GetPersonNameUsingFormatter(jsonString, (int) style, usePhoneticRepresentation);
		String_t* L_2 = V_0;
		int32_t L_3 = ___style1;
		bool L_4 = ___usePhoneticRepresentation2;
		String_t* L_5 = PInvoke_AppleAuth_GetPersonNameUsingFormatter_m579639B679D05DD5E952F09B075C0689E8879762(L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		// if (localizedString != null)
		String_t* L_6 = V_1;
		V_3 = (bool)((!(((RuntimeObject*)(String_t*)L_6) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_7 = V_3;
		if (!L_7)
		{
			goto IL_0022;
		}
	}
	{
		// return localizedString;
		String_t* L_8 = V_1;
		V_4 = L_8;
		goto IL_00d2;
	}

IL_0022:
	{
		// var orderedParts = new System.Collections.Generic.List<string>();
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_9 = (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)il2cpp_codegen_object_new(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_il2cpp_TypeInfo_var);
		List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06(L_9, /*hidden argument*/List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06_RuntimeMethod_var);
		V_2 = L_9;
		// if (string.IsNullOrEmpty(personName.NamePrefix))
		RuntimeObject* L_10 = ___personName0;
		NullCheck(L_10);
		String_t* L_11 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String AppleAuth.Interfaces.IPersonName::get_NamePrefix() */, IPersonName_tB6E0862B5573622746FCF68C4368BB51DE6F7B7A_il2cpp_TypeInfo_var, L_10);
		bool L_12 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229(L_11, /*hidden argument*/NULL);
		V_5 = L_12;
		bool L_13 = V_5;
		if (!L_13)
		{
			goto IL_0046;
		}
	}
	{
		// orderedParts.Add(personName.NamePrefix);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_14 = V_2;
		RuntimeObject* L_15 = ___personName0;
		NullCheck(L_15);
		String_t* L_16 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String AppleAuth.Interfaces.IPersonName::get_NamePrefix() */, IPersonName_tB6E0862B5573622746FCF68C4368BB51DE6F7B7A_il2cpp_TypeInfo_var, L_15);
		NullCheck(L_14);
		List_1_Add_mA348FA1140766465189459D25B01EB179001DE83(L_14, L_16, /*hidden argument*/List_1_Add_mA348FA1140766465189459D25B01EB179001DE83_RuntimeMethod_var);
	}

IL_0046:
	{
		// if (string.IsNullOrEmpty(personName.GivenName))
		RuntimeObject* L_17 = ___personName0;
		NullCheck(L_17);
		String_t* L_18 = InterfaceFuncInvoker0< String_t* >::Invoke(1 /* System.String AppleAuth.Interfaces.IPersonName::get_GivenName() */, IPersonName_tB6E0862B5573622746FCF68C4368BB51DE6F7B7A_il2cpp_TypeInfo_var, L_17);
		bool L_19 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229(L_18, /*hidden argument*/NULL);
		V_6 = L_19;
		bool L_20 = V_6;
		if (!L_20)
		{
			goto IL_0064;
		}
	}
	{
		// orderedParts.Add(personName.GivenName);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_21 = V_2;
		RuntimeObject* L_22 = ___personName0;
		NullCheck(L_22);
		String_t* L_23 = InterfaceFuncInvoker0< String_t* >::Invoke(1 /* System.String AppleAuth.Interfaces.IPersonName::get_GivenName() */, IPersonName_tB6E0862B5573622746FCF68C4368BB51DE6F7B7A_il2cpp_TypeInfo_var, L_22);
		NullCheck(L_21);
		List_1_Add_mA348FA1140766465189459D25B01EB179001DE83(L_21, L_23, /*hidden argument*/List_1_Add_mA348FA1140766465189459D25B01EB179001DE83_RuntimeMethod_var);
	}

IL_0064:
	{
		// if (string.IsNullOrEmpty(personName.MiddleName))
		RuntimeObject* L_24 = ___personName0;
		NullCheck(L_24);
		String_t* L_25 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String AppleAuth.Interfaces.IPersonName::get_MiddleName() */, IPersonName_tB6E0862B5573622746FCF68C4368BB51DE6F7B7A_il2cpp_TypeInfo_var, L_24);
		bool L_26 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229(L_25, /*hidden argument*/NULL);
		V_7 = L_26;
		bool L_27 = V_7;
		if (!L_27)
		{
			goto IL_0082;
		}
	}
	{
		// orderedParts.Add(personName.MiddleName);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_28 = V_2;
		RuntimeObject* L_29 = ___personName0;
		NullCheck(L_29);
		String_t* L_30 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String AppleAuth.Interfaces.IPersonName::get_MiddleName() */, IPersonName_tB6E0862B5573622746FCF68C4368BB51DE6F7B7A_il2cpp_TypeInfo_var, L_29);
		NullCheck(L_28);
		List_1_Add_mA348FA1140766465189459D25B01EB179001DE83(L_28, L_30, /*hidden argument*/List_1_Add_mA348FA1140766465189459D25B01EB179001DE83_RuntimeMethod_var);
	}

IL_0082:
	{
		// if (string.IsNullOrEmpty(personName.FamilyName))
		RuntimeObject* L_31 = ___personName0;
		NullCheck(L_31);
		String_t* L_32 = InterfaceFuncInvoker0< String_t* >::Invoke(3 /* System.String AppleAuth.Interfaces.IPersonName::get_FamilyName() */, IPersonName_tB6E0862B5573622746FCF68C4368BB51DE6F7B7A_il2cpp_TypeInfo_var, L_31);
		bool L_33 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229(L_32, /*hidden argument*/NULL);
		V_8 = L_33;
		bool L_34 = V_8;
		if (!L_34)
		{
			goto IL_00a0;
		}
	}
	{
		// orderedParts.Add(personName.FamilyName);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_35 = V_2;
		RuntimeObject* L_36 = ___personName0;
		NullCheck(L_36);
		String_t* L_37 = InterfaceFuncInvoker0< String_t* >::Invoke(3 /* System.String AppleAuth.Interfaces.IPersonName::get_FamilyName() */, IPersonName_tB6E0862B5573622746FCF68C4368BB51DE6F7B7A_il2cpp_TypeInfo_var, L_36);
		NullCheck(L_35);
		List_1_Add_mA348FA1140766465189459D25B01EB179001DE83(L_35, L_37, /*hidden argument*/List_1_Add_mA348FA1140766465189459D25B01EB179001DE83_RuntimeMethod_var);
	}

IL_00a0:
	{
		// if (string.IsNullOrEmpty(personName.NameSuffix))
		RuntimeObject* L_38 = ___personName0;
		NullCheck(L_38);
		String_t* L_39 = InterfaceFuncInvoker0< String_t* >::Invoke(4 /* System.String AppleAuth.Interfaces.IPersonName::get_NameSuffix() */, IPersonName_tB6E0862B5573622746FCF68C4368BB51DE6F7B7A_il2cpp_TypeInfo_var, L_38);
		bool L_40 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229(L_39, /*hidden argument*/NULL);
		V_9 = L_40;
		bool L_41 = V_9;
		if (!L_41)
		{
			goto IL_00be;
		}
	}
	{
		// orderedParts.Add(personName.NameSuffix);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_42 = V_2;
		RuntimeObject* L_43 = ___personName0;
		NullCheck(L_43);
		String_t* L_44 = InterfaceFuncInvoker0< String_t* >::Invoke(4 /* System.String AppleAuth.Interfaces.IPersonName::get_NameSuffix() */, IPersonName_tB6E0862B5573622746FCF68C4368BB51DE6F7B7A_il2cpp_TypeInfo_var, L_43);
		NullCheck(L_42);
		List_1_Add_mA348FA1140766465189459D25B01EB179001DE83(L_42, L_44, /*hidden argument*/List_1_Add_mA348FA1140766465189459D25B01EB179001DE83_RuntimeMethod_var);
	}

IL_00be:
	{
		// return string.Join(" ", orderedParts.ToArray());
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_45 = V_2;
		NullCheck(L_45);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_46 = List_1_ToArray_m9DD19D800AE6D84ED0729D5D97CAF84DF317DD38(L_45, /*hidden argument*/List_1_ToArray_m9DD19D800AE6D84ED0729D5D97CAF84DF317DD38_RuntimeMethod_var);
		String_t* L_47 = String_Join_m49371BED70248F0FCE970CB4F2E39E9A688AAFA4(_stringLiteralB858CB282617FB0956D960215C8E84D1CCF909C6, L_46, /*hidden argument*/NULL);
		V_4 = L_47;
		goto IL_00d2;
	}

IL_00d2:
	{
		// }
		String_t* L_48 = V_4;
		return L_48;
	}
}
// System.String AppleAuth.Extensions.PersonNameExtensions::JsonStringForPersonName(AppleAuth.Interfaces.IPersonName)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PersonNameExtensions_JsonStringForPersonName_m60F3E8CB4AEEFF4A5741EAB787BB034F9F71672B (RuntimeObject* ___personName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersonNameExtensions_JsonStringForPersonName_m60F3E8CB4AEEFF4A5741EAB787BB034F9F71672B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t * V_0 = NULL;
	String_t* V_1 = NULL;
	bool V_2 = false;
	String_t* V_3 = NULL;
	{
		// if (personName == null)
		RuntimeObject* L_0 = ___personName0;
		V_2 = (bool)((((RuntimeObject*)(RuntimeObject*)L_0) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_1 = V_2;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		// return null;
		V_3 = (String_t*)NULL;
		goto IL_00df;
	}

IL_0010:
	{
		// var stringBuilder = new System.Text.StringBuilder();
		StringBuilder_t * L_2 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_mF928376F82E8C8FF3C11842C562DB8CF28B2735E(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		// stringBuilder.Append("{");
		StringBuilder_t * L_3 = V_0;
		NullCheck(L_3);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_3, _stringLiteral60BA4B2DAA4ED4D070FEC06687E249E0E6F9EE45, /*hidden argument*/NULL);
		// TryAddKeyValue(StringDictionaryFormat, "_namePrefix", personName.NamePrefix, stringBuilder);
		RuntimeObject* L_4 = ___personName0;
		NullCheck(L_4);
		String_t* L_5 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String AppleAuth.Interfaces.IPersonName::get_NamePrefix() */, IPersonName_tB6E0862B5573622746FCF68C4368BB51DE6F7B7A_il2cpp_TypeInfo_var, L_4);
		StringBuilder_t * L_6 = V_0;
		PersonNameExtensions_TryAddKeyValue_m3BBD8554A10DC6B6D8BD30500D245EFEF1854C40(_stringLiteralFFC8C524A098DC00EE24D0A8DAB38ED9932880F6, _stringLiteral5B9ED5B09F24B9884FB9EE944F2D5B8980ED9BF9, L_5, L_6, /*hidden argument*/NULL);
		// TryAddKeyValue(StringDictionaryFormat, "_givenName", personName.GivenName, stringBuilder);
		RuntimeObject* L_7 = ___personName0;
		NullCheck(L_7);
		String_t* L_8 = InterfaceFuncInvoker0< String_t* >::Invoke(1 /* System.String AppleAuth.Interfaces.IPersonName::get_GivenName() */, IPersonName_tB6E0862B5573622746FCF68C4368BB51DE6F7B7A_il2cpp_TypeInfo_var, L_7);
		StringBuilder_t * L_9 = V_0;
		PersonNameExtensions_TryAddKeyValue_m3BBD8554A10DC6B6D8BD30500D245EFEF1854C40(_stringLiteralFFC8C524A098DC00EE24D0A8DAB38ED9932880F6, _stringLiteralBBC69D6340E1166E27C0A4765EF658BF4E6F12A8, L_8, L_9, /*hidden argument*/NULL);
		// TryAddKeyValue(StringDictionaryFormat, "_middleName", personName.MiddleName, stringBuilder);
		RuntimeObject* L_10 = ___personName0;
		NullCheck(L_10);
		String_t* L_11 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String AppleAuth.Interfaces.IPersonName::get_MiddleName() */, IPersonName_tB6E0862B5573622746FCF68C4368BB51DE6F7B7A_il2cpp_TypeInfo_var, L_10);
		StringBuilder_t * L_12 = V_0;
		PersonNameExtensions_TryAddKeyValue_m3BBD8554A10DC6B6D8BD30500D245EFEF1854C40(_stringLiteralFFC8C524A098DC00EE24D0A8DAB38ED9932880F6, _stringLiteral5DD507D4A1A6B837CF5B71E12253C51AD4289E52, L_11, L_12, /*hidden argument*/NULL);
		// TryAddKeyValue(StringDictionaryFormat, "_familyName", personName.FamilyName, stringBuilder);
		RuntimeObject* L_13 = ___personName0;
		NullCheck(L_13);
		String_t* L_14 = InterfaceFuncInvoker0< String_t* >::Invoke(3 /* System.String AppleAuth.Interfaces.IPersonName::get_FamilyName() */, IPersonName_tB6E0862B5573622746FCF68C4368BB51DE6F7B7A_il2cpp_TypeInfo_var, L_13);
		StringBuilder_t * L_15 = V_0;
		PersonNameExtensions_TryAddKeyValue_m3BBD8554A10DC6B6D8BD30500D245EFEF1854C40(_stringLiteralFFC8C524A098DC00EE24D0A8DAB38ED9932880F6, _stringLiteral8120F8F7D255BD92291AAA653E8164F6A6B1FF90, L_14, L_15, /*hidden argument*/NULL);
		// TryAddKeyValue(StringDictionaryFormat, "_nameSuffix", personName.NameSuffix, stringBuilder);
		RuntimeObject* L_16 = ___personName0;
		NullCheck(L_16);
		String_t* L_17 = InterfaceFuncInvoker0< String_t* >::Invoke(4 /* System.String AppleAuth.Interfaces.IPersonName::get_NameSuffix() */, IPersonName_tB6E0862B5573622746FCF68C4368BB51DE6F7B7A_il2cpp_TypeInfo_var, L_16);
		StringBuilder_t * L_18 = V_0;
		PersonNameExtensions_TryAddKeyValue_m3BBD8554A10DC6B6D8BD30500D245EFEF1854C40(_stringLiteralFFC8C524A098DC00EE24D0A8DAB38ED9932880F6, _stringLiteralBCACF7319C9F96F367F1418203A31C525A5E3CD9, L_17, L_18, /*hidden argument*/NULL);
		// TryAddKeyValue(StringDictionaryFormat, "_nickname", personName.Nickname, stringBuilder);
		RuntimeObject* L_19 = ___personName0;
		NullCheck(L_19);
		String_t* L_20 = InterfaceFuncInvoker0< String_t* >::Invoke(5 /* System.String AppleAuth.Interfaces.IPersonName::get_Nickname() */, IPersonName_tB6E0862B5573622746FCF68C4368BB51DE6F7B7A_il2cpp_TypeInfo_var, L_19);
		StringBuilder_t * L_21 = V_0;
		PersonNameExtensions_TryAddKeyValue_m3BBD8554A10DC6B6D8BD30500D245EFEF1854C40(_stringLiteralFFC8C524A098DC00EE24D0A8DAB38ED9932880F6, _stringLiteral63E86140555073BFEED4D739D25CCEC3567ED623, L_20, L_21, /*hidden argument*/NULL);
		// var phoneticRepresentationJson = JsonStringForPersonName(personName.PhoneticRepresentation);
		RuntimeObject* L_22 = ___personName0;
		NullCheck(L_22);
		RuntimeObject* L_23 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(6 /* AppleAuth.Interfaces.IPersonName AppleAuth.Interfaces.IPersonName::get_PhoneticRepresentation() */, IPersonName_tB6E0862B5573622746FCF68C4368BB51DE6F7B7A_il2cpp_TypeInfo_var, L_22);
		String_t* L_24 = PersonNameExtensions_JsonStringForPersonName_m60F3E8CB4AEEFF4A5741EAB787BB034F9F71672B(L_23, /*hidden argument*/NULL);
		V_1 = L_24;
		// TryAddKeyValue(StringObjectFormat, "_phoneticRepresentation", phoneticRepresentationJson, stringBuilder);
		String_t* L_25 = V_1;
		StringBuilder_t * L_26 = V_0;
		PersonNameExtensions_TryAddKeyValue_m3BBD8554A10DC6B6D8BD30500D245EFEF1854C40(_stringLiteralD7E7549C9E8D32D1A983F1A0B1165651D38EC876, _stringLiteral2AAEC0C992B44185948950AC0ABBF8A01636ABE4, L_25, L_26, /*hidden argument*/NULL);
		// stringBuilder.Append("}");
		StringBuilder_t * L_27 = V_0;
		NullCheck(L_27);
		StringBuilder_Append_mDBB8CCBB7750C67BE2F2D92F47E6C0FA42793260(L_27, _stringLiteralC2B7DF6201FDD3362399091F0A29550DF3505B6A, /*hidden argument*/NULL);
		// return stringBuilder.ToString();
		StringBuilder_t * L_28 = V_0;
		NullCheck(L_28);
		String_t* L_29 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_28);
		V_3 = L_29;
		goto IL_00df;
	}

IL_00df:
	{
		// }
		String_t* L_30 = V_3;
		return L_30;
	}
}
// System.Void AppleAuth.Extensions.PersonNameExtensions::TryAddKeyValue(System.String,System.String,System.String,System.Text.StringBuilder)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersonNameExtensions_TryAddKeyValue_m3BBD8554A10DC6B6D8BD30500D245EFEF1854C40 (String_t* ___format0, String_t* ___key1, String_t* ___value2, StringBuilder_t * ___stringBuilder3, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// if (string.IsNullOrEmpty(value))
		String_t* L_0 = ___value2;
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_000d;
		}
	}
	{
		// return;
		goto IL_0017;
	}

IL_000d:
	{
		// stringBuilder.AppendFormat(format, key, value);
		StringBuilder_t * L_3 = ___stringBuilder3;
		String_t* L_4 = ___format0;
		String_t* L_5 = ___key1;
		String_t* L_6 = ___value2;
		NullCheck(L_3);
		StringBuilder_AppendFormat_m9DBA7709F546159ABC85BA341965305AB044D1B7(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_0017:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C char* DEFAULT_CALL AppleAuth_GetPersonNameUsingFormatter(char*, int32_t, int32_t);
// System.String AppleAuth.Extensions.PersonNameExtensions_PInvoke::AppleAuth_GetPersonNameUsingFormatter(System.String,System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PInvoke_AppleAuth_GetPersonNameUsingFormatter_m579639B679D05DD5E952F09B075C0689E8879762 (String_t* ___payload0, int32_t ___style1, bool ___usePhoneticRepresentation2, const RuntimeMethod* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) (char*, int32_t, int32_t);

	// Marshaling of parameter '___payload0' to native representation
	char* ____payload0_marshaled = NULL;
	____payload0_marshaled = il2cpp_codegen_marshal_string(___payload0);

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(AppleAuth_GetPersonNameUsingFormatter)(____payload0_marshaled, ___style1, static_cast<int32_t>(___usePhoneticRepresentation2));

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	// Marshaling cleanup of parameter '___payload0' native representation
	il2cpp_codegen_marshal_free(____payload0_marshaled);
	____payload0_marshaled = NULL;

	return _returnValue_unmarshaled;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 AppleAuth.Native.AppleError::get_Code()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AppleError_get_Code_m2DC6FA24A78F376AC7759F74CB97456F98C3E3D5 (AppleError_t157640890ACABCEB007334DB77D1279DCB27223E * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// public int Code { get { return this._code; } }
		int32_t L_0 = __this->get__code_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public int Code { get { return this._code; } }
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.String AppleAuth.Native.AppleError::get_Domain()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AppleError_get_Domain_m5570BBA51B6AC6502731859394EE956DEAC8AFD5 (AppleError_t157640890ACABCEB007334DB77D1279DCB27223E * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// public string Domain { get { return this._domain; } }
		String_t* L_0 = __this->get__domain_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public string Domain { get { return this._domain; } }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String AppleAuth.Native.AppleError::get_LocalizedDescription()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AppleError_get_LocalizedDescription_m0528258B3E61E724A22BAA0CF7446F3FD0848A1E (AppleError_t157640890ACABCEB007334DB77D1279DCB27223E * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// public string LocalizedDescription { get { return this._localizedDescription; } }
		String_t* L_0 = __this->get__localizedDescription_2();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public string LocalizedDescription { get { return this._localizedDescription; } }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String[] AppleAuth.Native.AppleError::get_LocalizedRecoveryOptions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* AppleError_get_LocalizedRecoveryOptions_mAA19861613F1EE020B040A278D75F55AB5B03F97 (AppleError_t157640890ACABCEB007334DB77D1279DCB27223E * __this, const RuntimeMethod* method)
{
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* V_0 = NULL;
	{
		// public string[] LocalizedRecoveryOptions { get { return this._localizedRecoveryOptions; } }
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_0 = __this->get__localizedRecoveryOptions_3();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public string[] LocalizedRecoveryOptions { get { return this._localizedRecoveryOptions; } }
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_1 = V_0;
		return L_1;
	}
}
// System.String AppleAuth.Native.AppleError::get_LocalizedRecoverySuggestion()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AppleError_get_LocalizedRecoverySuggestion_m103E894877132C6986B995CE1094E5F665A0D924 (AppleError_t157640890ACABCEB007334DB77D1279DCB27223E * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// public string LocalizedRecoverySuggestion { get { return this._localizedRecoverySuggestion; } }
		String_t* L_0 = __this->get__localizedRecoverySuggestion_4();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public string LocalizedRecoverySuggestion { get { return this._localizedRecoverySuggestion; } }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String AppleAuth.Native.AppleError::get_LocalizedFailureReason()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AppleError_get_LocalizedFailureReason_m452D315F855E29C98CCCC5FAD4AD76439894B2FB (AppleError_t157640890ACABCEB007334DB77D1279DCB27223E * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// public string LocalizedFailureReason { get { return this._localizedFailureReason; } }
		String_t* L_0 = __this->get__localizedFailureReason_5();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public string LocalizedFailureReason { get { return this._localizedFailureReason; } }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void AppleAuth.Native.AppleError::OnBeforeSerialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppleError_OnBeforeSerialize_m672A853DB847E0037955E714AC8E7E23010F2E38 (AppleError_t157640890ACABCEB007334DB77D1279DCB27223E * __this, const RuntimeMethod* method)
{
	{
		// public void OnBeforeSerialize() { }
		return;
	}
}
// System.Void AppleAuth.Native.AppleError::OnAfterDeserialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppleError_OnAfterDeserialize_m6F38B1635730B512980DD3C80D1BEA4AB8D80AD9 (AppleError_t157640890ACABCEB007334DB77D1279DCB27223E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppleError_OnAfterDeserialize_m6F38B1635730B512980DD3C80D1BEA4AB8D80AD9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SerializationTools.FixSerializationForString(ref this._domain);
		String_t** L_0 = __this->get_address_of__domain_1();
		SerializationTools_FixSerializationForString_m8171A047B8FCDC62B846791D70C2DA21418D8650((String_t**)L_0, /*hidden argument*/NULL);
		// SerializationTools.FixSerializationForString(ref this._localizedDescription);
		String_t** L_1 = __this->get_address_of__localizedDescription_2();
		SerializationTools_FixSerializationForString_m8171A047B8FCDC62B846791D70C2DA21418D8650((String_t**)L_1, /*hidden argument*/NULL);
		// SerializationTools.FixSerializationForString(ref this._localizedRecoverySuggestion);
		String_t** L_2 = __this->get_address_of__localizedRecoverySuggestion_4();
		SerializationTools_FixSerializationForString_m8171A047B8FCDC62B846791D70C2DA21418D8650((String_t**)L_2, /*hidden argument*/NULL);
		// SerializationTools.FixSerializationForString(ref this._localizedFailureReason);
		String_t** L_3 = __this->get_address_of__localizedFailureReason_5();
		SerializationTools_FixSerializationForString_m8171A047B8FCDC62B846791D70C2DA21418D8650((String_t**)L_3, /*hidden argument*/NULL);
		// SerializationTools.FixSerializationForArray(ref this._localizedRecoveryOptions);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** L_4 = __this->get_address_of__localizedRecoveryOptions_3();
		SerializationTools_FixSerializationForArray_TisString_t_m163AA34D516D317C89666B944EEDE14BF8DCF53D((StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E**)L_4, /*hidden argument*/SerializationTools_FixSerializationForArray_TisString_t_m163AA34D516D317C89666B944EEDE14BF8DCF53D_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void AppleAuth.Native.AppleError::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppleError__ctor_mCAB1FA6B14D31603B7A898FB89E6B8A063A16F21 (AppleError_t157640890ACABCEB007334DB77D1279DCB27223E * __this, const RuntimeMethod* method)
{
	{
		// public int _code = 0;
		__this->set__code_0(0);
		// public string _domain = null;
		__this->set__domain_1((String_t*)NULL);
		// public string _localizedDescription = null;
		__this->set__localizedDescription_2((String_t*)NULL);
		// public string[] _localizedRecoveryOptions = null;
		__this->set__localizedRecoveryOptions_3((StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)NULL);
		// public string _localizedRecoverySuggestion = null;
		__this->set__localizedRecoverySuggestion_4((String_t*)NULL);
		// public string _localizedFailureReason = null;
		__this->set__localizedFailureReason_5((String_t*)NULL);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Byte[] AppleAuth.Native.AppleIDCredential::get_IdentityToken()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* AppleIDCredential_get_IdentityToken_mD9A23ADD2B974E3174AE1157B8C52AAAF8472A29 (AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0 * __this, const RuntimeMethod* method)
{
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	{
		// public byte[] IdentityToken { get { return this._identityToken; } }
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = __this->get__identityToken_9();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public byte[] IdentityToken { get { return this._identityToken; } }
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = V_0;
		return L_1;
	}
}
// System.Byte[] AppleAuth.Native.AppleIDCredential::get_AuthorizationCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* AppleIDCredential_get_AuthorizationCode_mE91B95D24E462A4FFAD1679F8F8F8839E0DE838C (AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0 * __this, const RuntimeMethod* method)
{
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	{
		// public byte[] AuthorizationCode { get { return this._authorizationCode; } }
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = __this->get__authorizationCode_10();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public byte[] AuthorizationCode { get { return this._authorizationCode; } }
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_1 = V_0;
		return L_1;
	}
}
// System.String AppleAuth.Native.AppleIDCredential::get_State()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AppleIDCredential_get_State_mA59F6A34B119CF63FC5BAD01E953DF87C7DAD71E (AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// public string State { get { return this._state; } }
		String_t* L_0 = __this->get__state_2();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public string State { get { return this._state; } }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String AppleAuth.Native.AppleIDCredential::get_User()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AppleIDCredential_get_User_mCB948350B33024427D5274E190387D8D716BE2FC (AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// public string User { get { return this._user; } }
		String_t* L_0 = __this->get__user_3();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public string User { get { return this._user; } }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String[] AppleAuth.Native.AppleIDCredential::get_AuthorizedScopes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* AppleIDCredential_get_AuthorizedScopes_mC3D225AC26C5E53B88CC33BE80D7C9C67B97C465 (AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0 * __this, const RuntimeMethod* method)
{
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* V_0 = NULL;
	{
		// public string[] AuthorizedScopes { get { return this._authorizedScopes; } }
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_0 = __this->get__authorizedScopes_4();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public string[] AuthorizedScopes { get { return this._authorizedScopes; } }
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_1 = V_0;
		return L_1;
	}
}
// AppleAuth.Interfaces.IPersonName AppleAuth.Native.AppleIDCredential::get_FullName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* AppleIDCredential_get_FullName_m0241AF6E01D81B58698B766FAE28FAF2D63BE612 (AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		// public IPersonName FullName { get { return this._fullName; } }
		FullPersonName_t2AC7C2FEA68179788173459B7B32BECF0C41B875 * L_0 = __this->get__fullName_6();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public IPersonName FullName { get { return this._fullName; } }
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// System.String AppleAuth.Native.AppleIDCredential::get_Email()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* AppleIDCredential_get_Email_m1DC5268DB27B573E5A5A0136AD66DB729AD105E6 (AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// public string Email { get { return this._email; } }
		String_t* L_0 = __this->get__email_7();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public string Email { get { return this._email; } }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// AppleAuth.Enums.RealUserStatus AppleAuth.Native.AppleIDCredential::get_RealUserStatus()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t AppleIDCredential_get_RealUserStatus_m33277B12D4375B895255B7066D941C178147CB05 (AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// public RealUserStatus RealUserStatus { get { return (RealUserStatus) this._realUserStatus; } }
		int32_t L_0 = __this->get__realUserStatus_8();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public RealUserStatus RealUserStatus { get { return (RealUserStatus) this._realUserStatus; } }
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void AppleAuth.Native.AppleIDCredential::OnBeforeSerialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppleIDCredential_OnBeforeSerialize_mA40DB44EC7DE2EC21B7CDD46F4B10B0A4615B11E (AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0 * __this, const RuntimeMethod* method)
{
	{
		// public void OnBeforeSerialize() { }
		return;
	}
}
// System.Void AppleAuth.Native.AppleIDCredential::OnAfterDeserialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppleIDCredential_OnAfterDeserialize_m21F884F713F24E7D7B9ACA8880E4784E9F082DE1 (AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppleIDCredential_OnAfterDeserialize_m21F884F713F24E7D7B9ACA8880E4784E9F082DE1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SerializationTools.FixSerializationForString(ref this._base64IdentityToken);
		String_t** L_0 = __this->get_address_of__base64IdentityToken_0();
		SerializationTools_FixSerializationForString_m8171A047B8FCDC62B846791D70C2DA21418D8650((String_t**)L_0, /*hidden argument*/NULL);
		// SerializationTools.FixSerializationForString(ref this._base64AuthorizationCode);
		String_t** L_1 = __this->get_address_of__base64AuthorizationCode_1();
		SerializationTools_FixSerializationForString_m8171A047B8FCDC62B846791D70C2DA21418D8650((String_t**)L_1, /*hidden argument*/NULL);
		// SerializationTools.FixSerializationForString(ref this._state);
		String_t** L_2 = __this->get_address_of__state_2();
		SerializationTools_FixSerializationForString_m8171A047B8FCDC62B846791D70C2DA21418D8650((String_t**)L_2, /*hidden argument*/NULL);
		// SerializationTools.FixSerializationForString(ref this._user);
		String_t** L_3 = __this->get_address_of__user_3();
		SerializationTools_FixSerializationForString_m8171A047B8FCDC62B846791D70C2DA21418D8650((String_t**)L_3, /*hidden argument*/NULL);
		// SerializationTools.FixSerializationForString(ref this._email);
		String_t** L_4 = __this->get_address_of__email_7();
		SerializationTools_FixSerializationForString_m8171A047B8FCDC62B846791D70C2DA21418D8650((String_t**)L_4, /*hidden argument*/NULL);
		// SerializationTools.FixSerializationForArray(ref this._authorizedScopes);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** L_5 = __this->get_address_of__authorizedScopes_4();
		SerializationTools_FixSerializationForArray_TisString_t_m163AA34D516D317C89666B944EEDE14BF8DCF53D((StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E**)L_5, /*hidden argument*/SerializationTools_FixSerializationForArray_TisString_t_m163AA34D516D317C89666B944EEDE14BF8DCF53D_RuntimeMethod_var);
		// SerializationTools.FixSerializationForObject(ref this._fullName, this._hasFullName);
		FullPersonName_t2AC7C2FEA68179788173459B7B32BECF0C41B875 ** L_6 = __this->get_address_of__fullName_6();
		bool L_7 = __this->get__hasFullName_5();
		SerializationTools_FixSerializationForObject_TisFullPersonName_t2AC7C2FEA68179788173459B7B32BECF0C41B875_m0CD7820D8502A3091B822BF4899CDF95314369E4((FullPersonName_t2AC7C2FEA68179788173459B7B32BECF0C41B875 **)L_6, L_7, /*hidden argument*/SerializationTools_FixSerializationForObject_TisFullPersonName_t2AC7C2FEA68179788173459B7B32BECF0C41B875_m0CD7820D8502A3091B822BF4899CDF95314369E4_RuntimeMethod_var);
		// this._identityToken = SerializationTools.GetBytesFromBase64String(this._base64IdentityToken, "_identityToken");
		String_t* L_8 = __this->get__base64IdentityToken_0();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_9 = SerializationTools_GetBytesFromBase64String_m861B8FE0EC904AC192A9D2DD8D5CC6B51A7F2CAD(L_8, _stringLiteralE3F56338D22165DC687E508DF1273220C341FAE9, /*hidden argument*/NULL);
		__this->set__identityToken_9(L_9);
		// this._authorizationCode = SerializationTools.GetBytesFromBase64String(this._base64AuthorizationCode, "_authorizationCode");
		String_t* L_10 = __this->get__base64AuthorizationCode_1();
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_11 = SerializationTools_GetBytesFromBase64String_m861B8FE0EC904AC192A9D2DD8D5CC6B51A7F2CAD(L_10, _stringLiteral2DE8C080871706C72188444F394AB7D4883B5D3A, /*hidden argument*/NULL);
		__this->set__authorizationCode_10(L_11);
		// }
		return;
	}
}
// System.Void AppleAuth.Native.AppleIDCredential::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AppleIDCredential__ctor_m233E172484F302A79A023E6112756E25D2E237E7 (AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0 * __this, const RuntimeMethod* method)
{
	{
		// public string _base64IdentityToken = null;
		__this->set__base64IdentityToken_0((String_t*)NULL);
		// public string _base64AuthorizationCode = null;
		__this->set__base64AuthorizationCode_1((String_t*)NULL);
		// public string _state = null;
		__this->set__state_2((String_t*)NULL);
		// public string _user = null;
		__this->set__user_3((String_t*)NULL);
		// public string[] _authorizedScopes = null;
		__this->set__authorizedScopes_4((StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)NULL);
		// public bool _hasFullName = false;
		__this->set__hasFullName_5((bool)0);
		// public FullPersonName _fullName = null;
		__this->set__fullName_6((FullPersonName_t2AC7C2FEA68179788173459B7B32BECF0C41B875 *)NULL);
		// public string _email = null;
		__this->set__email_7((String_t*)NULL);
		// public int _realUserStatus = 0;
		__this->set__realUserStatus_8(0);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean AppleAuth.Native.CredentialStateResponse::get_Success()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CredentialStateResponse_get_Success_m82535AF6717492354D8205926985762AA154C61D (CredentialStateResponse_t293F3810FEEAB306E15098AB47F7FE21607854B7 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// public bool Success { get { return this._success; } }
		bool L_0 = __this->get__success_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public bool Success { get { return this._success; } }
		bool L_1 = V_0;
		return L_1;
	}
}
// AppleAuth.Enums.CredentialState AppleAuth.Native.CredentialStateResponse::get_CredentialState()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CredentialStateResponse_get_CredentialState_m928023D0F25060F95DFECA3F167F7B129D1F9BFE (CredentialStateResponse_t293F3810FEEAB306E15098AB47F7FE21607854B7 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// public CredentialState CredentialState { get { return (CredentialState) this._credentialState; } }
		int32_t L_0 = __this->get__credentialState_3();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public CredentialState CredentialState { get { return (CredentialState) this._credentialState; } }
		int32_t L_1 = V_0;
		return L_1;
	}
}
// AppleAuth.Interfaces.IAppleError AppleAuth.Native.CredentialStateResponse::get_Error()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CredentialStateResponse_get_Error_m0A2A022847F24D12928ED533DB47FAE49D91E462 (CredentialStateResponse_t293F3810FEEAB306E15098AB47F7FE21607854B7 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		// public IAppleError Error { get { return this._error; } }
		AppleError_t157640890ACABCEB007334DB77D1279DCB27223E * L_0 = __this->get__error_4();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public IAppleError Error { get { return this._error; } }
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// System.Void AppleAuth.Native.CredentialStateResponse::OnBeforeSerialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CredentialStateResponse_OnBeforeSerialize_mCF51ED02727B0BC8154D07F96D3DB3EA05C4B02B (CredentialStateResponse_t293F3810FEEAB306E15098AB47F7FE21607854B7 * __this, const RuntimeMethod* method)
{
	{
		// public void OnBeforeSerialize() { }
		return;
	}
}
// System.Void AppleAuth.Native.CredentialStateResponse::OnAfterDeserialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CredentialStateResponse_OnAfterDeserialize_m517F35790E5F8574E84FA626C835ED67B66D3234 (CredentialStateResponse_t293F3810FEEAB306E15098AB47F7FE21607854B7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CredentialStateResponse_OnAfterDeserialize_m517F35790E5F8574E84FA626C835ED67B66D3234_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SerializationTools.FixSerializationForObject(ref this._credentialState, this._hasCredentialState);
		int32_t* L_0 = __this->get_address_of__credentialState_3();
		bool L_1 = __this->get__hasCredentialState_1();
		SerializationTools_FixSerializationForObject_TisInt32_t585191389E07734F19F3156FF88FB3EF4800D102_mF97F93CA0140582AB28C2FBBF667F05E87F95449((int32_t*)L_0, L_1, /*hidden argument*/SerializationTools_FixSerializationForObject_TisInt32_t585191389E07734F19F3156FF88FB3EF4800D102_mF97F93CA0140582AB28C2FBBF667F05E87F95449_RuntimeMethod_var);
		// SerializationTools.FixSerializationForObject(ref this._error, this._hasError);
		AppleError_t157640890ACABCEB007334DB77D1279DCB27223E ** L_2 = __this->get_address_of__error_4();
		bool L_3 = __this->get__hasError_2();
		SerializationTools_FixSerializationForObject_TisAppleError_t157640890ACABCEB007334DB77D1279DCB27223E_m5AE3CC0F4C6F2F346DFDECDE2B62C98BBD65DCE7((AppleError_t157640890ACABCEB007334DB77D1279DCB27223E **)L_2, L_3, /*hidden argument*/SerializationTools_FixSerializationForObject_TisAppleError_t157640890ACABCEB007334DB77D1279DCB27223E_m5AE3CC0F4C6F2F346DFDECDE2B62C98BBD65DCE7_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void AppleAuth.Native.CredentialStateResponse::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CredentialStateResponse__ctor_m14F70BAA0EDD1A6971E8D7E7F45B5F82D38DEA75 (CredentialStateResponse_t293F3810FEEAB306E15098AB47F7FE21607854B7 * __this, const RuntimeMethod* method)
{
	{
		// public bool _success = false;
		__this->set__success_0((bool)0);
		// public bool _hasCredentialState = false;
		__this->set__hasCredentialState_1((bool)0);
		// public bool _hasError = false;
		__this->set__hasError_2((bool)0);
		// public int _credentialState = 0;
		__this->set__credentialState_3(0);
		// public AppleError _error = null;
		__this->set__error_4((AppleError_t157640890ACABCEB007334DB77D1279DCB27223E *)NULL);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// AppleAuth.Interfaces.IPersonName AppleAuth.Native.FullPersonName::get_PhoneticRepresentation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* FullPersonName_get_PhoneticRepresentation_m12EAC6FB3B6497A6DB661B72405D4C7BAA7FDCC6 (FullPersonName_t2AC7C2FEA68179788173459B7B32BECF0C41B875 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		// public new IPersonName PhoneticRepresentation { get { return _phoneticRepresentation; } }
		PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0 * L_0 = __this->get__phoneticRepresentation_7();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public new IPersonName PhoneticRepresentation { get { return _phoneticRepresentation; } }
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// System.Void AppleAuth.Native.FullPersonName::OnAfterDeserialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FullPersonName_OnAfterDeserialize_mEA9D5C99803AB1C5CCD7353848CE73E1CD7588D7 (FullPersonName_t2AC7C2FEA68179788173459B7B32BECF0C41B875 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FullPersonName_OnAfterDeserialize_mEA9D5C99803AB1C5CCD7353848CE73E1CD7588D7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.OnAfterDeserialize();
		PersonName_OnAfterDeserialize_m32D4307910B89D3E33FBBE420CBA93641F8F0ACD(__this, /*hidden argument*/NULL);
		// SerializationTools.FixSerializationForObject(ref this._phoneticRepresentation, this._hasPhoneticRepresentation);
		PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0 ** L_0 = __this->get_address_of__phoneticRepresentation_7();
		bool L_1 = __this->get__hasPhoneticRepresentation_6();
		SerializationTools_FixSerializationForObject_TisPersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0_mA295ACB04AD3B5BDBC01ECD5D1F9280893829245((PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0 **)L_0, L_1, /*hidden argument*/SerializationTools_FixSerializationForObject_TisPersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0_mA295ACB04AD3B5BDBC01ECD5D1F9280893829245_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void AppleAuth.Native.FullPersonName::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FullPersonName__ctor_m6B6C1CCBB5E050F80C876443FB3B6B7EDF81A519 (FullPersonName_t2AC7C2FEA68179788173459B7B32BECF0C41B875 * __this, const RuntimeMethod* method)
{
	{
		// public bool _hasPhoneticRepresentation = false;
		__this->set__hasPhoneticRepresentation_6((bool)0);
		// public PersonName _phoneticRepresentation = null;
		__this->set__phoneticRepresentation_7((PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0 *)NULL);
		PersonName__ctor_m320D2DECA0176EE689309F89168BC54311BDD69C(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean AppleAuth.Native.LoginWithAppleIdResponse::get_Success()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LoginWithAppleIdResponse_get_Success_mD4595AEB2EB43101FB255838FF10DE5DA288AA8A (LoginWithAppleIdResponse_tEADF70491D90DDEE797098E7FFCEC61C231ACE4C * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// public bool Success { get { return this._success; } }
		bool L_0 = __this->get__success_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public bool Success { get { return this._success; } }
		bool L_1 = V_0;
		return L_1;
	}
}
// AppleAuth.Interfaces.IAppleError AppleAuth.Native.LoginWithAppleIdResponse::get_Error()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* LoginWithAppleIdResponse_get_Error_mA5F4E932F0B21B4D6089465CDB617439F5273BE7 (LoginWithAppleIdResponse_tEADF70491D90DDEE797098E7FFCEC61C231ACE4C * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		// public IAppleError Error { get { return this._error; } }
		AppleError_t157640890ACABCEB007334DB77D1279DCB27223E * L_0 = __this->get__error_6();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public IAppleError Error { get { return this._error; } }
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// AppleAuth.Interfaces.IAppleIDCredential AppleAuth.Native.LoginWithAppleIdResponse::get_AppleIDCredential()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* LoginWithAppleIdResponse_get_AppleIDCredential_mE60828381C62290D6318784BEA6CF5D36900369E (LoginWithAppleIdResponse_tEADF70491D90DDEE797098E7FFCEC61C231ACE4C * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		// public IAppleIDCredential AppleIDCredential { get { return this._appleIdCredential; } }
		AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0 * L_0 = __this->get__appleIdCredential_4();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public IAppleIDCredential AppleIDCredential { get { return this._appleIdCredential; } }
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// AppleAuth.Interfaces.IPasswordCredential AppleAuth.Native.LoginWithAppleIdResponse::get_PasswordCredential()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* LoginWithAppleIdResponse_get_PasswordCredential_m9DEAEB1B7F3015EC979C4B3150EAC04A9FD6787D (LoginWithAppleIdResponse_tEADF70491D90DDEE797098E7FFCEC61C231ACE4C * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		// public IPasswordCredential PasswordCredential { get { return this._passwordCredential; } }
		PasswordCredential_t64C056A6AE5B8356D435709C776795D925E35C40 * L_0 = __this->get__passwordCredential_5();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public IPasswordCredential PasswordCredential { get { return this._passwordCredential; } }
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// System.Void AppleAuth.Native.LoginWithAppleIdResponse::OnBeforeSerialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoginWithAppleIdResponse_OnBeforeSerialize_m1FF29265303F70312D5CF9B97B3F8A7F0C01A7F0 (LoginWithAppleIdResponse_tEADF70491D90DDEE797098E7FFCEC61C231ACE4C * __this, const RuntimeMethod* method)
{
	{
		// public void OnBeforeSerialize() { }
		return;
	}
}
// System.Void AppleAuth.Native.LoginWithAppleIdResponse::OnAfterDeserialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoginWithAppleIdResponse_OnAfterDeserialize_m95F017BF70EDB75F9113FDBBE7D991BD415F6AA0 (LoginWithAppleIdResponse_tEADF70491D90DDEE797098E7FFCEC61C231ACE4C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LoginWithAppleIdResponse_OnAfterDeserialize_m95F017BF70EDB75F9113FDBBE7D991BD415F6AA0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SerializationTools.FixSerializationForObject(ref this._error, this._hasError);
		AppleError_t157640890ACABCEB007334DB77D1279DCB27223E ** L_0 = __this->get_address_of__error_6();
		bool L_1 = __this->get__hasError_3();
		SerializationTools_FixSerializationForObject_TisAppleError_t157640890ACABCEB007334DB77D1279DCB27223E_m5AE3CC0F4C6F2F346DFDECDE2B62C98BBD65DCE7((AppleError_t157640890ACABCEB007334DB77D1279DCB27223E **)L_0, L_1, /*hidden argument*/SerializationTools_FixSerializationForObject_TisAppleError_t157640890ACABCEB007334DB77D1279DCB27223E_m5AE3CC0F4C6F2F346DFDECDE2B62C98BBD65DCE7_RuntimeMethod_var);
		// SerializationTools.FixSerializationForObject(ref this._appleIdCredential, this._hasAppleIdCredential);
		AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0 ** L_2 = __this->get_address_of__appleIdCredential_4();
		bool L_3 = __this->get__hasAppleIdCredential_1();
		SerializationTools_FixSerializationForObject_TisAppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0_mE518B6B97BEE8670FFFAE3C4BA679D63F7AB29B0((AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0 **)L_2, L_3, /*hidden argument*/SerializationTools_FixSerializationForObject_TisAppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0_mE518B6B97BEE8670FFFAE3C4BA679D63F7AB29B0_RuntimeMethod_var);
		// SerializationTools.FixSerializationForObject(ref this._passwordCredential, this._hasPasswordCredential);
		PasswordCredential_t64C056A6AE5B8356D435709C776795D925E35C40 ** L_4 = __this->get_address_of__passwordCredential_5();
		bool L_5 = __this->get__hasPasswordCredential_2();
		SerializationTools_FixSerializationForObject_TisPasswordCredential_t64C056A6AE5B8356D435709C776795D925E35C40_m2F6D3902FF446FBBEBAC5665578E2EF37BD51601((PasswordCredential_t64C056A6AE5B8356D435709C776795D925E35C40 **)L_4, L_5, /*hidden argument*/SerializationTools_FixSerializationForObject_TisPasswordCredential_t64C056A6AE5B8356D435709C776795D925E35C40_m2F6D3902FF446FBBEBAC5665578E2EF37BD51601_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void AppleAuth.Native.LoginWithAppleIdResponse::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoginWithAppleIdResponse__ctor_mCA177F81AD1DC44AF186230CDC8BCDB1BD489540 (LoginWithAppleIdResponse_tEADF70491D90DDEE797098E7FFCEC61C231ACE4C * __this, const RuntimeMethod* method)
{
	{
		// public bool _success = false;
		__this->set__success_0((bool)0);
		// public bool _hasAppleIdCredential = false;
		__this->set__hasAppleIdCredential_1((bool)0);
		// public bool _hasPasswordCredential = false;
		__this->set__hasPasswordCredential_2((bool)0);
		// public bool _hasError = false;
		__this->set__hasError_3((bool)0);
		// public AppleIDCredential _appleIdCredential = null;
		__this->set__appleIdCredential_4((AppleIDCredential_t753CBB2C0B37F65E994D32EBB99848B2931D0EE0 *)NULL);
		// public PasswordCredential _passwordCredential = null;
		__this->set__passwordCredential_5((PasswordCredential_t64C056A6AE5B8356D435709C776795D925E35C40 *)NULL);
		// public AppleError _error = null;
		__this->set__error_6((AppleError_t157640890ACABCEB007334DB77D1279DCB27223E *)NULL);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String AppleAuth.Native.PasswordCredential::get_User()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PasswordCredential_get_User_m9053330B4BCC8C8806CDCDB0AD6697C3819C166F (PasswordCredential_t64C056A6AE5B8356D435709C776795D925E35C40 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// public string User { get { return this._user; } }
		String_t* L_0 = __this->get__user_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public string User { get { return this._user; } }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String AppleAuth.Native.PasswordCredential::get_Password()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PasswordCredential_get_Password_m34CB954A5DFC8322D383FF93585073CB6319342C (PasswordCredential_t64C056A6AE5B8356D435709C776795D925E35C40 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// public string Password { get { return this._password; } }
		String_t* L_0 = __this->get__password_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public string Password { get { return this._password; } }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void AppleAuth.Native.PasswordCredential::OnBeforeSerialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PasswordCredential_OnBeforeSerialize_m43919643ADFDED9EE5870C2F34D22D4962167EEA (PasswordCredential_t64C056A6AE5B8356D435709C776795D925E35C40 * __this, const RuntimeMethod* method)
{
	{
		// public void OnBeforeSerialize() { }
		return;
	}
}
// System.Void AppleAuth.Native.PasswordCredential::OnAfterDeserialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PasswordCredential_OnAfterDeserialize_mA3F5E0C7A4AC250BB17845D83100B9A5AC02E784 (PasswordCredential_t64C056A6AE5B8356D435709C776795D925E35C40 * __this, const RuntimeMethod* method)
{
	{
		// SerializationTools.FixSerializationForString(ref this._user);
		String_t** L_0 = __this->get_address_of__user_0();
		SerializationTools_FixSerializationForString_m8171A047B8FCDC62B846791D70C2DA21418D8650((String_t**)L_0, /*hidden argument*/NULL);
		// SerializationTools.FixSerializationForString(ref this._password);
		String_t** L_1 = __this->get_address_of__password_1();
		SerializationTools_FixSerializationForString_m8171A047B8FCDC62B846791D70C2DA21418D8650((String_t**)L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppleAuth.Native.PasswordCredential::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PasswordCredential__ctor_m4CEA3FF72C5D44D5ADA0F05C9822D1C164734C78 (PasswordCredential_t64C056A6AE5B8356D435709C776795D925E35C40 * __this, const RuntimeMethod* method)
{
	{
		// public string _user = null;
		__this->set__user_0((String_t*)NULL);
		// public string _password = null;
		__this->set__password_1((String_t*)NULL);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// AppleAuth.Interfaces.ICredentialStateResponse AppleAuth.Native.PayloadDeserializer::DeserializeCredentialStateResponse(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PayloadDeserializer_DeserializeCredentialStateResponse_mB1B47BE54866E19949520EE2A62A56A2E59AE563 (PayloadDeserializer_t74E3362B6A34F9E80031FCDC097BE517ABE0489E * __this, String_t* ___payload0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PayloadDeserializer_DeserializeCredentialStateResponse_mB1B47BE54866E19949520EE2A62A56A2E59AE563_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		// return JsonUtility.FromJson<CredentialStateResponse>(payload);
		String_t* L_0 = ___payload0;
		CredentialStateResponse_t293F3810FEEAB306E15098AB47F7FE21607854B7 * L_1 = JsonUtility_FromJson_TisCredentialStateResponse_t293F3810FEEAB306E15098AB47F7FE21607854B7_m52466496B9EC0733C29DD4EBD0E9A613CB7C0A68(L_0, /*hidden argument*/JsonUtility_FromJson_TisCredentialStateResponse_t293F3810FEEAB306E15098AB47F7FE21607854B7_m52466496B9EC0733C29DD4EBD0E9A613CB7C0A68_RuntimeMethod_var);
		V_0 = L_1;
		goto IL_000a;
	}

IL_000a:
	{
		// }
		RuntimeObject* L_2 = V_0;
		return L_2;
	}
}
// AppleAuth.Interfaces.ILoginWithAppleIdResponse AppleAuth.Native.PayloadDeserializer::DeserializeLoginWithAppleIdResponse(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PayloadDeserializer_DeserializeLoginWithAppleIdResponse_m7C6E5F892CD5579916521D37323B5E6943E5F9F0 (PayloadDeserializer_t74E3362B6A34F9E80031FCDC097BE517ABE0489E * __this, String_t* ___payload0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PayloadDeserializer_DeserializeLoginWithAppleIdResponse_m7C6E5F892CD5579916521D37323B5E6943E5F9F0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		// return JsonUtility.FromJson<LoginWithAppleIdResponse>(payload);
		String_t* L_0 = ___payload0;
		LoginWithAppleIdResponse_tEADF70491D90DDEE797098E7FFCEC61C231ACE4C * L_1 = JsonUtility_FromJson_TisLoginWithAppleIdResponse_tEADF70491D90DDEE797098E7FFCEC61C231ACE4C_m9AFB93CAD117C86828C186AC2CB41B60B8FE871F(L_0, /*hidden argument*/JsonUtility_FromJson_TisLoginWithAppleIdResponse_tEADF70491D90DDEE797098E7FFCEC61C231ACE4C_m9AFB93CAD117C86828C186AC2CB41B60B8FE871F_RuntimeMethod_var);
		V_0 = L_1;
		goto IL_000a;
	}

IL_000a:
	{
		// }
		RuntimeObject* L_2 = V_0;
		return L_2;
	}
}
// System.Void AppleAuth.Native.PayloadDeserializer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PayloadDeserializer__ctor_m3DD225AE33F56326A8DF099FAEDDD75F65469D31 (PayloadDeserializer_t74E3362B6A34F9E80031FCDC097BE517ABE0489E * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String AppleAuth.Native.PersonName::get_NamePrefix()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PersonName_get_NamePrefix_m53B3E50F11C940E5A3D239FC7AE7B3AFF9C7608C (PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// public string NamePrefix { get { return _namePrefix; } }
		String_t* L_0 = __this->get__namePrefix_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public string NamePrefix { get { return _namePrefix; } }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String AppleAuth.Native.PersonName::get_GivenName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PersonName_get_GivenName_mE6FBDCBA9F1E094F9AF211E494817201B4DF91B6 (PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// public string GivenName { get { return _givenName; } }
		String_t* L_0 = __this->get__givenName_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public string GivenName { get { return _givenName; } }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String AppleAuth.Native.PersonName::get_MiddleName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PersonName_get_MiddleName_mCA9994B19EF2B5794EED00F890EB8AFC973A74A1 (PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// public string MiddleName { get { return _middleName; } }
		String_t* L_0 = __this->get__middleName_2();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public string MiddleName { get { return _middleName; } }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String AppleAuth.Native.PersonName::get_FamilyName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PersonName_get_FamilyName_mDE8C71F0B60B6534105B8377857565A5ABD50B27 (PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// public string FamilyName { get { return _familyName; } }
		String_t* L_0 = __this->get__familyName_3();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public string FamilyName { get { return _familyName; } }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String AppleAuth.Native.PersonName::get_NameSuffix()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PersonName_get_NameSuffix_m01EAB5B6F3D4A3DE331CF2EDC543C1A796D5C0D7 (PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// public string NameSuffix { get { return _nameSuffix; } }
		String_t* L_0 = __this->get__nameSuffix_4();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public string NameSuffix { get { return _nameSuffix; } }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String AppleAuth.Native.PersonName::get_Nickname()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PersonName_get_Nickname_mC7A33C610502A12950954A11F59D54E41D48FA30 (PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		// public string Nickname { get { return _nickname; } }
		String_t* L_0 = __this->get__nickname_5();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// public string Nickname { get { return _nickname; } }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// AppleAuth.Interfaces.IPersonName AppleAuth.Native.PersonName::get_PhoneticRepresentation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PersonName_get_PhoneticRepresentation_m4E7221EC4D7934C7D4816159993943B47818AE6C (PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		// public IPersonName PhoneticRepresentation { get { return null; } }
		V_0 = (RuntimeObject*)NULL;
		goto IL_0005;
	}

IL_0005:
	{
		// public IPersonName PhoneticRepresentation { get { return null; } }
		RuntimeObject* L_0 = V_0;
		return L_0;
	}
}
// System.Void AppleAuth.Native.PersonName::OnBeforeSerialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersonName_OnBeforeSerialize_m841722B7A1DCA5782A61621F0A62B16668BB5A20 (PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0 * __this, const RuntimeMethod* method)
{
	{
		// public void OnBeforeSerialize() { }
		return;
	}
}
// System.Void AppleAuth.Native.PersonName::OnAfterDeserialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersonName_OnAfterDeserialize_m32D4307910B89D3E33FBBE420CBA93641F8F0ACD (PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0 * __this, const RuntimeMethod* method)
{
	{
		// SerializationTools.FixSerializationForString(ref this._namePrefix);
		String_t** L_0 = __this->get_address_of__namePrefix_0();
		SerializationTools_FixSerializationForString_m8171A047B8FCDC62B846791D70C2DA21418D8650((String_t**)L_0, /*hidden argument*/NULL);
		// SerializationTools.FixSerializationForString(ref this._givenName);
		String_t** L_1 = __this->get_address_of__givenName_1();
		SerializationTools_FixSerializationForString_m8171A047B8FCDC62B846791D70C2DA21418D8650((String_t**)L_1, /*hidden argument*/NULL);
		// SerializationTools.FixSerializationForString(ref this._middleName);
		String_t** L_2 = __this->get_address_of__middleName_2();
		SerializationTools_FixSerializationForString_m8171A047B8FCDC62B846791D70C2DA21418D8650((String_t**)L_2, /*hidden argument*/NULL);
		// SerializationTools.FixSerializationForString(ref this._familyName);
		String_t** L_3 = __this->get_address_of__familyName_3();
		SerializationTools_FixSerializationForString_m8171A047B8FCDC62B846791D70C2DA21418D8650((String_t**)L_3, /*hidden argument*/NULL);
		// SerializationTools.FixSerializationForString(ref this._nameSuffix);
		String_t** L_4 = __this->get_address_of__nameSuffix_4();
		SerializationTools_FixSerializationForString_m8171A047B8FCDC62B846791D70C2DA21418D8650((String_t**)L_4, /*hidden argument*/NULL);
		// SerializationTools.FixSerializationForString(ref this._nickname);
		String_t** L_5 = __this->get_address_of__nickname_5();
		SerializationTools_FixSerializationForString_m8171A047B8FCDC62B846791D70C2DA21418D8650((String_t**)L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppleAuth.Native.PersonName::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PersonName__ctor_m320D2DECA0176EE689309F89168BC54311BDD69C (PersonName_t967C04923E9C987CC0ABD0AAAA1E2EB5B96D47E0 * __this, const RuntimeMethod* method)
{
	{
		// public string _namePrefix = null;
		__this->set__namePrefix_0((String_t*)NULL);
		// public string _givenName = null;
		__this->set__givenName_1((String_t*)NULL);
		// public string _middleName = null;
		__this->set__middleName_2((String_t*)NULL);
		// public string _familyName = null;
		__this->set__familyName_3((String_t*)NULL);
		// public string _nameSuffix = null;
		__this->set__nameSuffix_4((String_t*)NULL);
		// public string _nickname = null;
		__this->set__nickname_5((String_t*)NULL);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AppleAuth.Native.SerializationTools::FixSerializationForString(System.String&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializationTools_FixSerializationForString_m8171A047B8FCDC62B846791D70C2DA21418D8650 (String_t** ___originalString0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// if (string.IsNullOrEmpty(originalString))
		String_t** L_0 = ___originalString0;
		String_t* L_1 = *((String_t**)L_0);
		bool L_2 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_000f;
		}
	}
	{
		// originalString = null;
		String_t** L_4 = ___originalString0;
		*((RuntimeObject **)L_4) = (RuntimeObject *)NULL;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)L_4, (void*)(RuntimeObject *)NULL);
	}

IL_000f:
	{
		// }
		return;
	}
}
// System.Byte[] AppleAuth.Native.SerializationTools::GetBytesFromBase64String(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* SerializationTools_GetBytesFromBase64String_m861B8FE0EC904AC192A9D2DD8D5CC6B51A7F2CAD (String_t* ___base64String0, String_t* ___fieldName1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SerializationTools_GetBytesFromBase64String_m861B8FE0EC904AC192A9D2DD8D5CC6B51A7F2CAD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_0 = NULL;
	bool V_1 = false;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_2 = NULL;
	Exception_t * V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// if (base64String == null)
		String_t* L_0 = ___base64String0;
		V_1 = (bool)((((RuntimeObject*)(String_t*)L_0) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_1 = V_1;
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		// return null;
		V_2 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)NULL;
		goto IL_0048;
	}

IL_000e:
	{
		// var returnedBytes = default(byte[]);
		V_0 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)NULL;
	}

IL_0010:
	try
	{ // begin try (depth: 1)
		// returnedBytes = Convert.FromBase64String(base64String);
		String_t* L_2 = ___base64String0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1C7A851BFB2F0782FD7F72F6AA1DCBB7B53A9C7E_il2cpp_TypeInfo_var);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = Convert_FromBase64String_m079F788D000703E8018DA39BE9C05F1CBF60B156(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0044;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_001b;
		throw e;
	}

CATCH_001b:
	{ // begin catch(System.Exception)
		// catch (Exception exception)
		V_3 = ((Exception_t *)__exception_local);
		// Console.WriteLine("Received exception while deserializing byte array for " + fieldName);
		String_t* L_4 = ___fieldName1;
		String_t* L_5 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(_stringLiteralAFEC25D3F526944824BEA6749A38AE4CFE673EE2, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t5C8E87BA271B0DECA837A3BF9093AC3560DB3D5D_il2cpp_TypeInfo_var);
		Console_WriteLine_mA5F7E391799514350980A0DE16983383542CA820(L_5, /*hidden argument*/NULL);
		// Console.WriteLine("Exception: " + exception);
		Exception_t * L_6 = V_3;
		String_t* L_7 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(_stringLiteral441DEA5D56A75C98B8BD51BA0E048ECE7B587ED5, L_6, /*hidden argument*/NULL);
		Console_WriteLine_mA5F7E391799514350980A0DE16983383542CA820(L_7, /*hidden argument*/NULL);
		// returnedBytes = null;
		V_0 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)NULL;
		goto IL_0044;
	} // end catch (depth: 1)

IL_0044:
	{
		// return returnedBytes;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_8 = V_0;
		V_2 = L_8;
		goto IL_0048;
	}

IL_0048:
	{
		// }
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_9 = V_2;
		return L_9;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)L_2, (int32_t)L_3);
		return (RuntimeObject *)L_4;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
